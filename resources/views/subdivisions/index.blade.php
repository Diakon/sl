@section('head')
@stop
@section('scripts')
@stop

@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">{{ $app_title = 'Подразделения' }}</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('subdivisions.create') !!}">Добавить новое подразделение</a>
        </div>

        <div class="row">
            @if($subdivisions->isEmpty())
                <div class="well text-center">Подразделения не найдены.</div>
            @else
                @include('subdivisions.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $subdivisions])


    </div>
@endsection