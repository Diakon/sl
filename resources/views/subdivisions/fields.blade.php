<!--- Object Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('object_id', 'Объект:') !!}
    {!! Form::select('object_id', $subdivision['objects_arr'], null, ['class' => 'form-control']) !!}
</div>
<div style="clear: both;"></div>
<!--- Name Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('name', 'Название:') !!}
	{!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div style="clear: both;"></div>
<!--- Type Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('type', 'Тип:') !!}
	{!! Form::text('type', null, ['class' => 'form-control']) !!}
</div>
<div style="clear: both;"></div>
<!--- Description Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('description', 'Описание:') !!}
	{!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>
<div style="clear: both;"></div>
<!--- Subdivision Comments Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('subdivision_comments', 'Статус:') !!}
	{!! Form::select('subdivision_comments', $subdivision['status'],
	    ((isset($subdivision['status_selected']))?$subdivision['status_selected']:null), ['class' => 'form-control']) !!}
</div>
<div style="clear: both;"></div>

<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="/subdivisions" class="btn btn-default" >Отмена</a>
</div>
