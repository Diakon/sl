@section('head')
@stop
@section('scripts')
<script src="{{ URL::asset('/js/support/script.js') }}"></script>
@stop

@extends('app')

@extends('nav')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($subdivision, ['route' => ['subdivisions.update', $subdivision->id], 'method' => 'patch']) !!}

        @include('subdivisions.fields')

    {!! Form::close() !!}
</div>
@endsection
