<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>№</th>
            <th>Название</th>
            <th>Объект</th>
            <th>Тип</th>
            <th>Описание</th>
            <th>Статус</th>
            <th width="50px">Действия</th>
        </tr>
        <tr>
            {!! Form::model($request, ['route' => 'subdivisions.index', 'class' => 'form-inline', 'method' => 'get']) !!}
            <td></td>
            <td>
                {!! Form::text('name', null , ['class' => 'form-control', 'placeholder'=>'Название']) !!}
            </td>
            <td>
                {!! Form::select('object', collect($objects)->reject(function($item){return strpos($item, 'не в работе');})->sort()->all() , null, ['class' => 'form-control', 'placeholder'=>'Объект', 'onchange'=>'this.form.submit();']) !!}
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            {!! Form::close() !!}
        </tr>
    </thead>
    <tbody>
    @foreach($subdivisions as $subdivision)
        <tr>
            <td>{!! $subdivision->id !!}</td>
			<td>{!! $subdivision->name !!}</td>
			<td>{!! $subdivision->object !!}</td>
			<td>{!! $subdivision->type !!}</td>
			<td>{!! $subdivision->description !!}</td>
			<td>{!! $subdivision->subdivision_comments !!}</td>
            <td>
                <a href="{!! route('subdivisions.edit', [$subdivision->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('subdivisions.on', [$subdivision->id]) !!}" onclick="return confirm('Вы уверены, что хотите пометить подразделение как `в работе`?')"><i class="glyphicon glyphicon-ok"></i></a>
                <a href="{!! route('subdivisions.delete', [$subdivision->id]) !!}" onclick="return confirm('Вы уверены, что хотите пометить подразделение как `не в работе`?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
