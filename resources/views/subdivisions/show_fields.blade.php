<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $subdivision->id !!}</p>
</div>

<!-- Object Id Field -->
<div class="form-group">
    {!! Form::label('object_id', 'Object Id:') !!}
    <p>{!! $subdivision->object_id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $subdivision->name !!}</p>
</div>

<!-- Object Field -->
<div class="form-group">
    {!! Form::label('object', 'Object:') !!}
    <p>{!! $subdivision->object !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $subdivision->type !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $subdivision->description !!}</p>
</div>

<!-- Subdivision Comments Field -->
<div class="form-group">
    {!! Form::label('subdivision_comments', 'Subdivision Comments:') !!}
    <p>{!! $subdivision->subdivision_comments !!}</p>
</div>

