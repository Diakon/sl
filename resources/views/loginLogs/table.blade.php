<table class="table">
    <thead>
    <th>Id</th>
			<th>Id User</th>
			<th>Ip User</th>
			<th>Username</th>
			<th>Data</th>
			<th>Created At</th>
			<th>Action</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($loginLogs as $loginLog)
        <tr>
            <td>{!! $loginLog->id !!}</td>
			<td>{!! $loginLog->id_user !!}</td>
			<td>{!! $loginLog->ip_user !!}</td>
			<td>{!! $loginLog->username !!}</td>
			<td>{!! $loginLog->data !!}</td>
			<td>{!! $loginLog->created_at !!}</td>
			<td>{!! $loginLog->action !!}</td>
            <td>
                <a href="{!! route('loginLogs.edit', [$loginLog->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('loginLogs.delete', [$loginLog->id]) !!}" onclick="return confirm('Are you sure wants to delete this LoginLog?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
