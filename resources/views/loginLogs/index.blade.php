@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">LoginLogs</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('loginLogs.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($loginLogs->isEmpty())
                <div class="well text-center">No LoginLogs found.</div>
            @else
                @include('loginLogs.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $loginLogs])


    </div>
@endsection