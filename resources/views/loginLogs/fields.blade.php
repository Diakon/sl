<!--- Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('id', 'Id:') !!}
	{!! Form::number('id', null, ['class' => 'form-control']) !!}
</div>

<!--- Id User Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('id_user', 'Id User:') !!}
	{!! Form::number('id_user', null, ['class' => 'form-control']) !!}
</div>

<!--- Ip User Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('ip_user', 'Ip User:') !!}
	{!! Form::text('ip_user', null, ['class' => 'form-control']) !!}
</div>

<!--- Username Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('username', 'Username:') !!}
	{!! Form::text('username', null, ['class' => 'form-control']) !!}
</div>

<!--- Data Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('data', 'Data:') !!}
	{!! Form::text('data', null, ['class' => 'form-control']) !!}
</div>

<!--- Created At Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('created_at', 'Created At:') !!}
	{!! Form::date('created_at', null, ['class' => 'form-control']) !!}
</div>

<!--- Action Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('action', 'Action:') !!}
	{!! Form::text('action', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
