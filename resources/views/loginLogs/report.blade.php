@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
@stop

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@stop

@extends('app')

@extends('nav')

@section('content')

<div class="container">

    <div class="row">
        <h1>{{ $app_title = 'История авторизаций пользователей' }}</h1>

        @include('loginLogs.filter')

        <?php if (count($loginLog)) { ?>
        <table class="table table-striped table-bordered">
            <thead>
                <th>Дата</th><th>Логин</th><th>Первый логин</th><th>Последний логин</th>
            </thead>
            <tbody>
                @foreach($loginLog as $value)
                    <tr>
                        <td>{{ $value->date }}</td>
                        <td>
                            <a href="{!! route('admin.users.edit', [$value->id_user]) !!}" target="_blank">{{ $value->username }}</a>
                        </td>
                        <td>{{ $value->date_first }}</td>
                        <td>{{ $value->date_end }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <?php } ?>
    </div>
</div>

@endsection