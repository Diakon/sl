<div id="filterBlock" style="background: #c0c0c0; border-radius: 5px; padding: 10px; margin-bottom: 10px;">

{!! Form::model($request,array('route' => 'admin.users.log', 'class'=>'form-inline')) !!}

{!! Form::input('text', 's_pr_from', null, ['class'=>'form-control datepicker', 'placeholder'=>'Вход с']) !!}
{!! Form::input('text', 's_pr_to', null, ['class'=>'form-control datepicker', 'placeholder'=>'по']) !!}

<button type="submit" class="btn btn-primary">Найти</button>

{!! Form::close() !!}

</div>
