@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'loginLogs.store']) !!}

        @include('loginLogs.fields')

    {!! Form::close() !!}
</div>
@endsection
