@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($loginLog, ['route' => ['loginLogs.update', $loginLog->id], 'method' => 'patch']) !!}

        @include('loginLogs.fields')

    {!! Form::close() !!}
</div>
@endsection
