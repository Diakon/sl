<table class="table table-striped table-bordered">
    <thead>
        <th>ID</th>
        <th>Сделка</th>
        <th>Ставка</th>
        <th>Подразделение</th>
        <th>С</th>
        <th>По</th>
        <th width="50px">Возможные действия</th>
    </thead>
    <tbody>
    @foreach($tuovDealRates as $tuovDealRate)
        <tr>
            <td>{{ $tuovDealRate->id }}</td>
            <td>{{ $tuovDealRate->deal->nameWithType }}</td>
            <td>
                @if (!Route::input('id'))
                    {{ $tuovDealRate->rate }}
                @else
                    {!! Form::input('text', 'rate',  $tuovDealRate->rate, ['placeholder'=>'Укажите ставку', 'class' => 'form-control rate_data']) !!}
                @endif
            </td>
            <td>
                <a href="{!! url('tuovDealRates/subdivision', [$tuovDealRate->subdivision_id]) !!}">{{ $tuovDealRate->subdivision->nameAndObject }}</a>
            </td>
            <td>
                @if (!Route::input('id'))
                    {{ $tuovDealRate->from }}
                @else
                    {!! Form::text('from', $tuovDealRate->from, ['class' => 'form-control  datepicker from_data']) !!}
                @endif
            </td>
            <td>
                @if (!Route::input('id'))
                    {{ $tuovDealRate->to }}
                @else
                    {!! Form::text('from', $tuovDealRate->to, ['class' => 'form-control  datepicker to_data']) !!}
                @endif
            </td>
            <td>
                @if (Route::input('id'))
                    <a href="#" data-id="<?=$tuovDealRate->id;?>" class="applyRow"><i class="glyphicon glyphicon-ok"></i></a>
                @endif

                <a href="{!! route('tuovDealRates.edit', $tuovDealRate->id) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('tuovDealRates.delete', $tuovDealRate->id) !!}"
                   onclick="return confirm('Are you sure wants to delete this TuovDealRates?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>