<!--- Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('id', 'Id:') !!}
    {!! Form::number('id', null, ['class' => 'form-control', 'readonly']) !!}
</div>

<!--- Deal Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('deal_id', 'Сделка:') !!}
    {!! Form::select('deal_id', $activeDeals, null, ['class' => 'form-control']) !!}
</div>


<!--- Subdivision Id Field --->
<?php
    $subdivision = ((isset($_GET['subdivision']))?$_GET['subdivision']:null);
?>
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('subdivision_id', 'Подразделение:') !!}
    {!! Form::select('subdivision_id', $activeSubdivisions, Input::get('subdivision', null), ['class' => 'form-control']) !!}
</div>

<!--- From Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('from', 'Действует с:') !!}
	{!! Form::text('from', null, ['class' => 'form-control']) !!}
</div>

<!--- To Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('to', 'Действует по:') !!}
	{!! Form::text('to', null, ['class' => 'form-control']) !!}
</div>


<!--- Rate Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('rate', 'Ставка:') !!}
    {!! Form::number('rate', null, ['class' => 'form-control', 'min'=>"0.01",  'step' => '0.01']) !!}
</div>
<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="#" onclick="history.back();" class="btn btn-default">Отмена</a>
</div>

