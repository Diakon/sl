@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/tuovDealRates/style.css') }}"/>
@stop

@extends('app')
@extends('nav')

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ URL::asset('/js/tuovDealRates/tuovDealRates.js') }}"></script>
@stop

@section('content')
    <div class="container">
        @include('flash::message')

        <h1 class="text-center">{{ $app_title = 'Ставки для сделок' }}</h1>
        <div class="row">
            <div class="form-group form-inline">

                <a class="btn btn-primary pull-right" style="margin-top: 25px"
                   href="{!! route('tuovDealRates.create', ['subdivision'=>Route::input('id',0)]) !!}">Добавить
                    новую</a>
                {{ Form::select('subd', collect($activeSubdivisions)->except((isset($my_collection_ids)?$my_collection_ids:null)), Route::input('id'), ['class' => 'form-control pull-right chooseSubdivision', 'style' => 'margin-top: 25px']) }}

                @if ( !empty($objects) )

                    <ul class="nav nav-pills pull-right"  style="margin-top: 25px">
                        @foreach ($objects as $object=>$subdivisions)
                            @if( count($subdivisions)>1)
                                <li role="presentation" class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" title="Объект">
                                        {{ $object }} <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        @foreach($subdivisions as $sub)
                                            <li @if(Route::input('id') == $sub->id) class="active" @endif>
                                                <a href="{{ action('TuovDealRatesController@subdivision',[$sub->id])}}" title="Подразделение">{{$sub->name}}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @else
                                <li role="presentation" @if(Route::input('id') == $subdivisions[0]->id) class="active" @endif>
                                    <a title="Подразделение (Объект)" href="{{ action('TuovDealRatesController@subdivision',[$subdivisions[0]->id])}}">{{$subdivisions[0]->name}} ({{ $object }})</a>
                                </li>
                            @endif
                        @endforeach
                    </ul>
                @endif



            </div>
        </div>

        <div class="row">
            @if($tuovDealRates->isEmpty())
                <div class="well text-center">Ставки не найдены.</div>
            @else
                @include('tuovDealRates.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $tuovDealRates])
    </div>
@endsection