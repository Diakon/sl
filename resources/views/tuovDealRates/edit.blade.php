@extends('app')
@extends('nav')
@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ URL::asset('/js/tuovDealRates/tuovDealRates.js') }}"></script>
@stop
@section('content')
<div class="container">

    @include('common.errors')
    <h1>{{ $app_title = sprintf('Редактировать ставку №%d',$tuovDealRates->id) }}</h1>

    {!! Form::model($tuovDealRates, ['route' => ['tuovDealRates.update', $tuovDealRates->id], 'method' => 'patch']) !!}

        @include('tuovDealRates.fields')

    {!! Form::close() !!}
</div>
@endsection
