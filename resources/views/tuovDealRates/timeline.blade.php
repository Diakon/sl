<BR>
<?php
    $blueBlock = \DB::table('tuov_deal_rates')->where('id', $rate_id)->first();
    $redBlock = \DB::table('tuov_deal_rates')->where('id', $collisionsDeal)->first();

    //Сравниваем даты для формирования смещения красного блока относительно синего
    $margin_red = 0;
    $width_red = 250;
    $margin_red_point = 245;
    if ( $blueBlock->from > $redBlock->from ){
        //синий больше красного - смещаем красный влево
        $margin_red = $margin_red-200;
    }
    else if ( $blueBlock->from < $redBlock->from ){
        //синий меньше красного - смещаем красный вправо
        $margin_red = $margin_red + 200;
    }

    if ( $blueBlock->to > $redBlock->to ){
        //синий больше красного - уменьшаем размер красного блока
        /*
        $width_red = $width_red- 50;
        $margin_red =  $margin_red - 50;
        */
        $width_red = $width_red - 100;
        $margin_red = $margin_red - 100;
        $margin_red_point = $margin_red_point - 100;
    }
    else if ( $blueBlock->to < $redBlock->to ){
        //синий меньше красного - увеличиваем размер красного блока
        $width_red = $width_red + 100;
        $margin_red = $margin_red + 100;
        $margin_red_point = $margin_red_point + 100;
    }



?>
<center>
<div class="arrow-block">


<div class="blue-block" data-blue="{{$rate_id}}" data-red="{{$collisionsDeal}}" data-select="blue" >
    <p>Ставка #{{$rate_id}}:<BR><?=$blueBlock->rate;?> р.</p>


    <div class="pixel-date-from-blue">
        <p style="min-width:80px; padding-top: 10px; font-size:15px;  position: relative; margin-top: -30px; margin-left: -40px;"><?=$blueBlock->from;?></p>
    </div>

    <div class="pixel-date-to-blue">
            <p style="min-width:80px; padding-top: 10px; font-size:15px;   position: relative; margin-top: -30px; margin-left:-40px;"><?=$blueBlock->to;?></p>
    </div>

</div>

<div class="red-block" data-blue="{{$rate_id}}" data-red="{{$collisionsDeal}}" data-select="red" style="margin-left:<?=$margin_red;?>px; width:<?=$width_red;?>px">
        <p>Ставка #{{$collisionsDeal}}:<BR><?=$redBlock->rate;?> р.</p>

        <div class="pixel-date-from-red">
            <p style="padding-top: 10px; margin-left: -45px; font-size:15px;  width: 100px;  "><?=$redBlock->from;?></p>
        </div>

        <div class="pixel-date-to-red" style="margin-left: <?=$margin_red_point;?>px;">
                <p style="padding-top: 10px; margin-left: -45px; font-size:15px; width: 100px;"><?=$redBlock->to;?></p>
        </div>

</div>







</div>
</center>
