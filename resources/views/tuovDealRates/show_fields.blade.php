<!-- Deal Id Field -->
<div class="form-group">
    {!! Form::label('deal_id', 'Deal Id:') !!}
    <p>{!! $tuovDealRates->deal_id !!}</p>
</div>

<!-- Rate Field -->
<div class="form-group">
    {!! Form::label('rate', 'Rate:') !!}
    <p>{!! $tuovDealRates->rate !!}</p>
</div>

<!-- Subdivision Id Field -->
<div class="form-group">
    {!! Form::label('subdivision_id', 'Subdivision Id:') !!}
    <p>{!! $tuovDealRates->subdivision_id !!}</p>
</div>

<!-- From Field -->
<div class="form-group">
    {!! Form::label('from', 'From:') !!}
    <p>{!! $tuovDealRates->from !!}</p>
</div>

<!-- To Field -->
<div class="form-group">
    {!! Form::label('to', 'To:') !!}
    <p>{!! $tuovDealRates->to !!}</p>
</div>

