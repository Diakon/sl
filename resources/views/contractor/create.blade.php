@section('head')
@stop

@section('scripts')
@stop

@extends('app')

@extends('nav')

@section('content')
<div class="container">
    <div class="row">
        <h1 class="text-center">{{ $app_title = 'Добавить контрагента' }}</h1>
        @include('common.errors')

        {!! Form::open(['route' => 'contractor.store', 'class' => 'form-horizontal']) !!}

            @include('contractor.fields')

        {!! Form::close() !!}
    </div>
</div>
@endsection