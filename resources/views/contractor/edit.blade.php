@section('head')
@stop
@section('scripts')
@stop

@extends('app')

@extends('nav')

@section('content')
    <div class="container">
        <div class="row">
            <h1 class="text-center">{{ $app_title = sprintf('Контрагент #%s', $contractor->id)}}</h1>
            @include('common.errors')

            {!! Form::model($contractor, ['route' => ['contractor.update', $contractor->id], 'class' => 'form-inline', 'method' => 'patch', 'class' => 'form-horizontal']) !!}

            @include('contractor.fields')

            {!! Form::close() !!}

        </div>
    </div>
@endsection