@section('head')
@stop

@section('scripts')
@stop

@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">{{ $app_title="Список контрагентов" }}</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('contractor.create') !!}">Создать новую запись</a>
        </div>

        <div class="row">
            @include('contractor.table')
        </div>

        @include('common.paginate', ['records' => $contractors])


    </div>
@endsection