<?php
$contract = (isset($contractor)?$contractor->contract:null);
?>

<style>
    .block{
        margin-top:20px;
        /*display: none;*/
        border-radius: 5px;
        border: 1px solid black;
        padding-top:20px;
        background: #e7e7e7 none repeat scroll 0 0;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
    }
    .show_more {
        text-align: left;
        width: 100%;
        height:40px;
        margin-top:20px;
        cursor: default;
    }
    .show_more p {
        font-size: 21px;
    }
    .line  {
        border-top: 1px dashed #000;
        height: 18px;
    }
    #addNewRow {
        margin-left:10px;
    }
    .parent_copy
    {
        -ms-transform: scale(3);
        -moz-transform: scale(3);
        -webkit-transform: scale(3);
        -o-transform: scale(3);
    }

</style>

<script>
    /*
    $(document).on('click', '.show_more', function(){
        $(this).next().slideToggle();
    });
    */

    $(document).on('keyup','#ceo, #legal_address_index, #legal_address_city, #legal_address, #actual_address_index, #actual_address_city, #actual_address', function () {
        var className = '.parent_'+$(this).attr('id');
        var parent_row = $(this).parent().parent().next();
        var flag = parent_row.find('.parent_copy').data('flag');
        if (flag == 1){
            parent_row.find(className).val($(this).val());
        }
    });


    function cloneData(id_array, flag){
        var re = /\s*=\s*/;
        $.each(id_array, function( index, value ) {
            var tagList = value.split(re);
            if (flag == 1){
                $(tagList[1]).val($(tagList[0]).val());
                $(tagList[1]).prop( "disabled", true );
            } else {
                $(tagList[1]).prop( "disabled", false );
            }
        });
    }
</script>


<div class="col-sm-offset-1 col-sm-10 col-lg-offset-2 col-lg-8">

<div class="show_more btn btn-default">
    <p>
        <img src="/images/card.png" alt="Контрагент" style="vertical-align: middle; margin-top:-5px; margin-left:5px;">
        Контрагент
    </p>
</div>

<div class="block">
    <!--- Id Anketa Field --->
    <div class="form-group">
        {!! Form::label('type', 'Тип:', ['class' => 'col-sm-3 control-label']) !!}
        <div class="col-sm-7">
            {!! Form::select('type', DB::table('dir_contractor_types')->lists('name','id'), null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('short_name', 'Краткое наименование:', ['class' => 'col-sm-4 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('short_name', null, ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('full_name', 'Полное наименование:', ['class' => 'col-sm-4 control-label']) !!}
        <div class="col-sm-6">
            {!! Form::text('full_name', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>



<div class="show_more btn btn-default">
    <p>
        <img src="/images/dog.png" alt="Для договора" style="vertical-align: middle; margin-top:-5px; margin-left:5px;">
        Договор
    </p>
</div>
<div class="block">
<fieldset>
    <div class="form-group">
        {!! Form::label('ceo', 'Генеральный директор:', ['class' => 'col-sm-5 control-label']) !!}
        <div class="col-sm-5">
            {!! Form::text('ceo', (isset($contract)?$contract->ceo:null), ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('chief_accountant', 'Главный бухгалтер:', ['class' => 'col-sm-5 control-label']) !!}
        <div class="col-xs-10 col-sm-5">
            {!! Form::text('chief_accountant', (isset($contract)?$contract->chief_accountant:null), ['class' => 'form-control parent_ceo']) !!}
        </div>
        <div class="col-xs-2 col-sm-1">
            {!! Form::checkbox('ok', 1, null, ['style'=>'margin-top:11px;', 'class'=>'parent_copy', 'onclick'=>"var flag = 1; if($(this).data('flag') == 1){flag = 0; $(this).css('background','#fff');} else {\$(this).css('background','#e7e7e7');} cloneData(['#ceo=#chief_accountant'], flag); $(this).data('flag',flag);", 'data-flag'=>'0']) !!}
        </div>
    </div>




    <div class="form-group">
        {!! Form::label('action_on_basis', 'Действует на основании:', ['class' => 'col-sm-5 control-label']) !!}
        <div class="col-sm-5">
            {!! Form::text('action_on_basis', (isset($contract)?$contract->action_on_basis:'Устава'), ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="line"></div>
    <div class="form-group">
        {!! Form::label('legal_address_index', 'Юр. адрес:', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-2">
            {!! Form::text('legal_address_index', (isset($contract)?$contract->legal_address_index:null), ['class' => 'form-control', 'placeholder' => 'Индекс']) !!}
        </div>
        <div class="col-sm-2">
            {!! Form::text('legal_address_city', (isset($contract)?$contract->legal_address_city:null), ['id'=>'legal_address_city', 'class' => 'form-control', 'placeholder' => 'Город']) !!}
        </div>
        <div class="col-sm-4">
            {!! Form::text('legal_address', (isset($contract)?$contract->legal_address:null), ['id'=>'legal_address', 'class' => 'form-control', 'placeholder' => 'Адрес']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('actual_address_index', 'Факт. адрес:', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-2">
            {!! Form::text('actual_address_index', (isset($contract)?$contract->actual_address_index:null), ['class' => 'form-control parent_legal_address_index', 'placeholder' => 'Индекс']) !!}
        </div>
        <div class="col-sm-2">
            {!! Form::text('actual_address_city', (isset($contract)?$contract->actual_address_city:null), ['id'=>'actual_address_city',  'class' => 'form-control parent_legal_address_city', 'placeholder' => 'Город']) !!}
        </div>
        <div class="col-xs-10 col-sm-4">
            {!! Form::text('actual_address', (isset($contract)?$contract->actual_address:null), ['id'=>'actual_address',  'class' => 'form-control parent_legal_address', 'placeholder' => 'Адрес']) !!}
        </div>
        <div class="col-xs-2 col-sm-1">
            {!! Form::checkbox('ok', 1, null, ['style'=>'margin-top:11px;', 'class'=>'parent_copy', 'onclick'=>"var flag = 1; if($(this).data('flag') == 1){flag = 0; $(this).css('background','#fff');} else {\$(this).css('background','#e7e7e7');}
            cloneData(['#legal_address_index=#actual_address_index', '#legal_address_city=#actual_address_city', '#legal_address=#actual_address'], flag); $(this).data('flag',flag);",
            'data-flag'=>'0']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('mail_address_index', 'Почт. адрес:', ['class' => 'col-sm-2 control-label']) !!}
        <div class="col-sm-2">
            {!! Form::text('mail_address_index', (isset($contract)?$contract->mail_address_index:null), ['class' => 'form-control parent_actual_address_index', 'placeholder' => 'Индекс']) !!}
        </div>
        <div class="col-sm-2">
            {!! Form::text('mail_address_city', (isset($contract)?$contract->mail_address_city:null), ['id'=>'mail_address_city', 'class' => 'form-control parent_actual_address_city', 'placeholder' => 'Город']) !!}
        </div>
        <div class="col-xs-10 col-sm-4">
            {!! Form::text('mail_address', (isset($contract)?$contract->mail_address:null), ['id'=>'mail_address', 'class' => 'form-control parent_actual_address', 'placeholder' => 'Адрес']) !!}
        </div>
        <div class="col-xs-2 col-sm-1">
            {!! Form::checkbox('ok', 1, null, ['style'=>'margin-top:11px;', 'class'=>'parent_copy', 'onclick'=>"var flag = 1; if($(this).data('flag') == 1){flag = 0; $(this).css('background','#fff');} else {\$(this).css('background','#e7e7e7');}
            cloneData(['#actual_address_index=#mail_address_index', '#actual_address_city=#mail_address_city', '#actual_address=#mail_address'], flag); $(this).data('flag',flag);",
            'data-flag'=>'0']) !!}
        </div>
    </div>



    <div class="line"></div>
    <div class="form-group">
        {!! Form::label('property_form', 'Форма собственности:', ['class' => 'col-sm-5 control-label']) !!}
        <div class="col-sm-3">
            {!! Form::select('property_form', DB::table('dir_property_forms')->lists('name','id'), (isset($contractor)?$contractor->contract->property_form:0), ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="form-group">
        {!! Form::label('inn', 'ИНН:', ['class' => 'col-sm-5 control-label']) !!}
        <div class="col-sm-3">
            {!! Form::number('inn', (isset($contractor)?$contractor->contract->inn:null), ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('kpp', 'КПП:', ['class' => 'col-sm-5 control-label']) !!}
        <div class="col-sm-3">
            {!! Form::number('kpp', (isset($contractor)?$contractor->contract->kpp:null), ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('ogrn', 'ОГРН:', ['class' => 'col-sm-5 control-label']) !!}
        <div class="col-sm-3">
            {!! Form::number('ogrn', (isset($contractor)?$contractor->contract->ogrn:null), ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="form-group">
        {!! Form::label('taxation_system', 'Система налогообложения:', ['class' => 'col-sm-5 control-label']) !!}
        <div class="col-sm-3">
            {!! Form::select('taxation_system', DB::table('dir_taxation_systems')->lists('name','id'), (isset($contractor)?$contractor->contract->taxation_system:0), ['class' => 'form-control']) !!}
        </div>
    </div>
</fieldset>
</div>


<div class="show_more btn btn-default">
    <p>
        <img src="/images/bank.png" alt="Банк" style="vertical-align: middle; margin-top:-5px; margin-left:5px;">
        Банк
    </p>
</div>
<div class="block">
<fieldset>

    <div class="table-responsive">
        <table class="table-banks table table-striped table-bordered" style="min-width:600px;">
            <thead>
                <th>Расчетный счет</th>
                <th>Банк</th>
                <th title="Активен?" class="text-center">А</th>
                <th title="Основной?" class="text-center">О</th>
                <th width="25px"></th>
            </thead>
            @if ( isset($contractor) )
                <tbody>
                @foreach ($contractor->banks as $key=>$bank)
                    <tr>
                        <td>
                            {!! Form::hidden('banks[id][' . $key . ']', $bank->id) !!}
                            {!! Form::number('banks[score][' . $key . ']', $bank->score, ['class' => 'form-control']) !!}
                        </td>
                        <td>
                            {!! Form::select('banks[bank_id][' . $key . ']', $dir_banks, $bank->bank_id, ['class' => 'form-control select2']) !!}
                        </td>
                        <td class="text-center">
                            <div class="checkbox">
                                <label>
                                    {!! Form::hidden('banks[is_active][' . $key . ']', 0) !!}
                                    {!! Form::checkbox('banks[is_active][' . $key . ']', 1, $bank->is_active) !!}
                                </label>
                            </div>
                        </td>
                        <td class="text-center">
                            <div class="checkbox">
                                <label>
                                    {!! Form::hidden('banks[is_main][' . $key . ']', 0) !!}
                                    {!! Form::checkbox('banks[is_main][' . $key . ']', 1, $bank->is_main,
                                        ['class' => 'checkbox_banks_is_main', 'onclick'=>"$('.' + this.className).not(this).prop('checked', false);"]) !!}
                                </label>
                            </div>
                        </td>
                        <td>
                            <span class="glyphicon glyphicon-remove" aria-hidden="true" tabindex="-1" onclick="$(this).closest('tr').remove();"></span>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            @endif
        </table>
    </div>

    <div class="row">
        <button id="addNewRow" class="btn btn-link">
            <span  class="glyphicon glyphicon-plus" aria-hidden="true"></span>
            Добавить банк
        </button>
    </div>

</fieldset>
</div>

<!--- Submit Field --->
<div class="form-group" style="margin-top:20px; margin-left:1px;">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    {!! Html::linkRoute('contractor.index', 'Отмена', [], ['class' => 'btn btn-default', 'tabindex' => '-1']) !!}
</div>


    <script>
        $(function () {
            $(document).on('click','#addNewRow',function(){
                var tr = $('#addNewRowTable tr').clone();
                var table = $('.table-banks');
                console.log(table);
                var last_key = table.find('tr').size();
                tr.find('input, select').each(function (){
                    $(this).attr('disabled', false);
                    $(this).attr('name', $(this).attr('name').replace('%key',last_key));
                });
                table.append(tr);
                tr.find('select').select2({'language':'ru'});
                return false;
            });
        });
    </script>

    <table class="hidden" id="addNewRowTable">
        <tr>
            <td>
                {!! Form::number('banks[score][%key]', null, ['class' => 'form-control', 'disabled'=>true]) !!}
            </td>
            <td>
                {!! Form::select('banks[bank_id][%key]', $dir_banks, null, ['class' => 'form-control', 'disabled'=>true]) !!}
            </td>
            <td class="text-center">
                <div class="checkbox">
                    <label>
                        {!! Form::hidden('banks[is_active][%key]', 0, ['disabled'=>true]) !!}
                        {!! Form::checkbox('banks[is_active][%key]', 1, null, ['disabled'=>true]) !!}
                    </label>
                </div>
            </td>
            <td class="text-center">
                <div class="checkbox">
                    <label>
                        {!! Form::hidden('banks[is_main][%key]', 0, ['disabled'=>true]) !!}
                        {!! Form::checkbox('banks[is_main][%key]', 1, null,
                            ['class' => 'checkbox_banks_is_main', 'onclick'=>"$('.' + this.className).not(this).prop('checked', false);", 'disabled'=>true]) !!}
                    </label>
                </div>
            </td>
            <td>
                <span class="glyphicon glyphicon-remove" aria-hidden="true" tabindex="-1" onclick="$(this).closest('tr').remove();"></span>
            </td>
        </tr>
    </table>



</div>