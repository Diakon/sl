<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>№</th>
			<th>Тип</th>
			<th>Короткое название</th>
            <th>Полное название</th>
			<th></th>
        </tr>
        {!! Form::model($request, array('route' => 'contractor.index', 'class' => 'form-inline')) !!}
        {!! Form::submit('',['class'=>'hidden']) !!}
        <tr>
            <td></td>
            <td>
                {!! Form::select('type', DB::table('dir_contractor_types')->lists('name','id'), null , ['class' => 'form-control', 'placeholder'=>'Тип', 'onchange' => 'this.form.submit();']) !!}
            </td>
            <td>
                {!! Form::text('short_name', null , ['class' => 'form-control', 'placeholder'=>'Короткое название']) !!}
            </td>
            <td>
                {!! Form::text('full_name', null , ['class' => 'form-control', 'placeholder'=>'Полное название']) !!}
            </td>
            <td></td>
        </tr>
        {!! Form::close() !!}
    </thead>
    <tbody>
    @if($contractors->isEmpty())
        <tr class="info">
            <td colspan="5" class="text-center">Записей не обнаружено.</td>
        </tr>
    @else
        @foreach($contractors as $contractor)
            <tr>
                <td>{!! $contractor->id !!}</td>
                <td>{{ $contractor->types->name }}</td>
                <td>{{ $contractor->short_name }}</td>
                <td>{{ $contractor->full_name }}</td>
                <td>
                    <a href="{!! route('contractor.edit', [$contractor->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="{!! route('contractor.delete', [$contractor->id]) !!}" onclick="return confirm('Вы уверены, что хотите удалить?')"><i class="glyphicon glyphicon-remove"></i></a>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>