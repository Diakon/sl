@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'apis.store']) !!}

        @include('apis.fields')

    {!! Form::close() !!}
</div>
@endsection
