@section('head')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/js/support/select2/select2.css') }}" />
    <style>
        .select2-choice{
            min-width: 300px;
        }
    </style>
@stop
@section('scripts')
    <script src="{{ URL::asset('/js/support/select2/select2.min.js') }}"></script>
    <script>
        var row_id = 0;
        var template = '' +
                '<tr id = "row_%row_id%">'+
                '<td>{!! Form::input('text', 'DopBank[cor_score][%row_id%]', null, ['placeholder'=>'Расчетный счет']) !!}</td>'+
                '<td>{!! Form::select('DopBank[bank][%row_id%]', $banks, null, array('class'=>'bank_select select2input')) !!}</td>'+
                '<td>{!! Form::select('DopBank[inn][%row_id%]', $inns, null, array('class'=>'inn_select')) !!}</td>'+
                '<td align="center">{!! Form::checkbox('DopBank[main][%row_id%]', 1, false ) !!}</td>'+
                '<td align="center"><a href="#" onclick="$(this).parent().parent().remove(); return false;" ><i class="glyphicon glyphicon-remove"></i></a></td>'+
        '</tr>';

        function createTemplate(){
            ++row_id;
            tmp_template = template.replace(/%row_id%/g, row_id);
            $('tbody').append(tmp_template);
            $('select').select2();
        }

        createTemplate();

        $(document).on('click','#addNewRow',function(){
            createTemplate();
            $('select').select2();
            return false;
        });

    </script>
@stop
@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left"><?=$title;?></h1>
        </div>

        {!! Form::open(array('url' => '#')) !!}

        <div class="row">

            <table class="table table-striped table-bordered">
                <thead>
                    <th>Расчетный счет</th>
                    <th>Банк</th>
                    <th>ИНН</th>
                    <th>Основной</th>
                    <th width="50px">Действия</th>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

        <div class="row">
            <a href="#" id="addNewRow"><i class="glyphicon glyphicon-plus"><span style="position:absolute; margin-left:5px; margin-top:-2px; width:120px;">Добавить строку</span></i></a>
        </div>
        <HR>
        <div class="row">
            {!! Form::submit('Сохранить', array('class'=>'maintable', 'disabled')  ) !!}
        </div>
        {!! Form::close() !!}
    </div>
@endsection