<?php /* ?>
<!--- Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('id', 'Id:') !!}
	{!! Form::number('id', null, ['class' => 'form-control']) !!}
</div>
<?php */ ?>

<!--- Token Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('token', 'Токен:') !!}
	{!! Form::text('token', null, ['class' => 'form-control']) !!}
</div>

<div style="clear: both;"></div>

<!--- Status Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('status_api', 'Статус:') !!}
    {!! Form::select('status_api', array('1' => 'Активен', '2' => 'Не активен'), (!empty($api->status_api)?$api->status_api:null), ['class' => 'form-control']) !!}
</div>

<div style="clear: both;"></div>

<!--- Access Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('access', 'Наименование API:') !!}
	{!! Form::textarea('access', null, ['class' => 'form-control']) !!}
</div>

<div style="clear: both;"></div>

<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
</div>
