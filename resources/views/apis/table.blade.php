<table class="table">
    <thead>
            <th>#</th>
			<th>Токен</th>
			<th>Наименование API</th>
			<th>Дата создания</th>
            <th width="50px">Действия</th>
    </thead>
    <tbody>
    @foreach($apis as $api)
        <tr>
            <td>{!! $api->id !!}</td>
			<td>{!! $api->token !!}</td>
			<td>{!! $api->access !!}</td>
			<td>{!! date('d-m-Y H:is', strtotime($api->created_at)) !!}</td>
            <td>
                <a href="{!! route('apis.edit', [$api->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('apis.delete', [$api->id]) !!}" onclick="return confirm('Are you sure wants to delete this Api?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
