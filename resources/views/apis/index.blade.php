@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">API</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('apis.create') !!}">Добавить новый</a>
        </div>

        <div class="row">
            @if($apis->isEmpty())
                <div class="well text-center">Записей не найдено.</div>
            @else
                @include('apis.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $apis])


    </div>
@endsection