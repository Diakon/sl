@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($api, ['route' => ['apis.update', $api->id], 'method' => 'patch']) !!}

        @include('apis.fields')

    {!! Form::close() !!}
</div>
@endsection
