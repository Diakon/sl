@extends('app')

@section('content')
<style>
#users_table  {
margin-top:10px;
}

#users_table td,th{
padding:7px;
}
#users_table th {
text-align:center;
}
</style>
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Пользователи</div>
				
				<div class="panel-body">
				<a href="/users/add-form">Добавить пользователя</a>
					<table border="1" id="users_table">
						<tr>
							<th>ID</th>
							<th>Фио</th>
							<th>Логин</th>
							<th>Статус</th>
							<th>Активность</th>
							<th>Авторизация</th>
							<th>Последнее действие</th>
							<th colspan="2">Действия</th>
						
						</tr>
						@foreach ($users as $user)
						<tr>
							<td>{{$user->userid}}</td>
							<td>{{$user->fio}}</td>
							<td>{{$user->username}}</td>
							<td align="center">{{$lists['users_status'][$user->status]}}</td>
							<td align="center">{{$lists['enable'][$user->enable]}}</td>
							<td>{{$user->login_date}}</td>
							<td>{{$user->last_date}}</td>
							<td><a href="/users/update-form/{{$user->userid}}">Изменить</a></td>
							<td><a href="/users/delete/{{$user->userid}}" onclick="return confirm('Вы уверены?');">Удалить</a></td>
						</tr>
						@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
