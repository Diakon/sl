<table class="table">
    <thead>
			<th>Пользователь</th>
			<th>Цифровой код</th>
			<th>Дата создания</th>
            <?php $uid = \Auth::id(); if ( \DB::table('users')->where('userid', $uid)->first()->status == 15 ){ ?><th width="50px">Действия</th><? } ?>
    </thead>
    <tbody>
    @foreach($telegrams as $telegram)
        <?php
            //Если пользователь не админ - выводим только его цифровой код
            if ( $status != 15 && $telegram->user_id != $uid ){ continue; }
        ?>
        <tr>
			<td><?=\DB::table('users')->where('userid', $telegram->user_id)->pluck('fio');?></td>
			<td>{!! $telegram->digital_code !!}</td>
			<td>{!! date('d-m-Y H:i:s', strtotime($telegram->created_at)) !!}</td>

			<?php if ( $status == 15 ){ ?>
            <td>
                <a href="{!! route('telegrams.edit', [$telegram->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('telegrams.delete', [$telegram->id]) !!}" onclick="return confirm('Are you sure wants to delete this Telegram?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
            <? } ?>

        </tr>
    @endforeach
    </tbody>
</table>
