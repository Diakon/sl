@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($telegram, ['route' => ['telegrams.update', $telegram->id], 'method' => 'patch']) !!}

        @include('telegrams.fields')

    {!! Form::close() !!}
</div>
@endsection
