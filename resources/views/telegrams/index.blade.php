@section('head')
@stop

@section('scripts')
    <script src="{{ URL::asset('/js/support/script.js') }}"></script>
    <script src="{{ URL::asset('/js/telegrams/script.js') }}"></script>
@stop

@extends('app')

@extends('nav')

@section('content')

    <div class="container" id="table_telegrem_list">

            <h1>Регистрация в Telegram</h1>
            <p style="font-size: 20px; margin-top: 10px;">нажмите, чтобы получить код</p>

            <a href="#" class="btn btn-primary addNewCode" style="margin-top: 20px;">Получить</a>

            <div id="resultBlock" style="margin-top: 20px; display: none;">

                <p style="font-size: 20px;">Ваш код:</p><div id="dataResultBlock" style="margin-top:-28px; margin-left:90px;"><img src="/images/ajaxloader.gif" width="20px"></div>
                <p style="margin-top: 20px;" id="sendVilisMsg"></p>

            </div>
    </div>
@endsection