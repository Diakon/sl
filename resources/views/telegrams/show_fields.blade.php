<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $telegram->id !!}</p>
</div>

<!-- User Id Field -->
<div class="form-group">
    {!! Form::label('user_id', 'User Id:') !!}
    <p>{!! $telegram->user_id !!}</p>
</div>

<!-- Digital Code Field -->
<div class="form-group">
    {!! Form::label('digital_code', 'Digital Code:') !!}
    <p>{!! $telegram->digital_code !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $telegram->created_at !!}</p>
</div>

