<!--- Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    <?php /*
    {!! Form::label('id', 'Id:') !!}
	{!! Form::number('id', null, ['class' => 'form-control']) !!}
    */ ?>
</div>

<!--- User Id Field --->
<div class="form-group col-sm-6 col-lg-4" style="clear: both;">
    {!! Form::label('user_id', 'Пользователь:') !!}
    {!! Form::select('user_id', $telegram['users']) !!}
</div>




<!--- Digital Code Field --->
<div class="form-group col-sm-6 col-lg-4" style="clear: both;">
    {!! Form::label('digital_code', 'Цифровой код:') !!}
	{!! Form::text('digital_code', null, ['class' => 'form-control']) !!}
</div>

<?php /*
<!--- Created At Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('created_at', 'Created At:') !!}
	{!! Form::date('created_at', null, ['class' => 'form-control']) !!}
</div>
 */ ?>


<!--- Submit Field --->
<div class="form-group col-sm-12" style="clear: both;">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
</div>
