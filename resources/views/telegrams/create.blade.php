@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'telegrams.store']) !!}

        @include('telegrams.fields')

    {!! Form::close() !!}
</div>
@endsection
