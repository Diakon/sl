<?php if (isset($history)): ?>
    <h3 class="text-center">История изменений заявки.</h3>
    <table class="table table-striped table-bordered">
        <thead>
            <th>Дата</th>
            <th>Сообщения</th>
        </thead>
        <tbody>
        <?php foreach ($history as $data):  ?>
            <tr>
                <td><?=date('d-m-Y H:i:s',strtotime($data->when)) ?></td>
                <td><?=$data->message ?></td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>
<?php endif ?>