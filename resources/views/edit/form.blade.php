<style>
    .maintable input {
       height:25px;
       margin-left:5px;
       margin-top:8px;
    }
    .maintable thead tr th{
        padding: 10px;
        background-color: #d3d3d3;
    }
    .maintable tbody tr td{
        padding: 10px;
        vertical-align:top;
    }
    .font_serch {
        margin-top:30px;
        font-size:12px;
        line-height:5px;
    }
    .main_user_data, .user_country, .maintable{
        font-size: 12px;;
    }
    .inner_table {
        width: 100%;
        line-height:2px;
    }
    .inner_table thead tr th {
        font-size: 12px;
        background: #fff;
    }
    .inner_table tbody tr td {
        font-size: 12px;
    }
    .inner_table tbody tr td p, .user_country p {
        margin-top:-15px;
        margin-left:20px;
    }
    input.maintable {
        height: 125px;
        position: relative;
        top: -10px;
        width: 55px;
    }
    #hint_block {
        background: white none repeat scroll 0 0;
        border: 1px solid silver;
        box-shadow: 0 10px 6px -6px #777;
        cursor: pointer;
        padding: 2px;
        position: absolute;
        top: 23px;
        width: 200px;
        z-index: 10;
        margin-top:535px;
        margin-left: 5px;
    }
    #hint_lastnameField, #hint_nameField,  #hint_otchField {
        background: white none repeat scroll 0 0;
        border: 1px solid silver;
        box-shadow: 0 10px 6px -6px #777;
        cursor: pointer;
        padding: 2px;
        position: absolute;
        top: 23px;
        width: 200px;
        z-index: 10;
        margin-top:317px;
        margin-left: 110px;
    }
    #hint_nameField {
        margin-top: 360px;
        margin-left: 110px;
    }
    #hint_otchField {
        margin-top:403px;
        margin-left: 110px;
    }
</style>

{!! Form::hidden('pid', ((isset($soiskatel->id))?$soiskatel->id:0)) !!}
{!! Form::hidden('clickcount', 0, array('class'=>'clickcount')) !!}
{!! Form::hidden('keypresscount', 0, array('class'=>'keypresscount')) !!}

<table class="maintable" width="850px" cellspacing="0" cellpadding="0" border="1" align="center">

    <thead>
        <tr>
            <th><b>Данные заявки</b></th>
            <th><b>Данные оператора</b></th>
        </tr>
    </thead>

    <tbody>
        <tr>
            <td>
                <b>Номер клиента: <?=$soiskatel->id;?></b><BR>
                <b><font color="#339900"><?php $source_name_tmp = \DB::table('source_table')->where('source_id','=',(int)$soiskatel->searchsource)->first(); ?><?=((!empty($source_name_tmp))?($source_name_tmp->source_name):(''));?></font></b><br>
                <div class="font_serch">
                    <p><b>Искал: </b><?=$soiskatel->key;?></p>
                    <p><b>Где увидел: </b><?=$soiskatel->source;?></p>
                    <p><b>Заявка создана: </b><?=date('d-m-Y H:i:s', strtotime($soiskatel->created_when));?></p>
                    <p><b>Последнее изменение: </b><?=date('d-m-Y H:i:s', strtotime($soiskatel->modified_when));?></p>
                </div>
            </td>

            <td>
                <?php if (!empty($dublicate) && count($dublicate)>1){ ?>
                    <center><font color=red>Эта запись имеет дубликаты.<br>Номера дублированных заявок:<br>
                    <?php foreach($dublicate as $data){ ?>
                        <table border=1 width="340px" class="duplicatetable">
                            <tr>
                                <td class="duplicatetable">
                                    <a href="/edit/update/<?=$data->id;?>" target="_blank"><?=$data->id;?></a>
                                </td>
                                <td class="duplicatetable"><?=$data->whendate;?></td>
                                <td class="duplicatetable"><?php $statustable_tmp = \DB::table('statustable')->where('status_id','=',(int)$data->customer_status)->first(); ?><?=((!empty($statustable_tmp))?($statustable_tmp->status_name):(''));?></td>
                                <td class="duplicatetable"><?=(mb_substr($data->comments,0,200));?></td>
                            </tr>
                        </table><br>
                    <?php } ?>
                    </font></center>
                <?php } ?>
            </td>
        </tr>


        <tr>
            <td>
                <div class="main_user_data">
                    <span style="padding-right: 30px;"><b>Фамилия<span style="color: red">*</span>:</b></span>{!! Form::input('text', 'lastname', $soiskatel->lastname, array('id'=>'lastnameField', 'class'=>'fioInput')) !!}<BR>
                    <span style="padding-right: 60px;"><b>Имя<span style="color: red">*</span>:</b></span>{!! Form::input('text', 'firstname', $soiskatel->firstname, array('id'=>'nameField', 'class'=>'fioInput' )) !!}<BR>
                    <span style="padding-right: 30px;"><b>Отчество<span style="color: red">*</span>:</b></span>{!! Form::input('text', 'otch', $soiskatel->otch, array('id'=>'otchField', 'class'=>'fioInput')) !!}<BR>
                    <span style="padding-right: 35px;"><b>Возраст<span style="color: red">*</span>:</b></span>{!! Form::input('text', 'age', (int)$soiskatel->age, array('id'=>'ageField', 'size'=>3, 'style'=>'width:30px;')) !!} лет<BR>
                    <span style="padding-right: 25px;"><b>Вакансия<span style="color: red">*</span>:</b></span>{!! Form::input('text', 'vacancy', $soiskatel->vacancy, array('id'=>'vacancyField', 'size'=>24)) !!}<BR>
                    <b><span style="float: left; margin-top:13px;">Город:</span></b><div id="city_block" style="float: left; margin-left:50px;">{!! Form::input('text', 'city', $soiskatel->city, array('id'=>'city', 'size'=>24)) !!}</div><BR>
                </div>
            </td>
            <td>
                <table class="inner_table">
                    <thead>
                        <tr>
                            <th><b>Подработка</b></th><th><b>Вахта</b></th><th><b>Постоянная работа</b></th>
                        </tr>
                        <tr></tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                {!! Form::radio('interes_podrabotka', (($soiskatel->interes_podrabotka==1)?1:0), (($soiskatel->interes_podrabotka==1)?true:false)); !!}<p>Интересно</p><br>
                                {!! Form::radio('interes_podrabotka', (($soiskatel->interes_podrabotka==0)?1:0), (($soiskatel->interes_podrabotka==0)?true:false)); !!}<p>Не интересно</p><br>
                            </td>
                            <td>
                                {!! Form::radio('interes_vahta', (($soiskatel->interes_vahta==1)?1:0), (($soiskatel->interes_vahta==1)?true:false)); !!}<p>Интересно</p><br>
                                {!! Form::radio('interes_vahta', (($soiskatel->interes_vahta==0)?1:0), (($soiskatel->interes_vahta==0)?true:false)); !!}<p>Не интересно</p><br>
                            </td>
                            <td>
                                {!! Form::radio('interes_postrabota', (($soiskatel->interes_postrabota==1)?1:0), (($soiskatel->interes_postrabota==1)?true:false)); !!}<p>Интересно</p><br>
                                {!! Form::radio('interes_postrabota', (($soiskatel->interes_postrabota==0)?1:0), (($soiskatel->interes_postrabota==0)?true:false)); !!}<p>Не интересно</p><br>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                Кем работал: {!! Form::input('text', 'kemrabotal', $soiskatel->kemrabotal) !!}
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <b>Категория<span style="color: red">*</span>:</b><BR>
                                {!! Form::radio('category', (($soiskatel->category==1)?1:0), (($soiskatel->category==1)?true:false)); !!}<p>Ужешный</p><br>
                                {!! Form::radio('category', (($soiskatel->category==0)?1:0), (($soiskatel->category==0)?true:false)); !!}<p>Новичок</p><br>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr class="maintable">
            <td class="maintable">

                <table border="0">
                    <tbody>
                        <tr>
                            <td>
                                <span style="font-size:12px;"><b>Станция метро:</b></span>
                            </td>
                            <td>
                                {!! Form::select('mos_metro_station', $metro_list, $soiskatel->mos_metro_station, array('id'=>'mos_metro_station', 'size'=>5)); !!}
                            </td>
                        </tr>
                    </tbody>
                </table>





                <div class="user_country">
                    <div style="float: left; line-height:2px;">
                        <b>Пол:</b><BR>
                        {!! Form::radio('gender', 1, (($soiskatel->gender==1)?true:false)); !!}<p>Мужской</p><br>
                        {!! Form::radio('gender', 0, (($soiskatel->gender==0)?true:false)); !!}<p>Женский</p><br>
                    </div>
                    <div style="float: left; line-height:2px; margin-left: 24px">
                        <b>Гражданство:</b><BR>
                        <?php foreach ($country_list as $key=>$val) { ?>
                        {!! Form::radio('grazd', $key, (($soiskatel->grazd==$key)?true:false)); !!}<p><?=$val;?></p><br>
                        <?php } ?>


                    </div>
                    <div style="clear: both;">
                        <b>Телефон<span style="color: red">*</span>:</b>{!! Form::input('text', 'aon', $soiskatel->aon, array('id'=>'phoneChk')) !!}<span class="countryPhone"></span><BR>
                        <div id="getPhone" style="float: left;margin-left:65px;">
                            <img width="24px" height="26px" src="/images/sms_icon2.png">
                        </div>
                        <a href="sip:989136191279">
                            <img src="/images/pink_dayDown.gif">
                        </a>
                    </div>
                    <div style="clear: both;">
                        <b>E-mail:</b>{!! Form::input('text', 'email', $soiskatel->email) !!}<BR>
                    </div>
                    <div style="clear: both;">
                        <b>Источник поиска<span style="color: red">*</span>:</b>
                        {!! Form::select('searchsource', $sources_list, $soiskatel->searchsource, array('size'=>8)); !!}
                        <BR>
                    </div>
                    {!! Form::hidden('calldate', date('Y-m-d')) !!}
                    {!! Form::hidden('calltime', date('Y-m-d')) !!}

                </div>
            </td>
            <td class="maintable" style="vertical-align:top;padding-top:10px">
                <table class="choisetable" border="0">
                    <tbody>
                        <tr bgcolor="#FFFF99">
                            <td class="maintable">
                                <b><u>Основной</u> фактор выбора<br>места работы<span style="color: red">*</span>:</b><br>
                                {!! Form::select('factor1', $factorwork_list, $soiskatel->factor1, array('size'=>4)); !!}
                            </td>
                            <td class="maintable">
                                <b><u>Вторичный</u> фактор выбора<br>места работы<span style="color: red">*</span>:</b><br>
                                {!! Form::select('factor2', $factorwork_list, $soiskatel->factor2, array('size'=>4)); !!}
                            </td>
                        </tr>
                        <tr bgcolor="#FFFF99">
                            <td class="maintable">
                                <b>Альтернативый фактор выбора<br>места работы:</b><br>
                            </td>
                            <td class="maintable">
                                {!! Form::textarea('factor3', $soiskatel->factor3) !!}
                            </td>
                        </tr>
                        <td class="maintable">
                            <b>Второй телефон:</b>
                        </td>
                        <td class="maintable">
                            {!! Form::input('text', 'secondphone', $soiskatel->secondphone) !!}
                        </td>
                    </tbody>
                </table>

                <div style="clear: both;">
                    <b>Комментарий:</b>
                    {!! Form::textarea('comments', $soiskatel->comments) !!}<BR>
                </div>
                <div style="clear: both;">
                    {!! Form::checkbox('ask4address', 1, (($soiskatel->ask4address==1)?true:false)) !!}<p style="margin-top:-19px; margin-left:25px;">Спросил адрес<span style="color: red">*</span>?</p><BR>
                </div>
                <div style="clear: both;">
                    <b>Итог:</b>
                    {!! Form::select('customer_status', $status_list, $soiskatel->customer_status, array('size'=>8, 'id'=>'customer_status', 'onclick'=>'checkStatus()', 'onchange'=>'checkStatus()')) !!}
                    {!! Form::submit('Готово', array('class'=>'maintable', 'id'=>'mainsubmit', ((!$soiskatel->customer_status)?('disabled'):'')  )) !!}
                </div>
                <table border=0 width=300px>
                    <tr>
                        <td style="font-size: 12px;"><b>Готов<br>приступить<span style="color: red">*</span>:</b></td>
                        <td>{!! Form::input('text', 'day', $soiskatel->kogda_pridet, array('class'=>'date datepicker', 'id'=>'inputField')) !!}<?=$soiskatel->arrivaltime;?></td>
                    </tr>
                </table><br>


            </td>
        </tr>






    </tbody>



</table>

{!! Form::hidden('check_customer_status', 0, array('id'=>'check_customer_status')) !!}