@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
@stop

@section('scripts')
    <script src="{{ URL::asset('/js/support/script.js') }}"></script>

<?php if (isset($soiskatel->aon) && !empty($soiskatel->aon)){ ?>
    <script>
        var aon = '<?=$soiskatel->aon;?>';
    </script>
    <script src="{{ URL::asset('/js/edit/getPhone.js') }}"></script>
<?php } ?>

<script src="{{ URL::asset('/js/edit/sendSMS.js') }}"></script>
<script src="{{ URL::asset('/js/edit/sendBirza.js') }}"></script>
<script src="{{ URL::asset('/js/edit/callcenter/editpage.js') }}"></script>
<script src="{{ URL::asset('/js/edit/jquery-ui-1.10.3.custom.js') }}"></script>
<script src="{{ URL::asset('/js/edit/editphonechk.js') }}"></script>
<script src="{{ URL::asset('/js/edit/socket.io.min.js') }}"></script>
<script src="{{ URL::asset('/js/edit/asterisk.js') }}"></script>
<script src="{{ URL::asset('/js/edit/chk-lock.js') }}"></script>


@stop

@extends('app')

@extends('nav')

@section('content')
    <div class="container">
        <div id="lock_block">

        </div>

        <div class="row">
            @include('flash::message')

            <center>
                <a href="/edit/create" target="_blank" class="btn btn-primary" style="margin-bottom: 20px;">Новая заявка</a>
                <a href="/fullbase" class="btn btn-default" style="margin-bottom: 20px;">Вернуться к списку</a>
            </center>

            {!! Form::open(array('url' => '/edit/formupdate')) !!}
                @include('edit.form')
            {!! Form::close() !!}

            @include('edit.history')

            @include('edit.phonestat')
        </div>
    </div>



@endsection