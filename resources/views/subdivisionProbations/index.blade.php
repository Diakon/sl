@section('head')
@stop
@section('scripts')

@stop

@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Стажировочные дни на подразделениях</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('subdivisionProbations.create') !!}">Добавить новую запись</a>
        </div>

        <div class="row">
            @if($subdivisionProbations->isEmpty())
                <div class="well text-center">Записей не найдено.</div>
            @else
                @include('subdivisionProbations.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $subdivisionProbations])


    </div>
@endsection
