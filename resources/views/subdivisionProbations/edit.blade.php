@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop
@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
@stop

@extends('app')

@extends('nav')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($subdivisionProbations, ['route' => ['subdivisionProbations.update', $subdivisionProbations->id], 'method' => 'patch']) !!}

        @include('subdivisionProbations.fields')

    {!! Form::close() !!}
</div>
@endsection
