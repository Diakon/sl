<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $subdivisionProbations->id !!}</p>
</div>

<!-- Subdivision Id Field -->
<div class="form-group">
    {!! Form::label('subdivision_id', 'Subdivision Id:') !!}
    <p>{!! $subdivisionProbations->subdivision_id !!}</p>
</div>

<!-- Probation Days Field -->
<div class="form-group">
    {!! Form::label('probation_days', 'Probation Days:') !!}
    <p>{!! $subdivisionProbations->probation_days !!}</p>
</div>

<!-- Repeat Days Field -->
<div class="form-group">
    {!! Form::label('repeat_days', 'Repeat Days:') !!}
    <p>{!! $subdivisionProbations->repeat_days !!}</p>
</div>

<!-- Object Days Field -->
<div class="form-group">
    {!! Form::label('object_days', 'Object Days:') !!}
    <p>{!! $subdivisionProbations->object_days !!}</p>
</div>

<!-- Date Start Field -->
<div class="form-group">
    {!! Form::label('date_start', 'Date Start:') !!}
    <p>{!! $subdivisionProbations->date_start !!}</p>
</div>

<!-- Date End Field -->
<div class="form-group">
    {!! Form::label('date_end', 'Date End:') !!}
    <p>{!! $subdivisionProbations->date_end !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $subdivisionProbations->created_at !!}</p>
</div>

