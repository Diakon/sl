<!-- Subdivision Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('subdivision_id', 'Подразделение:') !!}
    {!! Form::select('subdivision_id', $subdivisions, ((isset($subdivisionProbations->subdivision_id))?$subdivisionProbations->subdivision_id:null), ['class' => 'form-control']) !!}
</div>

<div style="clear: both;"></div>

<!-- Probation Days Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('probation_days', 'Дни стажеровки:') !!}
	{!! Form::number('probation_days', null, ['class' => 'form-control']) !!}
</div>

<div style="clear: both;"></div>

<!-- Repeat Days Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('repeat_days', 'Повторные дни:') !!}
	{!! Form::number('repeat_days', null, ['class' => 'form-control']) !!}
</div>

<div style="clear: both;"></div>

<!-- Object Days Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('object_days', 'Дни на объекте:') !!}
	{!! Form::number('object_days', null, ['class' => 'form-control']) !!}
</div>

<div style="clear: both;"></div>

<!-- Date Start Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('date_from', 'Дата начала:') !!}
    {!! Form::text('date_from', null, ['class' => 'form-control']) !!}
</div>

<div style="clear: both;"></div>

<!-- Date End Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('date_to', 'Дата окончания:') !!}
    {!! Form::text('date_to', null, ['class' => 'form-control']) !!}
</div>

<div style="clear: both;"></div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="/subdivisionProbations" class="btn btn-default" >Отмена</a>
</div>

