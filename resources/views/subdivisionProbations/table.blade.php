<table class="table table-striped table-bordered">
    <thead>
            <th>№</th>
			<th>Подразделение</th>
			<th>Дни стажеровки</th>
			<th>Повторные дни</th>
			<th>Дни на объекте</th>
			<th>Дата начала</th>
			<th>Дата окончания</th>
            <th width="50px">Действия</th>
    </thead>
    <tbody>
    @foreach($subdivisionProbations as $subdivisionProbations)
        <tr>
            <td>{!! $subdivisionProbations->id !!}</td>
			<td>{!! $subdivisions[$subdivisionProbations->subdivision_id] !!}</td>
			<td>{!! $subdivisionProbations->probation_days !!}</td>
			<td>{!! $subdivisionProbations->repeat_days !!}</td>
			<td>{!! $subdivisionProbations->object_days !!}</td>
			<td>{!! $subdivisionProbations->date_from !!}</td>
			<td>{!! $subdivisionProbations->date_to !!}</td>
            <td>
                <a href="{!! route('subdivisionProbations.edit', [$subdivisionProbations->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('subdivisionProbations.delete', [$subdivisionProbations->id]) !!}" onclick="return confirm('Вы действительно хотите удалить эту запись?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
