@section('nav')
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!--	<a class="navbar-brand" href="#">Laravel</a> -->
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            {!! $menu !!}

            <ul class="nav navbar-nav navbar-right">
                @if (Auth::user())
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">{{ Auth::user()->username}} <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ url('/lcab/telegrams') }}">Телеграм</a></li>
                            <li><a href="{{ url('/personal_area') }}">Личный кабинет</a></li>
                            <li><a href="{{ url('/auth/logout') }}"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Выйти</a></li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>

    </div>
</nav>
@endsection

@section('bottomnav')
    <div style="height: 51px;"></div>
    <nav class="navbar navbar-inverse navbar-fixed-bottom">
        <div id="bottom-brand" class="navbar-header">
        </div>
        <div id="calls" class="container">
            <ul class="nav navbar-nav">
                <li class="active" role="presentation"><a href="/edit/create" target="_blank">Новая заявка</a></li>
            </ul>
        </div>
    </nav>
@endsection