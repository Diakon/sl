<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="{{ URL::asset('/js/jquery.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/bootstrap/css/bootstrap.min.css') }}" />
    <script src="{{ URL::asset('/bootstrap/js/bootstrap.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/js/libs/select2/select2.css') }}" />
    <script src="{{ URL::asset('/js/libs/select2/select2.min.js') }}"></script>
    {!! Html::script('/js/libs/select2/i18n/ru.js') !!}
    <script src="{{ URL::asset('/js/support/script.js') }}"></script>
    <script>var site = "<?=$_SERVER['SERVER_NAME']; ?>";</script>
    <script>var siteid = "";</script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>
<body>

<style>

    table {
        background: none repeat scroll 0 0 #f6f5f5;
        border-collapse: collapse;
        margin: 10px;
        border:1px solid #000000;
    }
    td, th {
        text-align: center;
        border: 1px solid #000000;
        padding: 10px;
    }
    .filter {
        padding: 10px;
    }

    .wrap {
        width: 300px;
        background: repeating-linear-gradient(
            45deg,
            #DDDDDD,
            #9b9b9b 10px,
            #A3A3A3 12px,
            #9b9b9b 15px
        );

    }
</style>

<?php $userAuthor = $authorfio.' ('.$authorname.' '.$authorphone.')'; ?>

<p style="margin-left: 10px;"><b>Заявка от <?=$userAuthor;?> на <?=$activityName;?></b></p></br>
<form id="sendForm">
        <?php
            if ((int)$support_type == 3){
            //Форма добавления пользователя
            $tbl_username = str_replace(" ","",$tbl_username);
        ?>

            <table>
                <tr bgcolor="#DDDDDD"><th>ФИО</th><th>Имя пользователя (username)</th><th>Пароль</th><th>E-mail</th><th>Телефон</th><th>Уровень доступа</th><th>Активен</th></tr>
                <tr>
                    <td><input type="text" name="User[fio]" value="<?=$tbl_fio?>" /></td>
                    <td><input type="text" name="User[username]" value="<?=$tbl_username?>" /></td>
                    <td><input type="text" name="User[pass]" value="<?=$pass?>" /></td>
                    <td><input type="text" name="User[email]" value="<?=$tbl_email?>" /></td>
                    <td><input type="text" name="User[phone]" value="<?=$tbl_phone?>" /></td>
                    <td>
                        <select name="User[lvl]">
                            <?php
                            foreach ($users_status as $key=>$val){
                                echo '<option value="'.$key.'" '.(($key==$tbl_lvl)?"selected":"").'  >'.$val.'</option>';
                            }
                            ?>
                        </select>
                    </td>
                    <td><input type="checkbox" name="User[activ]" value="1" checked></td>

                </tr>



            </table>
        <?php } ?>
    <input type="hidden" name="id" value="<?=$get_id;?>" />
</form>

<?php
if((int)$status_tiket != 3) { ?>
    <a class="sendFormBtn btn btn-success" href="#" style="margin-left: 10px;">Выполнить и закрыть заявку</a>
<?php } ?>

<a class="btn btn-danger" href="{{ route('support.listtiket') }}" style="margin-left: 10px;">Отмена</a>





</body>
</html>