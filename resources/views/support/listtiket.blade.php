@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <META HTTP-EQUIV="REFRESH" CONTENT="30">
@stop

@section('scripts')
    <script src="{{ URL::asset('/js/support/script.js') }}"></script>
    <script>var site = "<?=$_SERVER['SERVER_NAME']; ?>";</script>
    <script>var siteid = "";</script>
    <script>var typedepartment = "<?php if (isset($_GET['department']) && (int)$_GET['department']==2){echo '1';} else echo '0'; ?>";</script>
    <script>
        $("#filterObjects").select2({language: 'ru'});
        $(document).ready(function(){
            if (typedepartment==1){ $('.finParam').show();  }
        });
    </script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@stop

@extends('app')

@extends('nav')

@section('content')

    <style>

        td, th {
            text-align: center;
        }
        .wrap {
            width: 300px;
            background: repeating-linear-gradient(
                    45deg,
                    #DDDDDD,
                    #9b9b9b 10px,
                    #A3A3A3 12px,
                    #9b9b9b 15px
            );

        }
        .finParam {
            display: none;
        }
        .select2-container{
            min-width:200px;
            margin-top:-10px;
        }
    </style>

    <!-- Modal завершения задачи -->
    <div id="compliteModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="compliteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="title-modal"></div>
                </div>
                <div class="modal-body">
                    <b>Пояснение к решению задачи (не обязательно):</b>
                    <textarea style="width:515px;" rows="10" cols="45" name="bodycomplite" id="bodycomplite"></textarea>
                    <input type="hidden" id="userModal" name="user" />
                    <input type="hidden" id="typeModal" name="type" />

                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                    <button class="btn btn-primary" id="complitedBtn">Сохранить</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <h1>{{ $app_title = 'Просмотр IT заявок' }}</h1>
        <div class="row filter">
            {!! Form::model($request,['url' => '#', 'class' => 'form-inline']) !!}
                <b>Фильтровать по:</b></br>
                {!! Form::select('status', $statusArr, null, ['class' => "form-control", 'id'=>'filterStatus', 'placeholder' => 'Все статусы']) !!}
                @if (Auth::user()->hasRole('admin'))
                    {!! Form::select('department', $departmentArr, null, ['class' => "form-control", 'id'=>'filterDepartment', 'placeholder' => 'Все отделы']) !!}
                @endif
                {!! Form::text('times', null, ['class'=>'form-control datepicker', 'placeholder'=>'По дате', 'id'=>'filter_time']) !!}
                {!! Form::select('author', $filterAuthor, null, ['class' => "form-control", 'id'=>'filter_author']) !!}

                {{-- Если в Фин отделе - добавляю фильтр по объекту --}}
                @if ( Request::exists('department') && Request::get('department')==2 && false )
                    {!! Form::select('object_id', $objects, null, ['class' => "form-control"]) !!}
                @endif
                <a class="btn btn-primary" id="applyFilter">Фильтровать</a>
                <a class="btn btn-default" href="{{ route('support.listtiket') }}">Сбросить</a>
            {!! Form::close() !!}
        </div>

        <div class="text-center">
            @include('common.paginate', ['records' => $result])
        </div>

        <div class="row">
            <div id="block">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr bgcolor="#DDDDDD">
                        <th class="finParam">ID заявки</th>
                        <th>Автор заявки</th>
                        <th class="finParam">Анкета</th>
                        <th>Причина обращения</th>
                        <th>Отдел</th>
                        <th>Статус</th>
                        <th style="width:90px">Дата подачи заявки</th>
                        <th style="width:430px">Действия</th>
                    </tr>
                </thead>
                <tbody>
                <?php

                foreach($result as $row){

                    if (  ((int)$row->disabled == 1 && $user_status != 15) || ((int)$row->tiket_type ==3 && $user_status != 15)   ){ continue; }
                    $status = $statusArr[(int)$row->status];
                    $imgStatus = null;
                    switch ((int)$row->status) {
                        case 1:
                            $imgStatus = '<a href="#" class="shwtooltip" data-toggle="tooltip" data-original-title="Новая заявка"><i class="glyphicons glyphicons-file"></i></a>';
                            break;
                        case 2:
                            $imgStatus = '<a href="#" class="shwtooltip" data-toggle="tooltip" data-original-title="Задача в работе"><i class="glyphicons glyphicons-flag"></i></a>';
                            break;
                        case 3:
                            $imgStatus = '<a href="#" class="shwtooltip" data-toggle="tooltip" data-original-title="Задача решена"><i class="glyphicon glyphicon-ok"></i></a>';
                            break;
                        case 4:
                            $imgStatus = '<a href="#" class="shwtooltip" data-toggle="tooltip" data-original-title="Задача отложена"><i class="glyphicons glyphicons-clock"></i></a>';
                            break;
                        case 5:
                            $imgStatus = '<a href="#" class="shwtooltip" data-toggle="tooltip" data-original-title="Задача просмотрена"><i class="icon-eye-open"></i></a>';
                            break;
                    }

                    if ($row->disabled == 1){
                        $imgStatus = '<a href="#" class="shwtooltip" data-toggle="tooltip" data-original-title="Задача отключена"><i class="icon-lock"></i></a>';
                    }

                    $department = $departmentArr[(int)$row->department];
                    $vahta = $row->is_vahta;

                    $styleType = "";
                    if ( (int)$row->status == 1 && $row->disabled == 0 ){ $styleType = "style = 'background:#f8b9b7'"; }
                    if ( (int)$row->tiket_type == 3 && $row->disabled == 0 && (int)$row->status == 1 ){ $styleType = "class = ' wrap '"; }

                    //Если заявка из ТУОВ - указываю человека и делаю ссылкой на анкету
                    $anketa = "";
                    if ( (int)$row->source==2 ){
                        $anketa = '<a href="http://vls.secondlab.ru/op/anketa.php?select=update_form&id='.(int)$row->id_anketa.'" target="_blank">'.$row->pmlastname.' '.$row->pmfirstname.' '.$row->pmmidlename.'</a>';
                    }

                    echo '
                    <tr '.$styleType.'>
                        <td class="finParam">'.(int)$row->id.'</td>
                        <td>'.$row->fio.((!empty($row->email))?" [".$row->email."] ":"").''.((!empty($vahta))?"</br><b>заявка от вахтовика</b>":"").'</td>
                        <td class="finParam">'.$anketa.'</td>
                        <td>
                            <a href = "'.(((int)$row->tiket_type==3)?'/support/formcomplite':'/support/viewtiket').'?id='.(int)$row->id.'" target="'.(((int)$row->tiket_type!=3)?'_blank':'_self').'"  data-id="'.(int)$row->id.'" data-status="'.$row->status.'" class="openOrderURL">
                                ' . mb_strimwidth(strip_tags($row->request), 0, 30, "...", 'UTF-8') . '
                            </a>
                        </td>
                        <td>'.$department.'</td>
                        <td>'.$imgStatus.(($row->disabled==0)?$status:'Задача отключена').'</td>
                        <td>'.(date("Y-m-d", strtotime($row->created))).'</td>
                    ';
                    echo '<td>';

                    if ( (int)$row->status != 3 ){
                        if ($row->disabled==0 && $row->tiket_type!=3){
                            //Обычные заявки
                            echo '
                            <a href="#" class="shwtooltip btn btn-success" data-status="complite" data-id="'.((int)$row->id).'" data-toggle="tooltip" data-original-title="Задача решена">Задача решена</a>
                            <a href="#" class="shwtooltip btn btn-warning" data-status="inwork" data-id="'.((int)$row->id).'" data-toggle="tooltip" data-original-title="Задача в работе">Задача в работе</a>
                            <a href="#" class="shwtooltip btn btn-danger" data-status="proroguep" data-id="'.((int)$row->id).'" data-toggle="tooltip" data-original-title="Задача отложена">Задача отложена</a>
                        ';
                        }
                        else {
                            //Заявки через форму (таблицы support_type)
                            if ($row->disabled == 0){
                                echo '
                                <a href="/support/formcomplite?id='.((int)$row->id).'" class="btn btn-success" data-toggle="tooltip" data-original-title="Задача решена">Задача решена</a>
                                <a href="#" class="shwtooltip btn btn-warning" data-status="inwork" data-id="'.((int)$row->id).'" data-toggle="tooltip" data-original-title="Задача в работе">Задача в работе</a>
                                <a href="#" class="shwtooltip btn btn-danger" data-status="proroguep" data-id="'.((int)$row->id).'" data-toggle="tooltip" data-original-title="Задача отложена">Задача отложена</a>
                            ';
                            }
                        }




                        if (  $user_status == 15 ) {
                            switch ((int)$row->disabled) {
                                case 0:
                                    echo '<a href="#" class="shwtooltip btn" data-status="disabled_true" data-id="'.((int)$row->id).'" data-toggle="tooltip" data-original-title="Отключить заявку">Отключить заявку</a>';
                                    break;
                                case 1:
                                    echo '<a href="#" class="shwtooltip btn" data-status="disabled_false" data-id="'.((int)$row->id).'" data-toggle="tooltip" data-original-title="Включтить заявку">Включтить заявку</a>';
                                    break;
                            }

                        }
                    }
                    echo '
                    </td>
                </tr>
                ';

                }
                ?>
                </tbody>
            </table>
            </div>
        </div>

        <div class="text-center">
            @include('common.paginate', ['records' => $result])
        </div>
    </div>
@endsection
