@section('head')
    <link rel="stylesheet" href="{{ URL::asset('/js/support/summernote/summernote.css') }}" />
@stop

@section('scripts')
    <script src="{{ URL::asset('/js/support/summernote/summernote.js') }}"></script>
    <script src="{{ URL::asset('/js/support/script.js') }}"></script>
    <script src="{{ URL::asset('/js/scheduler/assets/redactor.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#body').summernote(
                {
                    height: 300
                }
            );
        });
    </script>
@stop

@extends('app')

@extends('nav')

@section('content')


    <style>
        body {
            background: #A3A3A3;
        }
        .blank {
            width: 500px;
            min-height: 530px;
            background: #E7E7E7;
            box-shadow: 0 0 10px rgba(0,0,0,0.5);
            padding: 10px;
            margin: 0 auto;
            border-radius: 5px;
        }
        .title {
            margin-left: 17%;
            font-size:25px;
            font-weight: 600;
        }
        .formRow {
            margin-top: 20px;
        }
        TEXTAREA {
            width: 485px;
        }
        .redactor-box .redactor-editor {
            min-height: 250px;
        }
    </style>



    <div class="blank" id="formshow">
        <form id="sendForm">
            <span class="title">{{ $app_title = 'Заявка в службу поддержки' }}</span>
            @include('common.errors')
            <?php
            //Админ могут делать заявки не от своего имени
            if ( Auth()->user()->ability('admin','') ){
                echo '
                    <div class="formRow">
                        <b>Вы можете составить заявку от имени другого пользователя:</b></br>';

                echo '
                        <div class="form-group col-xs-6">
                        <select id="userSelect" name="fromuser">
                            <option selected value="">Выберите пользователя</option>
                    ';

                foreach ($vahta as $key=>$val){
                    echo '<option value="vahta'.$key.'">Вахтовик ('.$val.')</option>';
                }

                if ( Auth()->user()->ability('admin','')) {

                    foreach ($users as $key=>$value){
                        echo '
                            <option value="'.$key.'">'.$value['fio'].((!empty($value['email']))?' ['.$value['email'].']':"").'</option>
                            ';
                    }

                }
                echo '
                        </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    ';
            }
            ?>
            <div class="formRow">
                <b>Укажите отдел в который Вы хотите направить запрос:</b>
                <div class="form-group col-xs-6">
                    <select id="" name="department" class="form-control">
                        <option value="1">Информационный отдел</option>
                        <option value="2">Финансовый отдел</option>
                        <option value="3">ОП отдел</option>
                    </select>
                </div>
                <div class="clearfix"></div>
            </div>

            <?php
            //Пользователи, которым можно выводить форму добавления пользователя
            switch (Auth::id()) {
                case 165:
                case 7:
                case 96:
                case 122:
                case 156:
                case 160:
                case 164:
                case 211:
                    echo '
                            <div class="formRow">
                                <a href="#" class="btn btn-info" onclick="$(\'#adduserModal\').modal(\'show\');">Добавить нового пользователя</a>
                            </div>
                        ';
            }

            ?>



            <div class="formRow">
                <b>Опишите проблему:</b>
                <textarea style="margin-top: 20px;" rows="10" cols="45" name="body" id="body"></textarea>
                <input type="hidden" value="1" name="typeorder" />
            </div>
            <div class="formRow">
                <a class="btn btn-success" id="sendButton">Отправить запрос</a>
            </div>

        </form>

    </div>

    <div class="blank" id="success" style="display: none;">
        <h3>Заявка успешно добавлена!</h3>
        <BR>
        <a href="/">Вернуться на главную</a>
    </div>

    <!-- Добавить пользователя модальное окно -->
    <div id="adduserModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h3 id="adduserModalLabel">Добавить нового пользователя</h3>
                </div>
                <div class="modal-body">
                    <b>Пожалуйста, заполните форму.</b></br><p>Поля с <span style="color:#ff001a">*</span> обязательны для заполнения</p>
                    <form id="userFormAdd" method="post" action="">
                        <table>
                            <tr><td>ФИО<span style="color:#ff001a">*</span></td><td><input type="text" id="user_fio" name="User[fio]" class="form-control" /></td></tr>
                            <tr><td>Телефон</td><td><input type="text" id="user_phone" name="User[phone]" class="form-control" /></td></tr>
                            <tr><td>Email</td><td><input type="text" id="user_email" name="User[email]" class="form-control" /></td></tr>
                            <tr><td>Уровень доступа<span style="color:#ff001a">*</span></td><td>
                                    {{ Form::select('User[alevel]', $users_status_list, 6, ['class'=>'form-control', 'id' => 'user_alevel']) }}
                                    <input type="hidden" value="3" name="typeorder" />
                                </td></tr>
                        </table>
                    </form>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                    <button class="btn btn-primary" id="userFormAddBtn">Создать запрос</button>
                </div>
            </div>
        </div>
    </div>

@endsection