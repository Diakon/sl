<?php
    $statusArr = [1=>"Новая", 2=>"В работе", 3=>"Закрыта", 4=>"Отложена", 5=>"Просмотрена"];
    $departmentArr = [1=>"Информационный", 2=>"Финансовый", 3=>"ОП"];
?>

@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/js/reports/daterangepicker/jquery.comiseo.daterangepicker.css" />
@stop

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="https://cdn.jsdelivr.net/momentjs/2.3.1/moment.min.js"></script>
    <script src="{{ URL::asset('/js/reports/daterangepicker/jquery.comiseo.daterangepicker.js') }}"></script>
    <script>
        $(function() { $(".daterangepicker").daterangepicker({
            presetRanges: [{
                text: 'Сегодня',
                dateStart: function() { return moment() },
                dateEnd: function() { return moment() }
            }, {
                text: 'Вчера',
                dateStart: function() { return moment().add('days', -1) },
                dateEnd: function() { return moment().add('days', -1) }
            }, {
                text: 'Предыдущие 7 дней',
                dateStart: function() { return moment().add('days', -6) },
                dateEnd: function() { return moment() }
            }, {
                text: 'Предыдущая неделя',
                dateStart: function() { return moment().add('weeks', -1).startOf('week').add('days',1) },
                dateEnd: function() { return moment().add('weeks', -1).endOf('week').add('days',1) }
            }, {
                text: 'Предыдущий месяц',
                dateStart: function() { return moment().add('month', -1).startOf('month') },
                dateEnd: function() { return moment().add('month', -1).endOf('month') }
            }],
            applyOnMenuSelect: false,
            datepickerOptions: {
                minDate: null,
                maxDate: 0
            }
        })});
    </script>
@stop

@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        <div class="row">
            <h1 class="text-center">{{ $app_title = 'Отчет по ИТ-заявкам' }}</h1>

            <div class="filter">
                {!! Form::model($request,['url' => '#', 'class' => 'form-horizontal', 'method' => 'GET']) !!}
                <div class="col-sm-7">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-6 control-label">Обращения от пользователя</label>
                        <div class="col-sm-6">
                            {!! Form::select('author_id', \App\User::getActiveUsersList(), null, ['class' => "form-control select2", 'placeholder' => 'Обращения от пользователя']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-6 control-label">С заявкой работал сотрудник</label>
                        <div class="col-sm-6">
                            {!! Form::select('worker_id', \App\User::getActiveUsersList(), null, ['class' => "form-control select2", 'placeholder' => 'С заявкой работал сотрудник']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-6 control-label">Статус заявки</label>
                        <div class="col-sm-6">
                            {!! Form::select('status', $statusArr, null, ['class' => "form-control", 'placeholder' => 'Статус заявки']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-6 control-label">Обращения в отдел</label>
                        <div class="col-sm-6">
                            {!! Form::select('department', $departmentArr, null, ['class' => "form-control", 'id'=>'filterDepartment', 'placeholder' => 'Обращения в отдел']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-6 control-label">Период</label>
                        <div class="col-sm-6">
                            {!! Form::text('times', null, ['class'=>'form-control daterangepicker', 'placeholder'=>'По дате', 'id'=>'filter_time']) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-6 control-label"></label>
                        <div class="col-sm-6">
                            {!! Form::submit('Фильтровать', ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>

            @if($supports->isEmpty())
                <div class="info">
                    Записей не обнаружено.
                </div>
            @else
                <table class="table table-striped table-bordered">
                    <thead>
                        <th>ID заявки</th>
                        <th>Автор заявки</th>
                        <th>Причина обращения</th>
                        <th>Отдел</th>
                        <th>Статус</th>
                        <th style="width:160px;">Дата подачи заявки/Закрытия заявки</th>
                        <th>Время выполнения (часов)</th>
                        <th></th>
                    </thead>
                    <tbody>
                        @foreach($supports as $support)
                            <tr>
                                <td>{{ $support->id }}</td>
                                <td>{{ $support->username }}</td>
                                <td>{!! $support->body !!}</td>
                                <td>
                                    {{ $departmentArr[$support->department] }}
                                </td>
                                <td>
                                    {{ $statusArr[$support->status_tiket] }}
                                </td>
                                <td>
                                    {{ $support->created_at }}
                                    /
                                    @if (!empty($support->date_end))
                                        {{ $support->date_end }}
                                    @else
                                        не закрыта
                                    @endif
                                </td>
                                <td>
                                    @if ( !empty($support->date_end))
                                        <?php
                                        $date1 = new DateTime($support->date_end);
                                        $date2 = new DateTime($support->created_at);
                                        $diff = $date2->diff($date1);
                                        echo $diff->format('%a') * 24 + $diff->format('%h');
                                        ?>
                                    @else
                                        не закрыта
                                    @endif
                                </td>
                                <td>
                                    <a href="{!! route('support.viewtiket', ['id' => $support->id]) !!}" target="_blank">
                                        <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif


        </div>
        @include('common.paginate', ['records' => $supports])
    </div>

@endsection