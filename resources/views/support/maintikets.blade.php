@section('head')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="{{ URL::asset('/css/support/style.css') }}">
<META HTTP-EQUIV="REFRESH" CONTENT="30">
@stop

@section('scripts')
    <script src="{{ URL::asset('/js/support/script.js') }}"></script>
    <script>var site = "<?=$_SERVER['SERVER_NAME']; ?>";</script>
    <script>var siteid = "";</script>
    <script>var typedepartment = "<?php if (isset($_GET['department']) && (int)$_GET['department']==2){echo '1';} else echo '0'; ?>";</script>
    <script>
        $("#filterObjects").select2({language: 'ru'});
        $(document).ready(function(){
            if (typedepartment==1){ $('.finParam').show();  }
        });
    </script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@stop

@extends('app')

@extends('nav')

@section('content')

<div id="block">

    <div>
        <center><h2>Мои заявки</h2></center>
    </div>

    <center>{!! $tables->render() !!}</center>
    <table>
        <tr bgcolor="#DDDDDD"><th>Дата подачи заявки</th><th>Причина обращения</th><th>Дата решения заявки</th><th>Исполнитель</th><th>Коментарий исполнителя</th><th>Отдел</th><th>Статус</th><th style="min-width:300px">Действия</th></tr>
        <?php foreach ($tables as $data) { ?>
            <?php $department = $departmentArr[(int)$data->department]; ?>
            <tr>
                <td><?=(date('d-m-Y H:i:s', strtotime($data->created)));?></td>
                <td><?=$data->request;?></td>
                <td><?=(date('d-m-Y H:i:s', strtotime($data->complite_date)));?></td>
                <td><?=$data->fio;?> [<?=$data->email;?>]</td>
                <td><?=(!empty($data->solution))?$data->solution:'нет коментаря';?></td>
                <td><?=$departmentArr[(int)$data->department];?></td>
                <td><?=$statusArr[(int)$data->status];?></td>
                <td>
                    <?php
                        if ( (int)$data->confirm_author != 2 ){
                            echo '
                                <a href="#" data-id="'.$data->id.'" data-status="2"  class="compliteBtn btn btn-success" >Заявка решена</a>
                                <a href="#" data-id="'.$data->id.'" data-status="1"  class="compliteBtn btn btn-danger" >Заявка не решена</a>
                            ';
                        }
                    ?>
                </td>
            </tr>
        <?php } ?>
    </table>
    <center>{!! $tables->render() !!}</center>

@endsection