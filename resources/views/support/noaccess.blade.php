<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script src="{{ URL::asset('/js/jquery.min.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/bootstrap/css/bootstrap.min.css') }}" />
    <script src="{{ URL::asset('/bootstrap/js/bootstrap.min.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/js/libs/select2/select2.css') }}" />
    <script src="{{ URL::asset('/js/libs/select2/select2.min.js') }}"></script>
    {!! Html::script('/js/libs/select2/i18n/ru.js') !!}
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/scheduler/style.css') }}" />
    <script src="{{ URL::asset('/js/scheduler/script.js') }}"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
</head>

<body>

<p>У Вас нет прав для доступа к этой странице. Сообщите администратору, если Вам действительно нужен доступ.</p>





</body>
</html>