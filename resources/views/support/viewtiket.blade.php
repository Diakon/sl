@section('head')
    <link rel="stylesheet" href="{{ URL::asset('/js/support/summernote/summernote.css') }}" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ URL::asset('/js/support/summernote/summernote.js') }}"></script>
    <script src="{{ URL::asset('/js/support/script.js') }}"></script>
    <script src="{{ URL::asset('/js/scheduler/assets/redactor.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#body').summernote(
                    {
                        height: 300
                    }
            );
        });
    </script>
@stop

@extends('app')

@extends('nav')

@section('content')


    <style>

        table {
            background: none repeat scroll 0 0 #f6f5f5;
        }
        th {
            text-align: left;
        }
        .user_data {
            padding: 10px;
            border-radius: 10px;
            background: #dad9d9;
        }
    </style>


    <!-- Modal завершения задачи -->
    <div id="compliteModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="compliteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="title-modal"></div>
                </div>
                <div class="modal-body">
                    <b>Пояснение к решению задачи (не обязательно):</b>
                    <textarea style="width:515px;" rows="10" cols="45" name="bodycomplite" id="bodycomplite"></textarea>
                    <input type="hidden" id="userModal" name="user" />
                    <input type="hidden" id="typeModal" name="type" />
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                    <button class="btn btn-primary" id="complitedBtn">Сохранить</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal установить дедлайн -->
    <div id="deadlineModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="deadlineModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="title-modal"></div>
                </div>
                <div class="modal-body">
                    <b>Установите время дедлайна:</b>
                    <input type="text" id="deadline_time" name="deadline_time" class="datepicker form-control" />
                    <input type="hidden" id="deadline_task_id" name="deadline_task_id" />
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                    <button class="btn btn-primary" id="compliteDeadlinedBtn">Сохранить</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal добавить задачу в JIRA -->
    <div id="jiroModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="jiroModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="bnt close" data-dismiss="modal" aria-hidden="true">×</button>
                    <div class="title-modal"></div>
                </div>
                <div class="modal-body">
                    <b>Для добавления задачи в JIRA введите ваш логин и пароль от JIRA:</b>
                    <input type="text" id="jiro_name" value="<?=((isset($jira_auth['login']))?($jira_auth['login']):(''));?>" name="jiro_name" class="form-control" placeholder="Логин" />
                    <input type="password" id="jiro_pass" value="<?=((isset($jira_auth['password']))?($jira_auth['password']):(''));?>" name="jiro_pass" class="form-control" placeholder="Пароль" />
                    <select id="type_task" class="form-control">
                        <option selected value="Story">Story</option>
                        <option value="Task">Task</option>
                        <option value="Bug">Bug</option>
                        <option value="Epic">Epic</option>
                    </select>
                    <BR><i>Внимание! Вводите именно <b>логин</b> от вашей учетной записи в JIRA, а не E-mail</i>
                    <BR><input type="checkbox" name="remember_pass_jira"  id="rememberPassJIRA" value="1" ><span style="margin-left:5px; position:absolute;">Запомнить</span>
                </div>
                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                    <button class="btn btn-primary" id="addJiroBtn" data-id="<?=((int)$_GET['id']);?>">Сохранить</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-xs-12"><h1> {{ $app_title = sprintf('IT заявка от %s', $soldata) }}</h1></div>

            <div class="col-sm-8">
                <table class="table table-bordered">
                    <tr>
                        <th style="width: 200px;">Автор заявки</th>
                        <td>
                            {{ $autor_fio }} [{{ $autor }}]<br/>
                            {{ $autor_email }}
                        </td>
                    </tr>
                    <tr>
                        <th>Проблема у пользователя</th>
                        <td>
                            @if ( !empty($u_id) && empty($vahta) )
                                {{ $from_fio }} [{{ $from }}]<br/>
                                {{ $from_email }}
                            @elseif ($vahta)
                                заявка от вахтовика
                            @endif
                        </td>
                    </tr>

                    <tr><th>Причина обращения</th><td><samp><?= $requst ?></samp></td></tr>
                    <tr><th>Заявка в отдел</th><td><?= $department ?></td></tr>
                    <tr><th>Дата подачи заявки</th><td>{{ $soldata }}</td></tr>
                    <tr>
                        <th>
                            Последний пользователь<br/>
                            работавший с заявкой
                        </th>
                        <td>
                            @if ( !empty($anwer_id) )
                                {{ $answer_fio }} [{{ $answer }}]<br/>
                                {{ $answer_email }}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th>Статус заявки</th><td><?= $statusStr ?></td>
                    </tr>
                    <tr>
                        <th>Дедлайн</th>
                        <td>
                            @if ( !empty($dead_line) )
                                {{ Carbon\Carbon::parse($dead_line)->format('Y-m-d') }}
                            @else
                                <a href="#" class="adddeadline btn btn-default"  data-id="{{ Request::get('id') }}">Установить дедлайн</a>
                            @endif
                        </td>
                    </tr>

                    @if ( Auth()->user()->ability('admin','') )
                        <tr>
                            <th>Заявка в JIRA</th>
                            <td>
                                @if ( empty($jiro) )
                                    <a href="#" class="btn btn-default" id="addToJiro">Добавить в JIRA</a></li>
                                @else
                                    <ul>
                                        <li>Добавил: {{ $jiro->jiro_uname }}</li>
                                        <li>Ссылка: <a href="{{ $jiro->jiro_url }}" target="_blank">просмотреть</a></li>
                                        <li>Дата создания в JIRA: {{ Carbon\Carbon::parse($jiro->jiro_created)->format('Y-m-d') }}</li>
                                    </ul>
                                @endif
                            </td>
                        </tr>
                    @endif
                </table>
            </div>

            <div class="col-sm-4">
                <div class="user_data">
                    <b>Данные об авторе заявки:</b>
                    <ul class="list-unstyled">
                        <?php
                        $data_user = explode("~~", $data_user);
                        foreach ($data_user as $key=>$val){
                            if ( ($key==0 || $key==1 || $key==5 || $key==6 || $key==7)){ continue; }
                            if ($key == 4){
                                //Электронный адрес
                                $email_author = trim( str_replace('E-mail:','',$val) );
                                echo '<li><strong>E-mail:</strong> <a href="mailto:'.$email_author.'">'.$email_author.'</a></li>';
                            }
                            elseif($key == 7){
                                //Статус
                                $status_author = trim( str_replace('Статус:','',$val) );
                                $status_author = \DB::table('users_status')->where('id', (int)$status_author)->first()->name;
                                echo '<li><strong>Статус:</strong> '.$status_author.'</li>';
                            }
                            else {
                                $val = explode(':', $val);
                                echo '<li><strong>' . $val[0] . ':</strong> ' . $val[1] . '</li>';
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>

            <div class="col-sm-8">
                <div class="user_data @if (count($history)<1)) hidden @endif" id="clickhistory"  style="cursor:pointer; margin-bottom:20px;">
                    <b>История работы с заявкой:</b>
                    <div id="showhistory" style="display:none;">
                        <table class="table table-bordered table-striped">
                            <tr><th>Дата</th><th>Пользователь</th><th>Статус</th><th>Коментарий</th></tr>
                            <?php
                            foreach ($history as $key=>$val){
                                echo '<tr><td>'.$val['date'].'</td><td>'.$val['fio'].'('.$val['username'].')</td><td>'.$val['status'].'</td><td>'.$val['solution'].'</td></tr>';
                            }
                            ?>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                @if ( $status != 3 )
                    <a href="#" class="shwtooltip btn btn-success" data-status="complite" data-id="{{ $get_id }}">Задача решена</a>
                    <a href="#" class="shwtooltip btn btn-warning" data-status="inwork" data-id="{{ $get_id }}">Задача в работе</a>
                    <a href="#" class="shwtooltip btn btn-danger" data-status="proroguep" data-id="{{ $get_id }}">Задача отложена</a>
                @endif
            </div>
        </div>
    </div>
@endsection