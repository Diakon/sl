@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
@stop

@section('scripts')
    <script src="{{ URL::asset('/js/support/script.js') }}"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script>
        function setCookie(){
            var url_hesh = $('.nav-tabs').find('.active').find('a').attr('href');
            document.cookie = "hash_fullbase="+url_hesh;
        }
        $(document).on('click','.btn-primary', function(){
            setCookie();
        });
        function getCookie(name) {
            var cookie = " " + document.cookie;
            var search = " " + name + "=";
            var setStr = null;
            var offset = 0;
            var end = 0;
            if (cookie.length > 0) {
                offset = cookie.indexOf(search);
                if (offset != -1) {
                    offset += search.length;
                    end = cookie.indexOf(";", offset)
                    if (end == -1) {
                        end = cookie.length;
                    }
                    setStr = unescape(cookie.substring(offset, end));
                }
                return(setStr);
            }
            return false;
        }

        $(document).ready(function(){
            if ( getCookie('hash_fullbase') ){
                //alert('dddd');
                var hash = getCookie('hash_fullbase');
                $('.nav-tabs li a[href="' + hash + '"]').click();
            }
        });

    </script>

@stop

@extends('app')

@extends('nav')

@section('content')
<style>
    .table{
    font-size: 12px;
    }
    th {
    background-color: #d3d3d3;
    }
</style>


<div style="padding: 40px;">

    <a class="btn btn-primary" style="margin-bottom: 20px;" href="/edit/create">Новая заявка</a>
    <div style="clear: both;"></div>
    @include('fullbase.filter')

  <?php
    $class_active = array();
    $k = (isset($_GET['active_page']))?(int)$_GET['active_page']:0;
    $class_active[$k] = 'active';
  ?>
  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="<?=(isset($class_active[0])?$class_active[0]:'');?>"><a href="#rezerv3day" aria-controls="rezerv3day" role="tab" data-toggle="tab">Выход из резерва через 3 дня</a></li>
    <li role="presentation" class="<?=(isset($class_active[1])?$class_active[1]:'');?>"><a href="#callback30" aria-controls="callback30" role="tab" data-toggle="tab">Обратный звонок (30 минут)</a></li>
    <li role="presentation" class="<?=(isset($class_active[2])?$class_active[2]:'');?>"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Перезвонить за 14 дней</a></li>
    <li role="presentation" class="<?=(isset($class_active[3])?$class_active[3]:'');?>"><a href="#soiskateli" aria-controls="soiskateli" role="tab" data-toggle="tab">Соискатели</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    <div role="tabpanel" class="tab-pane <?=(isset($class_active[0])?$class_active[0]:'');?>" id="rezerv3day">

        <table class="table">
            <tr>
                <th><b>ID Анкеты</b></th>
                <th><b>Телефон</b></th>
                <th><b>Должность</b></th>
                <th><b>Фамилия</b></th>
                <th><b>Имя</b></th>
                <th><b>Отчество</b></th>
                <th><b>Объект</b></th>
                <th><b>Общежитие</b></th>
            </tr>
            <?php foreach($rezerv_3day as $val){ ?>
                <tr>
                    <td><?=$val->id_anketa;?></td>
                    <td><?=$val->phone_mobile;?></td>
                    <td><?=$val->position;?></td>
                    <td><?=$val->lastname;?></td>
                    <td><?=$val->firstname;?></td>
                    <td><?=$val->patronymic;?></td>
                    <td><?=$val->object;?></td>
                    <td><?=$val->domitory;?></td>
                </tr>
            <?php } ?>
        </table>


    </div>

    <div role="callback30" class="tab-pane <?=(isset($class_active[1])?$class_active[1]:'');?>" id="callback30">
                <table class="table">
                    <tr>
                        <th><b>ID Анкеты</b></th>
                        <th><b>Телефон</b></th>
                        <th><b>Должность</b></th>
                        <th><b>Фамилия</b></th>
                        <th><b>Имя</b></th>
                        <th><b>Отчество</b></th>
                        <th><b>Объект</b></th>
                        <th><b>Общежитие</b></th>
                    </tr>
                    <?php foreach($callback30 as $val){ ?>
                        <tr>
                            <td><?=$val->id_anketa;?></td>
                            <td><?=$val->phone_mobile;?></td>
                            <td><?=$val->position;?></td>
                            <td><?=$val->lastname;?></td>
                            <td><?=$val->firstname;?></td>
                            <td><?=$val->patronymic;?></td>
                            <td><?=$val->object;?></td>
                            <td><?=$val->domitory;?></td>
                        </tr>
                    <?php } ?>
                </table>
    </div>

    <div role="tabpanel" class="tab-pane <?=(isset($class_active[2])?$class_active[2]:'');?>" id="profile">

            <center>{!! $callback['table']->appends(['active_page'=>'2'])->render() !!}</center>

            <table class="table">
                <tr>
                    <th class="feeld_id"><b>ID</b></th>
                    <th class="feeld_searchsource"><b>Источник</b></th>
                    <th class="feeld_city"><b>Город</b></th>
                    <th class="feeld_created_when"><b>Заявка создана</b></th>
                    <th class="feeld_modified_when"><b>Заявка изменена</b></th>
                    <th class="feeld_customer_status"><b>Статус сотрудника</b></th>
                    <th class="feeld_gender"><b>Пол</b></th>
                    <th class="feeld_vacancy"><b>Вакансия</b></th>
                    <th class="feeld_aon"><b>Телефонный номер</b></th>
                    <th class="feeld_secondphone"><b>Телефонный номер 2</b></th>
                    <th class="feeld_email"><b>email</b></th>
                    <th class="feeld_lastname"><b>Фамилия</b></th>
                    <th class="feeld_firstname"><b>Имя</b></th>
                    <th class="feeld_otch"><b>Отчество</b></th>
                    <th class="feeld_age"><b>Возраст</b></th>
                    <th class="feeld_grazd"><b>Гражданство</b></th>
                    <th class="feeld_kogda_pridet"><b>Когда приедет (мы)</b></th>
                    <th class="feeld_arrivaltime"><b>Когда приедет (он)</b></th>
                    <th class="feeld_factor1"><b>Фактор 1</b></th>
                    <th class="feeld_factor2"><b>Фактор 2</b></th>
                    <th class="feeld_factor3"><b>Фактор 3</b></th>
                    <th class="feeld_ask4address"><b>Спросил адрес?</b></th>
                    <th class="feeld_interes_podrabotka"><b>Интерес: Подработка</b></th>
                    <th class="feeld_interes_vahta"><b>Интерес: Вахта</b></th>
                    <th class="feeld_interes_postrabota"><b>Интерес: Постоянка</b></th>
                    <th class="feeld_mos_metro_station"><b>Станция метро</b></th>
                    <th class="feeld_user_creator"><b>Кто добавил</b></th>
                    <th class="feeld_kemrabotal"><b>Кем работал</b></th>
                    <th class="feeld_comments" style="max-width:300px;"><b>Комментарий</b></th>
                </tr>
                <?php foreach($callback['table'] as $val){ ?>
                    <tr onclick="setCookie(); location.href = '/edit/update/'+$(this).data('id');" data-id="<?=$val->id;?>" style="cursor: pointer; background:<?=($status_user[$val->customer_status]['color']);?>">
                        <td class="feeld_id"><?=$val->id;?></td>
                        <td class="feeld_searchsource"><?=$val->searchsource;?></td>
                        <td class="feeld_city"><?=$val->city;?></td>
                        <td class="feeld_created_when"><?=date('d-m-Y H:i:s',strtotime($val->created_when));?></td>
                        <td class="feeld_modified_when"><?=date('d-m-Y H:i:s',strtotime($val->modified_when));?></td>
                        <td class="feeld_customer_status"><?=(isset($status_user[$val->customer_status]['name'])?$status_user[$val->customer_status]['name']:'');?></td>
                        <td class="feeld_gender"><?=($val->gender==1)?'муж.':'жен.';?></td>
                        <td class="feeld_vacancy"><?=$val->vacancy;?></td>
                        <td class="feeld_aon"><?=$val->aon;?></td>
                        <td class="feeld_secondphone"><?=$val->secondphone;?></td>
                        <td class="feeld_email"><?=$val->email;?></td>
                        <td class="feeld_lastname"><?=$val->lastname;?></td>
                        <td class="feeld_firstname"><?=$val->firstname;?></td>
                        <td class="feeld_otch"><?=$val->otch;?></td>
                        <td class="feeld_age"><?=$val->age;?></td>
                        <td class="feeld_grazd"><?=(isset($nation[$val->grazd])?$nation[$val->grazd]:$val->grazd);?></td>
                        <td class="feeld_kogda_pridet"><?=($val->kogda_pridet!='0000-00-00')?(date('d-m-Y',strtotime($val->kogda_pridet))):'';?></td>
                        <td class="feeld_arrivaltime"><?=$val->arrivaltime;?></td>
                        <td class="feeld_factor1"><?=$val->factor1;?></td>
                        <td class="feeld_factor2"><?=$val->factor2;?></td>
                        <td class="feeld_factor3"><?=$val->factor3;?></td>
                        <td class="feeld_ask4address"><?=$val->ask4address;?></td>
                        <td class="feeld_interes_podrabotka"><?=$val->interes_podrabotka;?></td>
                        <td class="feeld_interes_vahta"><?=$val->interes_vahta;?></td>
                        <td class="feeld_interes_postrabota"><?=$val->interes_postrabota;?></td>
                        <td class="feeld_mos_metro_station"><?=$val->mos_metro_station;?></td>
                        <td class="feeld_user_creator"><?=$val->user_creator;?></td>
                        <td class="feeld_kemrabotal"><?=$val->kemrabotal;?></td>
                        <td class="feeld_comments" style="max-width:300px;"><?=$val->comments;?></td>
                    </tr>
                <?php } ?>
            </table>

            <center>{!! $callback['table']->appends(['active_page'=>'2'])->render() !!}</center>

    </div>


    <div role="soiskateli" class="tab-pane <?=(isset($class_active[3])?$class_active[3]:'');?>" id="soiskateli">

        <center>{!! $soiskateli['table']->appends(['active_page'=>'3'])->render() !!}</center>
           <table class="table">
                <tr>
                    <th class="feeld_id"><b>ID</b></th>
                    <th class="feeld_searchsource"><b>Источник</b></th>
                    <th class="feeld_city"><b>Город</b></th>
                    <th class="feeld_created_when"><b>Заявка создана</b></th>
                    <th class="feeld_modified_when"><b>Заявка изменена</b></th>
                    <th class="feeld_customer_status"><b>Статус сотрудника</b></th>
                    <th class="feeld_gender"><b>Пол</b></th>
                    <th class="feeld_vacancy"><b>Вакансия</b></th>
                    <th class="feeld_aon"><b>Телефонный номер</b></th>
                    <th class="feeld_secondphone"><b>Телефонный номер 2</b></th>
                    <th class="feeld_email"><b>email</b></th>
                    <th class="feeld_lastname"><b>Фамилия</b></th>
                    <th class="feeld_firstname"><b>Имя</b></th>
                    <th class="feeld_otch"><b>Отчество</b></th>
                    <th class="feeld_age"><b>Возраст</b></th>
                    <th class="feeld_grazd"><b>Гражданство</b></th>
                    <th class="feeld_kogda_pridet"><b>Когда приедет (мы)</b></th>
                    <th class="feeld_arrivaltime"><b>Когда приедет (он)</b></th>
                    <th class="feeld_factor1"><b>Фактор 1</b></th>
                    <th class="feeld_factor2"><b>Фактор 2</b></th>
                    <th class="feeld_factor3"><b>Фактор 3</b></th>
                    <th class="feeld_ask4address"><b>Спросил адрес?</b></th>
                    <th class="feeld_interes_podrabotka"><b>Интерес: Подработка</b></th>
                    <th class="feeld_interes_vahta"><b>Интерес: Вахта</b></th>
                    <th class="feeld_interes_postrabota"><b>Интерес: Постоянка</b></th>
                    <th class="feeld_mos_metro_station"><b>Станция метро</b></th>
                    <th class="feeld_user_creator"><b>Кто добавил</b></th>
                    <th class="feeld_kemrabotal"><b>Кем работал</b></th>
                    <th class="feeld_comments" style="max-width:300px;"><b>Комментарий</b></th>
                </tr>
                <?php foreach($soiskateli['table'] as $val){ ?>
                    <tr onclick="setCookie(); location.href = '/edit/update/'+$(this).data('id');" data-id="<?=$val->id;?>" style="cursor: pointer; background:<?=(isset($status_user[$val->customer_status]['color'])?$status_user[$val->customer_status]['color']:'');?>">
                        <td class="feeld_id"><?=$val->id;?></td>
                        <td class="feeld_searchsource"><?=$val->searchsource;?></td>
                        <td class="feeld_city"><?=$val->city;?></td>
                        <td class="feeld_created_when"><?=date('d-m-Y H:i:s',strtotime($val->created_when));?></td>
                        <td class="feeld_modified_when"><?=date('d-m-Y H:i:s',strtotime($val->modified_when));?></td>
                        <td class="feeld_customer_status"><?=(isset($status_user[$val->customer_status]['name'])?$status_user[$val->customer_status]['name']:'');?></td>
                        <td class="feeld_gender"><?=($val->gender==1)?'муж.':'жен.';?></td>
                        <td class="feeld_vacancy"><?=$val->vacancy;?></td>
                        <td class="feeld_aon"><?=$val->aon;?></td>
                        <td class="feeld_secondphone"><?=$val->secondphone;?></td>
                        <td class="feeld_email"><?=$val->email;?></td>
                        <td class="feeld_lastname"><?=$val->lastname;?></td>
                        <td class="feeld_firstname"><?=$val->firstname;?></td>
                        <td class="feeld_otch"><?=$val->otch;?></td>
                        <td class="feeld_age"><?=$val->age;?></td>
                        <td class="feeld_grazd"><?=(isset($nation[$val->grazd])?$nation[$val->grazd]:$val->grazd);?></td>
                        <td class="feeld_kogda_pridet"><?=($val->kogda_pridet!='0000-00-00')?(date('d-m-Y',strtotime($val->kogda_pridet))):'';?></td>
                        <td class="feeld_arrivaltime"><?=$val->arrivaltime;?></td>
                        <td class="feeld_factor1"><?=$val->factor1;?></td>
                        <td class="feeld_factor2"><?=$val->factor2;?></td>
                        <td class="feeld_factor3"><?=$val->factor3;?></td>
                        <td class="feeld_ask4address"><?=$val->ask4address;?></td>
                        <td class="feeld_interes_podrabotka"><?=$val->interes_podrabotka;?></td>
                        <td class="feeld_interes_vahta"><?=$val->interes_vahta;?></td>
                        <td class="feeld_interes_postrabota"><?=$val->interes_postrabota;?></td>
                        <td class="feeld_mos_metro_station"><?=$val->mos_metro_station;?></td>
                        <td class="feeld_user_creator"><?=$val->user_creator;?></td>
                        <td class="feeld_kemrabotal"><?=$val->kemrabotal;?></td>
                        <td class="feeld_comments" style="max-width:300px;"><?=$val->comments;?></td>
                    </tr>
                <?php } ?>
            </table>
        <center>{!! $soiskateli['table']->render() !!}</center>
    </div>

  </div>

</div>




@endsection
