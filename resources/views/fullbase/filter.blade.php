<a href="#" onclick="$('#filterBlock').slideToggle(); return false;" class="btn btn-info" style="margin-bottom: 10px;">Фильтр</a>
<div id="filterBlock" style="display: none; background: #c0c0c0; border-radius: 5px; padding: 10px; margin-bottom: 10px;">

    {!! Form::open(array('url' => '/fullbase')) !!}


    {!! Form::checkbox('s1', 1, ((isset($param_filter['s1']))?true:false) ) !!}<p style="margin-top:-16px; margin-left:17px;">Все</p>
    {!! Form::checkbox('s2', 1, ((isset($param_filter['s2']))?true:false) ) !!}<p style="margin-top:-16px; margin-left:17px;">Не трогали</p>
    {!! Form::checkbox('s3', 1, ((isset($param_filter['s3']))?true:false) ) !!}<p style="margin-top:-16px; margin-left:17px;">Не придет</p>
    {!! Form::checkbox('s5', 1, ((isset($param_filter['s5']))?true:false) ) !!}<p style="margin-top:-16px; margin-left:17px;">Перезвонить</p>
    {!! Form::checkbox('s6', 1, ((isset($param_filter['s6']))?true:false) ) !!}<p style="margin-top:-16px; margin-left:17px;">Думает </p>

    {!! Form::input('text', 'search_lastname', ((isset($param_filter['search_lastname']))?$param_filter['search_lastname']:null), ['placeholder'=>'Фамилия']) !!}
    {!! Form::input('text', 'search_phone', ((isset($param_filter['search_phone']))?$param_filter['search_phone']:null), ['placeholder'=>'Телефон']) !!}
    {!! Form::input('text', 'search_vacancy', ((isset($param_filter['search_vacancy']))?$param_filter['search_vacancy']:null), ['placeholder'=>'Вакансия']) !!}
    {!! Form::input('text', 's_pr_from', ((isset($param_filter['s_pr_from']) && !empty($param_filter['s_pr_from']))?(date('Y-m-d',strtotime($param_filter['s_pr_from']))):''), ['class'=>'datepicker', 'placeholder'=>'Придет с']) !!}
    {!! Form::input('text', 's_pr_to', ((isset($param_filter['s_pr_to']) && !empty($param_filter['s_pr_to']))?(date('Y-m-d',strtotime($param_filter['s_pr_to']))):''), ['class'=>'datepicker', 'placeholder'=>'Придет по']) !!}

    <BR><button type="submit" onclick="setCookie();" >Готово</button>

    {!! Form::close() !!}

</div>

<a href="#" onclick="$('#feeldsBlock').slideToggle(); return false;" class="btn btn-info" style="margin-bottom: 10px;">Поля</a>
<div id="feeldsBlock" style="display: none; background: #c0c0c0; border-radius: 5px; padding: 10px; margin-bottom: 10px;">

    {!! Form::open(array('url' => '/fullbase/feelds')) !!}
    <?php $styleList = array(); ?>
    <?php foreach ($feelds as $key=>$val){ ?>
    <?php if (!isset($val['selected'])){ $styleList[] = '.feeld_'.$key; } ?>
    {!! Form::checkbox($key, 1, ((isset($val['selected']))?true:false) ) !!}<p style="margin-top:-16px; margin-left:17px;"><?=$val['name'];?></p>
    <?php } ?>
    <BR><button type="submit" onclick="setCookie();" >Применить</button>

    {!! Form::close() !!}

            <!--Скрываем не нужные поля (сделаем на CSS - нечего мучить js)-->

    <?php
    if (!empty($styleList)){
        $styleList = implode(",", $styleList);
        echo '<style> '.$styleList.'{display:none;} </style>';
    }
    ?>

</div>
