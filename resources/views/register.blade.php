@extends('app')

@section('content')
<style>
.req_field {
color:red;
margin-left:3px;
}
</style>
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">{{isset($user->id)?'Изменение данных пользователя':'Добавление пользователя'}}</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Ошибка!</strong> Возникли проблемы с заполнением формы регистрации.<br><br>
							<ul>
								@foreach ($errors as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST"">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Логин <span class="req_field">*</span></label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="username" value="{{ isset($user) ? $user->username : old('username') }}">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">ФИО <span class="req_field">*</span></label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="fio" value="{{ isset($user) ? $user->fio : old('fio') }}">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Телефон</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="phone" value="{{ isset($user) ? $user->phone : old('phone') }}">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Внутренний номер</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="myextension" value="{{ isset($user) ? $user->myextension : old('myextension') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ isset($user) ? $user->email : old('email') }}">
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Активен</label>
							<div class="col-md-6">
								<select class="form-control" name="enable">
									@foreach ($lists['enable'] as $k=>$val)
										<option value="{{$k}}" {{(isset($user) && $user->enable==$k) ? 'selected' :''}}>{{$val}}</option>
									@endforeach
								</select>								
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label class="col-md-4 control-label">Статус</label>
							<div class="col-md-6">
								<select class="form-control" name="status">
									@foreach ($lists['users_status'] as $k=>$val)
										<option value="{{$k}}" {{(isset($user) && $user->status==$k) ? 'selected' :''}}>{{$val}}</option>
									@endforeach
								</select>
							</div>
						</div>
						
						
						<div class="form-group">
							<label class="col-md-4 control-label">Пароль @if (!isset($user->id))<span class="req_field">*</span>@endif</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password"  @if (isset($user->id)) placeholder="Оставьте пустым, если не хотите менять пароль" @endif >
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Подтверждение пароля @if (!isset($user->id))<span class="req_field">*</span>@endif</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Сохранить
								</button>
							</div>
						</div>
						<input type="hidden" name="id" value="{{ isset($user->id) ? $user->id:''}}">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
