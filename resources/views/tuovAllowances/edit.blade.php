@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
@stop

@extends('app')

@extends('nav')

@section('content')
<div class="container">

    <input type="hidden" class="bigdrop" id="e6" style="width:600px" value="3620194" />

    @include('common.errors')

    {!! Form::model($tuovAllowance, ['route' => ['tuovAllowances.update', $tuovAllowance->id], 'method' => 'patch']) !!}

        @include('tuovAllowances.fields')

    {!! Form::close() !!}
</div>
@endsection
