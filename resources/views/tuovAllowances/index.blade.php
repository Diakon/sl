@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ URL::asset('/js/support/script.js') }}"></script>
@stop



@extends('app')

@extends('nav')

@section('content')
    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">{{ $app_title = 'Надбавки линейным бригадирам при расчете з/п' }}</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('tuovAllowances.create') !!}">Создать новую запись</a>
        </div>

        <div class="row">
            @include('tuovAllowances.table')
        </div>

        @include('common.paginate', ['records' => $tuovAllowances])


    </div>
@endsection