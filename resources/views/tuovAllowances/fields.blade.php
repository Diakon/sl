<div class="col-sm-offset-3 col-sm-6 col-lg-offset-2 col-lg-8">
    <!--- Id Anketa Field --->
    <h4>Для всех</h4>

    <div class="form-group col-lg-6">
        {!! Form::label('id_anketa', 'Анкета:') !!}
        {!!
        Form::select('id_anketa',(isset($tuovAllowance)?[$tuovAllowance->anketa->id_anketa=>$tuovAllowance->anketa->fio]:[]),
        (isset($tuovAllowance)?$tuovAllowance->anketa->id_anketa:null),
        ['class' => 'form-control']) !!}
    </div>
    <!--- Allowance Field --->
    <div class="form-group col-lg-6">
        {!! Form::label('allowance', 'Надбавка (руб/час):') !!}
        {!! Form::number('allowance', null, ['class' => 'form-control', 'step'=>"0.01"]) !!}
    </div>
    <div style="clear: both;"></div>
    <!--- From Field --->
    <div class="form-group col-lg-6">
        {!! Form::label('from', 'С:') !!}
        {!! Form::text('from', null, ['class' => 'form-control datepicker']) !!}
    </div>
    <!--- To Field --->
    <div class="form-group col-lg-6">
        {!! Form::label('to', 'По:') !!}
        {!! Form::text('to', null, ['class' => 'form-control datepicker']) !!}
    </div>
    <div style="clear: both;"></div>


        <hr/>
        <h5>Дополнительно указывается для сделок</h5>
        <div class="form-group col-lg-6">
            {!! Form::label('to', 'Сделка:') !!}
            {!! Form::select('deal_id', $activeDeals, ((isset($_GET['deal_id']) &&
            (int)$_GET['deal_id']>0)?$_GET['deal_id']:null), ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-lg-6">
            {!! Form::label('to', 'Подразделение:') !!}
            {!! Form::select('subdivision_id', $activeSubdivisions, ((isset($_GET['subdivision']) &&
            (int)$_GET['subdivision']>0)?$_GET['subdivision']:null), ['class' => 'form-control']) !!}
        </div>
        <div style="clear: both;"></div>
    <!--- Submit Field --->
    <div class="form-group col-sm-12">
        {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
        <a href="/tuovAllowances" class="btn btn-default">Отмена</a>
    </div>
</div>

