@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ URL::asset('/js/support/script.js') }}"></script>
@stop

@extends('app')

@extends('nav')

@section('content')
<div class="container">
    <div class="row">
        @include('common.errors')

        {!! Form::open(['route' => 'tuovAllowances.store']) !!}

            @include('tuovAllowances.fields')

        {!! Form::close() !!}
    </div>
</div>
@endsection