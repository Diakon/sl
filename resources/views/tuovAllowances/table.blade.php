<div class="table-responsive">
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th>№</th>
			<th>Анкета</th>
			<th>Надбавка</th>
			<th>С</th>
			<th>По</th>
            <th width="50px">Действия</th>
        </tr>
        <tr>
            <td></td>
            <td>
                {!! Form::model($request, array('url' => '/tuovAllowances', 'class' => 'form-inline')) !!}
                    {!! Form::text('id_anketa', null , ['class' => 'form-control', 'placeholder'=>'Номер анкеты']) !!}
                {!! Form::close() !!}
            </td>
            <td></td>
            <td>
                {!! Form::model($request, array('url' => '/tuovAllowances', 'class' => 'form-inline')) !!}
                    {!! Form::text('from', null , ['class' => 'form-control datepicker', 'placeholder'=>'C']) !!}
                {!! Form::close() !!}
            </td>
            <td>
                {!! Form::model($request, array('url' => '/tuovAllowances', 'class' => 'form-inline')) !!}
                    {!! Form::text('to', null , ['class' => 'form-control datepicker', 'placeholder'=>'По']) !!}
                {!! Form::close() !!}
            </td>
            <td></td>
        </tr>
    </thead>
    <tbody>
    @if($tuovAllowances->isEmpty())
        <tr class="info">
            <td colspan="6" class="text-center">Записей не обнаружено.</td>
        </tr>
    @else
        @foreach($tuovAllowances as $tuovAllowance)
            <tr>
                <td>{!! $tuovAllowance->id !!}</td>
                <td>
                    <a href='http://vls.secondlab.ru/op/anketa.php?select=update_form&id={!! $tuovAllowance->id_anketa !!}' target="_blank">
                        <samp>{{ $tuovAllowance->anketa->firstname }} {{ $tuovAllowance->anketa->lastname }}</samp> <code>{{ $tuovAllowance->id_anketa }}</code>
                    </a>
                </td>
                <td>{{ $tuovAllowance->allowance }}</td>
                <td>{{ date('d-m-Y', strtotime($tuovAllowance->from)) }}</td>
                <td>{{ date('d-m-Y', strtotime($tuovAllowance->to)) }}</td>
                <td>
                    <a href="{!! route('tuovAllowances.edit', [$tuovAllowance->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="{!! route('tuovAllowances.delete', [$tuovAllowance->id]) !!}" onclick="return confirm('Are you sure wants to delete this TuovAllowance?')"><i class="glyphicon glyphicon-remove"></i></a>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
</div>