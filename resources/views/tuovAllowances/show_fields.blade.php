<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $tuovAllowance->id !!}</p>
</div>

<!-- Id Anketa Field -->
<div class="form-group">
    {!! Form::label('id_anketa', 'Id Anketa:') !!}
    <p>{!! $tuovAllowance->id_anketa !!}</p>
</div>

<!-- Allowance Field -->
<div class="form-group">
    {!! Form::label('allowance', 'Allowance:') !!}
    <p>{!! $tuovAllowance->allowance !!}</p>
</div>

<!-- From Field -->
<div class="form-group">
    {!! Form::label('from', 'From:') !!}
    <p>{!! $tuovAllowance->from !!}</p>
</div>

<!-- To Field -->
<div class="form-group">
    {!! Form::label('to', 'To:') !!}
    <p>{!! $tuovAllowance->to !!}</p>
</div>

