@extends('app')

@extends('nav')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					Добрый день, {{ Auth()->user()->fio }}!
					<br><br>
					<a href="/auth/logout" class="btn btn-default">
						<span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
						Выйти
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
