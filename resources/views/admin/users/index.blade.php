@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">{{ $app_title = 'Пользователи' }}</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('admin.users.create') !!}">Добавить пользователя</a>
        </div>

        <div class="row">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                    <th>Id</th>
                    <th>ФИО</th>
                    <th>Логин</th>
                    <th>Активность</th>
                    <th>Авторизация</th>
                    <th>Последнее действие</th>
                    <th>Телефоны</th>
                    <th></th>
                    </tr>
                    <tr class="filters">
                        <th></th>
                        <th>
                            {!! Form::model($request, ['route' => 'admin.users.index', 'class' => 'form-inline']) !!}
                                {!! Form::text('fio', null , ['class' =>'form-control', 'placeholder'=>'ФИО']) !!}
                            {!! Form::close() !!}
                        </th>
                        <th>
                            {!! Form::model($request, ['route' => 'admin.users.index', 'class' => 'form-inline']) !!}
                                {!! Form::text('username', null , ['class' =>'form-control', 'placeholder'=>'Логин']) !!}
                            {!! Form::close() !!}
                        </th>
                        <th>
                            {!! Form::model($request, ['route' => 'admin.users.index', 'class' => 'form-inline']) !!}
                                {!! Form::select('enable', ['Отключен', 'Активен'], null , ['class' =>'form-control', 'placeholder'=>'Активность', 'onchange'=>'this.form.submit()']) !!}
                            {!! Form::close() !!}
                        </th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($users as $user)
                    <tr>
                        <td>{{ $user->userid }}</td>
                        <td>{{ $user->fio }}</td>
                        <td>{{ $user->username }}</td>
                        <td>{{ $user->enable_label }}</td>
                        <td>{{ $user->login_date }}</td>
                        <td>{{ $user->last_date }}</td>
                        <td>{{ $user->phone }}</td>
                        <td>
                            <a href="{!! route('admin.users.edit', [$user->userid]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                            @if( Auth::user()->userid != $user->userid )
                                <a href="{!! route('admin.users.delete', [$user->userid]) !!}" onclick="return confirm('Are you sure wants to delete this user?')"><i class="glyphicon glyphicon-remove"></i></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        @include('common.paginate', ['records' => $users])

    </div>
@endsection