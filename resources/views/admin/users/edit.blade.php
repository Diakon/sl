@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        <div class="row">
            <h1>{{ $app_title = 'Редактировать пользователя' }}</h1>
            @include('common.errors')
            @include('flash::message')

            {!! Form::model($user, ['route' => ['admin.users.update', $user->userid], 'method' => 'patch']) !!}

                @include('admin.users.fields')

            {!! Form::close() !!}
        </div>

    </div>
@endsection