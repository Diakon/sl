@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        <div class="row">
            <h1>{{ $app_title = 'Создать нового пользователя' }}</h1>

            @include('common.errors')

            @include('flash::message')

            {!! Form::open(['route' => 'admin.users.store']) !!}

                @include('admin.users.fields')

            {!! Form::close() !!}
        </div>

    </div>
@endsection