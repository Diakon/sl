<div class="form-group">
    {!! Form::label('username', 'Логин:') !!}
    @if(isset($user->userid))
        {!! Form::text('username', null, ['class' => 'form-control', 'readonly']) !!}
    @else
        {!! Form::text('username', null, ['class' => 'form-control']) !!}
    @endif

</div>
<div class="form-group">
    {!! Form::label('fio', 'Фио:') !!}
    {!! Form::text('fio', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('phone', 'Телефон:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('email', 'E-mail:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('enable', 'Активен:') !!}
    {!! Form::select('enable', [0 => 'Нет', 1 => 'Да'], null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('password', 'Пароль:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('password_confirmation', 'Повторите пароль еще раз:') !!}
    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <ul class="list-unstyled" style="-webkit-column-count:3;-moz-column-count:3;column-count:3;">
    @foreach ($roles->all() as $role)
    <li>
        <label>
            {!! Form::checkbox('role[]', $role->id, (isset($user->userid)?$user->hasRole($role->name):null)) !!}
            {{ $role->display_name }}
        </label>
    </li>
    @endforeach
    </ul>
</div>
<div class="form-group">
    {!! Form::Submit((isset($user->userid)?'Изменить':'Сохранить'), ['class' => 'btn btn-primary']) !!}
    {!! Html::linkRoute('admin.users.index','Отмена', null, ['class' => 'btn btn-default']) !!}
</div>