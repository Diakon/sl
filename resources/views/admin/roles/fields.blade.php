<div class="form-group @if(isset($role->id)) hidden @endif ">
    {!! Form::label('name', 'Название (системное):') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('display_name', 'Отображаемое имя:') !!}
    {!! Form::text('display_name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    {!! Form::label('description', 'Описание (не обязательно):') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group">
    <ul class="list-unstyled" style="-webkit-column-count:3;-moz-column-count:3;column-count:3;">
    @foreach ($permissions->all() as $permission)
        <li>
            <label title="{{ $permission->description }}">
                {!! Form::checkbox('permissions[]', $permission->id, (isset($role->id)?$role->hasPermission($permission->name):null)) !!}
                {{ $permission->display_name }}
            </label>
        </li>
    @endforeach
    </ul>
</div>


<div class="form-group">
    {!! Form::Submit((isset($role->id)?'Изменить':'Сохранить'), ['class' => 'btn btn-primary']) !!}
    {!! Html::linkRoute('admin.roles.index','Отмена', null, ['class' => 'btn btn-default']) !!}
</div>