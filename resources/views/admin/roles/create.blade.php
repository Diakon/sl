@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        <div class="row">
            <h1>{{ $app_title = 'Создать новую роль' }}</h1>

            @include('common.errors')

            @include('flash::message')

            {!! Form::open(['route' => 'admin.roles.store']) !!}

                @include('admin.roles.fields')

            {!! Form::close() !!}
        </div>
    </div>
@endsection
