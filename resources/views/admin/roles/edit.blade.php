@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        <div class="row">
            <h1>{{ $app_title = 'Редактировать роль' }}</h1>
            @include('common.errors')
            @include('flash::message')

            {!! Form::model($role, ['route' => ['admin.roles.update', $role->id], 'method' => 'patch']) !!}

                @include('admin.roles.fields')

            {!! Form::close() !!}
        </div>

    </div>
@endsection