@extends('app')

@extends('nav')

@section('content')
    <div class="container">

        <div class="row">
            <h1 class="pull-left">{{ $app_title = 'Роли и права' }}</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('admin.roles.create') !!}">Добавить роль</a>
            <table class="table table-striped table-bordered">
                <thead>
                <th>Название (системное)</th>
                <th>Отображаемое имя</th>
                <th>Описание</th>
                <th>Список разрешений</th>
                <th></th>
                </thead>
                <tbody>
                @foreach($roles->get() as $role)
                    <tr>
                        <td>{{ $role->name }}</td>
                        <td>{{ $role->display_name }}</td>
                        <td>{{ $role->description }}</td>
                        <td>
                            <ul class="list-unstyled">
                                @foreach($role->perms as $perm)
                                    <li>{{ $perm->display_name }}</li>
                                @endforeach
                            </ul>
                        </td>
                        <td>
                            <a href="{!! route('admin.roles.edit', [$role->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                        </td>

                    </tr>

                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection