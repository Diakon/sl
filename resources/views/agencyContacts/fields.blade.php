<div style="clear: both;"></div>
<!--- Card Staffing Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('card_staffing_id', 'Организация контрагента') !!}
	<?php /* {!! Form::number('card_staffing_id', null, ['class' => 'form-control']) !!} */ ?>
	{!! Form::select('card_staffing_id', $cardContact['card_staffing'], (isset($cardContact->card_staffing_id)?$cardContact->card_staffing_id:null), ['class' => 'form-control']) !!}
</div>
<div style="clear: both;"></div>
<!--- Fio Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('fio', 'ФИО:') !!}
	{!! Form::text('fio', null, ['class' => 'form-control']) !!}
</div>
<div style="clear: both;"></div>
<!--- User Login Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('user_login', 'Пользователь:') !!}
	{!! Form::text('user_login', null, ['class' => 'form-control']) !!}
</div>
<div style="clear: both;"></div>
<!--- Position Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('position', 'Должность:') !!}
	{!! Form::text('position', null, ['class' => 'form-control']) !!}
</div>
<div style="clear: both;"></div>
<!--- Phone Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('phone', 'Телефон:') !!}
	{!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>
<div style="clear: both;"></div>
<!--- Phone Mobile Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('phone_mobile', 'Мобильный телефон:') !!}
	{!! Form::text('phone_mobile', null, ['class' => 'form-control']) !!}
</div>
<div style="clear: both;"></div>
<!--- Email Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('email', 'Email:') !!}
	{!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>
<div style="clear: both;"></div>

<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('agencyContacts.index') }}" class="btn btn-default" >Отмена</a>
</div>
