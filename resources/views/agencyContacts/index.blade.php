@section('head')
@stop

@section('scripts')
<script src="{{ URL::asset('/js/support/script.js') }}"></script>
@stop

@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Список контактных лиц</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('agencyContacts.create') !!}">Добавить нового</a>
        </div>

        <div class="row">
            @if($cardContacts->isEmpty())
                <div class="well text-center">Контактные лица не найдены</div>
            @else
                @include('agencyContacts.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $cardContacts])


    </div>
@endsection