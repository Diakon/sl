<table class="table table-striped table-bordered">
    <thead>
        <th>№</th>
        <th>Организация</th>
        <th>ФИО</th>
        <th>Position</th>
        <th>Телефон</th>
        <th>Телефон (моб)</th>
        <th>Email</th>
        <th width="50px">Действия</th>
    </thead>
    <tbody>
    @foreach($cardContacts as $cardContact)
        <tr>
            <td>{!! $cardContact->id !!}</td>
			<td><?=($cardContact->card_staffing_id!=0)?(\DB::table('card_staffing')->where('id', $cardContact->card_staffing_id)->first()->official_name):('<i>не указана</i>');?></td>
			<td>{!! $cardContact->fio !!}</td>
			<td>{!! $cardContact->position !!}</td>
			<td>{!! $cardContact->phone !!}</td>
			<td>{!! $cardContact->phone_mobile !!}</td>
			<td>{!! $cardContact->email !!}</td>
            <td>
                <a href="{!! route('agencyContacts.edit', [$cardContact->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('agencyContacts.delete', [$cardContact->id]) !!}" onclick="return confirm('Вы действительно хотите удалить запись?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
