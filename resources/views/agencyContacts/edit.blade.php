@section('head')
@stop

@section('scripts')
<script src="{{ URL::asset('/js/support/script.js') }}"></script>
<script src="{{ URL::asset('/js/cardstaffings/jquery.maskedinput.js') }}"></script>
<script src="{{ URL::asset('/js/cardstaffings/script.js') }}"></script>
@stop

@extends('app')

@extends('nav')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($cardContact, ['route' => ['agencyContacts.update', $cardContact->id], 'method' => 'patch']) !!}

        @include('agencyContacts.fields')

    {!! Form::close() !!}
</div>
@endsection
