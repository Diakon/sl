<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $cardContact->id !!}</p>
</div>

<!-- Card Staffing Id Field -->
<div class="form-group">
    {!! Form::label('card_staffing_id', 'Card Staffing Id:') !!}
    <p>{!! $cardContact->card_staffing_id !!}</p>
</div>

<!-- Fio Field -->
<div class="form-group">
    {!! Form::label('fio', 'Fio:') !!}
    <p>{!! $cardContact->fio !!}</p>
</div>

<!-- User Login Field -->
<div class="form-group">
    {!! Form::label('user_login', 'User Login:') !!}
    <p>{!! $cardContact->user_login !!}</p>
</div>

<!-- Position Field -->
<div class="form-group">
    {!! Form::label('position', 'Position:') !!}
    <p>{!! $cardContact->position !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $cardContact->phone !!}</p>
</div>

<!-- Phone Mobile Field -->
<div class="form-group">
    {!! Form::label('phone_mobile', 'Phone Mobile:') !!}
    <p>{!! $cardContact->phone_mobile !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $cardContact->email !!}</p>
</div>

<!-- Author Id Field -->
<div class="form-group">
    {!! Form::label('author_id', 'Author Id:') !!}
    <p>{!! $cardContact->author_id !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $cardContact->updated_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $cardContact->created_at !!}</p>
</div>

