@section('head')
@stop

@section('scripts')
<script src="{{ URL::asset('/js/support/script.js') }}"></script>
@stop

@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Список организаций</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('agencyStaffing.create') !!}">Добавить новую организацию</a>
        </div>

        <div class="row">
            @if($cardStaffings->isEmpty())
                <div class="well text-center">Список организаций пуст.</div>
            @else
                @include('agencyStaffing.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $cardStaffings])


    </div>
@endsection