@section('head')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop

@section('scripts')
<script src="{{ URL::asset('/js/support/script.js') }}"></script>
<script src="{{ URL::asset('/js/cardstaffings/jquery.maskedinput.js') }}"></script>
<script src="{{ URL::asset('/js/cardstaffings/script.js') }}"></script>
@stop

@extends('app')

@extends('nav')

@section('content')
<div class="container" style="background: #f0f0f0; width: 100%; height: 100%; margin-top:-20px;">

    @include('common.errors')

    {!! Form::open(['route' => 'agencyStaffing.store']) !!}

        @include('agencyStaffing.fields')

    {!! Form::close() !!}
</div>
@endsection
