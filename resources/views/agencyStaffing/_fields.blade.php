<!--- Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('id', 'Id:') !!}
	{!! Form::number('id', null, ['class' => 'form-control']) !!}
</div>

<!--- Balance Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('balance', 'Balance:') !!}
	{!! Form::number('balance', null, ['class' => 'form-control']) !!}
</div>

<!--- Official Name Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('official_name', 'Official Name:') !!}
	{!! Form::text('official_name', null, ['class' => 'form-control']) !!}
</div>

<!--- Provisional Name Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('provisional_name', 'Provisional Name:') !!}
	{!! Form::text('provisional_name', null, ['class' => 'form-control']) !!}
</div>

<!--- Alternative  Name Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('alternative _name', 'Alternative  Name:') !!}
	{!! Form::text('alternative _name', null, ['class' => 'form-control']) !!}
</div>

<!--- Address Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('address', 'Address:') !!}
	{!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!--- Region Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('region', 'Region:') !!}
	{!! Form::number('region', null, ['class' => 'form-control']) !!}
</div>

<!--- City Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('city', 'City:') !!}
	{!! Form::number('city', null, ['class' => 'form-control']) !!}
</div>

<!--- Phone Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('phone', 'Phone:') !!}
	{!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<!--- Fax Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('fax', 'Fax:') !!}
	{!! Form::text('fax', null, ['class' => 'form-control']) !!}
</div>

<!--- Email Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('email', 'Email:') !!}
	{!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!--- Web Site Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('web_site', 'Web Site:') !!}
	{!! Form::text('web_site', null, ['class' => 'form-control']) !!}
</div>

<!--- Comments Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('comments', 'Comments:') !!}
	{!! Form::textarea('comments', null, ['class' => 'form-control']) !!}
</div>

<!--- Author Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('author_id', 'Author Id:') !!}
	{!! Form::number('author_id', null, ['class' => 'form-control']) !!}
</div>

<!--- Updated At Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('updated_at', 'Updated At:') !!}
	{!! Form::date('updated_at', null, ['class' => 'form-control']) !!}
</div>

<!--- Created At Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('created_at', 'Created At:') !!}
	{!! Form::date('created_at', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
