<style>
    .alert{
        margin-top: 20px !important;
    }
    .feeld-margin {
        margin-left: 20px;
        float: left;
    }
    .contact_table {
        margin-left: 20px;
        width: 1140px;
        border-radius: 5px;;
        border: 2px solid #828790;
        background: #ffffff;
    }
    .contact_table th {
        background: #fcfcfc;
        border: 1px solid #e9e9e9;
        padding: 5px;
    }
    .contact_table td {
        padding: 10px;
    }
</style>
<div style="margin-top:20px;">
    <div class="top_bar">
        <table>
            <tr>
                <td>
                    <button type="submit" class="btn btn-primary"><img src="/images/save_btn.png" alt="Сохранить и закрыть"  style="vertical-align: middle">Cохранить и закрыть</button>
                </td>
                @if (isset($cardStaffing->id))
                <td>
                    <div style="margin-left: 10px;">
                        <a href="{{ route('agencyContacts.create') }}" target="_blank" class="btn btn-default"><img src="/images/card.png" alt="Новое контактное лицо"  style="vertical-align: middle"> Новое контактное лицо</a>
                    </div>
                </td>
                @endif
                <td>
                    <div style="margin-left: 10px;">
                        <a href="{{ route('agencyStaffing.index') }}" class="btn btn-default"><img src="/images/list.png" alt="К списку"  style="vertical-align: middle"> К списку</a>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <HR>
</div>


<table>
    <tr>
        <td>
            <div class="feeld-margin">
            {!! Form::label('balance', 'Баланс:') !!}
        	{!! Form::number('balance', null, ['class' => 'form-control', 'style'=>'width:140px;']) !!}
        	</div>
            <div class="feeld-margin">
            {!! Form::label('provisional_name', 'Условное название:') !!}
            {!! Form::text('provisional_name', null, ['class' => 'form-control', 'style'=>'width:250px;']) !!}
            </div>
            <div class="feeld-margin">
            {!! Form::label('official_name', 'Официальное наименование:') !!}
            {!! Form::text('official_name', null, ['class' => 'form-control', 'style'=>'width:450px;']) !!}
            </div>
            <div class="feeld-margin">
            {!! Form::label('alternative _name', 'Альтернативное название:') !!}
            {!! Form::text('alternative _name', null, ['class' => 'form-control', 'style'=>'width:250px;']) !!}
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="feeld-margin">
                {!! Form::label('address', 'Адрес:') !!}
                {!! Form::text('address', null, ['class' => 'form-control',  'style'=>'width:910px;']) !!}
            </div>
            <div class="feeld-margin">
                {!! Form::label('region', 'Регион:') !!}
                {!! Form::select('region', $cardStaffing['region_list'], null, ['class' => 'form-control']) !!}
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <div class="feeld-margin">
                <img style="float:left; padding:10px; margin-top:-8px; margin-left:-10px;" src="/images/phone.png" />{!! Form::label('phone', 'Телефон:') !!}
                {!! Form::text('phone', null, ['class' => 'form-control', 'style'=>'width:300px;']) !!}
            </div>
            <div class="feeld-margin">
                <img style="float:left; padding:10px; margin-top:-8px; margin-left:-10px;" src="/images/fax.png" />{!! Form::label('fax', 'Факс:') !!}
                {!! Form::text('fax', null, ['class' => 'form-control', 'style'=>'width:240px;']) !!}
            </div>
            <div class="feeld-margin">
                <img style="float:left; padding:10px; margin-top:-8px; margin-left:-10px;" src="/images/mail.png" />{!! Form::label('email', 'Email:') !!}
                {!! Form::email('email', null, ['class' => 'form-control', 'style'=>'width:240px;']) !!}
            </div>
            <div class="feeld-margin">
                <img style="float:left; padding:10px; margin-top:-8px; margin-left:-10px;" src="/images/iexplorer.png" />{!! Form::label('web_site', 'Web-сайт:') !!}
            	{!! Form::text('web_site', null, ['class' => 'form-control', 'style'=>'width:300px;']) !!}
            </div>
        </td>
    </tr>
</table>

<span style="margin-left: 20px;">Контактные лица:</span>
<div id="contacTableList">
<table class="contact_table">
    <thead>
        <tr>
            <th>ФИО</th>
            <th>Должность</th>
            <th>Телефоны</th>
            <th>Пользователь</th>
        </tr>
    </thead>
    <tbody>
    @if (isset($cardStaffing->id))
        @foreach($cardStaffing['card_staffing'] as $data)
            <tr>
                <td>
                    <a href="{{ route('agencyContacts.edit', $data->id) }}" target="_blank">
                        <img style="vertical-align: middle" alt="Новое контактное лицо" src="/images/card.png">{{ $data->fio }}
                    </a>
                </td>
                <td>{{ $data->position }}</td>
                <td>{{ $data->phone }}{{ ((!empty($data->phone_mobile))?(', '.$data->phone_mobile.' (м)'):('')) }}</td>
                <td>{{ $data->user_login }}</td>
            </tr>
        @endforeach
    @else
        <tr>
            <td colspan="4">Сохраните организацию для того что бы иметь возможность добавить к ней контактные лица.</td>
        </tr>
    @endif
    </tbody>
</table>
</div>

<div style="margin-top: 20px; margin-left:20px; width:1140px;">
    {!! Form::label('comments', 'Доп. информация:') !!}
	{!! Form::textarea('comments', null, ['class' => 'form-control']) !!}
</div>

@if (isset($cardStaffing->id))
    <HR>
    Карточка:<BR>
    Создана: <span style="color:#006000">{{ date('d.m.Y H:i:s',strtotime($cardStaffing->created_at)) }}</span><BR>
    Пользователь: <span style="color:#006000"><?=(((int)$cardStaffing->author_id!=0)?(\DB::table('users')->where('userid', $cardStaffing->author_id)->first()->username):(""));?></span><BR>
    Изменена: <span style="color:#800000">{{ date('d.m.Y H:i:s', strtotime($cardStaffing->updated_at)) }}</span><BR>
@endif