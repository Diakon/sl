<table class="table table-striped table-bordered">
    <thead>
        <th>№</th>
        <th>Баланс</th>
        <th>Официальное название</th>
        <th>Альтернативное название</th>
        <th>Телефон</th>
        <th>Факс</th>
        <th>Email</th>
        <th width="50px">Действия</th>
    </thead>
    <tbody>
    @foreach($cardStaffings as $cardStaffing)
        <tr>
            <td>{!! $cardStaffing->id !!}</td>
			<td>{!! $cardStaffing->balance !!}</td>
			<td>{!! $cardStaffing->official_name !!}</td>
			<td>{!! $cardStaffing->alternative_name !!}</td>
			<td>{!! $cardStaffing->phone !!}</td>
			<td>{!! $cardStaffing->fax !!}</td>
			<td>{!! $cardStaffing->email !!}</td>
            <td>
                <a href="{!! route('agencyStaffing.edit', [$cardStaffing->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('agencyStaffing.delete', [$cardStaffing->id]) !!}" onclick="return confirm('Вы действительно хотите удалить запись?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
