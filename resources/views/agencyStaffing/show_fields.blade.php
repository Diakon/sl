<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $cardStaffing->id !!}</p>
</div>

<!-- Balance Field -->
<div class="form-group">
    {!! Form::label('balance', 'Balance:') !!}
    <p>{!! $cardStaffing->balance !!}</p>
</div>

<!-- Official Name Field -->
<div class="form-group">
    {!! Form::label('official_name', 'Official Name:') !!}
    <p>{!! $cardStaffing->official_name !!}</p>
</div>

<!-- Provisional Name Field -->
<div class="form-group">
    {!! Form::label('provisional_name', 'Provisional Name:') !!}
    <p>{!! $cardStaffing->provisional_name !!}</p>
</div>

<!-- Alternative  Name Field -->
<div class="form-group">
    {!! Form::label('alternative _name', 'Alternative  Name:') !!}
    <p>{!! $cardStaffing->alternative _name !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $cardStaffing->address !!}</p>
</div>

<!-- Region Field -->
<div class="form-group">
    {!! Form::label('region', 'Region:') !!}
    <p>{!! $cardStaffing->region !!}</p>
</div>

<!-- City Field -->
<div class="form-group">
    {!! Form::label('city', 'City:') !!}
    <p>{!! $cardStaffing->city !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone:') !!}
    <p>{!! $cardStaffing->phone !!}</p>
</div>

<!-- Fax Field -->
<div class="form-group">
    {!! Form::label('fax', 'Fax:') !!}
    <p>{!! $cardStaffing->fax !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'Email:') !!}
    <p>{!! $cardStaffing->email !!}</p>
</div>

<!-- Web Site Field -->
<div class="form-group">
    {!! Form::label('web_site', 'Web Site:') !!}
    <p>{!! $cardStaffing->web_site !!}</p>
</div>

<!-- Comments Field -->
<div class="form-group">
    {!! Form::label('comments', 'Comments:') !!}
    <p>{!! $cardStaffing->comments !!}</p>
</div>

<!-- Author Id Field -->
<div class="form-group">
    {!! Form::label('author_id', 'Author Id:') !!}
    <p>{!! $cardStaffing->author_id !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $cardStaffing->updated_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $cardStaffing->created_at !!}</p>
</div>

