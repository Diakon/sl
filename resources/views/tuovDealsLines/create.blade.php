@extends('app')
@extends('nav')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/jquery-ui/jquery-ui-1.10.3.custom.min.css') }}" />
@stop

@section('scripts')
    <script src="{{ URL::asset('/js/jquery-ui/jquery.ui.datepicker-ru.js') }}"></script>
    <script src="{{ URL::asset('/js/jquery-ui/jquery-ui-1.10.3.custom.min.js') }}"></script>
    <script src="{{ URL::asset('/js/jquery-ui/datepicker.js') }}"></script>
    <script src="{{ URL::asset('/js/tuovDealRates/tuovDealRates.js') }}"></script>
@stop
@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'tuovDealsLines.store']) !!}

        @include('tuovDealsLines.fields')

    {!! Form::close() !!}
</div>
@endsection
