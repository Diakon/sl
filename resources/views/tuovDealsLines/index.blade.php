@extends('app')
@extends('nav')
@section('head')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/jquery-ui/jquery-ui-1.10.3.custom.min.css') }}" />
@stop

@section('scripts')
    <script src="{{ URL::asset('/js/jquery-ui/jquery.ui.datepicker-ru.js') }}"></script>
    <script src="{{ URL::asset('/js/jquery-ui/jquery-ui-1.10.3.custom.min.js') }}"></script>
    <script src="{{ URL::asset('/js/jquery-ui/datepicker.js') }}"></script>
    <script src="{{ URL::asset('/js/tuovDealRates/tuovDealRates.js') }}"></script>
@stop
@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">TuovDealsLines</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('tuovDealsLines.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($tuovDealsLines->isEmpty())
                <div class="well text-center">No TuovDealsLines found.</div>
            @else
                @include('tuovDealsLines.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $tuovDealsLines])


    </div>
@endsection