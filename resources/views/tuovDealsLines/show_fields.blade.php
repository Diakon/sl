<!-- Deal Id Field -->
<div class="form-group">
    {!! Form::label('deal_id', 'Deal Id:') !!}
    <p>{!! $tuovDealsLine->deal_id !!}</p>
</div>

<!-- Subdivision Id Field -->
<div class="form-group">
    {!! Form::label('subdivision_id', 'Subdivision Id:') !!}
    <p>{!! $tuovDealsLine->subdivision_id !!}</p>
</div>

<!-- From Field -->
<div class="form-group">
    {!! Form::label('from', 'From:') !!}
    <p>{!! $tuovDealsLine->from !!}</p>
</div>

<!-- To Field -->
<div class="form-group">
    {!! Form::label('to', 'To:') !!}
    <p>{!! $tuovDealsLine->to !!}</p>
</div>

<!-- Kmin Field -->
<div class="form-group">
    {!! Form::label('kmin', 'Kmin:') !!}
    <p>{!! $tuovDealsLine->kmin !!}</p>
</div>

<!-- Kmax Field -->
<div class="form-group">
    {!! Form::label('kmax', 'Kmax:') !!}
    <p>{!! $tuovDealsLine->kmax !!}</p>
</div>

<!-- Rate Field -->
<div class="form-group">
    {!! Form::label('rate', 'Rate:') !!}
    <p>{!! $tuovDealsLine->rate !!}</p>
</div>

