<table class="table">
    <thead>
    <th>Deal Id</th>
			<th>Subdivision Id</th>
			<th>From</th>
			<th>To</th>
			<th>Kmin</th>
			<th>Kmax</th>
			<th>Rate</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($tuovDealsLines as $tuovDealsLine)
        <tr>
            <td>{!! $tuovDealsLine->deal_id !!}</td>
			<td>{!! $tuovDealsLine->subdivision_id !!}</td>
			<td>{!! $tuovDealsLine->from !!}</td>
			<td>{!! $tuovDealsLine->to !!}</td>
			<td>{!! $tuovDealsLine->kmin !!}</td>
			<td>{!! $tuovDealsLine->kmax !!}</td>
			<td>{!! $tuovDealsLine->rate !!}</td>
            <td>
                <a href="{!! route('tuovDealsLines.edit', [$tuovDealsLine->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('tuovDealsLines.delete', [$tuovDealsLine->id]) !!}" onclick="return confirm('Are you sure wants to delete this TuovDealsLine?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
