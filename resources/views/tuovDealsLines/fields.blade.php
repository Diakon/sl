<!--- Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('id', 'Id:') !!}
    {!! Form::number('id', null, ['class' => 'form-control', 'readonly']) !!}
</div>
<!-- Deal Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('deal_id', 'Сделка:') !!}
    {!! Form::select('deal_id', $activeDeals, null, ['class' => 'form-control']) !!}
</div>

<!-- Subdivision Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('subdivision_id', 'Подразделение:') !!}
    {!! Form::select('subdivision_id', $activeSubdivisions, ((isset($_GET['subdivision']) && (int)$_GET['subdivision']>0)?$_GET['subdivision']:null), ['class' => 'form-control']) !!}
</div>

<!-- From Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('from', 'Действует с:') !!}
	{!! Form::text('from', null, ['class' => 'form-control datepicker']) !!}
</div>

<!-- To Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('to', 'Действует по:') !!}
	{!! Form::text('to', null, ['class' => 'form-control datepicker']) !!}
</div>

<!-- Kmin Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('koef_min', 'Коэф.мин.:') !!}
	{!! Form::text('koef_min', null, ['class' => 'form-control']) !!}
</div>

<!-- Kmax Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('koef_max', 'Коэф.макс.:') !!}
	{!! Form::text('koef_max', null, ['class' => 'form-control']) !!}
</div>

<!-- Rate Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('rate', 'Ставка:') !!}
	{!! Form::text('rate', null, ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
