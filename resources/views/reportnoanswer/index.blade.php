@section('head')
@stop

@section('scripts')
@stop

@extends('app')

@extends('nav')

@section('content')
<style>
    .table{
    font-size: 12px;
    }
    th {
    background-color: #d3d3d3;
    }
</style>



<div class="container">
    <div class="row">
        <h1>{{ $app_title = 'Анализ пропущенных звонков' }}</h1>

        <table class="table table-striped">
            <thead>
                <tr>
                    <td>Дата</td>
                    <td>День недели</td>
                    <td>Пропущеные звонки</td>
                </tr>
            </thead>
            <tbody>
                @foreach ( $dates_array as $date => $weekday )
                    <tr>
                        <td><?=(date('d.m.Y', strtotime($date)));?></td>
                        <td><?=$weekday;?></td>
                        <td>
                        @if ( !empty($dates_no_answer_val[$date]) )
                            <ul class="list-unstyled" style="-webkit-column-count:3;-moz-column-count:3;column-count:3;">
                            @foreach ($dates_no_answer_val[$date] as $phone=>$customer)
                                <li>
                                    {{ $phone }}
                                    @if (!empty($customer))
                                        - <a href="http://vls.secondlab.ru/edit.php?pid={{ $customer->id_anketa or $customer->id }}" target='_blank' >Соискатель {{ $customer->id_anketa or $customer->id }} {{ $customer->firstname }}  {{ $customer->lastname }}</a>
                                    @endif
                                </li>
                            @endforeach
                            </ul>
                        @else
                            <b>Пропущенных звонков нет. Отличная работа!</b>
                        @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>




@endsection