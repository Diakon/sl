@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop

@section('scripts')
    <script>
        var bootstrapTooltip = $.fn.tooltip.noConflict();
        $.fn.bstooltip = bootstrapTooltip;
    </script>
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ URL::asset('/js/tuov/report.js') }}"></script>
@stop

@extends('app')

@extends('nav')

@section('content')

    <div class="container-fluid">

        <div class="row padtop">
            <form method="GET">

                <div class="col-lg-2">
                    <input class="form-control datepicker" name="from" type="text"
                           value="{{ $data['from'] }}"/>
                </div>

                <div class="col-lg-2">
                    <input class="form-control datepicker" name="to" type="text"
                           value="<?= $data['to'] ?>"/>
                </div>
                <div class="col-lg-2">
                    {{ Form::select('oid', $objects->pluck('name', 'id') , $data['oid'], ['class' => 'form-control']) }}
                </div>

                <div class="col-lg-2">
                    {{ Form::select('action', [
                                        'bySubdivision' => 'По подразделениям',
                                        'byObject' => 'По объекту',
                                        'byWorker' => 'По компании',], $data['action'], ['class' => 'form-control']) }}
                </div>

                <div class="col-lg-2">
                    <input class="btn btn-success" type="submit" value="Сгенерировать">
                </div>
                @if($filename)
                    <div class="col-lg-2">
                        <a href="<?=$filename?>" target="_blank" class="btn btn-success"> Скачать отчет</a>
                    </div>
                @endif
            </form>
        </div>

        @if(isset($data['content']))
            <div class="table-responsive">
                {!! $data['content'] !!}
            </div>
        @elseif(isset($data['oid']))
            <h2>идет рассчет, обновите позднее.</h2>
        @endif

    </div>
@endsection

