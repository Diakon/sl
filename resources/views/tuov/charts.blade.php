<script language="javascript" type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.js"></script>
<script language="javascript" type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.categories.min.js"></script>
<script language="javascript" type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/jquery.flot.resize.min.js"></script>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/flot/0.8.3/excanvas.min.js"></script><![endif]-->
<script type="text/javascript">
    $(function() {

        var datasets = {!! json_encode($charts) !!};

        // hard-code color indices to prevent them from shifting as
        // countries are turned on/off

        var i = 0;
        $.each(datasets, function(key, val) {
            val.color = i;
            ++i;
        });

        // insert checkboxes
        var choiceContainer = $("#choices");
        $.each(datasets, function(key, val) {
            choiceContainer.append("<br/><input type='checkbox' name='" + key +
                    "' checked='checked' id='id" + key + "'></input> " +
                    "<label for='id" + key + "'>"
                    + val.label + "</label>");
        });

        choiceContainer.find("input").click(plotAccordingToChoices);
        @if (Input::has('document_category'))
            choiceContainer.find('input').prop('checked', false);
            choiceContainer.find("input[id='id{{Input::get('document_category')}}']").trigger('click');
        @endif

        function plotAccordingToChoices() {

            var data = [];

            choiceContainer.find("input:checked").each(function () {
                var key = $(this).attr("name");
                if (key && datasets[key]) {
                    data.push(datasets[key]);
                }
            });

            if (data.length > 0) {
                var placeholder = $("#placeholder");
                $.plot(placeholder, data, {
                    series: {
                        bars: {
                            show: true,
                            barWidth: 0.6,
                            align: "center"
                        }
                    },
                    yaxis: {
                        min: 0,
                        tickDecimals: 0
                    },
                    xaxis: {
                        mode: "categories",
                        tickDecimals: 0
                    }
                });
                placeholder.resize(function () {
                    console.log("Placeholder is now "
                            + $(this).width() + "x" + $(this).height()
                            + " pixels");
                });

            }
        }

        /*$('#tuov-charts-link').click(function () {
            $('#placeholder').parent().show();
            $(this).hide();
            plotAccordingToChoices();
            return false;
        }).trigger('click');*/
         plotAccordingToChoices();
    });

</script>

<div class="row">
    <div class="col-sm-12">
        <!--a href="javascript:void(0);" id="tuov-charts-link">График добавления документов</a-->
        <div class="charts" style="xdisplay:none;">
            <div id="placeholder" class="demo-placeholder pull-left col-sm-8" style="min-width:300px; height: 300px"></div>
            <div class="pull-right col-sm-4 hidden-xs" id="choices"></div>
        </div>
    </div>
</div>