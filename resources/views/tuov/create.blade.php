@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop
@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
@stop

@extends('app')

@extends('nav')

@section('content')
    <div class="container">
        <div class="row">
            <h1 class="text-center">{{ $app_title = 'Добавить новый документ' }}</h1>
                @include('common.errors')

                {!! Form::open(['route' => 'tuovDeduction.store', 'files' => true]) !!}

                @include('tuov.fields')

                {!! Form::close() !!}
        </div>
    </div>
@endsection