<?php
use \App\Models\Subdivision;
use \App\Models\LuovDocument;
use \App\Models\DirDocumentCategory;

$search_form_options = ['route' => 'tuov.rates.index', 'class' => 'form-inline', 'method' => 'get'];

?>

<table class="table-tuov-keys table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th>№</th>
            <th>Должность</th>
            <th>Ставка</th>
            <th>Разряд</th>
            <th>Подразделение</th>
            <th>С</th>
            <th>По</th>
            <th>По умолчанию</th>
            <th>Без лин.надбавки</th>
            <th style="width: 70px;"></th>
        </tr>
        {!! Form::model($request, $search_form_options) !!}
        <tr>
            <td></td>
            <td>
                {!! Form::text('position', null , ['class' => 'form-control', 'placeholder'=>'Должность', 'onchange' => 'this.form.submit();']) !!}
            </td>
            <td>
                {!! Form::text('rate', null , ['class' => 'form-control', 'placeholder'=>'Ставка', 'onchange' => 'this.form.submit();']) !!}
            </td>
            <td></td>
            <td>{{ Form::select('subdivision_id', $activeSubdivisions,  null , ['class' => 'form-control', 'placeholder'=>'Подразделение', 'onchange' => 'this.form.submit();']) }}</td>
            <td>
                {!! Form::text('date_from', null , ['class' => 'form-control', 'style' =>'width:80px;padding:2px;', 'placeholder'=>'Дата от', 'onchange' => 'this.form.submit();']) !!}

            </td>
            <td>{!! Form::text('date_to', null , ['class' => 'form-control', 'style' =>'width:80px;padding:2px;', 'placeholder'=>'Дата до', 'onchange' => 'this.form.submit();']) !!}</td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        {!! Form::close() !!}
    </thead>
    <tbody>
    @if($rates->isEmpty())
        <tr class="info">
            <td colspan="10" class="text-center">Записей не обнаружено.</td>
        </tr>
    @else
        @foreach($rates as $rate)
            <tr>
                <td>{{ $rate->id }}</td>
                <td>{{ $rate->position }}</td>
                <td>{{ $rate->rate }}</td>
                <td>{{ $rate->rank }}</td>
                <td>{{ $rate->subdivision->nameAndObject }}</td>
                <td>{{ $rate->date_from }}</td>
                <td>{{ $rate->date_to }}</td>
                <td>@if ($rate->default>0) <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> @endif</td>
                <td>@if ($rate->without_allowance>0) <span class="glyphicon glyphicon-ok" aria-hidden="true"></span> @endif</td>
                <td>

                    <a href="{!! route('tuov.rates.edit', $rate->id) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="{!! route('tuov.rates.delete', $rate->id) !!}"
                       onclick="return confirm('Are you sure wants to delete this SubdivisionRates?')"><i class="glyphicon glyphicon-remove"></i></a>

                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>