@extends('app')
@extends('nav')
@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
@stop
@section('content')
<div class="container">

    @include('common.errors')
    <h1>{{ $app_title = sprintf('Редактировать ставку №%d', $rate->id) }}</h1>

    {!! Form::model($rate, ['route' => ['tuov.rates.update', $rate->id], 'method' => 'patch']) !!}

        @include('tuov.rates.fields')

    {!! Form::close() !!}
</div>
@endsection
