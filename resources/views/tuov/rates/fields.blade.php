<!--- Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('id', 'Id:') !!}
    {!! Form::number('id', null, ['class' => 'form-control', 'readonly']) !!}
</div>
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('position', 'Должность:') !!}
    {!! Form::text('position', null, ['class' => 'form-control']) !!}
</div>

<div style="clear: both;"></div>

<!--- Rate Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('rate', 'Ставка:') !!}
    {!! Form::number('rate', null, ['class' => 'form-control', 'min'=>"0.01",  'step' => '0.01']) !!}
</div>
<!--- rank Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('rank', 'Разряд:') !!}
    {!! Form::text('rank', null, ['class' => 'form-control']) !!}
</div>

<div style="clear: both;"></div>

<!--- From Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('date_from', 'Действует с:') !!}
    {!! Form::text('date_from', null, ['class' => 'form-control datepicker']) !!}
</div>

<!--- To Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('date_to', 'Действует по:') !!}
    {!! Form::text('date_to', null, ['class' => 'form-control datepicker']) !!}
</div>

<div style="clear: both;"></div>

<!--- To Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('default', 'По умолчанию:') !!}
    {!! Form::hidden('default', 0) !!}
    {!! Form::checkbox('default', 1, null) !!}
</div>

<!--- To Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('without_allowance', 'Без лин.надбавки:') !!}
    {!! Form::hidden('without_allowance', 0) !!}
    {!! Form::checkbox('without_allowance', 1, null) !!}
</div>

<div style="clear: both;"></div>

<!--- Subdivision Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('subdivision_id', 'Подразделение:') !!}
    {!! Form::select('subdivision_id', $activeSubdivisions, Input::get('subdivision_id', null), ['class' => 'form-control']) !!}
</div>

<div style="clear: both;"></div>

<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="#" onclick="history.back();" class="btn btn-default">Отмена</a>
</div>

