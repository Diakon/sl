@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop
@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
@stop

@extends('app')

@extends('nav')

@section('content')
    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">{{ $app_title = 'Ставки для почасовки' }}</h1>
            <div class="form-group form-inline">
                <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('tuov.rates.create', ['subdivision_id' => Request::input('subdivision_id')]) !!}">Добавить новый</a>
                {{ Form::select('subdivision_id', collect($activeSubdivisions), Request::input('subdivision_id'), ['class' => 'form-control pull-right chooseSubdivision', 'style' => 'margin-top: 25px',
                    'onchange' => 'window.location = location.origin + location.pathname + "?" + this.name + "=" + $(this).val();']) }}
            </div>

            <div class="clearfix"></div>

            @include('tuov.rates.table')
        </div>
        @include('common.paginate', ['records' => $rates])
    </div>
@endsection