@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop
@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ URL::asset('/js/tuov.js') }}"></script>
@stop

@extends('app')

@extends('nav')

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="
                @if($deduction->category->id!=6)
                    col-sm-offset-3 col-sm-6
                    @if ($deduction->category->id==9 || $deduction->category->id==14)
                        col-lg-offset-1 col-lg-10
                    @else
                        col-lg-offset-2 col-lg-8
                    @endif
                @else
                    col-xs-12
                @endif">

                @include('flash::message')

                <div class="pull-left">
                    <h1>{{ $app_title = sprintf('Документ #%s', $deduction->id)}}</h1>
                    <p class="help-block">
                        Подразделение: {!! link_to_route('subdivisions.edit', $deduction->subdivision->name, $deduction->subdivision_id, ['target' => '_blank', 'title' => 'Открыть в новой вкладке']) !!}
                        Дата: {{ $deduction->date }}
                        @if ( in_array($deduction->document_category, [1]) )
                            Смена: @if ($deduction->shift>0) День @else Ночь @endif
                        @endif
                        Категория: {{ $deduction->category->name }}
                        Статус: {{ $deduction->document_status }}
                    </p>
                </div>
                <a class="btn btn-default pull-right" style="margin-top: 25px" href="{{ $deduction->filepath() }}" download>Скачать документ</a>

                <div class='clearfix'></div>
                @include('common.errors')

                @include('tuov.fields_edit')
            </div>
        </div>
    </div>
@endsection