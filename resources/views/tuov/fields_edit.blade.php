    <div class="row">
        <div id="doc_container_main" class="hidden">
            <div id="doc_container" style="height: 400px;overflow: scroll;">
                <img id="doc_img" onload="setTimeout(function () {AppTuov.edit.image.show();},100);" src='{{ $deduction->filepath() }}?r={{ rand(111,9999) }}' data-angle="0" style="max-width: 100%;" />
            </div>
            <div class="text-center">
                        <span class="glyphicon glyphicon-repeat" style="-moz-transform: scaleX(-1);
                            -o-transform: scaleX(-1);
                            -webkit-transform: scaleX(-1);
                            transform: scaleX(-1);" onclick="AppTuov.edit.image.rotate(-90);"></span>
                <span class="glyphicon glyphicon-repeat" onclick="AppTuov.edit.image.rotate(90);"></span>
                &nbsp;&nbsp;
                <span class="glyphicon glyphicon-zoom-out" aria-hidden="true" onclick="AppTuov.edit.image.zoom(-1);"></span>
                <span class="glyphicon glyphicon-zoom-in" aria-hidden="true" onclick="AppTuov.edit.image.zoom(1);"></span>
            </div>
        </div>


        <div style="padding-bottom:10px;">
            <a id="params_head" href="javascript:void(0);">
                <span id="open_text" style="display: inline;" onclick="$('#default_params').toggle();$(this).text('Скрыть блок параметров'); return false;">Задать параметры по умолчанию</span>
                <span id="close_text" style="display: none;" onclick="$('#default_params').toggle();$(this).hide();$('#open_text').show(); return false;">Скрыть блок параметров</span>
            </a>
            <div id="default_params" style="display: none;">
                {!! Form::open(['class' => 'form-inline', 'id' =>'form_tuov_default']) !!}
                @foreach($buildForm as $name=>$element)
                    @if (!isset($element[3]['exclude_filter']))
                        <div class="form-group @if( isset($element[3]['has-error']) && $element[3]['has-error'] == true ) has-warning @endif ">
                            {!! Form::label($name, $element[2], ['class' => "control-label"]) !!}
                            @if ($element[0] == 'select')
                                {!! Form::select($name, $element[1], null, ['class'=>"form-control"]) !!}
                            @else
                                {!! Form::text($name, null, ['class'=>"form-control"]) !!}
                            @endif
                        </div>
                    @endif
                @endforeach
                {!! Form::label('dinner', 'Обед(часы)') !!}
                {!! Form::text('dinner', 1, ['class'=>"sum form-control"]) !!}

                <input class="btn btn-default" type="button" id="recount" value="Применить ко всем" onclick="AppTuov.edit.recount(this);" />
                {!! Form::close() !!}
            </div>
        </div>


        <div class="form-group">
            {!! Form::button('Добавить', ['id' => 'add', 'class' => 'btn btn-default', 'tabindex' => 1,
                            'onclick' => 'AppTuov.edit.add()']) !!}

            @if ($deduction->category->id == 14)
                <button id="add_tab" class="btn btn-default" data-date="{{ $deduction->date }}" data-date_end="{{ $deduction->date_end }}" onclick="AppTuov.edit.dateRange.addDate(this);">Добавить дату</button>
            @endif
        </div>
    </div>

    <style>
        .table .form-group {
            margin-bottom: 0;
        }
    </style>

    {!! Form::model($deduction, ['route' => ['tuovDeduction.update', $deduction->id], 'method' => 'patch', 'id' => 'form_tuov_edit']) !!}
    <div id="container">
        <div>
            <ul class="nav nav-pills"></ul>
        </div>

        <div class="tab-content">
            <div class="table-responsive">
                <table class="table ztable-bordered table-condensed">
                    @section('table-thead')
                    <thead>
                        <th>#</th>
                        <th xclass="xcol-xs-4">ФИО</th>
                        <th>ID</th>
                        @foreach($buildForm as $name)
                            <th @if( isset($name[3]['has-error']) && $name[3]['has-error'] == true ) title="Ошибка" style="color:#8a6d3b;" @endif>{{ $name[2] }}</th>
                        @endforeach
                        <th></th>
                    </thead>
                    @endsection
                    @yield('table-thead')
                    <tbody>
                        <?php $summ = [] ?>
                        @foreach($rows as $key=>$row)
                            @if($key>1 && $key%10==0)
                            @endif
                        <tr>
                            <td><label>{{ sprintf('%02s', count($rows)-$key) }}</label></td>
                            <td>
                                <input class="fio form-control" tabindex="-1" style="width:280px;" onkeyup="AppTuov.edit.hint.init(this)" value="{{ $row->anketa->lastname }} {{ $row->anketa->firstname }} {{ $row->anketa->patronymic }}" />
                            </td>
                            <td>
                                <div class="form-group">
                                    {!! Form::text('anketa_id[]', $row->anketa_id, ['class' => "id form-control", 'tabindex' => "-1", 'readonly', 'style' => 'width: 70px']) !!}
                                </div>
                            </td>

                            @foreach($buildForm as $name=>$element)
                                <td>
                                    <div class="form-group @if( isset($element[3]['has-error']) && $element[3]['has-error'] == true ) has-warning @endif ">
                                        @if ($element[0] == 'select')
                                            {!! Form::select($name.'[]', $element[1], $row->$name, ['class'=>"form-control"]) !!}
                                        @else
                                            {!! Form::text($name.'[]', $row->$name, ['class'=>"form-control"]) !!}
                                            <?php if (!isset($summ[$name])) {$summ[$name]=0;} $summ[$name] += $row->$name ?>
                                        @endif
                                    </div>
                                </td>
                            @endforeach
                            <td>
                                <span class="glyphicon glyphicon-remove" aria-hidden="true" tabindex="-1" onclick="AppTuov.edit.remove(this);"></span>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <th></th>
                        <th></th>
                        <th></th>
                        @foreach($buildForm as $name=>$element)
                            <th @if( isset($element[3]['has-error']) && $element[3]['has-error'] == true ) title="Ошибка" style="color:#8a6d3b;" @endif >
                                @if( isset($element[3]['total-amount']) && $element[3]['total-amount'] == true )
                                    <div class="form-group @if( isset($element[3]['has-error']) && $element[3]['has-error'] == true ) has-warning @endif ">
                                        <input title="Итого {{ $element[2] }}" disabled name="amount_{{ $name }}" value="{{ $summ[$name] or '' }}"
                                               class="total_amount form-control @if(!isset($summ[$name]) || $summ[$name]==0) hidden @endif" />
                                    </div>
                                @endif
                            </th>
                        @endforeach
                        <th></th>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="form-group">
            @can ('update', $deduction)
                {!! Form::submit('Сохранить', ['class' => 'btn btn-primary', 'tabindex' => -1]) !!}
            @endcan
            {!! Html::linkRoute('tuovDeduction.index', 'Отмена', [], ['class' => 'btn btn-default', 'tabindex' => '-1']) !!}
        </div>
    </div>
    {!! Form::close() !!}


<script type="application/javascript">
    $(function() {
        AppTuov.edit.dateRange.init();
        AppTuov.edit.init();
    });
</script>


<div id="hint" style="position:absolute !important; background-color:white;"></div>

<div id="tpl_row" class="hidden">
    <table>
    <tr>
        <td><label></label></td>
        <td><input class="fio form-control" value="" onkeyup="AppTuov.edit.hint.init(this)" style="width:280px;" /></td>
        <td>
            <div class="form-group">
                <input class="form-control id" tabindex="-1" readonly disabled name="anketa_id[]"  value="" style="width: 70px" />
            </div>
        </td>
        @foreach($buildForm as $name=>$element)
        <td>
            <div class="form-group @if( isset($element[3]['has-error']) && $element[3]['has-error'] == true ) has-warning @endif ">
                @if ($element[0] == 'select')
                    {!! Form::select($name.'[]', $element[1], null, ['class'=>"form-control", 'disabled']) !!}
                @else
                    {!! Form::text($name.'[]', null, ['class'=>"form-control ", 'disabled']) !!}
                @endif
            </div>
        @endforeach
        </td>
        <td><span class="glyphicon glyphicon-remove" aria-hidden="true" tabindex="-1" onclick="AppTuov.edit.remove(this);"></span></td>
    </tr>
    </table>
</div>
