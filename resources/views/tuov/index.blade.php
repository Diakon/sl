@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop
@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ URL::asset('/js/tuov.js') }}"></script>
@stop

@extends('app')

@extends('nav')

@section('content')
    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">{{ $app_title = 'Список документов' }}</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('tuovDeduction.create') !!}">Добавить новый документ</a>

            <div class="clearfix"></div>

            @include('tuov.table')
        </div>
        @include('common.paginate', ['records' => $documents])
        <span id="helpBlock" class="help-block">
            &uarr; &darr; - перемещение по списку документов <br/>
            &larr; &rarr; - переключение на предыщую/следующую страницу <br/>
            enter, dblclick - Перейти к редактированию документа
        </span>
    </div>
    <script type="application/javascript">
        $(function() {
            AppTuov.navigateTable.init();
            AppTuov.history.init();
        });
    </script>
@endsection