<div class="col-sm-offset-3 col-sm-6 col-lg-offset-2 col-lg-8">
    <div class="form-group col-lg-12 img-tuov-excel-example" style="display:none;" >
        <samp>
            Пример заполнения Excel:
            <img src="/images/tuov_doc_excel_example.png" />
            Формулы в поле "сумма" работают (но гарантии правильного подсчета нет)!
        </samp>
    </div>
    <div class="form-group col-lg-12">
        {!! Form::label('document[]', 'Файл в формате png, jpeg. Для аванса, штрафа/премии, СИЗ, оф.зп, акция, сл.записка еще xls и xlsx. Excel файл допускается только один, изображения "склеиваются":') !!}
        {!! Form::file('document[]', ['class' => 'form-control', 'multiple' => "multiple"]) !!}
    </div>

    <div class="form-group col-lg-3">
        {!! Form::label('subdivision_id', 'Подразделение:') !!}
        {!! Form::select('subdivision_id', (new \App\Models\Subdivision)->getActiveSubdivisions(), null,
                ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-lg-3">
        {!! Form::label('date', 'Дата:') !!}
        {!! Form::text('date', null, ['class' => 'form-control datepicker']) !!}
    </div>

    <div class="form-group col-lg-3 hidden">
        {!! Form::label('date_end', 'Дата до:') !!}
        {!! Form::text('date_end', null, ['class' => 'form-control datepicker']) !!}
    </div>

    <div class="form-group col-lg-3">
        {!! Form::label('document_category', 'Тип:') !!}
        {!! Form::select('document_category', (new \App\Models\DirDocumentCategory())->activeList(), null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-lg-3">
        {!! Form::label('shift', 'Смена:') !!}
        {!! Form::select('shift', ['Ночь','День'], null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group col-lg-12">
        {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
        {!! Html::linkRoute('tuovDeduction.index', 'Отмена', [], ['class' => 'btn btn-default']) !!}
    </div>
</div>
<script>
    $(function() {
        $('select[name="document_category"]').on('change', function(){
            var excelAllow = '{{ implode(',',$excel_type)}}';
            var div = $('select[name="shift"]').closest('div');
            $(this).val()=='1' ? div.show() : div.hide();
            $.inArray( $(this).val(), excelAllow.split(',') ) > -1 ? $('.img-tuov-excel-example').show() : $('.img-tuov-excel-example').hide();

            var date_end_div = $('input[name="date_end"]').closest('div');
            $(this).val()=='14' ? date_end_div.removeClass('hidden') : date_end_div.addClass('hidden');
        }).trigger('change');
    });
</script>