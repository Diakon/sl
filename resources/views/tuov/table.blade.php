<?php
use \App\Models\Subdivision;
use \App\Models\LuovDocument;
use \App\Models\DirDocumentCategory;

$search_form_options = ['route' => 'tuovDeduction.index', 'class' => 'form-inline', 'method' => 'get'];
?>

@include( 'tuov.charts' )

<table class="table-tuov-keys table table-striped table-bordered table-hover" data-history_uri="{!! route('tuovDeduction.history', '%7Bid%7D') !!}">
    <thead>
        <tr>
            <th>№</th>
            <th>Подразделение</th>
            <th>Категория</th>
            <th>Дата</th>
            <th>Смена</th>
            <th>Тип</th>
            <th>Статус</th>
            <th>Создатель</th>
            <th>Дата создания</th>
            <th style="width: 70px;"></th>
        </tr>
        {!! Form::model($request, $search_form_options) !!}
        <tr>
            <td></td>
            <td>
                {!! Form::select('subdivision_id', (new Subdivision)->getActiveSubdivisions(), null , ['class' => 'form-control', 'placeholder'=>'Подразделение', 'onchange'=>'this.form.submit();']) !!}
            </td>
            <td>
                {!! Form::select('document_category', (new \App\Models\DirDocumentCategory())->activeList(), null , ['class' => 'form-control', 'placeholder'=>'Категория', 'onchange'=>'this.form.submit();']) !!}
            </td>
            <td>
                {!! Form::text('date_from', null , ['class' => 'form-control', 'style' =>'width:80px;padding:2px;', 'placeholder'=>'Дата от', 'onchange' => 'this.form.submit();']) !!}
                {!! Form::text('date_to', null , ['class' => 'form-control', 'style' =>'width:80px;padding:2px;', 'placeholder'=>'Дата до', 'onchange' => 'this.form.submit();']) !!}
            </td>
            <td></td>
            <td></td>
            <td>
                {!! Form::select('document_status', LuovDocument::documentStatus(), null , ['class' => 'form-control', 'placeholder'=>'Статус', 'onchange'=>'this.form.submit();']) !!}
            </td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        {!! Form::close() !!}
    </thead>
    <tbody>
    @if($documents->isEmpty())
        <tr class="info">
            <td colspan="10" class="text-center">Записей не обнаружено.</td>
        </tr>
    @else
        @foreach($documents as $deduction)
            <tr>
                <td>{!! $deduction->id !!}</td>
                <td> @if ($deduction->subdivision) {{ $deduction->subdivision->nameAndObject }} @endif </td>
                <td>{{ $deduction->category->name }}</td>
                <td>
                    {{ $deduction->date }}
                    @if ($deduction->document_category == 14)
                        /<br/> {{ $deduction->date_end }}
                    @endif
                </td>
                <td>
                    @if ( in_array($deduction->document_category, [1]) )
                        @if ($deduction->shift>0) День @else Ночь @endif</td>
                    @endif
                <td>{{ $deduction->document_type }}</td>
                <td>{{ $deduction->document_status }}</td>
                <td>{{ $deduction->user->username }}</td>
                <td title="{{ \Carbon\Carbon::parse($deduction->created_at)->format('H:i:s') }}">
                    {{ \Carbon\Carbon::parse($deduction->created_at)->format('Y-m-d') }}
                </td>
                <td>
                    @if (LuovDocument::STATUS_DELETE != $deduction->document_status)
                        <a href="{!! route('tuovDeduction.edit', [$deduction->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                        <a href="{!! route('tuovDeduction.delete', [$deduction->id]) !!}" onclick="return confirm('Вы уверены?') ? true : false;"><i class="glyphicon glyphicon-remove"></i></a>
                    @endif
                        <a href="#" title="История изменения документа" data-document_id="{{ $deduction->id }}" data-toggle="modal" data-target=".history-modal-md"><i class="glyphicon glyphicon-list-alt"></i></a>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>

<!-- Modal завершения задачи -->
<div id="x" class="modal history-modal-md" tabindex="-1" role="dialog" aria-labelledby="historyModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <div class="title-modal">История статуса документа (<span class="document_id"></span>)</div>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Закрыть</button>
            </div>
        </div>
    </div>
</div>