@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($objects, ['route' => ['objects.update', $objects->id], 'method' => 'patch']) !!}

        @include('objects.fields')

    {!! Form::close() !!}
</div>
@endsection
