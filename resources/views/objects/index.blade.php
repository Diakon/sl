@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Objects</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('objects.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($objects->isEmpty())
                <div class="well text-center">No Objects found.</div>
            @else
                @include('objects.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $objects])


    </div>
@endsection