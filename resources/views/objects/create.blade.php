@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'objects.store']) !!}

        @include('objects.fields')

    {!! Form::close() !!}
</div>
@endsection
