<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $objects->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $objects->name !!}</p>
</div>

<!-- Description Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $objects->description !!}</p>
</div>

<!-- Coordinator Field -->
<div class="form-group">
    {!! Form::label('coordinator', 'Coordinator:') !!}
    <p>{!! $objects->coordinator !!}</p>
</div>

<!-- Comments Field -->
<div class="form-group">
    {!! Form::label('comments', 'Comments:') !!}
    <p>{!! $objects->comments !!}</p>
</div>

<!-- Juridical Name Field -->
<div class="form-group">
    {!! Form::label('juridical_name', 'Juridical Name:') !!}
    <p>{!! $objects->juridical_name !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $objects->address !!}</p>
</div>

<!-- Fio Field -->
<div class="form-group">
    {!! Form::label('fio', 'Fio:') !!}
    <p>{!! $objects->fio !!}</p>
</div>

<!-- Domitory Field -->
<div class="form-group">
    {!! Form::label('domitory', 'Domitory:') !!}
    <p>{!! $objects->domitory !!}</p>
</div>

