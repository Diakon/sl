<!--- Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('id', 'Id:') !!}
	{!! Form::number('id', null, ['class' => 'form-control', 'readonly']) !!}
</div>

<!--- Name Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('name', 'Name:') !!}
	{!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!--- Description Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('description', 'Description:') !!}
	{!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!--- Coordinator Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('coordinator', 'Coordinator:') !!}
	{!! Form::text('coordinator', null, ['class' => 'form-control']) !!}
</div>

<!--- Comments Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('comments', 'Comments:') !!}
	{!! Form::text('comments', null, ['class' => 'form-control']) !!}
</div>

<!--- Juridical Name Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('juridical_name', 'Juridical Name:') !!}
	{!! Form::text('juridical_name', null, ['class' => 'form-control']) !!}
</div>

<!--- Address Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('address', 'Address:') !!}
	{!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!--- Fio Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('fio', 'Fio:') !!}
	{!! Form::text('fio', null, ['class' => 'form-control']) !!}
</div>

<!--- Domitory Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('domitory', 'Domitory:') !!}
	{!! Form::text('domitory', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
