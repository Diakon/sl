<table class="table">
    <thead>
    <th>Id</th>
			<th>Name</th>
			<th>Description</th>
			<th>Coordinator</th>
			<th>Comments</th>
			<th>Juridical Name</th>
			<th>Address</th>
			<th>Fio</th>
			<th>Domitory</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($objects as $objects)
        <tr>
            <td>{!! $objects->id !!}</td>
			<td>{!! $objects->name !!}</td>
			<td>{!! $objects->description !!}</td>
			<td>{!! $objects->coordinator !!}</td>
			<td>{!! $objects->comments !!}</td>
			<td>{!! $objects->juridical_name !!}</td>
			<td>{!! $objects->address !!}</td>
			<td>{!! $objects->fio !!}</td>
			<td>{!! $objects->domitory !!}</td>
            <td>
                <a href="{!! route('objects.edit', [$objects->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('objects.delete', [$objects->id]) !!}" onclick="return confirm('Are you sure wants to delete this Objects?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
