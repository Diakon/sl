<!--- Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('id', 'Id:') !!}
	{!! Form::number('id', null, ['class' => 'form-control', 'readonly']) !!}
</div>

<!--- Taskmaster Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('taskmaster_id', 'Координатор:') !!}
    {!! Form::select('taskmaster_id', $activeUsers, null, ['class' => 'form-control']) !!}
</div>

<!--- Object Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('object_id', 'Объект:') !!}
    {!! Form::select('object_id', collect($activeObjects)->map(function($item){return $item->name;}), null, ['class' => 'form-control']) !!}
</div>

<!--- Default Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('default', 'Default:') !!}
	{!! Form::text('default', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
