@extends('app')
@extends('nav')
@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($taskmasterObjects, ['route' => ['taskmasterObjects.update', $taskmasterObjects->id], 'method' => 'patch']) !!}

        @include('taskmasterObjects.fields')

    {!! Form::close() !!}
</div>
@endsection
