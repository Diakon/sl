@extends('app')
@extends('nav')
@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'taskmasterObjects.store']) !!}

        @include('taskmasterObjects.fields')

    {!! Form::close() !!}
</div>
@endsection
