@extends('app')
@extends('nav')
@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">{{ $app_title = 'Назначение дополнительных координаторов' }}</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('taskmasterObjects.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($taskmasterObjects->isEmpty())
                <div class="well text-center">No TaskmasterObjects found.</div>
            @else
                @include('taskmasterObjects.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $taskmasterObjects])


    </div>
@endsection