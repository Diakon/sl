<table class="table table-bordered table-striped">
    <thead>
    <th>Id</th>
			<th>Координатор</th>
			<th>Объект</th>
			<th>Default</th>
    <th width="50px">Действия</th>
    </thead>
    <tbody>
    @foreach($taskmasterObjects as $taskmasterObjects)
        <tr>
            <td>{{ $taskmasterObjects->id }}</td>
			<td><?= $activeUsers[$taskmasterObjects->taskmaster_id] ?></td>
			<td>{{ $taskmasterObjects->object->name }}</td>
			<td>{{ $taskmasterObjects->default }}</td>
            <td>
                <a href="{!! route('taskmasterObjects.edit', [$taskmasterObjects->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('taskmasterObjects.delete', [$taskmasterObjects->id]) !!}" onclick="return confirm('Are you sure wants to delete this TaskmasterObjects?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
