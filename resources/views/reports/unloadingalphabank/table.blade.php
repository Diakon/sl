<div style="clear: both;">
    <b>Выгрузить данные сотрудников, которые не имеют карту банка:</b><BR>
    {!! Form::select('banks', $banks, 4, ['id'=>'banks_list']) !!}
</div>

<div style="clear: both;">
    <b>Статус сотрудника:</b><BR>
    {!! Form::select('status', $status, 9, ['id'=>'status_list']) !!}
</div>


<div style="clear: both;">
    <b>Наличие скана паспорта:</b><BR>
    {!! Form::select('pass_scan', [0=>'Не важно', 1=>'Да', 2=>'Нет'], 1, ['id'=>'pass_list']) !!}
</div>

<div style="clear: both;">
    <b>Период:</b><BR>
    {!! Form::input('text', 'dateperiod', null, ['id' => 'e4']) !!}
</div>


<div id="ajaxloader" style="display: none;">
    <img src="/images/ajaxloader.gif" style="width:50px; margin-top:10px;" />
</div>

<div id="input_form" style="clear: both; margin-top: 30px;">
    {!! Form::submit('Сформировать', ['class'=>'maintable', 'id'=>'mainsubmit' ]) !!}
</div>