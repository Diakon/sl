@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/js/reports/daterangepicker/jquery.comiseo.daterangepicker.css" />
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css" rel="stylesheet">
@stop
@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ URL::asset('/js/reports/script.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/momentjs/2.3.1/moment.min.js"></script>
    <script src="{{ URL::asset('/js/reports/daterangepicker/jquery.comiseo.daterangepicker.js') }}"></script>
    <script>
        $(function() { $("#e4").daterangepicker({
            presetRanges: [{
                text: 'Сегодня',
                dateStart: function() { return moment() },
                dateEnd: function() { return moment() }
            }, {
                text: 'Завтра',
                dateStart: function() { return moment().add('days', 1) },
                dateEnd: function() { return moment().add('days', 1) }
            }, {
                text: 'Следующие 7 дней',
                dateStart: function() { return moment() },
                dateEnd: function() { return moment().add('days', 6) }
            }, {
                text: 'Следующая неделя',
                dateStart: function() { return moment().add('weeks', 1).startOf('week') },
                dateEnd: function() { return moment().add('weeks', 1).endOf('week') }
            }],
            applyOnMenuSelect: false,
            datepickerOptions: {
                minDate: null,
                maxDate: null
            }
        })});
    </script>
@stop

@extends('app')

@extends('nav')

@section('content')
    <style>
        .blank {
            width: 750px;
            height: 450px;
            background: #E7E7E7;
            box-shadow: 0 0 10px rgba(0,0,0,0.5);
            padding: 10px;
            margin: 0 auto;
            margin-top: 5%;
            border-radius: 5px;
        }
    </style>
    <div class="container">
        <div class="blank row">
            <h1 class="pull-left">{{ $app_title = 'Выгрузка по Альфабанку' }}</h1>

            <div style="clear: both; padding: 40px;">
                {!! Form::open(array('url' => '#')) !!}
                @include('reports.unloadingalphabank.table')
                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection