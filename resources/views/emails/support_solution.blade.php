<!DOCTYPE html>
<html>
<head>
    <title>{{$title}}</title>
</head>
<body>
<h1>{{$title}}</h1>
<hr>
    <p>Вы оставили в службе поддержки заявку:</p>
    {!! $description !!}<br/><br/>

    <p>По Вашей заявке дан ответ:</p>
    {!! $resultTiket !!}<br/>

    @if (!empty($solution))
        <p>Коментарий:</p>
        {{ $solution }}
    @endif

    <hr>
    <p>Дата обращения: {{$date}}</p>

</body>
</html>