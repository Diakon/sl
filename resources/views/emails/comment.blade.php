<!DOCTYPE html>
<html>
<head>
    <title>Добавлен коментарий к задаче №: {{$taskid}}</title>
</head>
<body>
<h3>Задача {{$title}} </h3></BR>

<hr>
<b>Информация о авторе:</b>
<table>
    <tr>
        <th>Автор события</th>
        <th>Дата</th>
    </tr>

    <tr>
        <td>{{$username}}</td>
        <td>{{$datetime}}</td>
    </tr>
</table>
<b>Текст коментария:</b>
</br>
{{$comment}}


</body>

</html>