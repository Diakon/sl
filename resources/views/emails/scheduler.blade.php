<!DOCTYPE html>
<html>
<head>
    <title>{{$titlerow}}</title>
</head>
<body>
<h3>{{$title}} [ {{$taskid}} ]</h3></BR>

<b>Описание задачи:</b></br>
<p>{{$description}}</p></br>
<hr>
<b>Информация о авторе</b>
<table>
    <tr>
        <th>Автор события/Логин</th>
        <th>Дата</th>
    </tr>

    <tr>
        <td>{{$username}}/{{$userid}}</td>
        <td>{{$datetime}}</td>
    </tr>
</table>
{{$content}}
</br>
{{$comment}}


</body>

</html>