<!DOCTYPE html>
<html>
<head>
    <title></title>
</head>
<body>
    <h1>{{$title}}</h1>
    <hr>
    <p>Не найдены следующие анкеты в Excel файле:</p>
    @foreach ($users as $user)
        {!! $user !!}
    @endforeach

    <p>Документ сохранен под ID: {{ $document['id'] }}.</p>
</body>

</html>