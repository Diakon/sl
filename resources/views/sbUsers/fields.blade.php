<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('username', 'Имя пользователя:') !!}
	{!! Form::text('username', null, ['class' => 'form-control']) !!}
</div>
<div style="clear: both;"></div>
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('password', 'Пароль:') !!}
	{!! Form::password('password', ['class' => 'form-control']) !!}
</div>
<div style="clear: both;"></div>
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('password_repeat', 'Повторите пароль еще раз:') !!}
	{!! Form::password('password_repeat', ['class' => 'form-control']) !!}
</div>
<?php
    if (isset($sbUsers->id)){
        echo '
            <div style="clear: both;"></div>
            <div class="form-group col-sm-6 col-lg-4">
                Если Вы не хотите менять пароль пользователю - оставьте поля ввода пароля пустыми
            </div>
            <div style="clear: both;"></div>
            ';
    }
?>
<div style="clear: both;"></div>
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('active', 'Статус:') !!}
	{!! Form::select('active', array('1' => 'Активно', '0' => 'Не активно'), null, ['class' => 'form-control']) !!}
</div>

<div style="clear: both;"></div>
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('objects_id', 'Объект:') !!}
	{!! Form::select('objects_id', $sbUsers['objects_arr'], null, ['class' => 'form-control']) !!}
</div>

<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="/sbUsers" class="btn" >Отмена</a>
</div>
