@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Список работников объекта</h1>
        </div>

        <a href="/sb/logout" class="btn btn btn-primary pull-right">Выйти</a>

        <div class="row">
            @if(empty($model))
                <div class="well text-center">Нет записей.</div>
            @else
               <table class="table">
                   <thead>
               			<th>Фамилия</th>
               			<th>Имя</th>
               			<th>Отчество</th>
               			<th>Скан паспорта</th>
                        <th width="50px">Действия</th>
                   </thead>
                   <tbody>
                   @foreach($model as $data)

                    <?php
                        $scan = null;
                        $url = \app_path().'/../../secondlab/public/img/clients/'.$data->id_anketa.'/scan.pdf';
                        if(file_exists($url)){
                            $scan  = 'http://vls.secondlab.ru/img/clients/'.$data->id_anketa.'/scan.pdf';
                        }
                    ?>
                       <tr>
                           <td>{!! $data->lastname !!}</td>
                           <td>{!! $data->firstname !!}</td>
                           <td>{!! $data->patronymic !!}</td>
                           <td><?php echo ((!empty($scan))?'<a class="btn btn-success" href="'.$scan.'" target="_blank">Скачать</a>':'Файл отсутствует'); ?></td>
                           <td>
                               <a href="sb?id=<?=$data->id_anketa;?>"><i class="glyphicon glyphicon-edit"></i></a>
                           </td>
                       </tr>
                   @endforeach
                   </tbody>
               </table>
            @endif
        </div>
    </div>
@endsection