@section('head')
@stop
@section('scripts')
    <script src="{{ URL::asset('/js/support/script.js') }}"></script>
@stop

@extends('app')

@extends('nav')

@section('content')

<div class="container">
    <h2>Создать нового пользователя СБ</h2>

    @include('common.errors')
    @include('flash::message')

    {!! Form::open(['route' => 'sbUsers.store']) !!}

        @include('sbUsers.fields')

    {!! Form::close() !!}
</div>
@endsection
