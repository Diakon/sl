<table class="table table-striped table-bordered">
    <thead>
        <th>Id</th>
        <th>Пользователь</th>
        <th>Объект</th>
        <th>Статус</th>
        <th width="50px">Действия</th>
    </thead>
    <tbody>
    @foreach($sbUsers as $sbUsers)
        <tr>
            <td>{!! $sbUsers->id !!}</td>
            <td>{!! $sbUsers->username !!}</td>
            <td>{!! $sbUsers->object->name or null !!}</td>
            <td>{!! $sbUsers->active==1?'Активно':'Отключен' !!}</td>
            <td>
                <a href="{!! route('sbUsers.edit', [$sbUsers->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('sbUsers.delete', [$sbUsers->id]) !!}" onclick="return confirm('Are you sure wants to delete this SbUsers?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
