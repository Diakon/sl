@section('head')
@stop
@section('scripts')
    <script src="{{ URL::asset('/js/support/script.js') }}"></script>
@stop


@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">{{ $app_title = 'Пользователи службы безопастности' }}</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('sbUsers.create') !!}">Добавить нового</a>
        </div>

        <div class="row">
            @if($sbUsers->isEmpty())
                <div class="well text-center">Нет записей.</div>
            @else
                @include('sbUsers.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $sbUsers])


    </div>
@endsection