@extends('app')

@section('content')
    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Анкета сотрудника</h1>
        </div>

        <a href="/sb/logout" class="btn btn btn-primary pull-right">Выйти</a>

        <div class="row" style="margin-top: 20px; margin-bottom: 50px;">

            @include('sbUsers.anketafields')

        </div>


        <div class="row">
            <a href="/sb" class="btn">Назад</a>
        </div>

    </div>


@endsection
