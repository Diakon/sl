@extends('app')

@section('content')

    <div class="container">
        <h2>Авторизация</h2>
        @include('flash::message')

        <div>
                {!! Form::open(array('url' => 'sb/auth')) !!}

                    <div class="form-group col-sm-6 col-lg-4">
                        {!! Form::label('username', 'Логин:') !!}
                    	{!! Form::text('username', null, ['class' => 'form-control', 'style' => 'max-width:206px;']) !!}
                    </div>
                    <div style="clear: both;"></div>
                    <div class="form-group col-sm-6 col-lg-4">
                        {!! Form::label('password', 'Пароль:') !!}
                    	{!! Form::password('password', ['class' => 'form-control', 'style' => 'max-width:206px;']) !!}
                    </div>

                    <div class="form-group col-sm-12">
                        {!! Form::submit('Войти', ['class' => 'btn btn-primary']) !!}
                    </div>

                {!! Form::close() !!}
        </div>


    </div>
@endsection