<?php
    $url = \app_path().'/../../secondlab/public/img/clients/'.$model->id_anketa.'/';

    $scan = null;
    if(file_exists($url.'scan.pdf')){
        $scan  = 'http://vls.secondlab.ru/img/clients/'.$model->id_anketa.'/scan.pdf';
    }

    $photo_img = 'images/nophoto.jpg';
    if(file_exists($url.'photo.jpg')){
        $photo_img  = 'http://vls.secondlab.ru/img/clients/'.$model->id_anketa.'/photo.jpg';
    }
?>

<div class="col-xs-12 col-sm-6 col-md-6">
      <div class="photo">
        <img id="photo" width="150px" style="cursor:hand;cursor:pointer;width:149px;" onclick="window.open('<?=$photo_img;?>', '', 'width=600,height=500')" src="<?=$photo_img;?>">
        <BR>
        <?php
            if (empty($scan)){ echo '<div style="margin-top:20px; max-width:150px; background-color:#a4a4a4; border-radius:4px; padding:10px;">Скан отсутствует</div>'; }
            else { echo '<a class="btn btn-success" style="margin-top:20px;" href="'.$scan.'" target="_blank">Скачать скан паспорта</a>'; }
        ?>
      </div>
</div>


<div class="col-xs-12 col-sm-6 col-md-6">
    <fieldset>
        <legend>Общая информация</legend>
        <table>
            <tr><td>Анкета:</td><td><?=$model->id_anketa;?></td></tr>
            <tr><td>Договор:</td><td><?=$model->dog_num;?></td></tr>
            <tr><td>Дата собеседования:</td><td><?=(date('d-m-Y',strtotime($model->meet_date)));?></td></tr>
            <tr><td>Фамилия:</td><td><?=$model->lastname;?></td></tr>
            <tr><td>Имя:</td><td><?=$model->firstname;?></td></tr>
            <tr><td>Отчеcтво:</td><td><?=$model->patronymic;?></td></tr>
            <tr><td>Гражданство:</td><td><?=(((int)$model->nation)?'РФ':'');?></td></tr>
            <tr><td>Дата рождения:</td><td><?=(date('d-m-Y',strtotime($model->birth_date)));?></td></tr>
            <tr><td>Возраст:</td><td><?=$model->age;?></td></tr>
            <tr><td>Место рождения:</td><td><?=$model->birth_place;?></td></tr>
            <tr><td>Пол:</td><td><?=(($model->gender==1)?'муж.':'жен.');?></td></tr>
            <tr><td>Семейное положение:</td><td><?=(($model->family_status==1)?'холост / не замужем.':'женат / замужем.');?></td></tr>
        </table>
</fieldset>
</div>

<div class="col-xs-12 col-sm-6 col-md-6">
    <fieldset>
        <legend>Паспорт</legend>
        <table>
            <tr><td>Кем выдан:</td><td><?=$model->give;?></td></tr>
            <tr><td>Дата выдачи:</td><td><?=(date('d-m-Y',strtotime($model->give_date)));?></td></tr>
            <tr><td>Код поразд.:</td><td><?=$model->code;?></td></tr>
            <tr><td>Серия:</td><td><?=$model->series;?></td></tr>
            <tr><td>Номер:</td><td><?=$model->number;?></td></tr>
            <tr><td>ИНН:</td><td><?=$model->inn;?></td></tr>
        </table>
</fieldset>
</div>

<div class="col-xs-12 col-sm-6 col-md-6">
    <fieldset>
        <legend>Адрес</legend>
        <table>
            <tr><td>Страна:</td><td><?=(($model->country==1)?'Россия':'');?></td></tr>
            <tr><td>Регион:</td><td><?=$model->region.' '.$model->region_socr;?></td></tr>
            <tr><td>Район:</td><td><?=$model->district.' '.$model->district_socr;?></td></tr>
            <tr><td>Город:</td><td><?=$model->city;?></td></tr>
            <tr><td>Населен. пункт:</td><td><?=$model->np;?></td></tr>
            <tr><td>Улица:</td><td><?=$model->street;?></td></tr>
            <tr><td>Дом:</td><td><?=$model->house;?></td></tr>
            <tr><td>Корпус:</td><td><?=$model->building;?></td></tr>
            <tr><td>Квартира:</td><td><?=$model->flat;?></td></tr>
            <tr><td>Индекс:</td><td><?=$model->index;?></td></tr>
            <tr><td>Телефон дом.:</td><td><?=$model->phone_home;?></td></tr>
            <tr><td>Телефон моб.:</td><td><?=$model->phone_mobile;?></td></tr>
            <tr><td>E-mail:</td><td><?=$model->email;?></td></tr>
            <tr><td>Одноклассники:</td><td><?=$model->odnoklassniki;?></td></tr>
        </table>
</fieldset>
</div>

