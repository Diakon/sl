@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">SbUsers</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('sbUsers.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($sbUsers->isEmpty())
                <div class="well text-center">No SbUsers found.</div>
            @else
                @include('sbUsers.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $sbUsers])


    </div>
@endsection