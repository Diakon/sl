@section('head')
@stop
@section('scripts')
<script src="{{ URL::asset('/js/support/script.js') }}"></script>
@stop

@extends('app')

@extends('nav')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($sbUsers, ['route' => ['sbUsers.update', $sbUsers->id], 'method' => 'patch']) !!}

        @include('sbUsers.fields')

    {!! Form::close() !!}
</div>
@endsection
