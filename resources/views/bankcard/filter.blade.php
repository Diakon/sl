<style>
    #s2id_id_anketa_filter, #s2id_object_filter, #s2id_pers_status_list, #select2-drop, #s2id_card_status_filter, #s2id_banks_list_filter {
        min-width: 200px;
    }
</style>

<div id="xxfilterBlock" style="background: #c0c0c0; border-radius: 5px; padding: 10px; margin-bottom: 10px;">

    {!! Form::model($request, ['route' => 'bankcard.index']) !!}
        <table>
            <tr>
                <td>Сотрудник</td>
                <td>Объект</td>
                <td>Статус сотрудника</td>
                <td>Статус карты</td>
                <td>Банк</td>
                <td>Номер карты</td>
            </tr>
            <tr>
                <td>{!! Form::select('id_anketa', ($request->has('id_anketa')?[$request->id_anketa=>\App\Models\PersMain::findOrFail($request->id_anketa)->fio]:[]), null, ['id'=>'id_anketa', 'class' => 'form-control', 'style' => 'min-width:250px' ]) !!}</td>
                <td>{!! Form::select('id_object', $object, null, ['class' => 'form-control', 'id'=>'object_filter' ]) !!}</td>
                <td>{!! Form::select('pers_status_list[]', $pers_status_list,  null, ['class' => 'form-control', 'id'=>'pers_status_list', "multiple"=>"multiple" ]) !!}</td>
                <td>{!! Form::select('id_card_status[]', $cardStatus, null, ['class' => 'form-control', 'id'=>'card_status_filter', "multiple"=>"multiple" ]) !!}</td>
                <td>{!! Form::select('id_bank[]', $banks_list, null, ['class' => 'form-control', 'id'=>'banks_list_filter', "multiple"=>"multiple" ]) !!}</td>
                <td>{!! Form::input('text', 'card_number', null, ['class' => 'form-control']) !!}</td>
            </tr>
        </table>
        <button type="submit" class="btn btn-default" >Найти</button>
    {!! Form::close() !!}

</div>