@section('head')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
@stop

@section('scripts')
<script src="{{ URL::asset('/js/bankcard/jquery.maskedinput.js') }}"></script>
<script src="{{ URL::asset('/js/bankcard/script.js') }}"></script>
<script src="{{ URL::asset('/js/support/script.js') }}"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@stop

@extends('app')

@extends('nav')

@section('content')
<style>
    .table{
    font-size: 12px;
    }
    th {
    background-color: #d3d3d3;
    }
</style>

<div id="addbankModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="addbankModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="btn close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="addbankModalLabel">Добавить новый банк/БИК</h3>
            </div>
            <div class="modal-body">
                {!! Form::select('id_bank', $banks_list, null, ['id'=>'new_banks_list_select', 'class' => 'form-control' ]) !!}
                {!! Form::input('text', 'new_bank_name', null, ['id'=>'new_bank_name', 'placeholder'=>'Если необходимо добавить новый банк - укажите его имя', 'class' => 'form-control']) !!}
                {!! Form::input('text', 'new_bik_name', null, ['id'=>'new_bik_name', 'placeholder'=>'Если необходимо добавить новый БИК - укажите номер', 'class' => 'form-control']) !!}
                <p>
                    Если Вам необходимо добавить новый банк, укажите его наименование в поле. Если Вам необходимо добавить новый БИК к банку - выберите нужный банк из списка
                    и укажите БИК в поле ввода БИКа
                </p>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                <button class="btn btn-primary addNewBankBtn">Сохранить</button>
            </div>
        </div>
    </div>
</div>

<div style="zpadding: 40px;" id="table_data" class="container-fluid">

    <h1 class="text-center">{{ $app_title = 'Список банковских карт' }}</h1>

    @include('bankcard.filter')


    <a href="#" onclick="$('#addbankModal').modal('show'); return false;" class="btn">Добавить новый банк / БИК</a>

    <div>
        <center>{!! $table->render() !!}</center>

        <table class="table">
            <thead>
                <tr>
                    <td>{!! Form::checkbox('row_selector', null, null, ['onclick'=>'if(this.checked) {$(".rowCheckBox").prop("checked", true);} else {$(".rowCheckBox").prop("checked", false);}' ]) !!}</td>
                    <td>ID анкеты</td>
                    <td>ФИО</td>
                    <td>Объект</td>
                    <td>Статус сотрудника</td>
                    <td>№ карты</td>
                    <td>Расчетный счет карты</td>
                    <td>Статус</td>
                    <td>Срок действия</td>
                    <td>Банк</td>
                    <td>БИК</td>

                    <td>Действие</td>
                </tr>
            </thead>
            <tbody>
            {!! Form::open(array('url' => '/bankcardUpdate')) !!}
                <?php foreach($table as $data){ ?>
                    <tr>
                        <td>{!! Form::checkbox('row[]', null, null, ['data-id'=>$data->id_anketa, 'class'=>'rowCheckBox' ]) !!}</td>
                        <td><?=$data->id_anketa;?></td>
                        <td>
                        {!! Form::input('text', 'lastname', $data->lastname, ['placeholder'=>'Фамилия', 'data-feeld'=>'lastname', 'data-table'=>'pers_main', 'class'=>'rowData', 'style'=>"width:100px;" ]) !!}
                        {!! Form::input('text', 'firstname', $data->firstname, ['placeholder'=>'Имя', 'data-feeld'=>'firstname', 'data-table'=>'pers_main', 'class'=>'rowData', 'style'=>"width:100px;" ]) !!}
                        {!! Form::input('text', 'patronymic', $data->patronymic, ['placeholder'=>'Отчество', 'data-feeld'=>'patronymic', 'data-table'=>'pers_main', 'class'=>'rowData', 'style'=>"width:100px;" ]) !!}
                        </td>
                        <td>
                        <?php /*
                        {!! Form::select('object', $object, (int)$data->object, ['data-table'=>'pers_status', 'data-feeld'=>'object', 'class'=>'rowData' ]); !!}
                        */ ?>
                        <?=((isset($object[(int)$data->object]))?$object[(int)$data->object]:'');?>
                        </td>
                        <td>
                        <?php /*
                        {!! Form::select('pers_status_name', $pers_status_list, $data->pers_status_name, ['data-table'=>'pers_status', 'data-feeld'=>'status_name', 'class'=>'rowData' ]); !!}
                        */ ?>
                        <?=((isset($pers_status_list[(int)$data->pers_status_name]))?$pers_status_list[(int)$data->pers_status_name]:'');?>
                        </td>
                        <td>
                        {!! Form::input('text', 'card_number', $data->card_number, ['placeholder'=>'Номер карты', 'data-feeld'=>'card_number',  'data-table'=>'pers_bank', 'class'=>'rowData', 'style'=>"width:110px;" ]) !!}
                        </td>
                        <td>
                        {!! Form::input('text', 'account', $data->account, ['placeholder'=>'Расчетный счет карты', 'data-feeld'=>'account',  'data-table'=>'pers_bank', 'class'=>'rowData', 'style'=>"width:110px;" ]) !!}
                        </td>
                        <td>
                        {!! Form::select('card_status', $cardStatus, (int)$data->card_status, ['data-table'=>'pers_bank', 'data-feeld'=>'card_status', 'class'=>'rowData', 'style'=>"width:130px;" ]); !!}
                        </td>
                        <td>
                        {!! Form::input('text', 'card_expire', $data->card_expire, ['placeholder'=>'Срок действия', 'data-feeld'=>'card_expire', 'data-table'=>'pers_bank', 'class'=>'card_expire rowData', 'style'=>"width:90px;" ]) !!}
                        </td>
                        <td>
                        {!! Form::select('bank_name', $banks_list, (int)$data->bank_name, ['data-table'=>'pers_bank', 'data-feeld'=>'bank_name', 'data-values'=>(int)$data->bank_name, 'class'=>'rowData bank_name_select' ]); !!}
                        </td>
                        <td>
                        <select class="rowData bank_bik_name_select" name="bik" data-feeld="bik" data-table="dir_banks_bik" style="width: 120px;" >
                            <option value="0"></option>
                            <?php foreach($dir_banks_bik as $data_bik){ ?>
                                <option value="<?=$data_bik->id;?>" class="bank_<?=$data_bik->dir_banks_id;?>" <?=$data_bik->id==$data->bik?'selected':'';?> ><?=$data_bik->bik;?></option>
                            <?php } ?>
                        </select>
                        <?php /* {!! Form::select('bik', $bik_list, $data->bik, ['data-table'=>'dir_banks_bik', 'data-feeld'=>'bik', 'class'=>'rowData' ]); !!} */ ?>
                        </td>
                        <td><a href="#" class="btn btn-xs btn-primary applyRow" data-id=<?=$data->id_anketa;?> >Применить</a></td>
                    </tr>
                <?php } ?>
            {!! Form::close() !!}
            </tbody>
        </table>

        <center>{!! $table->render() !!}</center>


        <div style="padding: 20px; border-radius: 5px; background:  rgb(192, 192, 192); " class="row">
            <div class="col-md-5 col-lg-2">
                {!! Form::select('card_status', $cardStatus, null, ['id'=>'status_to_all', 'class' => 'form-control', 'placeholder' => 'Статус']) !!}
                <a href="#" class="applyToSelectRows btn btn-primary">Применить к отмеченым</a>
            </div>
        </div>


    </div>








@endsection