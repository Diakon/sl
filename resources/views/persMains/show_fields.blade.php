<!-- Id Anketa Field -->
<div class="form-group">
    {!! Form::label('id_anketa', 'Id Anketa:') !!}
    <p>{!! $persMain->id_anketa !!}</p>
</div>

<!-- Old Id Field -->
<div class="form-group">
    {!! Form::label('old_id', 'Old Id:') !!}
    <p>{!! $persMain->old_id !!}</p>
</div>

<!-- Create Date Field -->
<div class="form-group">
    {!! Form::label('create_date', 'Create Date:') !!}
    <p>{!! $persMain->create_date !!}</p>
</div>

<!-- Dog Num Field -->
<div class="form-group">
    {!! Form::label('dog_num', 'Dog Num:') !!}
    <p>{!! $persMain->dog_num !!}</p>
</div>

<!-- Meet Date Field -->
<div class="form-group">
    {!! Form::label('meet_date', 'Meet Date:') !!}
    <p>{!! $persMain->meet_date !!}</p>
</div>

<!-- Firstname Field -->
<div class="form-group">
    {!! Form::label('firstname', 'Firstname:') !!}
    <p>{!! $persMain->firstname !!}</p>
</div>

<!-- Lastname Field -->
<div class="form-group">
    {!! Form::label('lastname', 'Lastname:') !!}
    <p>{!! $persMain->lastname !!}</p>
</div>

<!-- Patronymic Field -->
<div class="form-group">
    {!! Form::label('patronymic', 'Patronymic:') !!}
    <p>{!! $persMain->patronymic !!}</p>
</div>

<!-- Nation Field -->
<div class="form-group">
    {!! Form::label('nation', 'Nation:') !!}
    <p>{!! $persMain->nation !!}</p>
</div>

<!-- Birth Date Field -->
<div class="form-group">
    {!! Form::label('birth_date', 'Birth Date:') !!}
    <p>{!! $persMain->birth_date !!}</p>
</div>

<!-- Birth Place Field -->
<div class="form-group">
    {!! Form::label('birth_place', 'Birth Place:') !!}
    <p>{!! $persMain->birth_place !!}</p>
</div>

<!-- Age Field -->
<div class="form-group">
    {!! Form::label('age', 'Age:') !!}
    <p>{!! $persMain->age !!}</p>
</div>

<!-- Gender Field -->
<div class="form-group">
    {!! Form::label('gender', 'Gender:') !!}
    <p>{!! $persMain->gender !!}</p>
</div>

<!-- Family Status Field -->
<div class="form-group">
    {!! Form::label('family_status', 'Family Status:') !!}
    <p>{!! $persMain->family_status !!}</p>
</div>

<!-- Education Field -->
<div class="form-group">
    {!! Form::label('education', 'Education:') !!}
    <p>{!! $persMain->education !!}</p>
</div>

<!-- Speciality Field -->
<div class="form-group">
    {!! Form::label('speciality', 'Speciality:') !!}
    <p>{!! $persMain->speciality !!}</p>
</div>

<!-- Find With Field -->
<div class="form-group">
    {!! Form::label('find_with', 'Find With:') !!}
    <p>{!! $persMain->find_with !!}</p>
</div>

<!-- Subdivision Field -->
<div class="form-group">
    {!! Form::label('subdivision', 'Subdivision:') !!}
    <p>{!! $persMain->subdivision !!}</p>
</div>

<!-- Criminal Field -->
<div class="form-group">
    {!! Form::label('criminal', 'Criminal:') !!}
    <p>{!! $persMain->criminal !!}</p>
</div>

<!-- To Army Field -->
<div class="form-group">
    {!! Form::label('to_army', 'To Army:') !!}
    <p>{!! $persMain->to_army !!}</p>
</div>

<!-- In Army Field -->
<div class="form-group">
    {!! Form::label('in_army', 'In Army:') !!}
    <p>{!! $persMain->in_army !!}</p>
</div>

<!-- Pmo Field -->
<div class="form-group">
    {!! Form::label('pmo', 'Pmo:') !!}
    <p>{!! $persMain->pmo !!}</p>
</div>

<!-- Criminal State Field -->
<div class="form-group">
    {!! Form::label('criminal_state', 'Criminal State:') !!}
    <p>{!! $persMain->criminal_state !!}</p>
</div>

<!-- Criminal Status Field -->
<div class="form-group">
    {!! Form::label('criminal_status', 'Criminal Status:') !!}
    <p>{!! $persMain->criminal_status !!}</p>
</div>

<!-- Medical Type Field -->
<div class="form-group">
    {!! Form::label('medical_type', 'Medical Type:') !!}
    <p>{!! $persMain->medical_type !!}</p>
</div>

<!-- Medical Status Field -->
<div class="form-group">
    {!! Form::label('medical_status', 'Medical Status:') !!}
    <p>{!! $persMain->medical_status !!}</p>
</div>

<!-- Medical Expire Field -->
<div class="form-group">
    {!! Form::label('medical_expire', 'Medical Expire:') !!}
    <p>{!! $persMain->medical_expire !!}</p>
</div>

<!-- Disabled Field -->
<div class="form-group">
    {!! Form::label('disabled', 'Disabled:') !!}
    <p>{!! $persMain->disabled !!}</p>
</div>

<!-- Disable Date Field -->
<div class="form-group">
    {!! Form::label('disable_date', 'Disable Date:') !!}
    <p>{!! $persMain->disable_date !!}</p>
</div>

<!-- Badge Number Field -->
<div class="form-group">
    {!! Form::label('badge_number', 'Badge Number:') !!}
    <p>{!! $persMain->badge_number !!}</p>
</div>

<!-- No Money Field -->
<div class="form-group">
    {!! Form::label('no_money', 'No Money:') !!}
    <p>{!! $persMain->no_money !!}</p>
</div>

<!-- No Sim Field -->
<div class="form-group">
    {!! Form::label('no_sim', 'No Sim:') !!}
    <p>{!! $persMain->no_sim !!}</p>
</div>

