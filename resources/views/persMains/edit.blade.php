@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($persMain, ['route' => ['persMains.update', $persMain->id], 'method' => 'patch']) !!}

        @include('persMains.fields')

    {!! Form::close() !!}
</div>
@endsection
