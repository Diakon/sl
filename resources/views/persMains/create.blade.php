@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'persMains.store']) !!}

        @include('persMains.fields')

    {!! Form::close() !!}
</div>
@endsection
