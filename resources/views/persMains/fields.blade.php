<!--- Id Anketa Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('id_anketa', 'Id Anketa:') !!}
	{!! Form::number('id_anketa', null, ['class' => 'form-control']) !!}
</div>

<!--- Old Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('old_id', 'Old Id:') !!}
	{!! Form::number('old_id', null, ['class' => 'form-control']) !!}
</div>

<!--- Create Date Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('create_date', 'Create Date:') !!}
	{!! Form::date('create_date', null, ['class' => 'form-control']) !!}
</div>

<!--- Dog Num Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('dog_num', 'Dog Num:') !!}
	{!! Form::text('dog_num', null, ['class' => 'form-control']) !!}
</div>

<!--- Meet Date Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('meet_date', 'Meet Date:') !!}
	{!! Form::date('meet_date', null, ['class' => 'form-control']) !!}
</div>

<!--- Firstname Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('firstname', 'Firstname:') !!}
	{!! Form::text('firstname', null, ['class' => 'form-control']) !!}
</div>

<!--- Lastname Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('lastname', 'Lastname:') !!}
	{!! Form::text('lastname', null, ['class' => 'form-control']) !!}
</div>

<!--- Patronymic Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('patronymic', 'Patronymic:') !!}
	{!! Form::text('patronymic', null, ['class' => 'form-control']) !!}
</div>

<!--- Nation Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('nation', 'Nation:') !!}
	{!! Form::text('nation', null, ['class' => 'form-control']) !!}
</div>

<!--- Birth Date Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('birth_date', 'Birth Date:') !!}
	{!! Form::date('birth_date', null, ['class' => 'form-control']) !!}
</div>

<!--- Birth Place Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('birth_place', 'Birth Place:') !!}
	{!! Form::text('birth_place', null, ['class' => 'form-control']) !!}
</div>

<!--- Age Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('age', 'Age:') !!}
	{!! Form::text('age', null, ['class' => 'form-control']) !!}
</div>

<!--- Gender Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('gender', 'Gender:') !!}
	{!! Form::text('gender', null, ['class' => 'form-control']) !!}
</div>

<!--- Family Status Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('family_status', 'Family Status:') !!}
	{!! Form::text('family_status', null, ['class' => 'form-control']) !!}
</div>

<!--- Education Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('education', 'Education:') !!}
	{!! Form::text('education', null, ['class' => 'form-control']) !!}
</div>

<!--- Speciality Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('speciality', 'Speciality:') !!}
	{!! Form::text('speciality', null, ['class' => 'form-control']) !!}
</div>

<!--- Find With Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('find_with', 'Find With:') !!}
	{!! Form::text('find_with', null, ['class' => 'form-control']) !!}
</div>

<!--- Subdivision Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('subdivision', 'Subdivision:') !!}
	{!! Form::text('subdivision', null, ['class' => 'form-control']) !!}
</div>

<!--- Criminal Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('criminal', 'Criminal:') !!}
	{!! Form::text('criminal', null, ['class' => 'form-control']) !!}
</div>

<!--- To Army Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('to_army', 'To Army:') !!}
	{!! Form::text('to_army', null, ['class' => 'form-control']) !!}
</div>

<!--- In Army Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('in_army', 'In Army:') !!}
	{!! Form::text('in_army', null, ['class' => 'form-control']) !!}
</div>

<!--- Pmo Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('pmo', 'Pmo:') !!}
	{!! Form::text('pmo', null, ['class' => 'form-control']) !!}
</div>

<!--- Criminal State Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('criminal_state', 'Criminal State:') !!}
	{!! Form::text('criminal_state', null, ['class' => 'form-control']) !!}
</div>

<!--- Criminal Status Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('criminal_status', 'Criminal Status:') !!}
	{!! Form::text('criminal_status', null, ['class' => 'form-control']) !!}
</div>

<!--- Medical Type Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('medical_type', 'Medical Type:') !!}
	{!! Form::text('medical_type', null, ['class' => 'form-control']) !!}
</div>

<!--- Medical Status Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('medical_status', 'Medical Status:') !!}
	{!! Form::text('medical_status', null, ['class' => 'form-control']) !!}
</div>

<!--- Medical Expire Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('medical_expire', 'Medical Expire:') !!}
	{!! Form::date('medical_expire', null, ['class' => 'form-control']) !!}
</div>

<!--- Disabled Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('disabled', 'Disabled:') !!}
	{!! Form::text('disabled', null, ['class' => 'form-control']) !!}
</div>

<!--- Disable Date Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('disable_date', 'Disable Date:') !!}
	{!! Form::date('disable_date', null, ['class' => 'form-control']) !!}
</div>

<!--- Badge Number Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('badge_number', 'Badge Number:') !!}
	{!! Form::number('badge_number', null, ['class' => 'form-control']) !!}
</div>

<!--- No Money Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('no_money', 'No Money:') !!}
	{!! Form::text('no_money', null, ['class' => 'form-control']) !!}
</div>

<!--- No Sim Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('no_sim', 'No Sim:') !!}
	{!! Form::text('no_sim', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
