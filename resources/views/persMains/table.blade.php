<table class="table">
    <thead>
    <th>Id Anketa</th>
			<th>Old Id</th>
			<th>Create Date</th>
			<th>Dog Num</th>
			<th>Meet Date</th>
			<th>Firstname</th>
			<th>Lastname</th>
			<th>Patronymic</th>
			<th>Nation</th>
			<th>Birth Date</th>
			<th>Birth Place</th>
			<th>Age</th>
			<th>Gender</th>
			<th>Family Status</th>
			<th>Education</th>
			<th>Speciality</th>
			<th>Find With</th>
			<th>Subdivision</th>
			<th>Criminal</th>
			<th>To Army</th>
			<th>In Army</th>
			<th>Pmo</th>
			<th>Criminal State</th>
			<th>Criminal Status</th>
			<th>Medical Type</th>
			<th>Medical Status</th>
			<th>Medical Expire</th>
			<th>Disabled</th>
			<th>Disable Date</th>
			<th>Badge Number</th>
			<th>No Money</th>
			<th>No Sim</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($persMains as $persMain)
        <tr>
            <td>{!! $persMain->id_anketa !!}</td>
			<td>{!! $persMain->old_id !!}</td>
			<td>{!! $persMain->create_date !!}</td>
			<td>{!! $persMain->dog_num !!}</td>
			<td>{!! $persMain->meet_date !!}</td>
			<td>{!! $persMain->firstname !!}</td>
			<td>{!! $persMain->lastname !!}</td>
			<td>{!! $persMain->patronymic !!}</td>
			<td>{!! $persMain->nation !!}</td>
			<td>{!! $persMain->birth_date !!}</td>
			<td>{!! $persMain->birth_place !!}</td>
			<td>{!! $persMain->age !!}</td>
			<td>{!! $persMain->gender !!}</td>
			<td>{!! $persMain->family_status !!}</td>
			<td>{!! $persMain->education !!}</td>
			<td>{!! $persMain->speciality !!}</td>
			<td>{!! $persMain->find_with !!}</td>
			<td>{!! $persMain->subdivision !!}</td>
			<td>{!! $persMain->criminal !!}</td>
			<td>{!! $persMain->to_army !!}</td>
			<td>{!! $persMain->in_army !!}</td>
			<td>{!! $persMain->pmo !!}</td>
			<td>{!! $persMain->criminal_state !!}</td>
			<td>{!! $persMain->criminal_status !!}</td>
			<td>{!! $persMain->medical_type !!}</td>
			<td>{!! $persMain->medical_status !!}</td>
			<td>{!! $persMain->medical_expire !!}</td>
			<td>{!! $persMain->disabled !!}</td>
			<td>{!! $persMain->disable_date !!}</td>
			<td>{!! $persMain->badge_number !!}</td>
			<td>{!! $persMain->no_money !!}</td>
			<td>{!! $persMain->no_sim !!}</td>
            <td>
                <a href="{!! route('persMains.edit', [$persMain->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('persMains.delete', [$persMain->id]) !!}" onclick="return confirm('Are you sure wants to delete this PersMain?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
