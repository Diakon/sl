@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">PersMains</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('persMains.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($persMains->isEmpty())
                <div class="well text-center">No PersMains found.</div>
            @else
                @include('persMains.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $persMains])


    </div>
@endsection