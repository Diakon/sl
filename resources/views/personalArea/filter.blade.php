<a href="#" onclick="$('#filterBlock').slideToggle(); return false;" class="btn btn-info" style="margin-bottom: 10px;">Фильтр</a>
<div id="filterBlock" style="display: none; background: #c0c0c0; border-radius: 5px; padding: 10px; margin-bottom: 10px;">

    {!! Form::open(array('url' => '/personal_area')) !!}


    {!! Form::input('text', 's_pr_from', ((isset($param_filter['s_pr_from']) && !empty($param_filter['s_pr_from']))?(date('Y-m-d',strtotime($param_filter['s_pr_from']))):''), ['class'=>'datepicker', 'placeholder'=>'Период с']) !!}
    {!! Form::input('text', 's_pr_to', ((isset($param_filter['s_pr_to']) && !empty($param_filter['s_pr_to']))?(date('Y-m-d',strtotime($param_filter['s_pr_to']))):''), ['class'=>'datepicker', 'placeholder'=>'Период по']) !!}

    <BR><button type="submit" onclick="setCookie();" >Готово</button>

    {!! Form::close() !!}

</div>