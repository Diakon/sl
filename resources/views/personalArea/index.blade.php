@section('head')
<style>
    table.mycalls_table {
        border: 2px solid black;
        border-collapse: collapse;
        max-width: 40%;
        min-width: 300px;
    }
    table.mycalls_table td {
        border: 1px solid black;
    }
    table.anketahistory_table {
        border: 2px solid black;
        border-collapse: collapse;
        max-width: 80%;
        min-width: 700px;
    }
    table.anketahistory_table th {
        background-color: #d3d3d3;
        border: 1px solid black;
        font-family: times,serif;
        font-size: 14px;
        padding: 7px;
    }
    table.anketahistory_table {
        border-collapse: collapse;
    }
    table.anketahistory_table td {
        border: 1px solid black;
    }
    table.customer_table {
        border: 2px solid black;
        border-collapse: collapse;
        margin-left: 10px;
        max-width: 80%;
        min-width: 1125px;
    }
    table.customer_table {
        border-collapse: collapse;
    }
    table.customer_table th {
        background-color: #d3d3d3;
        border: 1px solid black;
        font-family: times,serif;
        font-size: 14px;
        padding: 7px;
    }
    table.customer_table td {
        border: 1px solid black;
        text-align: center;
    }
    .filter {
        clear: both;
    }
</style>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
@stop
@section('scripts')
    <script src="{{ URL::asset('/js/support/script.js') }}"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
@stop


@extends('app')

@extends('nav')

@section('content')


    <div class="container">

        <div style="float: left">
            <h3>Мой номер телефона: </h3>
            <div style="float: left"><img width="100px" height="100px" src="/images/phone-icon-small.jpg"></div>
            <div style="float: left"><p style="font-size: 58px; position:absolute; margin-top:10px;"><?=$user->myextension;?></p></div>

            <div style="padding-top: 100px;">
                <h3>Сегодня я поставил статусы:</h3>
                <?php if ($soiskateli_count!=0) { ?>
                    <table class="mycalls_table">
                        <tr><td>Придет <?=$when_com;?>:</td><td><?=$soiskateli_count;?> чел</td></tr>
                        <tr>
                            <td></td>
                            <td>
                            <?php foreach ($soiskateli as $data) {?>
                                <a href="/edit/update/<?=$soiskateli->id;?>"><?=$soiskateli->id;?></a><br>
                            <?php } ?>
                            </td>
                        </tr>
                    </table>
                <?php } ?>

                <table class="mycalls_table">
                    <?php
                        $i = 0 ;
                        foreach ($anketa_history as $data){
                    ?>
                            <tr>
                                <td align=right>
                                    <b>
                                        <span style="color:<?=(($data->status_set == 12)?('#66CCFF'):('#000'));?>" >
                                            <?=\DB::table('statustable')->where('status_id', $data->status_set)->pluck('status_name');?>
                                        </span>
                                    </b>
                                </td>
                                <td width=50 align=center>
                                    <span style="color:<?=(($data->status_set == 12)?('#66CCFF'):('#000'));?>" >
                                        <?=$data->count_history;?> шт
                                    </span>
                                </td>
                            </tr>
                    <?php $i = $i + $data->count_history; } ?>
                        <tr>
                            <td align=right><b>Всего статусов сегодня:</b></td>
                            <td width=50 align=center><?=$i;?> шт</td>
                        </tr>
                </table>



            </div>



        </div>

        <div style="float: left; margin-left: 150px;">
            <h3>Сегодня я сделал звонков: </h3>

            <p>Входящих звонков:</p>
            <p>Исходящих звонков:</p>
            <table class="mycalls_table">
                <tbody>
                <tr>
                    <td></td>
                    <td>Кол-во</td>
                    <td>Общее время**</td>
                    <td>Чистое время</td>
                </tr>
                <?php
                    $i = 0;
                    $sum_minut = 0;
                    foreach ($calls_inc as $data){
                        $i = $i + $data->count_row;
                        $data->sum_duration = round($data->sum_duration / 60);
                        $data->sum_billsec = round($data->sum_billsec / 60);
                        $sum_minut = $sum_minut + $data->sum_duration;
                ?>
                <tr>
                    <td align=right <?=(($data->disposition == 'NO ANSWER')?('bgcolor="lime"'):(''));?>><b><?=$crd_status[$data->disposition];?></b></td>
                    <td width=50 align=center <?=(($data->disposition == 'NO ANSWER')?('bgcolor="lime"'):(''));?>><?=$data->count_row;?> шт</td>
                    <td width=50 align=center <?=(($data->disposition == 'NO ANSWER')?('bgcolor="lime"'):(''));?>><?=$data->sum_duration;?> мин</td>
                    <td width=50 align=center <?=(($data->disposition == 'NO ANSWER')?('bgcolor="lime"'):(''));?>><?=$data->sum_billsec;?> мин</td>
                </tr>
                <?php
                    }
                ?>
                </tbody>
            </table>
            <br>Всего входящих звонков: <?=$i;?>шт, <?=$sum_minut;?>мин<br><font color="lime">*Входящие НЕ отвеченные - НЕ считаются</font>';







            <p style="padding-top: 40px;">Исходящих звонков:</p>
            <table class="mycalls_table">
                <tbody>
                <tr>
                    <td></td>
                    <td>Кол-во</td>
                    <td>Общее время**</td>
                    <td>Чистое время</td>
                </tr>
                <?php
                    $i = 0;
                    $sum_minut = 0;
                    foreach ($calls_out as $data) {
                        $i = $i + $data->count_row;
                        $data->sum_duration = round($data->sum_duration / 60);
                        $data->sum_billsec = round($data->sum_billsec / 60);
                        $sum_minut = $sum_minut + $data->sum_duration;
                        ?>
                        <tr>
                            <td align=right><b><?=$crd_status[$data->disposition];?></b></td>
                            <td width=50 align=center><?=$data->count_row;?> шт</td>
                            <td width=50 align=center><?=$data->sum_duration;?> мин</td>
                            <td width=50 align=center><?=$data->sum_billsec;?> мин</td>
                        </tr>
                        <?php
                    }
                ?>
                </tbody>
            </table>
            <br>Всего исходящих звонков: <?=$i;?> шт, <?=$sum_minut;?> мин
        </div>



        <div class="filter">
            <HR>
            @include('personalArea.filter')
        </div>

        <div style="clear: both; padding-top: 20px;">
            <center>
                <h3>Моя история за <?=$period_history;?> дней:</h3>
                <table class="customer_table">
                    <tbody>
                    <tr>
                        <th>
                            <b>id</b>
                        </th>
                        <th>
                            <b>Источник</b>
                        </th>
                        <th>
                            <b>Город</b>
                        </th>
                        <th style="min-width: 70;">
                            <b>Заявка создана</b>
                        </th>
                        <th style="min-width: 70;">
                            <b>Заявка изменена</b>
                        </th>
                        <th>
                            <b>Статус сотрудника</b>
                        </th>
                        <th>
                            <b>Пол</b>
                        </th>
                        <th>
                            <b>Вакансия</b>
                        </th>
                        <th>
                            <b>Телефонный номер</b>
                        </th>
                        <th>
                            <b>Фамилия</b>
                        </th>
                        <th>
                            <b>Возраст</b>
                        </th>
                        <th>
                            <b>Гражданство</b>
                        </th>
                        <th style="min-width: 70;">
                            <b>Когда приедет (мы)</b>
                        </th>
                        <th>
                            <b>Кем работал</b>
                        </th>
                        <th>
                            <b>Комментарий</b>
                        </th>
                    </tr>
                    <?php foreach ($tabel_history_14 as $data) {?>
                    <tr  bgcolor="<?=((isset($customer_status_array[$data->customer_status]))?($customer_status_array[$data->customer_status]['color']):('FFFF99'));?>">
                        <td><?=$data->id;?></td>
                        <td><?=$data->searchsource;?></td>
                        <td><?=$data->city;?></td>
                        <td><?=$data->created_when;?></td>
                        <td><?=$data->modified_when;?></td>
                        <td><?=((isset($customer_status_array[$data->customer_status]))?($customer_status_array[$data->customer_status]['name']):('не определен'));?></td>
                        <td><?=(($data->gender==0)?('Ж'):('М'));?></td>
                        <td><?=$data->vacancy;?></td>
                        <td><?=$data->aon;?></td>
                        <td><?=$data->lastname;?></td>
                        <td><?=$data->age;?></td>
                        <td><?=$nation_array[$data->grazd];?></td>
                        <td><?=$data->kogda_pridet;?></td>
                        <td><?=$data->kemrabotal;?></td>
                        <td><?=$data->comments;?></td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </center>
        </div>


        <div style="clear: both; padding-top: 20px;">
            <center>
            <h3>Расшифровка записей на странице:</h3>
            <table class="customer_table">
                <tbody>
                    <tr>
                        <th>Дата</th>
                        <th align="center">ID соискателя</th>
                        <th>Сообщения</th>
                    </tr>
                    <?php foreach ($tabel_history as $data) {?>
                        <tr>
                            <td style="padding-left: 40px; padding-right: 40px;"><?=(date('d-m-Y H:i:s',strtotime($data->when)));?></td>
                            <td style="padding-left: 140px; padding-right: 140px;"><a href="/edit/update/<?=$data->id_ankety;?>"><?=$data->id_ankety;?></a></td>
                            <td><?=$data->message;?></td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
            </center>
        </div>

    </div>
@endsection