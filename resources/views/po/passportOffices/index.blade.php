@section('head')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/jquery-ui/jquery-ui-1.10.3.custom.min.css') }}" />
@stop

@extends('app')
@extends('nav')

@section('scripts')
    <script src="{{ URL::asset('/js/jquery-ui/jquery.ui.datepicker-ru.js') }}"></script>
    <script src="{{ URL::asset('/js/jquery-ui/jquery-ui-1.10.3.custom.min.js') }}"></script>
    <script src="{{ URL::asset('/js/jquery-ui/datepicker.js') }}"></script>
@stop

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Паспорта подразделений</h1>
        </div>

        <div class="row">
            @if(empty($passportOffices))
                <div class="well text-center">Нет записей.</div>
            @else
                @include('po.passportOffices.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $passportOffices])


    </div>
@endsection