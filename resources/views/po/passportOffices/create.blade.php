@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'passportOffices.store']) !!}

        @include('po.passportOffices.fields')

    {!! Form::close() !!}
</div>
@endsection
