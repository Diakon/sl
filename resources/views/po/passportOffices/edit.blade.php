@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($passportOffices, ['route' => ['passportOffices.update', $passportOffices->id], 'method' => 'patch']) !!}

        @include('passportOffices.fields')

    {!! Form::close() !!}
</div>
@endsection
