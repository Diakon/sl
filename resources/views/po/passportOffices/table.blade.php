<table style="margin-top: 50px;" class="table table-striped table-bordered">
    <thead>
            <th>№</th>
			<th>Сокр</th>
			<th>Адрес</th>
			<th>Ставка</th>
			<th>Заявка</th>
			<th>Должность</th>
            <th>Статус</th>
            <th>Дата запуска</th>
    </thead>
    <tbody>
    @foreach($passportOffices as $passportOffices)
        <tr>
            <td>{!! $passportOffices->id !!}</td>
            <?php
                $object = \DB::table('po_objects')->where('id', $passportOffices->object_id)->first();
                $vacans = \DB::table('po_job_vacancy')->where('id', $passportOffices->job_vacancy_id)->first();
            ?>
			<td>{!! $object->name !!}</td>
			<td>{!! $object->address !!}</td>
            <td>{!! $passportOffices->manage_costs !!}</td>
            <td>{!! $passportOffices->request !!}</td>
            <td>{!! $vacans->name !!}</td>
            <td></td>
			<td></td>
        </tr>
    @endforeach
    </tbody>
</table>

