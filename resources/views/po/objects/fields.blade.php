<!-- Name Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('name', 'Короткое название:') !!}
	{!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Address Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('address', 'Адрес:') !!}
	{!! Form::text('address', null, ['class' => 'form-control']) !!}
</div>

<!-- Dormitory Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('dormitory_id', 'Адресс общежития:') !!}
	{!! Form::select('dormitory_id', DB::table('domitory')->lists('name','id'), null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('is_active', 'Активен:') !!}
    {!! Form::hidden('is_active', 0) !!}
    {!! Form::checkbox('is_active', 1) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    {!! Html::linkRoute('po.objects.index', 'Отмена', [], ['class' => 'btn btn-default']) !!}
</div>
