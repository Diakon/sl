<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Короткое название:') !!}
    <p>{{ $object->name }}</p>
</div>

<!-- Address Field -->
<div class="form-group col-sm-6">
    {!! Form::label('address', 'Адрес:') !!}
    <p>{{ $object->address }}</p>
</div>

<!-- Dormitory Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('dormitory_id', 'Адрес общежития:') !!}
    <p>{{ $object->dormitory->name }}</p>
</div>

<!-- Created At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('created_at', 'Дата создания:') !!}
    <p>{{ $object->created_at }}</p>
</div>

<table class="table ztable-bordered table-condensed">
    <tbody>
        <tr>
            <td><a class="btn btn-default" href="{{ route('po.applications.index', $object->id) }}">Заявки ({{ $object->applications->count() }})</a></td>
            <td><a class="btn btn-default" href="{{ route('po.expense.index', $object->id) }}">Расходы </a></td>
        </tr>
        <tr>
            <td>
                <ul class="list-unstyled">
                    <li>Общее кол-во человек: {{ $object->applications->sum('request') }}</li>
                    <li>Общее кол-во часов: {{ $object->applications->sum('clock') }}</li>
                </ul>
            </td>
            <td>
                <ul class="list-unstyled">
                    @if(is_object($object->expense))
                        @foreach ($object->expense->toArray() as $key => $expense)
                            @if($expense > 0)
                                <li>{{ $object->expense->getAttributeNames()[$key] }}: {{ $expense*1 }} </li>
                            @endif
                        @endforeach
                    @endif
                </ul>
            </td>
        </tr>
    </tbody>
</table>







