<table class="table table-bordered table-striped">
    <thead>
        <th>Название</th>
        <th>Активен?</th>
        <th>Адресс</th>
        <th>Адресс общежития</th>
        <th style="width:70px;"></th>
    </thead>
    <tbody>
    @foreach($objects as $object)
        <tr>
    		<td>{{ $object->name }}</td>
            <td>@if($object->is_active)Да@elseНет@endif</td>
			<td>{{ $object->address }}</td>
			<td>{{ $object->dormitory->name }}</td>
			<td>
                <a title="Просмотр" href="{!! route('po.objects.show', [$object->id]) !!}"><i class="glyphicon glyphicon-eye-open"></i></a>
                <a title="Редактировать" href="{!! route('po.objects.edit', [$object->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a title="Удалить" href="{!! route('po.objects.delete', [$object->id]) !!}" onclick="return confirm('Are you sure wants to delete this PO/Object?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
