@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop
@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
@stop

@extends('app')

@extends('nav')

@section('content')
<div class="container">

    <h1 class="pull-center">{{ $app_title = 'Редактировать объект' }}</h1>

    @include('common.errors')

    {!! Form::model($object, ['route' => ['po.objects.update', $object->id], 'method' => 'patch']) !!}

        @include('po.objects.fields')

    {!! Form::close() !!}
</div>
@endsection
