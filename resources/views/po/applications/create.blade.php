@extends('app')

@extends('nav')

@section('content')
<div class="container">

            <h1 class="pull-center">{{ $app_title = sprintf('Добавить заявку (приходы) (%s)', $object->name) }}</h1>

            @include('common.errors')

            {!! Form::open(['route' => ['po.applications.store',$object->id]]) !!}

                @include('po.applications.fields')

            {!! Form::close() !!}
</div>
@endsection
