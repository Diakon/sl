@section('head')
@stop

@extends('app')

@extends('nav')

@section('content')
<div class="container">

            <h1 class="pull-center">{{ $app_title = sprintf('Редактировать заявку (приход) (%s)', $object->name) }}</h1>

            @include('common.errors')

            {!! Form::model($jobApplication, ['route' => ['po.applications.update', $jobApplication->object_id, $jobApplication->id], 'method' => 'patch']) !!}

                @include('po.applications.fields')

            {!! Form::close() !!}
</div>
@endsection
