@section('head')
@stop
@section('scripts')
@stop


@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">{{ $app_title="Список заявок (приходов)" }}</h1>
            @if (isset($object->id))
                <div class="pull-right">
                    <a class="btn btn-default" style="margin-top: 25px" href="{!! route('po.objects.show', $object->id) !!}">Перейти к паспорту </a>
                    <a class="btn btn-primary" style="margin-top: 25px" href="{!! route('po.applications.create', $object->id) !!}">Добавить новую заявку</a>
                </div>
            @endif
        </div>

        <div class="row">
            @if($jobApplications->isEmpty())
                <div class="well text-center">Записи не найдены.</div>
            @else
                @include('po.applications.table')
            @endif
        </div>

    </div>
@endsection