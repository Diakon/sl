<table class="table table-bordered table-striped">
    <thead>
		<th>Объект</th>
		<th>Вакансия</th>
		<th>Управленчиские расходы</th>
		<th>Кол-во человек</th>
		<th>ВЗ сотрудника</th>
		<th>Нам платят без НДС</th>
		<th>График работы</th>
		<th>Заявка</th>
		<th width="50px">Действия</th>
    </thead>
    <tbody>
    @foreach($jobApplications as $jobApplication)
        <tr>
			<td>
				<a href='{{ Route('po.objects.show', $jobApplication->object_id) }}'>
					<samp>{{ $jobApplication->object->name }}</samp> <code>{!! $jobApplication->object_id !!}</code>
				</a>
			</td>
            <td><?=\DB::table('po_job_vacancy')->where('id', $jobApplication->job_vacancy_id)->first()->name;?></td>
			<td><?=(($jobApplication->manage_costs)?('Да'):('Нет'));?></td>
			<td>{!! $jobApplication->request !!}</td>
			<td>{!! $jobApplication->vz_employee !!}</td>
			<td>{!! $jobApplication->payment_us_no_nds !!}</td>
			<td>{!! $jobApplication->schedule_day !!} / {!! $jobApplication->schedule_month !!}</td>
			<td>{!! $jobApplication->clock !!} / {!! $jobApplication->payed !!}</td>
            <td>
			    <a href="{!! route('po.applications.edit', [$jobApplication->object_id, $jobApplication->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('po.applications.delete', [$jobApplication->object_id, $jobApplication->id]) !!}" onclick="return confirm('Are you sure wants to delete this JobApplication?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
