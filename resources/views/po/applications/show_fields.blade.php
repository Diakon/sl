<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $jobApplication->id !!}</p>
</div>

<!-- Job Vacancy Id Field -->
<div class="form-group">
    {!! Form::label('job_vacancy_id', 'Job Vacancy Id:') !!}
    <p>{!! $jobApplication->job_vacancy_id !!}</p>
</div>

<!-- Author Id Field -->
<div class="form-group">
    {!! Form::label('author_id', 'Author Id:') !!}
    <p>{!! $jobApplication->author_id !!}</p>
</div>

<!-- Manage Costs Field -->
<div class="form-group">
    {!! Form::label('manage_costs', 'Manage Costs:') !!}
    <p>{!! $jobApplication->manage_costs !!}</p>
</div>

<!-- Request Field -->
<div class="form-group">
    {!! Form::label('request', 'Request:') !!}
    <p>{!! $jobApplication->request !!}</p>
</div>

<!-- Vz Employee Field -->
<div class="form-group">
    {!! Form::label('vz_employee', 'Vz Employee:') !!}
    <p>{!! $jobApplication->vz_employee !!}</p>
</div>

<!-- Payment Us No Nds Field -->
<div class="form-group">
    {!! Form::label('payment_us_no_nds', 'Payment Us No Nds:') !!}
    <p>{!! $jobApplication->payment_us_no_nds !!}</p>
</div>

<!-- Schedule Day Field -->
<div class="form-group">
    {!! Form::label('schedule_day', 'Schedule Day:') !!}
    <p>{!! $jobApplication->schedule_day !!}</p>
</div>

<!-- Schedule Week Field -->
<div class="form-group">
    {!! Form::label('schedule_month', 'Schedule month:') !!}
    <p>{!! $jobApplication->schedule_month !!}</p>
</div>

<!-- Clock Field -->
<div class="form-group">
    {!! Form::label('clock', 'Clock:') !!}
    <p>{!! $jobApplication->clock !!}</p>
</div>

<!-- Payed Field -->
<div class="form-group">
    {!! Form::label('payed', 'Payed:') !!}
    <p>{!! $jobApplication->payed !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $jobApplication->updated_at !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $jobApplication->created_at !!}</p>
</div>

