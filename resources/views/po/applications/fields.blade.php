<style>
    .table-vcenter th {
        vertical-align: middle!important;
        text-align: center!important;
    }
</style>

<script type="application/javascript">
    $(function () {
        $('input[name="schedule_day"], input[name="schedule_month"], input[name="vz_employee"]').on('change', function () {
            var day = $('input[name="schedule_day"]').val();
            var month = $('input[name="schedule_month"]').val();
            var employee = $('input[name="vz_employee"]').val();
            $('input[name="clock"]').val(day * month);
            $('input[name="payed"]').val(Math.round(day * month * employee * 100) / 100);

        });
    });
</script>

<table class="table table-striped table-bordered table-condensed table-vcenter">
    <thead>
    <tr>
        <th rowspan="2">Вакансия</th>
        <th rowspan="2" style="width:50px;">
            <div style="-webkit-writing-mode: vertical-lr; writing-mode:tb-rl;">
                Управленческие<br/>расходы
            </div>
        </th>
        <th rowspan="2">Заявка (кол-во человек)</th>
        <th rowspan="2">ВЗ сотрудника (рублей)</th>
        <th rowspan="2">Нам платят без НДС (рублей)</th>
        <th class="text-center" colspan="2">График работы</th>
        <th class="text-center" colspan="2">Заявка (общее)</th>
    </tr>
    <tr>
        <th>В день (часов)</th>
        <th>В месяц (дней)</th>
        <th>Часов</th>
        <th>Рублей</th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td>{!! Form::select('job_vacancy_id', \App\Models\JobVacancy::lists(), null, ['class'=>'form-control']) !!}</td>
            <td class="text-center" style="vertical-align: middle!important;">
                {!! Form::hidden('manage_costs', 0) !!}
                {!! Form::checkbox('manage_costs', 1) !!}
            </td>
            <td>{!! Form::number('request', null, ['class'=>'form-control', 'min' => '1']) !!}</td>
            <td>{!! Form::number('vz_employee', null, ['class'=>'form-control', 'step' => "0.1", 'min' => '0']) !!}</td>
            <td>{!! Form::number('payment_us_no_nds', null, ['class'=>'form-control', 'step' => "0.01"]) !!}</td>
            <td>{!! Form::number('schedule_day', null, ['class'=>'form-control', 'min'=>"1", 'max'=>"24"]) !!}</td>
            <td>{!! Form::number('schedule_month', null, ['class'=>'form-control', 'min'=>"1", 'max'=>"31"]) !!}</td>
            <td>{!! Form::number('clock', null, ['class'=>'form-control', 'min'=>"1"]) !!}</td>
            <td>{!! Form::number('payed', null, ['class'=>'form-control', 'step' => "0.01", 'min' => '0']) !!}</td>
        </tr>
    </tbody>
</table>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('po.applications.index', $object->id) }}" class="btn btn-default">Вернуться</a>

    <a href="{{ route('po.vacancies.create') }}?uri={{ Request()->getRequestUri() }}" class='btn btn-default'>
        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Добавить вакансию в справочник
    </a>
</div>