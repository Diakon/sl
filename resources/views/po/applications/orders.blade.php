@section('head')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/js/support/select2/select2.css') }}" />
    <style>
        th, td {
            max-width: 100px;
            text-align: center;
        }
        input {
            max-width: 100px;
        }
    </style>
@stop
@section('scripts')
    <script>
        var row_id = 0;
        var template = '' +
                '<tr id = "row_%row_id%">'+
                '<td>{!! Form::select('Orders[job_vacancy_id][%row_id%]', $vakans, null, array('class'=>'order_select select2input')) !!}</td>'+
                '<td style="max-width: 110px;">{!! Form::checkbox('Orders[manage_costs][%row_id%]', null) !!}</td>'+
                '<td>{!! Form::input('text', 'Orders[request][%row_id%]', null, ['placeholder'=>'кол. человек']) !!}</td>'+
                '<td>{!! Form::input('text', 'Orders[vz_employee][%row_id%]', null, ['placeholder'=>'в рублях']) !!}</td>'+
                '<td>{!! Form::input('text', 'Orders[payment_us_no_nds][%row_id%]', null, ['placeholder'=>'в рублях', 'data-id'=>'%row_id%', 'class'=>'OrdersPayUsNoNDS']) !!}</td>'+
                '<td>{!! Form::input('text', 'Orders[schedule_day][%row_id%]', null, ['placeholder'=>'кол. часов в день']) !!}</td>'+
                '<td>{!! Form::input('text', 'Orders[schedule_month][%row_id%]', null, ['placeholder'=>'кол. дней в месяц']) !!}</td>'+
                '<td>{!! Form::input('text', 'Orders[clock][%row_id%]', null, ['placeholder'=>'в часах', 'data-id'=>'%row_id%', 'class'=>'OrdersHours']) !!}</td>'+
                '<td>{!! Form::input('text', 'Orders[payed][%row_id%]', null, ['placeholder'=>'в рублях', 'class'=>'OrdersRub']) !!}</td>'+
                '<td align="center"><a href="#" onclick="$(this).parent().parent().remove(); return false;" ><i class="glyphicon glyphicon-remove"></i></a></td>'+
        '</tr>';

        function createTemplate(){
            ++row_id;
            tmp_template = template.replace(/%row_id%/g, row_id);
            $('tbody').append(tmp_template);
            $('select').select2();
        }

        createTemplate();

        $('#addNewRow').on('click', function(){
            createTemplate();
            $('select').select2();
            return false;
        });

        $(document).on('keyup', '.OrdersHours, .OrdersPayUsNoNDS', function(){
            var id = $(this).data('id');
            var pay = $('#row_'+id).find('.OrdersPayUsNoNDS').val();
            var hour = $('#row_'+id).find('.OrdersHours').val();
            if (pay.length>0 && hour.length>0){
                var results = pay * hour;
                $('#row_'+id).find('.OrdersRub').val(results);
            }
        });

        $(document).on('keyup','input', function(){
            $(this).val($(this).val().replace(/[a-zA-Zа-яА-Я]/,""));
        });


    </script>
@stop
@extends('app')

@extends('nav')

@section('content')

    <div class="container" style="margin-left:40px;">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">{{ $app_title = "Экран `Заявка` (приходы)" }}</h1>
        </div>

        {!! Form::open(array('url' => '#')) !!}

        <div class="row">

            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th rowspan="2">Вакансия</th>
                        <th rowspan="2" style="max-width: 110px;">Управленчиские расходы</th>
                        <th rowspan="2">Заявка</th>
                        <th rowspan="2">ВЗ сотрудника</th>
                        <th rowspan="2">Нам платят без НДС</th>
                        <th colspan="2">График работы</th>
                        <th colspan="2">Заявка</th>
                        <th width="50px" rowspan="2">Действия</th>
                    </tr>
                    <tr>
                        <th>В день</th>
                        <th>В неделю</th>
                        <th>Часов</th>
                        <th>Рублей</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>

        <div class="row">
            <a href="#" id="addNewRow"><i class="glyphicon glyphicon-plus"><span style="position:absolute; margin-left:5px; margin-top:-2px; width:120px;">Добавить строку</span></i></a>
        </div>
        <HR>
        <div class="row">
            {!! Form::submit('Сохранить', array('class'=>'maintable btn btn-primary')  ) !!}
            <a href="{{ route('po.applications.index', $object->id) }}" class="btn btn-default">Вернуться</a>
        </div>
        {!! Form::close() !!}
    </div>
@endsection