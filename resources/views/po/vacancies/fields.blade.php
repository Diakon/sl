@if( Request()->has('uri') )
    {!! Form::hidden('uri', Request()->get('uri')) !!}
@endif
<div class="col-sm-6 col-lg-4">
    <!-- Name Field -->
    <div class="form-group">
        {!! Form::label('name', 'Вакансия:') !!}
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
    </div>

    @if (!isset($jobVacancy) or !$jobVacancy->childrens->count() )
        <div class="form-group">
            {!! Form::label('parent_id', 'Родительская категория:') !!}
            {!! Form::select('parent_id', $parents, null, ['class' => 'form-control', 'placeholder'=>'Родительская категория']) !!}
        </div>
    @endif
</div>

<div style="clear: both;"></div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
    @if( Request()->has('uri') )
        {!! Html::link(Request()->get('uri'), 'Отмена', ['class' => 'btn btn-default']) !!}
    @endif
    {!! Html::linkRoute('po.vacancies.index', 'Перейти к списку вакансий', [], ['class' => 'btn btn-default']) !!}
</div>


