@section('head')
@stop
@section('scripts')
@stop

@extends('app')

@extends('nav')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'po.vacancies.store']) !!}

        @include('po.vacancies.fields')

    {!! Form::close() !!}
</div>
@endsection
