@section('head')
@stop
@section('scripts')
@stop


@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Список вакансий</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('po.vacancies.create') !!}">Добавить вакансию</a>
        </div>

        <div class="row">
            @if($jobVacancies->isEmpty())
                <div class="well text-center">Вакансии не найдены.</div>
            @else
                @include('po.vacancies.table')
            @endif
        </div>


    </div>
@endsection