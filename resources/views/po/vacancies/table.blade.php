<table class="table">
    <thead>
            <th>№</th>
			<th>Вакансия</th>
			<th>Дата создания</th>
    <th width="50px">Действия</th>
    </thead>
    <tbody>
    @foreach($jobVacancies as $jobVacancy)
        <tr>
            <td>{{ $jobVacancy->id }}</td>
			<td><b>{{ $jobVacancy->name }}</b></td>
			<td>{{ $jobVacancy->created_at }}</td>
            <td>
                <a href="{!! route('po.vacancies.edit', [$jobVacancy->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('po.vacancies.delete', [$jobVacancy->id]) !!}" onclick="return confirm('Are you sure wants to delete this JobVacancy?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
        @foreach($jobVacancy->childrens as $children)
            <tr>
                <td>{{ $children->id }}</td>
                <td><i>&#8212; {{ $children->name }}</i></td>
                <td>{{ $children->created_at }}</td>
                <td>
                    <a href="{!! route('po.vacancies.edit', [$children->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                    <a href="{!! route('po.vacancies.delete', [$children->id]) !!}" onclick="return confirm('Are you sure wants to delete this JobVacancy?')"><i class="glyphicon glyphicon-remove"></i></a>
                </td>
            </tr>
        @endforeach
    @endforeach
    </tbody>
</table>