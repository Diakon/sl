@section('head')
@stop
@section('scripts')
@stop

@extends('app')

@extends('nav')
@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($jobVacancy, ['route' => ['po.vacancies.update', $jobVacancy->id], 'method' => 'patch']) !!}

        @include('po.vacancies.fields')

    {!! Form::close() !!}
</div>
@endsection
