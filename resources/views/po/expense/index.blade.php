@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop
@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
@stop

@extends('app')

@extends('nav')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-sm-offset-2 col-sm-8 col-lg-offset-2 col-lg-8">
                <h1 class="pull-center">{{ $app_title = sprintf('Операционные расходы (%s)', $object->name) }}</h1>

                @include('common.errors')

                {!! Form::model($expense, ['route' => ['po.expense.update', $object->id]]) !!}

                    @include('po.expense.fields')

                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection