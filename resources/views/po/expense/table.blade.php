<table class="table table-bordered table-striped">
    <thead>
    		<th>Объект</th>
			<th>Проживание</th>
			<th>Транспорт</th>
			<th>Питание</th>
			<th>МК</th>
			<th>УД</th>
			<th>Др. документы</th>
			<th style="width:50px">Действия</th>
    </thead>
    <tbody>
	@foreach($expenses as $expense)
        <tr>
            <td>
				<a href='{{ Route('po.objects.show', $expense->object_id) }}'>
					<samp>{{ $expense->object->name }}</samp> <code>{!! $expense->object_id !!}</code>
				</a>
			</td>
			<td>{{ $expense->dormitory_cost }}</td>
			<td>{{ $expense->transport_cost }}</td>
			<td>{{ $expense->food_cost }}</td>
			<td>{{ $expense->mk_cost }}</td>
			<td>{{ $expense->ud_cost }}</td>
			<td>{{ $expense->other_cost }}</td>
			<td>
			    <a href="{!! route('po.expense.index', [$expense->object_id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
