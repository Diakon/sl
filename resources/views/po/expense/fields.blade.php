    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-8">
        <div class="form-group col-xs-9">
            {!! Form::label('Кол-во человек / часов в заявках:') !!}
        </div>
        <div class="form-group col-xs-3">
            <p class="form-control-static">{{ $object->applications->sum('request') }} / {{ $object->applications->sum('clock') }}</p>
        </div>

        @foreach([
            'dormitory_cost',
            'transport_cost',
            'food_cost',
            'mk_cost',
            'ud_cost',
            'other_cost',
        ] as $cell)
            <div class="form-group col-xs-7">
                {!! Form::label($cell, \App\Models\PO\Expense::getAttributeNamesStatic()[$cell]) !!}
            </div>
            <div class="form-group col-xs-1">
                <div class="checkbox">
                    {!! Form::checkbox($cell.'_check', null, (isset($expense->$cell) && $expense->$cell>0?true:false)) !!}
                </div>
            </div>
            <div class="form-group col-xs-4">
                <div class="input-group">
                    {!! Form::text($cell, null, ['class' => 'form-control', 'readonly', 'step'=>"0.01"]) !!}
                    <span class="input-group-btn">
                        <button type="button" data-object_id="{{ $object->id }}" data-method="{{ str_replace('_cost','',$cell) }}" class="btn btn-default get-expense-ajax">
                            <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
                        </button>
                    </span>
                </div>
            </div>
        @endforeach

        <div class="form-group">
            {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
            <a href="{{ route('po.objects.show', $object->id) }}" class="btn btn-default">Вернуться</a>
            {!! Html::linkRoute('po.objects.index', 'Отмена', [], ['class' => 'btn btn-default']) !!}
        </div>
    </div>



    <script>
        $(function(){
            $('.get-expense-ajax').on('click', function() {
                var $obj = $(this);
                var object_id = $obj.data('object_id');
                var method_type = $obj.data('method');
                $.get('/api/v1/ko/object/' + object_id + '/expense/' + method_type, function( data ) {
                    if (typeof data.data != 'underfined') {
                        $obj.closest('.input-group').find('input').val(data.data);
                    }
                });
            });

            $('input[type="checkbox"]').on('click', func = function() {
                var input = $(this).closest(".form-group").next().find("input");
                var button = $(this).closest(".form-group").next().find("button");
                input.prop("readonly", !this.checked);
                button.prop("disabled", !this.checked);
                if (!this.checked) {
                    input.val('');
                } else {
                    input.focus();
                }
            }).each(func);
        });
    </script>