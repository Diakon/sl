@section('head')
@stop
@section('scripts')
@stop


@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Список операционных расходов</h1>
        </div>

        <div class="row">
            @if($expenses->isEmpty())
                <div class="well text-center">Записи не найдены.</div>
            @else
                @include('po.expense.table')
            @endif
        </div>

    </div>
@endsection