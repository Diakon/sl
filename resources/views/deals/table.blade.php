<table class="table table-bordered table-striped">
    <thead>
        <th>Название</th>
        <th>Тип</th>
        <th width="50px">Действия</th>
    </thead>
    <tbody>
    @foreach($deals as $deal)
        <tr>
            <td>{!! $deal->name !!}</td>
            <td><?= $types[$deal->type] ?></td>
            <td>
                <a href="{!! route('deals.edit', [$deal->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('deals.delete', [$deal->id]) !!}" onclick="return confirm('Are you sure wants to delete this Deal?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
