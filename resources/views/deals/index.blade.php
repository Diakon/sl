@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">{{ $app_title = 'Сделки' }}</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('deals.create') !!}">Создать новую</a>
        </div>

        <div class="row">
            @if($deals->isEmpty())
                <div class="well text-center">Не найдено ни 1 активной сделки.</div>
            @else
                @include('deals.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $deals])


    </div>
@endsection