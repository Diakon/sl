<!--- Name Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('name', 'Название:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('type', 'Тип:') !!}
    {!! Form::select('type', ['1'=>'Обычная','2'=>'Ошибка'], null, ['class' => 'form-control']) !!}
    
</div>



<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Сохранить', ['class' => 'btn btn-primary']) !!}
</div>
