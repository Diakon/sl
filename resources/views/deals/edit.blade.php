@extends('app')

@extends('nav')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($deal, ['route' => ['deals.update', $deal->id], 'method' => 'patch']) !!}

        @include('deals.fields')

    {!! Form::close() !!}
</div>
@endsection
