<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $app_title or 'Laravel' }} - {{ Config('app.url')}}</title>

    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/bootstrap/css/bootstrap.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/js/libs/select2/select2.css') }}"/>

    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>

    <![endif]-->
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <script>
        @if( Auth::check() )
        var mylogin = "{{ Auth()->user()->username }}";
        var myip = "{{ Request::ip() }}";
        @endif
    </script>

    @yield('head')

    {!! Html::script('//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js') !!}
    {!! Html::script('/js/jquery-ui/jquery.ui.datepicker-ru.js') !!}
    {!! Html::script('/js/libs/select2/select2.min.js') !!}
    {!! Html::script('/js/libs/select2/i18n/ru.js') !!}
    {!! Html::script('/js/socket-io/socket.io-1.3.7.js') !!}
    {!! Html::script('/bootstrap/js/bootstrap.min.js') !!}
    {!! Html::script('/js/core.js') !!}

    @if ( App::environment('production') )
        {!! Html::script('/js/asterisk/aster-client.js') !!}
    @endif


</head>
<body>
@yield('nav')

@yield('content')

    <!-- Scripts -->
@yield('scripts')

@if ( App::environment('production') )
    @yield('bottomnav')
@endif
</body>
</html>
