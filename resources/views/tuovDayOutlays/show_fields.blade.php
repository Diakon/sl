<!-- Subdivision Id Field -->
<div class="form-group">
    {!! Form::label('subdivision_id', 'Subdivision Id:') !!}
    <p>{!! $tuovDayOutlay->subdivision_id !!}</p>
</div>

<!-- Rate Field -->
<div class="form-group">
    {!! Form::label('rate', 'Rate:') !!}
    <p>{!! $tuovDayOutlay->rate !!}</p>
</div>

<!-- Date From Field -->
<div class="form-group">
    {!! Form::label('date_from', 'Date From:') !!}
    <p>{!! $tuovDayOutlay->date_from !!}</p>
</div>

<!-- Date To Field -->
<div class="form-group">
    {!! Form::label('date_to', 'Date To:') !!}
    <p>{!! $tuovDayOutlay->date_to !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $tuovDayOutlay->name !!}</p>
</div>

