@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
@stop



@extends('app')

@extends('nav')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Дорожные</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('tuovDayOutlays.create') !!}">Добавить новую запись</a>
        </div>

        <div class="row">
            @if($tuovDayOutlays->isEmpty())
                <div class="well text-center">Дорожные не найдены</div>
            @else
                @include('tuovDayOutlays.table')
            @endif
        </div>

        @include('common.paginate', ['records' => $tuovDayOutlays])


    </div>
@endsection
