@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
@stop

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
@stop



@extends('app')

@extends('nav')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($tuovDayOutlay, ['route' => ['tuovDayOutlays.update', $tuovDayOutlay->id], 'method' => 'patch']) !!}

        @include('tuovDayOutlays.fields')

    {!! Form::close() !!}
</div>
@endsection
