<!-- Subdivision Id Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('subdivision_id', 'Подразделение:') !!}
	{!! Form::select('subdivision_id', (new \App\Models\Subdivision)->getActiveSubdivisions(), ((isset($tuovDayOutlay->subdivision_id))?$tuovDayOutlay->subdivision_id:null),
    ['class' => 'form-control']) !!}
</div>

<!-- Rate Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('rate', 'Сумма:') !!}
	{!! Form::text('rate', null, ['class' => 'form-control']) !!}
</div>

<!-- Date From Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('date_from', 'Действует С:') !!}
	{!! Form::text('date_from', null, ['class' => 'form-control']) !!}
</div>

<!-- Date To Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('date_to', 'Действует По:') !!}
	{!! Form::text('date_to', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('name', 'Название:') !!}
	{!! Form::text('name', 'Дорожные', ['class' => 'form-control']) !!}
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
