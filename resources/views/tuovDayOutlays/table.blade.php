<table class="table">
    <thead>
        <th>Подразделение</th>
		<th>Ставка</th>
		<th>Действует С:</th>
		<th>Действует По:</th>
		<th>Название</th>
    <th width="50px">Action</th>
    </thead>
    <tbody>
    @foreach($tuovDayOutlays as $tuovDayOutlay)
        <tr>
            <td><?= (new \App\Models\Subdivision)->getActiveSubdivisions()[$tuovDayOutlay->subdivision_id] ?></td>
			<td>{!! $tuovDayOutlay->rate !!}</td>
			<td>{!! $tuovDayOutlay->date_from !!}</td>
			<td>{!! $tuovDayOutlay->date_to !!}</td>
			<td>{!! $tuovDayOutlay->name !!}</td>
            <td>
                <a href="{!! route('tuovDayOutlays.edit', [$tuovDayOutlay->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                <a href="{!! route('tuovDayOutlays.delete', [$tuovDayOutlay->id]) !!}" onclick="return confirm('Are you sure wants to delete this TuovDayOutlay?')"><i class="glyphicon glyphicon-remove"></i></a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
