@section('head')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/scheduler/style.css') }}" />
@stop

@section('scripts')
    <script src="{{ URL::asset('/js/scheduler/script.js') }}"></script>
    <script src="{{ URL::asset('/js/scheduler/calendar.js') }}"></script>
    <script src="{{ URL::asset('/js/scheduler/ajaxupload.min.js') }}"></script>
@stop

@extends('app')

@extends('nav')

@section('content')

<?php
    $form_users = [];
    foreach ($users as $key=>$value){
        $form_users[$key] = $value['fio'] . ((!empty($value['email']))?' [' . $value['email'] . ']':'');
    }
?>

<div id="userDownlist">
    {!! Form::open(['class' => 'form-inline']) !!}
        {!! Form::select('fromuser', $form_users, null,  ['class'=>'form-control', 'id' => 'selectUser']) !!}
        <a style="" href="#" class="btn btn-primary" id="selectUserBtn">Выбрать</a>
    {!! Form::close() !!}
</div>


<div id="userListTable" style="display: none">
    <b>Добавить подчиненность</b></br><hr style="margin-top:-1px; border: 1px solid black">

    <!-- Список пользователей -->
    <div class="feeld">
        <a href="#" id="addUsersListShow" >Пользователи</a>
        <table id="addUsersList" style="display: none;">
            <tr bgcolor="#DDDDDD"><th></th><th>Пользователи</th></tr>
            <?php
            foreach ( $users as $key=>$val ){
                echo '<tr><td><input class="addUsersChk" type="checkbox"  value="'.$key.'"></td><td>'.$val['username'].'</td></tr>';
            }
            ?>



            <tr><td colspan="2"><a href="#" id="addUsersListBtn"> Добавить отмеченые</a></td></tr>
        </table>

    </div>

    <!-- Список отделов -->
    <div class="feeld">
        <a href="#" id="addDepartamentListShow" >Отделы</a>
        <table id="addDepartamentList" style="display: none;">
            <tr bgcolor="#DDDDDD"><th></th><th>Отделы</th></tr>
            <?php
            foreach ( $department as $key=>$val ){
                echo '<tr><td><input class="addDepartamentChk" type="checkbox"  value="'.$key.'"></td><td>'.$val.'</td></tr>';
            }
            ?>
            <tr><td colspan="2" style="border:0px solid black;"><input type="radio"  name="type_order" value="1" style="margin-top:-2px;">Начальник
            <input type="radio" name="type_order" value="2" checked  style="margin-top:-2px;">Сотрудник</td></tr>
            <tr><td colspan="2" style="border:0px solid black;"><a href="#" id="addDepartamentListBtn"> Добавить отмеченые</td></a></tr>
        </table>

    </div>


</div>
@endsection