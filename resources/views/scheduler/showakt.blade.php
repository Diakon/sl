@section('head')
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/scheduler/style.css') }}" />
@stop

@section('scripts')
    <script src="{{ URL::asset('/js/scheduler/jquery-mask.min.js') }}"></script>
    <script src="{{ URL::asset('/js/scheduler/calendar.js') }}"></script>
    <script src="{{ URL::asset('/js/scheduler/ajaxupload.min.js') }}"></script>
    <script src="{{ URL::asset('/js/scheduler/script.js') }}"></script>
@stop

@extends('app')

@extends('nav')

@section('content')
<style>
    #addtasksForm table {
        border-collapse: collapse;
        margin: 10px;
        border:0px;
    }
    #addtasksForm table td, th {
        text-align: left;
        border: 0px;
        padding: 10px;
    }
    .filter {
        padding: 10px;
    }
    .comment_block {
        border-radius:3px;
        background: #eeeeee;
        width:500px;
        padding:10px;
        margin-bottom:10px;
        box-shadow: 0 0 5px rgba(0,0,0,0.5);
    }
    .comment_name {
        width:500px;
        margin-left:-10px;
        margin-top:-10px;
        border-radius:2px 2px 0 0;
        background:#cccccc;
        padding-left:20px;
    }


    .status-block {
        width: 20px;
        height: 20px;
        background: #eeeeee;
        font-size: 9px;
        border:1px solid #9d9d9d;
        float: left;
    }

    .status20{
        background: #f0660a;
    }
    .status40{
        background: #f6b70d;
    }
    .status60{
        background: #fcff0e;
    }
    .status80{
        background: #c9fa54;
    }
    .status100{
        background: #16e800;
    }

</style>

<span style="margin-left: 10px; font-size:20px;">
Акт №<?php echo $act_data['act']->id; ?> от <?php echo (date("d-m-Y H:i:s", strtotime($act_data['act']->created_at))); ?>
</span>

<table>
    <tr>
        <td>Название задачи:</td><td><?php echo $act_data['act']->name;?></td>
    </tr>
    <tr>
        <td>Автор:</td><td><?php echo (\DB::table('users')->where('userid', $act_data['act']->user_id)->first()->username); ?></td>
    </tr>
    <tr>
        <td>Статус</td>
        <td>
            <select name="UpdateAkt[status]" id="UpdateAkt_status" data-act="<?php echo $act_data['act']->id; ?>" >
                <?php
                foreach ($status_arr as $key=>$val){
                    echo '<option value="'.$key.'" '.(($key==$act_data['act']->status)?'selected':'').' >'.$val.'</option>';
                }
                ?>
            </select>
            <div style="display: none;" id="chngStatusAjaxLoader">
                <img src="/images/ajaxloader.gif" style="width: 80px; margin-top:-20%; margin-left: 10%; position:relative; z-index:99;" />
            </div>
        </td>
    </tr>
</table>

<?php if ($owner_akt) { ?>
    <div style="padding: 10px;">
        <?php if ($act_data['act']->completed==0){ ?>
            <i>Закрытие акта приведет к закрытию всех задач в акте</i><br>
            <a class="btn btn btn-success actionAktBtn" href="#" id="aktFormCompliteBtn" data-act="<?php echo $act_data['act']->id; ?>" >Закрыть акт</a>
        <?php }
        else { ?>
            <i>Повторное открытие акта приведет к тому, что все задачи акта станут снова открыты</i><br>
            <a class="btn btn btn-danger actionAktBtn" href="#" id="userFormNoCompliteBtn" data-act="<?php echo $act_data['act']->id; ?>" >Открыть акт</a>
        <?php } ?>
    </div>
<? } ?>

<!--- Вывожу задачи акта --->
<div id="listTasksAct">
<?php

    foreach ($act_tasks as $keyRow => $valRow){
        echo '<h3>Задачи №'.($keyRow+1).'</h3>';
        echo '<HR>';
        echo '
        <table class="task_list">
            <tr>
                <th></th><th>Название</th><th>Поручено</th><th>Контр. срок</th><th>Статус</th><th>Закрыта</th><th>Действия</th>
            </tr>
        ';
        foreach ($valRow as $key=>$data){

            $worker = \DB::table('scheduler_worker')->where('type',1)->where('scheduler_tasks_id', $key)->first();
            $worker = \DB::table('users')->where('userid', $worker->worker_id)->first();
            $status = "
                <a href='#' data-id='".$key."' data-status='20' class='chgStatusSquBtn'><div onmouseleave='$(this).removeClass(\"status20\"); $(this).empty();'  onmouseover='$(this).addClass(\"status20\"); $(this).append(20);' class='status-block' ".(((int)$data['status']>=20)?"style='background: #f0660a;'":'')."></div></a>
                <a href='#' data-id='".$key."' data-status='40' class='chgStatusSquBtn'><div onmouseleave='$(this).removeClass(\"status40\"); $(this).empty();'  onmouseover='$(this).addClass(\"status40\"); $(this).append(40);' class='status-block' ".(((int)$data['status']>=40)?"style='background: #f6b70d;'":'')."></div></a>
                <a href='#' data-id='".$key."' data-status='60' class='chgStatusSquBtn'><div onmouseleave='$(this).removeClass(\"status60\"); $(this).empty();'  onmouseover='$(this).addClass(\"status60\"); $(this).append(60);' class='status-block' ".(((int)$data['status']>=60)?"style='background: #fcff0e;'":'')."></div></a>
                <a href='#' data-id='".$key."' data-status='80' class='chgStatusSquBtn'><div onmouseleave='$(this).removeClass(\"status80\"); $(this).empty();'  onmouseover='$(this).addClass(\"status80\"); $(this).append(80);' class='status-block' ".(((int)$data['status']>=80)?"style='background: #c9fa54;'":'')."></div></a>
                <a href='#' data-id='".$key."' data-status='100' class='chgStatusSquBtn'><div onmouseleave='$(this).removeClass(\"status100\"); $(this).empty();'  onmouseover='$(this).addClass(\"status100\"); $(this).append(100);' class='status-block' ".(((int)$data['status']==100)?"style='background: #16e800;'":'')."></div></a>";
            echo '
                <tr>
                    <td>'.((!empty($data['burn']))?'<img src = "/images/burn.gif" />':'').'</td>
                    <td>'.$data['name'].'</td>
                    <td>'.$worker->username.'</td>
                    <td>'.date('d-m-Y', strtotime($data['deadline'])).'</td>
                    <td>'.$status.'</td>
                    <td>'.(((int)$data['completed']==0)?'<span style="color:red;">НЕТ</span>':'<span style="color: green;">ДА</span>').'</td>
                    <td>
                        <a href="/scheduler/mytasks/show/'.$key.'">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                </tr>
            ';



        }

        echo '</table>';


    }

?>
</div>

<HR>
<a href="/scheduler/mytasks" class="btn btn-inverse" style="margin-left: 10px; margin-bottom: 10px;">&#9668; Вернуться назад</a>

@endsection