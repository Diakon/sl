@section('head')
<link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/scheduler/style.css') }}" />
<link rel="stylesheet" href="{{ URL::asset('/js/scheduler/assets/redactor.css') }}" />
@stop

@section('scripts')
<script src="{{ URL::asset('/js/scheduler/jquery-mask.min.js') }}"></script>
<script src="{{ URL::asset('/js/scheduler/calendar.js') }}"></script>
<script src="{{ URL::asset('/js/scheduler/ajaxupload.min.js') }}"></script>
<script src="{{ URL::asset('/js/scheduler/assets/redactor.js') }}"></script>
<script src="{{ URL::asset('/js/scheduler/script.js') }}"></script>
<script type="text/javascript">
    $(function()
    {
        $('#addComentTxt').redactor();
    });
</script>
@stop

@extends('app')

@extends('nav')

@section('content')
<style>
    #addtasksForm table {
        border-collapse: collapse;
        margin: 10px;
        border:0px;
    }
    #addtasksForm table td, th {
        text-align: left;
        border: 0px;
        padding: 10px;
    }
    .filter {
        padding: 10px;
    }
    .comment_block {
        border-radius:3px;
        background: #eeeeee;
        width:500px;
        padding:10px;
        margin-bottom:10px;
        box-shadow: 0 0 5px rgba(0,0,0,0.5);
    }
    .comment_name {
        width:500px;
        margin-left:-10px;
        margin-top:-10px;
        border-radius:2px 2px 0 0;
        background:#cccccc;
        padding-left:20px;
    }
    .addNewWorker {
        width:270px;
        display: none;
    }

</style>

<div id="addcommentModal" class="modal" tabindex="-1" role="dialog" aria-labelledby="addcommentModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">?</button>
                <h3 id="addcommentModalLabel">Написать коментарий</h3>
            </div>
            <div class="modal-body">
                <input type="hidden" data-id="<?=$task_id;?>" id="addComentTaskID" >
                <textarea style="width: 515px; height: 200px;" id="addComentTxt"></textarea>
            </div>
            <div style="display: none;" id="addComentAjaxloder">
                <img src="/images/ajaxloader.gif" style="width: 150px; margin-top:-50%; margin-left: 35%;  position:relative; z-index:99;" />
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Отмена</button>
                <button class="btn btn-primary addCommnetBtn">Сохранить</button>
            </div>
        </div>
    </div>
</div>

<div style="margin-left: 10px;">
<?php
    //Если задача, часть акта - показываю файлы и название акта
    if ($task->type_task == 2){
        $akt = \DB::table('scheduler_act')->where('id', $task->user_id)->first();
        echo '<h2>Задача является частью акта №'.$akt->id.' от '.(date("d-m-Y H:i:s", strtotime($akt->created_at))).'</h2>';
        echo '<b>Название задачи: '.$akt->name.'</b><BR>';
        $files = \DB::table('scheduler_file')->where('task_id', $akt->id)->where('type_task',2)->get();
        if ( !empty($files) ){
            echo '<div style="padding: 10px; background-color: #d3d3d3; border-radius: 3px; margin-bottom: 10px; width:500px;">';
            echo 'К задаче прикреплены файлы:';
            foreach ($files as $data){
                $data_file  = str_replace("uploads/", "", $data->url_to_file);
                echo '<p><a href="'.\App::make('url')->to('/').'/'.$data->url_to_file.'" target="_blank">'.$data->file_name.'</a>'.(($owner_task)?'<a href="#" class="delete-task-file" data-file="'.$data_file.'" style="margin-left:5px;"><i class="icon-trash"></i></a>':'').'</p>';
            }
            echo '</div>';
        }
        echo '<HR>';
    }
?>
</div>

<h2><span style="margin: 10px;">Работа с задачей</span></h2>

        <form id="addtasksForm" method="post" action="/scheduler/mytasks/show/<?=$task_id;?>" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <table>

                <tr>
                    <td>Срочно <input <?php if (!$owner_task) { echo 'disabled="true"'; } ?> type="checkbox" name="Updatetasks[burn]" id="tasks_burn" <?php if(!empty($task->burn)){echo 'checked';} ?> ></td>
                </tr>
                <tr>
                    <td>
                        Контрольный срок
                        <input <?php if (!$owner_task) { echo 'disabled="true"'; } ?> type="text" onfocus="this.select();lcs(this)"
                               onclick="event.cancelBubble=true;this.select();lcs(this)" id="tasks_deadline" name="Updatetasks[deadline]" value="<?php echo date('d-m-Y', strtotime($task->deadline)); ?>" >
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Название
                        <input <?php if (!$owner_task) { echo 'disabled="true"'; } ?> type="text" name="Updatetasks[name]" id="tasks_name" style="width:85%" value="<?php echo $task->name; ?>" >
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Описание
                        <textarea <?php if (!$owner_task) { echo 'disabled="true"'; } ?> name="Updatetasks[description]" id="tasks_description" style="width:85%; min-height:200px;"><?php echo $task->description; ?></textarea>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Работа поручена:</td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="float: left; margin-top:-30px; padding-bottom: 20px;" id="workersListBlock">
                            <?php

                            if (!empty($worker_task_departament)){
                                foreach ($worker_task_departament as $key=>$value){
                                    echo '</BR>'.$value.' (отдел) '.(($owner_task)?'<a href="#" class="btn deleteTaskWorker" data-worker="'.$key.'" data-type="2" data-id="'.$task_id.'" alt="Удалить"> - </a>':'');
                                }
                            }
                            ?>
                            <?php
                            if (!empty($worker_task_user)) {
                                foreach ($worker_task_user as $key=>$value){
                                    echo '</BR>'.$value.' (сотрудник) '.(($owner_task)?'<a href="#" class="btn deleteTaskWorker" data-worker="'.$key.'" data-type="1" data-id="'.$task_id.'" alt="Удалить"> - </a>':'');
                                }
                            }
                            ?>
                        </div>
                    </td>
                </tr>

                    <tr>
                        <td><a href="#" class="btn btn-success" onclick="$('.addNewWorker').slideToggle(); return false;"><?php if ($owner_task){ echo ' + Добавить нового исполнителя'; } else{ echo ' Делегировать задачу другому пользователю'; } ?></a></td>
                    </tr>
                    <tr>
                        <?php if($owner_task){ ?>
                        <td class="addNewWorker">
                            Отдел
                            <select multiple  name="Updatetasks[taskworker_department][]" class="update_taskworkdepartment">
                            <?php
                                foreach ($departments_list as $key=>$val){
                                    echo "<option value='".$key."'>".$val."</option>";
                                }
                            ?>
                            </select>
                        </td>
                        <?php }?>
                        <td class="addNewWorker">
                            Сотрудник
                            <select <?php if($owner_task){ ?>multiple<?php } ?>  name="Updatetasks[taskworker_users][]" class="update_tasksworkusers">
                                <?php
                                if(!$owner_task){ echo "<option selected value='0'>Не указан</option>"; }
                                foreach ($users_list as $key=>$val){
                                    echo "<option value='".$key."'>".$val."</option>";
                                }
                                ?>
                            </select>
                        </td>
                    </tr>



                <tr>
                    <td>
                        Статус
                        <select name="Updatetasks[status]" class="userFornUpdateStatus" >
                            <?php
                            foreach ($status_arr as $key=>$val){
                                echo '<option value="'.$key.'" '.(($key==$task->status)?'selected':'').' >'.$val.'</option>';
                            }
                            ?>
                        </select>
                        </br><span style="color: #696969; font-size: 10px;">Для изменения статуса задачи, просто выбирите необходимый процент выполнения</span>
                        <br>
                        <div style="display: none;" id="chngStatusAjaxLoader">
                            <img src="/images/ajaxloader.gif" style="width: 150px; margin-top:-30%; margin-left: 20%; position:relative; z-index:99;" />
                        </div>
                    </td>
                </tr>

            </table>
            <?php if ($owner_task) { ?>
            <div style="padding: 10px;">
                <input type="hidden" name="Updatetasks[completed]" id="Updatetasks_completed" value="<?php echo $task->completed; ?>" />
                <button class="btn btn-primary" id="userFormUpdateBtn">Сохранить изменения</button>
                <?php if ($task->completed==0){ ?><button class="btn btn btn-success" id="userFormCompliteBtn">Закрыть заявку</button><?php }
                else { ?><button class="btn btn btn-danger" id="userFormNoCompliteBtn">Повторно открыть заявку</button><?php } ?>
            </div>
            <? } else {
                echo '
                <button style="margin-left:10px;" class="btn btn-primary" id="userFormDelegateBtn">Делегировать задачу</button>
                ';

            }
            ?>
        </form>
<!--  Список файлов -->

<div style="margin-left: 10px; padding: 10px; background-color: #d3d3d3; border-radius: 3px; margin-bottom: 10px; width:500px;">
<a class="UploadButton btn btn-info" id="UploadButton"><i class="icon-download-alt"></i>Прикрепить файл</a>
<div id="InfoBox" data-id="<?=$task_id;?>"></div>
<ul id="filesBox" ></ul>
<?php

    if (!empty($task_files)){
        foreach ($task_files as $data){
            $data_file  = str_replace("uploads/", "", $data->url_to_file);
            echo '<p><a href="'.\App::make('url')->to('/').'/'.$data->url_to_file.'" target="_blank">'.$data->file_name.'</a>'.(($owner_task)?'<a href="#" class="delete-task-file" data-file="'.$data_file.'" style="margin-left:5px;"><i class="icon-trash"></i></a>':'').'</p>';
        }
    }

?>
</div>

<!-- Блок коментариев -->
<div style="margin-left:10px;" id="commentBlock">
<?php

    foreach ($task_comments as $value){
        echo '
            <div class="comment_block">
                <div class="comment_name">'.$value['author'].' ['.(date('d-m-Y H:i:s', strtotime($value['created_at']))).']</div>
                </br>'.$value['comment'].'
            </div>
        ';
    }


?>

<a class="btn btn-info" onclick="$('#addcommentModal').modal('show'); return false;" href="#" style="margin-bottom: 20px;">Коментировать</a>

</div>


<HR>
<a href="/scheduler/mytasks" class="btn btn-inverse" style="margin-left: 10px; margin-bottom: 10px;">&#9668; Вернуться назад</a>


@endsection