@section('head')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('/css/scheduler/style.css') }}" />
@stop

@section('scripts')
    <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
    <script src="{{ URL::asset('/js/scheduler/jquery-mask.min.js') }}"></script>
    <script src="{{ URL::asset('/js/scheduler/ajaxupload.min.js') }}"></script>

<script>
    // Подчиненные департаменты и пользователи
    var user_list_data =  "<?php
        foreach ($user_list_data as $key=>$val){
            echo "<option value='".$key."'>".$val."</option>";
        }
        ?>";
    var department_list_data = "<?php
        foreach ($department_list_data as $key=>$val){
            echo "<option value='".$key."'>".$val."</option>";
        }
        ?>";

    //Все пользователи и департаменты
    var all_user_list_data =  "<?php
        foreach ($all_user_list_data as $key=>$val){
            echo "<option value='".$key."'>".$val."</option>";
        }
        ?>";
    var all_department_list_data = "<?php
        foreach ($all_department_list_data as $key=>$val){
            echo "<option value='".$key."'>".$val."</option>";
        }
        ?>";
</script>
<script src="{{ URL::asset('/js/scheduler/script.js') }}"></script>
@stop

@extends('app')

@extends('nav')

@section('content')

<style>
    #addtasksBlock table {
        border-collapse: collapse;
        margin: 10px;
        border:0px;
    }
    #addtasksBlock table td, th {
        text-align: left;
        border: 0px;
        padding: 10px;
    }
    .filter {
        padding: 10px;
    }

    .wrap {
        width: 300px;
        background: repeating-linear-gradient(
            45deg,
            #DDDDDD,
            #9b9b9b 10px,
            #A3A3A3 12px,
            #9b9b9b 15px
        );

    }
    .blank {
        width: 700px;
        /*min-height: 530px;*/
        background: #f6f5f5;
        box-shadow: 0 0 10px rgba(0,0,0,0.5);
        padding: 10px;
        margin-top: 10px;
        margin-left: 10px;
        margin-bottom: 10px;;
        /*margin: 0 auto;*/
        padding: 10px;

        border-radius: 5px;
    }

    .task_list th {
        padding: 10px;
        text-align: center;
        background: #cccccc;
        border: 1px solid #ffffff;
    }
    .task_list td {
        padding: 2px;
        background: #eeeeee;
        border: 1px solid #b3b3b3;
    }

    .status-block {
        width: 20px;
        height: 20px;
        background: #eeeeee;
        font-size: 9px;
        border:1px solid #9d9d9d;
        float: left;
    }

    #upload{
        margin:30px 200px; padding:15px;
        font-weight:bold; font-size:1.3em;
        font-family:Arial, Helvetica, sans-serif;
        text-align:center;
        background:#f2f2f2;
        color:#3366cc;
        border:1px solid #ccc;
        width:150px;
        cursor:pointer !important;
        -moz-border-radius:5px; -webkit-border-radius:5px;
    }

    .status20{
        background: #f0660a;
    }
    .status40{
        background: #f6b70d;
    }
    .status60{
        background: #fcff0e;
    }
    .status80{
        background: #c9fa54;
    }
    .status100{
        background: #16e800;
    }


    .blankAct {
        width: 1100px;
        /*min-height: 530px;*/
        background: #f6f5f5;
        box-shadow: 0 0 10px rgba(0,0,0,0.5);
        padding: 10px;
        margin-top: 10px;
        margin-left: 10px;
        margin-bottom: 10px;;
        /*margin: 0 auto;*/
        padding: 10px;

        border-radius: 5px;
    }
    .rowAct {
        clear: both;
    }
    .rowAct .line {
        float:left;
        padding-left: 10px;
    }
    .openTaskRow {
        cursor: pointer;
    }

</style>


<h2><span style="margin: 10px;">Мои задачи</span></h2>

<!-- Добавить задачу -->
<div id="addtasksBlock" class="blank" style="display: none;">

    <h3>Новая задача</h3>
    <form id="addtasksForm" method="post" action="/scheduler/mytasks" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <table>

            <tr>
                <td style="min-width:350px;">Срочно <input type="checkbox" name="Addtasks[burn]" id="tasks_burn" ></td>
                <td>
                    <input type="hidden" name="Addtasks[type_task]" id="tasks_type_task" value="1" />
                    <!--
                    Тип
                    <select id="tasks_action">
                        <?php
                        foreach ($type_action as $key=>$val){
                            echo '<option value="'.$key.'">'.$val.'</option>';
                        }
                        ?>
                    </select>
                    -->
                </td>
            </tr>
            <tr>
                <td>
                Контрольный срок
                <input type="text" id="tasks_deadline" class="datepicker" name="Addtasks[deadline]" >
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Название
                    <input type="text" name="Addtasks[name]" id="tasks_name" style="width:85%" >
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Описание
                    <textarea name="Addtasks[description]" id="tasks_description" style="width:85%; min-height:200px;"></textarea>
                </td>
            </tr>
            <tr>
                <td>
                    Отдел
                    <select multiple  name="Addtasks[worker_department][]" id="tasks_type_workdepartment">
                    </select>
                </td>
                <td>
                    Сотрудник
                    <select multiple  name="Addtasks[worker_users][]" id="tasks_type_workusers">
                    </select>
                </td>
            </tr>

        </table>

        <hr>
        <a class="UploadButton btn btn-info" id="UploadButton"><i class="icon-download-alt"></i>Прикрепить файл</a>
        <div id="InfoBox" data-id="0"></div>
        <ul id="filesBox" ></ul>
        <hr>


        </BR>
        <button class="btn cancelFormBtn">Отмена</button>
        <button class="btn btn-primary" id="userFormAddBtn">Создать</button>
    </form>

</div>

<!-- Добавить акт -->
<div id="addactBlock" class="blankAct" style="display: none;">

    <div style="display: none;">
        <div id="ActFormTemplate">
        <div class="rowAct">
            <div class="ActNum" style="position:absolute; margin-top:26px; margin-left:-5px; font-size:20px;"></div>
            <div class="line">
                <input type="text" name="Addact[task_name][]" class="form-control addacttask_name"  placeholder="Название" >
                <BR>
                <input type="text" class="form-control datepicker" name="Addact[task_deadline][]" placeholder="Контрольный срок">
            </div>
            <div class="line"><textarea name="Addact[task_description][]" class="form-control addacttask_description" placeholder="Описание"></textarea></div>
            <div class="line" style="margin-top:-7px;">
                Сотрудник
                <select multiple  name="Addact[taskworker_users][]" class="form-control addacttask_tasksworkusers">
                    <?php
                    foreach ($all_user_list_data as $key=>$val){
                        echo "<option value='".$key."'>".$val."</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="line" style="zmargin-top:-7px;">
                Отдел
                <select multiple  name="Addact[taskworker_department][]" class="form-control addacttask_taskworkdepartment">
                    <?php
                    foreach ($all_department_list_data as $key=>$val){
                        echo "<option value='".$key."'>".$val."</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="line" style="zmargin-top:20px;">
                <input type="checkbox" name="Addact[task_burn][]" class="form-control addacttask_burn" placeholder="Срочно" >
                <BR>
                    <div class="blockDeleteActTaskRow">
                    </div>
            </div>
        </div>
    </div>
    </div>

    <form id="addactForm" method="post" action="/scheduler/mytasks" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <h3>Новый Акт</h3>
        <div class="line">
            <input type="text" name="Addact[name]" id="addActTaskName"  class="form-control addact_name" style="margin-left:10px; width:422px;" placeholder="Название акта" />
        </div>

        <div id="NewActTaskBlock"></div>

        <div class="rowAct">
            <hr>
            <a class="UploadButton btn btn-info" id="UploadActButton"><i class="icon-download-alt"></i>Прикрепить файл</a>
            <div id="InfoActBox" data-id="0"></div>
            <ul id="filesActBox" ></ul>
            <hr>
        </div>

        <div class="rowAct">
            <a href="#" id="addNewActTaskBtn" class="btn btn-success" style="margin-bottom: 10px;">+Добавить задачу</a>
            </BR>
            <button class="btn cancelFormActBtn">Отмена</button>
            <button class="btn btn-primary" id="userFormAddActBtn">Создать</button>
        </div>
    </form>

</div>

<a href="#" class="addtasksLink btn btn-success" style="margin: 10px;">Добавить задачу</a>
<a href="#" class="addactLink btn btn-warning" style="margin: 10px;">Добавить акт</a>

<ul class="nav nav-tabs" role="tablist" id="taskTab">
    <li role="presentation"><a href="#complite" aria-controls="complite" role="tab" data-toggle="tab" id="compliteTabBtn">Завершенные</a></li>
    <li role="presentation" class="active"><a href="#outbox" aria-controls="outbox" role="tab" data-toggle="tab">Исходящие</a></li>
    <li role="presentation"><a href="#inbox" aria-controls="inbox" role="tab" data-toggle="tab">Входящие</a></li>
</ul>

<div class="tab-content" id="tabTaskList">
    <div role="tabpanel" class="tab-pane" id="complite">
        <table class="task_list">
            <tr>
                <th></th><th>Тип(Акт/Задача)</th><th>Название</th><th>Назначено</th><th>Контр. срок</th><th>Статус</th><th>Действия</th>
            </tr>

            <?php
            foreach ($task_list['complite'] as $data){
                //исходящие
                $status = "
                            <div class='status-block' ".(($data->status>=20)?"style='background: #f0660a;'":'')."></div>
                            <div class='status-block' ".(($data->status>=40)?"style='background: #f6b70d;'":'')."></div>
                            <div class='status-block' ".(($data->status>=60)?"style='background: #fcff0e;'":'')."></div>
                            <div class='status-block' ".(($data->status>=80)?"style='background: #c9fa54;'":'')."></div>
                            <div class='status-block' ".(($data->status==100)?"style='background: #16e800;'":'')."></div>";
                //Список тех, кому назначили задачу
                //отделы
                $depart_name = \DB::table('scheduler_worker')
                    ->leftJoin('dir_department', 'dir_department.id', '=', 'scheduler_worker.worker_id')
                    ->select('dir_department.name as name')
                    ->where('scheduler_tasks_id', $data->id)->where('type', 2)
                    ->get();

                //пользователи
                $users_name = \DB::table('scheduler_worker')
                    ->leftJoin('users', 'users.userid', '=', 'scheduler_worker.worker_id')
                    ->select('users.username as name')
                    ->where('scheduler_tasks_id', $data->id)->where('type', 1)
                    ->get();
                $assigned = "";
                if (!empty($depart_name)){
                    $assigned .= "<b>Отделы:</b></br>";
                    foreach ($depart_name as $data_name){
                        $assigned .= $data_name->name.'</br>';
                    }
                }
                if (!empty($users_name)){
                    $assigned .= "<b>Пользователи:</b></br>";
                    foreach ($users_name as $data_name){
                        $assigned .= $data_name->name.'</br>';
                    }
                }

                echo '
                <tr data-id="'.$data->id.'" data-type="show">
                    <td class="openTaskRow">'.((!empty($data->burn))?'<img src = "/images/burn.gif" />':'').'</td>
                    <td class="openTaskRow">'.(($data->type_task==2)?'Акт №'.$data->id.' от '.(date('d.m.Y H:i:s', strtotime($data->created_at))):' Задача №'.$data->id).'</td>
                    <td class="openTaskRow">'.$data->name.'</td>
                    <td>'.$assigned.'</td>
                    <td class="openTaskRow">'.date('d-m-Y', strtotime($data->deadline)).'</td>
                    <td>'.$status.'</td>
                    <td>
                        <a href="/scheduler/mytasks/show/'.$data->id.'">
                            <i class="icon-eye-open"></i>
                        </a>
                    </td>
                </tr>
            ';
            }


            ?>
        </table>

    </div>
    <div role="tabpanel" class="tab-pane active" id="outbox">

        <table class="task_list">

            <tr>
                <th></th><th>Тип(Акт/Задача)</th><th>Название</th><th>Назначено</th><th>Контр. срок</th><th>Статус</th><th>Действия</th>
            </tr>

            <?php
            foreach ($task_list['outbox'] as $data){
                //исходящие
                $status = "";

                $status .= "
                            <a href='#' data-id='".$data->id."' data-status='20' class='chgStatusSquBtn'><div  onmouseleave='$(this).removeClass(\"status20\"); $(this).empty();'  onmouseover='$(this).addClass(\"status20\"); $(this).append(20);' class='status-block' ".(($data->status>=20)?"style='background: #f0660a;'":'')."></div></a>
                            <a href='#' data-id='".$data->id."' data-status='40' class='chgStatusSquBtn'><div  onmouseleave='$(this).removeClass(\"status40\"); $(this).empty();'  onmouseover='$(this).addClass(\"status40\"); $(this).append(40);' class='status-block' ".(($data->status>=40)?"style='background: #f6b70d;'":'')."></div></a>
                            <a href='#' data-id='".$data->id."' data-status='60' class='chgStatusSquBtn'><div  onmouseleave='$(this).removeClass(\"status60\"); $(this).empty();'  onmouseover='$(this).addClass(\"status60\"); $(this).append(60);' class='status-block' ".(($data->status>=60)?"style='background: #fcff0e;'":'')."></div></a>
                            <a href='#' data-id='".$data->id."' data-status='80' class='chgStatusSquBtn'><div  onmouseleave='$(this).removeClass(\"status80\"); $(this).empty();'  onmouseover='$(this).addClass(\"status80\"); $(this).append(80);' class='status-block' ".(($data->status>=80)?"style='background: #c9fa54;'":'')."></div></a>
                            <a href='#' data-id='".$data->id."' data-status='100' class='chgStatusSquBtn'><div onmouseleave='$(this).removeClass(\"status100\"); $(this).empty();'  onmouseover='$(this).addClass(\"status100\"); $(this).append(100);' class='status-block' ".(($data->status==100)?"style='background: #16e800;'":'')."></div></a>";

                //Список тех, кому назначили задачу
                $assigned = "";
                $tasks_id = array();
                $depart_name = null;
                $users_name = null;
                if ($data->akt == 0){
                    $tasks_id = array($data->id);

                    //отделы
                    $depart_name = \DB::table('scheduler_worker')
                        ->leftJoin('dir_department', 'dir_department.id', '=', 'scheduler_worker.worker_id')
                        ->select('dir_department.name as name')
                        ->whereIn('scheduler_tasks_id', $tasks_id)->where('type', 2)
                        ->get();

                }
                else {
                    foreach (\DB::table('scheduler_tasks')->where('user_id',$data->id)->where('type_task',2)->get() as $tasks){
                        $tasks_id[] = $tasks->id;
                    }
                }

                //пользователи
                $users_name = \DB::table('scheduler_worker')
                        ->leftJoin('users', 'users.userid', '=', 'scheduler_worker.worker_id')
                        ->select('users.username as name')
                        ->whereIn('scheduler_tasks_id', $tasks_id)->where('type', 1)
                        ->get();

                if (!empty($depart_name)){
                        $assigned .= "<b>Отделы:</b></br>";
                        foreach ($depart_name as $data_name){
                            $assigned .= $data_name->name.'</br>';
                        }
                }
                if (!empty($users_name)){
                        $assigned .= "<b>Пользователи:</b></br>";
                        foreach ($users_name as $data_name){
                            $assigned .= $data_name->name.'</br>';
                        }
                }
                if (!empty($users_name_act)){
                        $assigned .= "<b>Пользователи:</b></br>";
                        foreach ($users_name_act as $data_name){
                            $assigned .= $data_name->name.'</br>';
                        }
                 }


                echo '
                <tr data-id="'.$data->id.'" data-type="'.(($data->akt==0)?'show':'showakt').'">
                    <td class="openTaskRow">'.((!empty($data->burn))?'<img src = "/images/burn.gif" />':'').'</td>
                    <td class="openTaskRow">'.(($data->akt==1)?'Акт №'.$data->id.' от '.(date('d.m.Y H:i:s', strtotime($data->created_at))):' Задача №'.$data->id).'</td>
                    <td class="openTaskRow">'.$data->name.'</td>
                    <td>'.$assigned.'</td>
                    <td class="openTaskRow">'.(($data->akt==0)?date('d-m-Y', strtotime($data->deadline)):'').'</td>
                    <td style="vertical-align: top;">'.$status.'<a href="#" style="margin-left:10px; margin-right:10px;" class="btn btn-mini btn-primary fastCloseTask" data-id="'.$data->id.'" data-type="'.((isset($data->type_task) && $data->type_task == 1)?1:2).'">закрыть</a></td>
                    <td style="vertical-align: top;" >
                        '.(($data->akt==0)?'<a href="/scheduler/mytasks/show/'.$data->id.'" >':'<a href="/scheduler/mytasks/showakt/'.$data->id.'">').'
                            <i class="icon-pencil"></i>
                        </a>
                        <a href="#" data-id="'.$data->id.'" data-type="'.(($data->akt==0)?1:2).'" class="delete-task deleteLitBtn" >
                            <i class="icon-trash"></i>
                        </a>
                        ';
                echo '
                    </td>
                </tr>
            ';
            }


            ?>


        </table>

    </div>
    <div role="tabpanel" class="tab-pane" id="inbox">

        <table class="task_list">

            <tr>
                <th></th><th>Тип(Акт/Задача)</th><th>Название</th><th>Автор заявки</th><th>Контр. срок</th><th>Статус</th><th>Действия</th>
            </tr>

        <?php

        $act_name = "";
        //входящие
        foreach ($task_list['inbox'] as $data){

            $status = "
                            <a href='#' data-id='".$data->id."' data-status='20' class='chgStatusSquBtn'><div onmouseleave='$(this).removeClass(\"status20\"); $(this).empty();'  onmouseover='$(this).addClass(\"status20\"); $(this).append(20);' class='status-block' ".(($data->status>=20)?"style='background: #f0660a;'":'')."></div></a>
                            <a href='#' data-id='".$data->id."' data-status='40' class='chgStatusSquBtn'><div onmouseleave='$(this).removeClass(\"status40\"); $(this).empty();'  onmouseover='$(this).addClass(\"status40\"); $(this).append(40);' class='status-block' ".(($data->status>=40)?"style='background: #f6b70d;'":'')."></div></a>
                            <a href='#' data-id='".$data->id."' data-status='60' class='chgStatusSquBtn'><div onmouseleave='$(this).removeClass(\"status60\"); $(this).empty();'  onmouseover='$(this).addClass(\"status60\"); $(this).append(60);' class='status-block' ".(($data->status>=60)?"style='background: #fcff0e;'":'')."></div></a>
                            <a href='#' data-id='".$data->id."' data-status='80' class='chgStatusSquBtn'><div onmouseleave='$(this).removeClass(\"status80\"); $(this).empty();'  onmouseover='$(this).addClass(\"status80\"); $(this).append(80);' class='status-block' ".(($data->status>=80)?"style='background: #c9fa54;'":'')."></div></a>
                            <a href='#' data-id='".$data->id."' data-status='100' class='chgStatusSquBtn'><div onmouseleave='$(this).removeClass(\"status100\"); $(this).empty();'  onmouseover='$(this).addClass(\"status100\"); $(this).append(100);' class='status-block' ".(($data->status==100)?"style='background: #16e800;'":'')."></div></a>";

            if ($data->type_task == 1) { $author = \DB::table('users')->where('userid', $data->user_id)->first()->username; }
            else {
                $author = \DB::table('scheduler_act')->where('id', $data->user_id)->first();
                $author = \DB::table('users')->where('userid', $author->user_id)->first()->username;
            }

            echo '
                <tr data-id="'.$data->id.'" data-type="show">
                    <td class="openTaskRow">'.((!empty($data->burn))?'<img src = "/images/burn.gif" />':'').'</td>
                    <td class="openTaskRow">'.(($data->type_task==2)?'Акт №'.$data->user_id.' от '.(date('d.m.Y H:i:s', strtotime($data->created_at))):' Задача №'.$data->id).'</td>
                    <td class="openTaskRow">'.$data->name.'</td>
                    <td>'.$author.'</td>
                    <td class="openTaskRow">'.date('d-m-Y', strtotime($data->deadline)).'</td>
                    <td>'.$status.'</td>
                    <td>
                        <a href="/scheduler/mytasks/show/'.$data->id.'">
                            <i class="icon-pencil"></i>
                        </a>
                    </td>
                </tr>
            ';
        }


        ?>


        </table>

    </div>

</div>
@endsection