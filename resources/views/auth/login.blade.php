@extends('app')

@extends('nav')

@section('content')
<div class="container-fluid" xmlns="http://www.w3.org/1999/html">
	<div class="row">
		<div class="col-md-4 col-md-offset-8">
			<div class="panel panel-default">
				<div class="panel-heading">Авторизация</div>
				<div class="panel-body">
					@if ($message)
						<div class="alert alert-danger">
							{{$message}}
						</div>
					@endif

					{!! Form::model($request,['url' => '/auth/login', 'class' => "form-horizontal"]) !!}
						<div class="form-group">
							{!! Form::label('username', 'Логин', ['class' => "col-md-4 control-label"]) !!}
							<div class="col-md-6">
								{!! Form::text('username', null, ['class' => 'form-control']) !!}
							</div>
						</div>
						<div class="form-group">
							{!! Form::label('password', 'Пароль', ['class' => "col-md-4 control-label"]) !!}
							<div class="col-md-6">
								{!! Form::password('password', ['class' => 'form-control']) !!}
							</div>
						</div>
						<div class="form-group">

							<div class="col-md-offset-4 col-md-6">
								<div class="checkbox">
									<label>
										{!! Form::checkbox('remember') !!} Запомнить
									</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button class="btn btn-primary" type="submit">
									<span class="glyphicon glyphicon-log-in" aria-hidden="true"></span> Войти
								</button>
							</div>
						</div>

					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		window.onload = function () {
			var resolution = $(window).width();
			var images = Array(
					"shuttleapproaching_nasa_6048.jpg",
					"as12-51-7507.jpg",
					"av_oa4_n2123201533427am63.jpg",
					"cv8exhyueaajw0__0.jpg",
					"iss045e094082.jpg",
					"iss045e166104.jpg",
					"nh-mountainousshorline.jpg",
					"20120909_shoreline_pascal.jpg",
					"europa-original.jpg",
					"heic1517a_0.jpg",
					"hubble_friday_09042015.jpg",
					"hubble_friday_09102015.jpg",
					"hubble_friday_09252015.jpg",
					"hubble_friday_102315.jpg",
					"hubble_friday_12042015.jpg",
					"pia19142_malhi-mojave.jpg",
					"pia19145.jpg",
					"Pluto04_NewHorizons_1042.jpg"
			);

			var imageUrl = images[Math.floor(Math.random() * images.length)];
			if (resolution >= 1024) {
				var fullUrl = "url(/images/background/1366/" + imageUrl + "_1366.jpg)";
				$("body").css({
					"background": fullUrl,
					"background-size": "100%"
				});
			}else if (resolution >= 640){
				var fullUrl = "url(/images/background/640/" + imageUrl + "_640.jpg)";
				$("body").css({
					"background": fullUrl,
					"background-size": "100%"
				});
			}
		};
	</script>
@endsection