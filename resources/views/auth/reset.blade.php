@extends('app')

@extends('nav')

@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default">
				<div class="panel-heading">{{ $app_title = 'Сбросить пароль' }}</div>
				<div class="panel-body">
					@include('common.errors')

					<form class="form-horizontal" role="form" method="POST" action="{{ route('password/reset') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="token" value="{{ $token }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Логин</label>
							<div class="col-md-6">
								<p class="form-control-static">{{ $username }}</p>
								<input type="hidden" class="form-control" name="username" value="{{ $username or 'defaultusername' }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Новый пароль</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Подтвердите пароль</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">
									Сбросить пароль
								</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
