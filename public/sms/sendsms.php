<?php
session_start();
$homedir = $_SERVER['DOCUMENT_ROOT'];
require_once $homedir.'/auth.php';
require_once $homedir.'/config.php';
require_once $homedir.'/Classes/PHPExcel/IOFactory.php';
require_once $homedir.'/sms/smsc_api.php'; 
require_once $homedir.'/lib/navigation.php';

$sender_list = get_sender();

?>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<LINK href="/stylesheet/pagenav.css" rel="stylesheet" type="text/css">
<style>

body {font-family:Arial;}
#send td{
font-family:Arial;
font-size:12px;
}
.error {
color:red;
}

.sms_table {
margin-top:40px;
font-size:13px;
}

.sms_table th {
background:#EEEEEE;
}

#status_block {

font-size:13px;
line-height:1.6;
background:#EEEEEE;
width:200px;
padding:10px;

}

</style>
<body>
<h3>Отправка SMS</h3>
<font color=red>Никаких формул в файле!</font><br><br>
<form  action="<?=$_SERVER['SCRIPT_NAME']?>" method="post" enctype="multipart/form-data">
<input type="hidden" name="action" value="send">
<table border=0><tr><td>Укажите откуда брать данные:</td><td>Пример заполнения Excel:</td></tr><tr><td>
<table id="send" border=0 bgcolor="#EEEEE" cellpadding="5">
<tr><td>Колонка телефонов:</td><td><input name="column_phone" value="G" style="width:50px"></td></tr>
<tr><td>Колонка текста:</td><td><input name="column_text" value="H" style="width:50px"></td></tr>
<tr><td>Отправитель:</td><td>
<select name="sender">
<?php
foreach ($sender_list as $val)
{
 echo "<option value=\"$val\">$val</option>";
}
?>
<option value="79168877777">79168877777</option>
<option value="79684477777">79684477777</option>
<option value="SMSC.RU">SMSC.RU</option>

</select>
</td></tr>
<tr><td>Файл Excel:</td><td><input type="file" name="xls"></td></tr>
<tr><td></td><td><input type="submit" name="send" value="Отправить"></td></tr>
</table></td><td><img src="/img/sms_excel_primer.jpg"></td></table><br><br>
</form>

<?php



print "<input type='button' value=\"Обновить\" onclick=\"document.location='{$_SERVER['SCRIPT_NAME']}?{$_SERVER['QUERY_STRING']}'\"><br><br>";
$action = $_REQUEST['action'];

if ($action=='send') send_sms_xls();
if ($action=='detail') print_sms_xls_details();
else
print_sms_xls();

function send_sms_xls()
{

	global $homedir;
	$err = array();
	$uploaddir = $homedir.'/tmp';
	if ($_FILES['xls'])
	{
		$ext = preg_replace("/.*\.(.{3,4})$/","$1",$_FILES['xls']['name']);
		if ($ext=='xls' || $ext=='xlsx')
		{
			$file = "$uploaddir/".time().".$ext"; 
			if (move_uploaded_file($_FILES['xls']['tmp_name'], $file))
			chmod($file, 0777);
		}
		else $err[] = 'Неверный формат файла'; 
	}
	
	if (!$column_phone = strtoupper($_REQUEST['column_phone'])) $err[] = 'Не задана колонка для телефона'; 
	if (!$column_text = strtoupper($_REQUEST['column_text'])) $err[] = 'Не задана колонка для текста'; 
	
	if (count($err)>0)
	{
		echo "<div class=\"error\">";
		foreach($err as $val)
		echo "$val<br>";
		echo '</div>';
	}
	else
	{
		$sender = $_REQUEST['sender'];
		if (!$sender) $sender = '79260526919';
		$objPHPExcel = PHPExcel_IOFactory::load($file);
		//echo '<hr />';
		$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		
		$file_name = mysql_real_escape_string($_FILES['xls']['name']);
		$query = "INSERT INTO sms_xls (file_name,sender) VALUES ('$file_name','$sender')";
		mysql_query($query);
		$parent_id = mysql_insert_id();
		
		$send_cnt=0;
		$cancel_cnt=0;
		foreach ($sheetData as $mass)
			foreach ($mass as $k => $val) {
			
			if ($k==$column_phone) 
			{
				$val = preg_replace('/\D/','',$val);
				if (preg_match('/\d{10}/',$val))
				{
					$phone = preg_replace('/.*(\d{10})$/',"7$1",preg_replace('/\D/','',$val));
					$text = $mass[$column_text];
					$param = send_sms($phone, $text, $translit = 0, $time = 0, $id = 0, $format = 0, $sender, $query='fmt=1');
					
					if ($param[1]>0) { 
					
						$send_cnt++; 
						//list($status) = get_status_sms($param[0], $phone);
						$text = mysql_real_escape_string($text);
						$query = "INSERT INTO sms_xls_details (parent_id,sms_id,phone,msg,sms_cnt,cost,sender) VALUES ($parent_id,{$param[0]},'$phone','$text',{$param[1]},'{$param[2]}','$sender')";
						mysql_query($query);
					}
					else $cancel_cnt++;
				}
			}
		}
		unlink($file);
		
		if ($parent_id)
		{
			$query = "UPDATE sms_xls SET send_cnt=$send_cnt,cancel_cnt=$cancel_cnt WHERE id=$parent_id";
			mysql_query($query);
		}
	}

}


function print_sms_xls()
{
	if (!($page = $_REQUEST['page'])) $page=1;
	$onpage=100;
	$status_arr = array();
	
	$query = "SELECT count(id) FROM sms_xls";
	list($rows) = mysql_fetch_row(mysql_query($query));
	$pages_count = ceil($rows/$onpage);
	if ($page>$pages_count) $page=$pages_count;
	
	if ($pages_count>1) pagenav($page,$pages_count);
	$balance = get_balance();
	
	print <<<END
	<br>
	Баланс: <b>$balance руб</b>
	<table class="sms_table" border=1 style="border-collapse:collapse" cellpadding="5">
	<tr>
	<th>Дата</th>
	<th>Файл</th>
	<th>Отправлено</th>
	<th>Доставлено</th>
	<th>Не доставлено</th>
	<th>Не отправлено</th>
	<th>Отправитель</th>
	<th>Действие</th>
	</tr>
END;

	
	$first_limit = ($page-1)*$onpage;
	$query = "SELECT id,date_format(date,'%d.%m.%Y %H:%i:%s') as date, file_name,send_cnt,cancel_cnt,sender FROM sms_xls ORDER BY id DESC LIMIT $first_limit,$onpage";
	$res = mysql_query($query);
	$rows = mysql_num_rows($res);
	while($row = mysql_fetch_array($res))
	{
		$status_arr = Status_cnt($row['id']);
		$success =  ($status_arr[1]['count'])?$status_arr[1]['count']:0;
		$failed = $row['send_cnt']-$success;
		$cancel_cnt = $row['cancel_cnt'];
		
		
		print <<<END
		<tr>
		<td>{$row['date']}</td>
		<td>{$row['file_name']}</td>
		<td align="center">{$row['send_cnt']}</td>
		<td align="center">$success</td>
		<td align="center">$failed</td>
		<td align="center">{$row['cancel_cnt']}</td>
		<td align="center">{$row['sender']}</td>
		<td><a href="{$_SERVER['SCRIPT_NAME']}?action=detail&parent_id={$row['id']}">Подробнее</a></td>
		</tr>
END;
	}
	
	if (!$rows) print "<tr><td colspan=\"8\" align=\"center\">Не найдено ни одной записи!</td></tr>";
	
	print <<<END
	</table>
	<br>
END;

	if ($pages_count>1) pagenav($page,$pages_count);

}



function print_sms_xls_details()
{
	$id = $_REQUEST['parent_id'];
	$status_arr = array();
	$status_arr = Status_cnt($id);

	$query = "SELECT send_cnt FROM sms_xls WHERE id=$id";
	list($count) = mysql_fetch_array(mysql_query($query));
	
	print <<<END
	<a href="{$_SERVER['SCRIPT_NAME']}" style="font-size:14px;">Вернуться назад</a><br><br>
	<div id="status_block">
	Всего: <b>$count</b> <br>
END;
	
	foreach($status_arr as $k=>$v)
	{
		$proc = round(($v['count']/$count)*100);
		print "{$v['name']}: <b>{$v['count']}</b> ($proc%)<br>";
	}
	print <<<END
	</div>
	<table class="sms_table" border=1 style="border-collapse:collapse" cellpadding="5">
	<tr>
	<th>Дата</th>
	<th>Номер</th>
	<th>Кол-во смс</th>
	<th>Стоимость</th>
	<th width="400px">Сообщение</th>
	<th>Статус</th>
	<th>Отправитель</th>
	</tr>
END;
	$query = "SELECT a.sms_id,a.phone,a.sms_cnt,a.cost,a.msg,date_format(a.send_date,'%d.%m.%Y %H:%i:%s') as date,a.sender,b.name FROM sms_xls_details as a
	LEFT JOIN sms_status as b ON a.status=b.id WHERE a.parent_id=$id ORDER BY a.id";
	$res = mysql_query($query);
	while($row = mysql_fetch_array($res))
	{
		$row['cost'] = ($row['cost'])?sprintf('%0.2f р',$row['cost']):'';
		$row['sms_cnt'] = ($row['sms_cnt']>0)?$row['sms_cnt']:'';
		print <<<END
		<tr>
		<td>{$row['date']}</td>
		<td>{$row['phone']}</td>
		<td align="center">{$row['sms_cnt']}</td>
		<td align="center">{$row['cost']}</td>
		<td>{$row['msg']}</td>
		<td>{$row['name']}</td>
		<td align="center">{$row['sender']}</td>
		</tr>
END;
	}
	print <<<END
	</table>
END;

}



function Status_cnt($id)
{
	$arr = array();
	$query = "SELECT a.status,count(a.status) as cnt,b.name FROM sms_xls_details as a
	LEFT JOIN sms_status as b ON a.status=b.id WHERE parent_id=$id GROUP BY a.status";
	$res = mysql_query($query);
	while($row = mysql_fetch_array($res))
	{
		$ind = $row['status']; 
		$arr[$ind] = array();
		$arr[$ind]['name'] = $row['name'];
		$arr[$ind]['count'] = $row['cnt'];
	}	
	return $arr;
}






?>
</body>
</html>