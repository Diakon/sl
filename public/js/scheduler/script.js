$(document).ready(function(){

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });



    $(document).on('mouseover', '.shwtooltip', function(){
        $(this).tooltip('show');
    });

    $("#userSelect").select2({language: 'ru'});

    function updateOrder(){
        $("#userDownlist").load(document.location.href+" #userDownlist");
    }


    $(document).on('click', '#selectUserBtn', function(){
        if ( $("#selectUser").val().length==0 ){
            alert ("Вы не выбрали пользователя!");
        }
        else {
            $.ajax({
                url:     '/scheduler/ajax',
                type:     'POST',
                dataType: 'json',
                encode          : true,
                data: {id:$("#selectUser").val(), step:1},
                success: function(response) {
                    //console.log(response);
                    $("#userDownlist").html("");
                    var html = "";
                    html += '<h3>Данные о подчиненности пользователя</h3></br>';
                    html += '<b>Текущая подчиненность</b></br><hr style="margin-top:-1px; border: 1px solid black">';
                    html += '<table>';
                    html += '<tr bgcolor="#DDDDDD"><th>Имя</th><th>Тип</th><th>Права</th><th>Действия</th></tr>';
                    $.each(response, function( index, value ) {
                        if (value['type_order'].length==0 || value['id'].length==0){ return true; }
                        html += '<tr class="row'+value['id']+'"><td>'+value['name']+'</td><td>'+value['type_order']+'</td><td>'+value['rule']+'</td><td><a href="#" data-id="'+value['id']+'" class="btn btn-danger delUserRole">удалить</i></a></td></tr>';

                    });
                    html += '</table>';
                    $("#userDownlist").html(html);

                    $("#userListTable").show();

                }
            });

        }
        return false
    });

    $(document).on('click', '.delUserRole', function(){
        var id = $(this).data("id");
        $.ajax({
            url:     '/scheduler/ajax',
            type:     'POST',
            dataType: 'json',
            data: {id:id, step:2},
            success: function(response) {
                if (response != "ok") { alert("Ошибка удаления! Обратитесь к администратору"); }
                else {
                    $(".row"+id).hide();
                }
            }
        });
        return false;
    });

    //Закрытие акта/задания из списка задач
    $(document).on('click', '.fastCloseTask', function(){
        var id = $(this).data("id");
        var type = $(this).data("type");
        if(confirm('Вы уверены, что хотите закрыть заявку?')){
            $.ajax({
                url:     '/scheduler/ajax',
                type:     'POST',
                dataType: 'json',
                data: {id:id, type:type, closeTask:1}
            });
            $(this).parent().parent().remove();
            return false;
        }
        return false;
    });

    $(document).on("click", "#compliteTabBtn", function(){
        $("#complite").load(document.location.href+" #complite");
    });

    $('#addUsersListShow').click(function(){
        $('#addUsersList').slideToggle();
        return false;
    });
    $('#addDepartamentListShow').click(function(){
        $('#addDepartamentList').slideToggle();
        return false;
    });

    //добавляем пользователя или отдел
    $(document).on('click', '#addUsersListBtn, #addDepartamentListBtn', function(){

        if ($(this).attr("id") == "addUsersListBtn"){
            //Добавление пользователя
            var arr=$('input:checkbox:checked').filter('.addUsersChk').map(function() {return this.value;}).get();
            var type_order = 2;
            var rule = 2;
        }
        else if ($(this).attr("id") == "addDepartamentListBtn"){
            //Отдела
            var arr=$('input:checkbox:checked').filter('.addDepartamentChk').map(function() {return this.value;}).get();
            var type_order = 1;
            var rule = $('input[name=type_order]:checked').val();
        }

        $.ajax({
            url:     '/scheduler/ajax',
            type:     'POST',
            dataType: 'json',
            data: {id:arr, step:3, type_order:type_order, rule:rule},
            success: function(response) {
                if (response == "ok") {
                    alert("Данные обнавлены успешно!");
                    document.location.href = "http://"+site+"/scheduler";
                }
                if (response == "end_session"){
                    alert("Время действия ссесии истекло. Выбирите пользователя заново");
                    document.location.href = "http://"+site+"/scheduler";
                }
            }
        });


    });

    //меняем статус в списке задач
    $(document).on('click', '.chgStatusSquBtn', function(){
        var status = $(this).data('status');
        var id = $(this).data('id');
        $.ajax({
            url:     '/scheduler/ajax',
            type:     'POST',
            dataType: 'json',
            data: {status_val:status, task_id:id},
            success: function(response) {
                $("#tabTaskList").load(document.location.href+" #tabTaskList");
            }
        });
    });



    //на странице добавления задания при открытии страницы ставим подчиненные отделы и сотрудников
    if ( typeof user_list_data !=="undefined" && typeof department_list_data !=="undefined" ){

        $('#taskTab a:last').tab('show');

        //Валидация формы новой задачи
        $(document).on('click', '#userFormAddBtn', function(){

            var tasks_burn = $("#tasks_burn").val();
            var tasks_deadline = $("#tasks_deadline").val();
            var tasks_name = $("#tasks_name").val();
            var tasks_description = $("#tasks_description").val();
            var tasks_type_workdepartment = $("#tasks_type_workdepartment").val();
            var tasks_type_workusers = $("#tasks_type_workusers").val();

            var ErrLog = "";

            if (tasks_deadline.length == 0){
                ErrLog += 'Вы не указали дату контрольного срока\n';
            }
            if (tasks_name.length == 0){
                ErrLog += 'Вы не указали название задачи\n';
            }
            /*
            if (tasks_description.length == 0){
                ErrLog += 'Вы не указали описание задачи\n';
            }
            */


            if (tasks_type_workdepartment == null && tasks_type_workusers == null){
                ErrLog += 'Вы не указали исполнителей и/или отделы \n';
            }

            if ( ErrLog.length != 0 ){
                alert(ErrLog);
                return false;
            }

        });

        //Клонирование задачи в Анкете
        var i = 0;
        function addActForm(i){
            $("#ActFormTemplate").clone().appendTo("#NewActTaskBlock");
            $(".ActNum").last().append(i+1);

            $(".addacttask_name").last().attr('name', 'Addact[task_name]['+i+'][]');
            $(".addacttask_deadline").last().attr('name', 'Addact[task_deadline]['+i+'][]');
            $(".addacttask_description").last().attr('name', 'Addact[task_description]['+i+'][]');
            $(".addacttask_burn").last().attr('name', 'Addact[task_burn]['+i+'][]');

            $(".addacttask_taskworkdepartment").last().attr('name', 'Addact[taskworker_department]['+i+'][]');
            $(".addacttask_tasksworkusers").last().attr('name', 'Addact[taskworker_users]['+i+'][]');

            $('.addacttask_name').last().data('taskrow', i+1);
            $('.addacttask_deadline').last().data('taskrow', i+1);
            $('.addacttask_description').last().data('taskrow', i+1);
            $('.addacttask_taskworkdepartment').last().data('taskrow', i+1);
            $('.addacttask_tasksworkusers').last().data('taskrow', i+1);
            //blockDeleteActTaskRow
            if (i != 0 ){
                var html = '<span style="position:absolute; margin-top:-23px; margin-left:20px;" ><a alt="Удалить задачу" class="btn deleteActTaskRow" href="#">X</a></span>';
                $('.blockDeleteActTaskRow').last().html(html);
            }

        }
        addActForm(i);
        $(document).on('click', '#addNewActTaskBtn', function(){
            ++i;
            addActForm(i);
            return false;
        });
        $(document).on('click','.deleteActTaskRow', function(){
            $(this).parent().parent().parent().parent().remove();
            return false;
        });
        //Валидация формы акта
        $(document).on('click', '#userFormAddActBtn', function(){

            var ErrLog = "";

            if ( $("#addActTaskName").val().length == 0 ){
                ErrLog += 'Вы не указали Название акта\n';
            }

            $('.addacttask_name').each(function(key, value) {
                if (key>0){
                    if ($(this).val().length == 0 ){
                        ErrLog += 'В задаче №'+$(this).data('taskrow')+' не указано название задачи\n';
                    }
                }
            });
            $('.addacttask_deadline').each(function(key, value) {
                if (key>0){
                    if ($(this).val().length == 0 ){
                        ErrLog += 'В задаче №'+$(this).data('taskrow')+' не указан  контрольный срок\n';
                    }
                }
            });
            /*
            $('.addacttask_description').each(function(key, value) {
                if (key>0){
                    if ($(this).val().length == 0 ){
                        ErrLog += 'В задаче №'+$(this).data('taskrow')+' не указано описание\n';
                    }
                }
            });
            */

            var arrUserDep = [];
            $('.addacttask_taskworkdepartment').each(function(key, value) {
                if (key>0){
                    if ( $(this).val() == null ){
                        arrUserDep[key] = $(this).data('taskrow');
                    }
                }
            });
            $('.addacttask_tasksworkusers').each(function(key, value) {
                if (key>0){
                    if ( $(this).val() == null ){
                        if (arrUserDep[key]){
                            ErrLog += 'В задаче №'+arrUserDep[key]+' не указаны исполнители и/или отделы\n';
                        }
                    }
                }
            });


            if ( ErrLog.length != 0 ){
                alert(ErrLog);
                return false;
            }

        });


        $(document).on('click', '.show_act_status_link', function(){
            $('.show_act_status').slideToggle();
            return false;
        });


        function selectChangeTasks(action){
            if (action == 1){
                $("#tasks_type_workusers").html(user_list_data);
                $("#tasks_type_workdepartment").html(department_list_data);
                $("#tasks_type_task").val(1);
            }
            if (action == 2){
                $("#tasks_type_workusers").html(all_user_list_data);
                $("#tasks_type_workdepartment").html(all_department_list_data);
                $("#tasks_type_task").val(2);
            }
        }

        $(document).on('click', '.addtasksLink, .cancelFormBtn', function(){
            $('#addtasksBlock').slideToggle();
            return false;
        });


        $(document).on('click', '.addactLink, .cancelFormActBtn', function(){
            $('#addactBlock').slideToggle();
            return false;
        });

        $("#tasks_type_workusers").html(user_list_data);
        $("#tasks_type_workdepartment").html(department_list_data);

        //Если выбрали акт - выводим все, если Задача - только подчмненные
        $(document).on('change', '#tasks_action', function(){
            var val = $(this).val();
            selectChangeTasks(val);
        });

        //Удаление записи
        $(document).on('click', '.deleteLitBtn', function(){
            if(confirm('Вы уверены, что хотите удалить задание?')){

                task_id = $(this).data('id');
                type = $(this).data('type');
                $(this).parent().parent().remove();
                $.ajax({
                    url:     '/scheduler/ajax',
                    type:     'POST',
                    dataType: 'json',
                    data: {delete_flag:1, task_id:task_id, type:type}
                });

            }
        });


    }

    //Удаление исполнителя в задании
    $(document).on('click', '.deleteTaskWorker', function(){
        if(confirm('Вы уверены, что хотите удалить исполнителя?\nЕсли вы удалите всех исполнителей - задание автоматически будет удалено.')){
            var id = $(this).data("id");
            var type = $(this).data("type");
            var worker = $(this).data("worker");
            $.ajax({
                url:     '/scheduler/ajax',
                type:     'POST',
                dataType: 'json',
                data: {id:id, type:type, worker:worker, deleteworker:1},
                success: function(response) {
                    //if (type == 2) { window.close(); }
                    $("#workersListBlock").load(document.location.href+" #workersListBlock");
                }
            });

            return false;
        }
    });

    //загрузка файла на сервер в задании
    $(function(){

        $("#UploadButton").ajaxUpload({
            url : "/scheduler/ajaxuploadfile",
            headers: { 'X-XSRF-TOKEN' :$('meta[name="csrf-token"]').attr('content') },
            name: "file",
            data: {_token:$('meta[name="csrf-token"]').attr('content'), task_id:$('#InfoBox').data('id'), type:1},
            onSubmit: function() {
                $('#InfoBox').html('<img src="../../images/ajaxloader.gif" width="25px;" />');
            },
            onComplete: function(result) {
                //$('#InfoBox').html('File uploaded with result' + result);
                $('#InfoBox').empty();
                if(result==="send_err"){
                    alert('Ошибка при загрузке файла на сервер!\nПопробуйте снова. Если ошибка будет повторяться - сообщите администратору!');
                }
                else {
                    var resultArr = result.split('|',2);
                    var txtBox = $("#filesBox").html();
                    $('#filesBox').html(txtBox+'<p>'+resultArr[0]+'<a href="#" class="delete-task-file" data-file="'+resultArr[1]+'" style="margin-left:5px;"><i class="icon-trash"></i></a></p>');
                }
            }
        });
    });

    //Удаление файла
    $(document).on('click', '.delete-task-file', function(){
        var dataFile = $(this).data('file');
        $.ajax({
            url:     '/scheduler/ajaxdeletefile',
            type:     'POST',
            dataType: 'json',
            data: {name_file:dataFile}
        });
        $('[data-file="'+dataFile+'"]').parent().css('display', 'none');
        return false;
    });

    //загрузка файла на сервер в АКТЕ
    $(function(){

        $("#UploadActButton").ajaxUpload({
            url : "/scheduler/ajaxuploadfile",
            headers: { 'X-XSRF-TOKEN' :$('meta[name="csrf-token"]').attr('content') },
            name: "file",
            data: {_token:$('meta[name="csrf-token"]').attr('content'), task_id:$('#InfoActBox').data('id'), type:2},
            onSubmit: function() {
                $('#InfoActBox').html('<img src="../../images/ajaxloader.gif" width="25px;" />');
            },
            onComplete: function(result) {
                $('#InfoActBox').empty();
                if(result==="send_err"){
                    alert('Ошибка при загрузке файла на сервер!\nПопробуйте снова. Если ошибка будет повторяться - сообщите администратору!');
                }
                else {
                    var resultArr = result.split('|',2);
                    var txtBox = $("#filesActBox").html();
                    $('#filesActBox').html(txtBox+'<p>'+resultArr[0]+'<a href="#" class="delete-task-file" data-file="'+resultArr[1]+'" style="margin-left:5px;"><i class="icon-trash"></i></a></p>');
                }
            }
        });
    });

    $(document).on('click', '.addCommnetBtn', function(){
        if ($('#addComentTxt').val().length ==0){
            alert ('Вы не написали коментарий');
            return false;
        } else {
            $('#addComentAjaxloder').show();
            task_id = $('#addComentTaskID').data('id');
            $.ajax({
                url:     '/scheduler/ajax',
                type:     'POST',
                dataType: 'json',
                data: {comment:$('#addComentTxt').val(), task_id:task_id},
                success: function(response) {
                    $('#addComentAjaxloder').hide();
                    if (response == "ok") {
                        $("#commentBlock").load(document.location.href+" #commentBlock");
                    }
                    else { alert("Ошибка добавления коментария. Попробуйте снова!"); }
                    $('#addcommentModal').modal('hide');
                }
            });
        }
    });


    //Валидация формы при обновлении
    function valideBeforeUpdate(){
        var tasks_deadline = $("#tasks_deadline").val();
        var tasks_name = $("#tasks_name").val();
      // var tasks_description = $("#tasks_description").val();
        var ErrLog = "";
        if (tasks_deadline.length == 0){ ErrLog += 'Вы не указали дату контрольного срока\n'; }
        if (tasks_name.length == 0){ ErrLog += 'Вы не указали название задачи\n'; }
       // if (tasks_description.length == 0){ ErrLog += 'Вы не указали описание задачи\n'; }
        if ( ErrLog.length != 0 ){ alert(ErrLog); return false; }
        else { return true ;}
    }
    $(document).on('click', '#userFormUpdateBtn', function(){
        if (!valideBeforeUpdate()){ return false; }
    });

    //делигирование
    $(document).on('click', '#userFormDelegateBtn', function(){
        var pers = $('.addNewWorker .update_tasksworkusers').val();
        if ( pers == null || pers == 0 ){ alert('Вы не выбрали пользователю, которому хотите делегировать задачу!'); return false; }
    });

    //Закрыть заявку
    $(document).on('click', '#userFormCompliteBtn', function (){
        if (!valideBeforeUpdate){ return false; }
        else {
            $('.userFornUpdateStatus').val('100');
            $('#Updatetasks_completed').val('1');
        }
    });
    //Открыть ранее закрытую заявку
    $(document).on('click', '#userFormNoCompliteBtn', function (){
        if (!valideBeforeUpdate){ return false; }
        else {
            $('#Updatetasks_completed').val('0');
        }
    });

    //изменение статуса в заявке
    $(document).on('change','.userFornUpdateStatus', function(){
        $('#chngStatusAjaxLoader').show();
        var statusVal = $(this).val();
        var task_id = $('#addComentTaskID').data('id');
        $.ajax({
            url:     '/scheduler/ajax',
            type:     'POST',
            dataType: 'json',
            data: {status_val:statusVal, task_id:task_id},
            success: function(response) {
                $('#chngStatusAjaxLoader').hide();
                if (response == "ok") {
                    alert ('Статус задачи изменен');
                }
                else { alert("Ошибка изменения статуса. Попробуйте снова!"); }
            }
        });
    });




    //Закрыть акт
    $(document).on('click', '.actionAktBtn', function (){
        var id = $(this).attr("id");
        var status = 0;
        var complite = 2;

        if (id == "aktFormCompliteBtn"){
            status = 100;
            complite = 1;
        }
        $('#UpdateAkt_status').val('100');
        $.ajax({
            url:     '/scheduler/ajaxact',
            type:     'POST',
            dataType: 'json',
            data: { status_val:status, complite_act:complite,  id:$(this).data("act") },
            success: function(response) {
                //$("#listTasksAct").load(document.location.href+" #listTasksAct");
                location.reload(true);
            }
        });
    });
    //Открыть ранее закрытую заявку
    $(document).on('click', '#userFormNoCompliteBtn', function (){
        if (!valideBeforeUpdate){ return false; }
        else {
            $('#Updatetasks_completed').val('0');
        }
    });

    //изменение статуса в акте
    $(document).on('change','#UpdateAkt_status', function(){
        $('#chngStatusAjaxLoader').show();
        var statusVal = $(this).val();
        var akt_id = $(this).data('act');
        $.ajax({
            url:     '/scheduler/ajaxact',
            type:     'POST',
            dataType: 'json',
            data: {status_val:statusVal, akt_id:akt_id},
            success: function(response) {
                $('#chngStatusAjaxLoader').hide();
                if (response == "ok") {
                    alert ('Статус акта изменен');
                }
                else { alert("Ошибка изменения статуса. Попробуйте снова!"); }
            }
        });
    });

    //Обновляем страницу по таймеру, на проверку того, что ссесия не истекла
    //проверка, что ссеисия пользователя не окончилась
    function chkSsesion(){
        $.ajax({
            url:     '/support/chkauth',
            type:     'POST',
            dataType: 'json',
            data: {url:document.location.href},
            success: function(response) {
                if (response=="ssesion_err"){
                    alert("Ваша ссесия истекла! Вы будете перенаправлены на страницу авторизации.");
                    location.href = '/auth/login';
                }
            }
        });
    }
    setInterval(function() { chkSsesion() }, 7000);

    $(document).on('click', '.openTaskRow', function(){
        var id = $(this).parent().data("id");
        var type = $(this).parent().data("type");
        document.location.href = '/scheduler/mytasks/'+type+'/'+id;
    });

});