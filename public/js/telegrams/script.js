$(document).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('.addNewCode').click(function(event) {

        $('#resultBlock').show();

        $.ajax({
            url:     '/lcab/telegrams/newtelegramcode',
            type:     'POST',
            dataType: 'html',
            success: function(response) {
                $('#dataResultBlock').empty().append('<span style="font-size:35px;">'+response+'</span>');
                $('#sendVilisMsg').empty().append('Отправьте этот код Вилису.');
            }
        });
    });
});

