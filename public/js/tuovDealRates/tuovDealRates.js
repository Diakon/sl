$(document).ready(function(){

    $('.chooseSubdivision').change(function(){
        window.location = location.origin+'/tuovDealRates/subdivision/'+$(this).val();
    });

    var dates = $( "[name='from'], [name='to']" ).datepicker({
        onSelect: function( selectedDate ) {
            var option = this.name == "from" ? "minDate" : "maxDate",
                instance = $( this ).data( "datepicker" ),
                date = $.datepicker.parseDate(
                    instance.settings.dateFormat ||
                    $.datepicker._defaults.dateFormat,
                    selectedDate, instance.settings );
            dates.not( this ).datepicker( "option", option, date );
            $(this).trigger('change');
        },
        dateFormat: 'yy-mm-dd'
    });

    $(document).on('click', '.applyRow', function(){

        //Получаем данные по строкам
        var id = $(this).data('id');
        var tr = $(this).closest('tr');
        var rate = tr.find('.rate_data').val();
        var from = tr.find('.from_data').val();
        var to = tr.find('.to_data').val();

        if( id.length==0 || rate.length==0 || from.length==0 || to.length==0 ){
            alert('Укажит даты С и По, а также ставку');
        } else {
            $.ajax({
                type: 'POST',
                url: '/tuovDealRates/ajax',
                dataType: "json",
                data: {id:id, rate:rate, from:from, to:to},
                success: function(data) {
                    tr.removeClass('danger').addClass('success');
                },
                error: function(data) {
                    var json = data.responseJSON;
                    for (var key in json) {
                        var val = json[key];
                        var td = tr.find('td').eq(1);
                        td.find('div').html('');
                        td.append($('<div/>',{html: val}));
                        //(key + ' = ' + val);
                    }
                    tr.removeClass('success').addClass('danger');
                }
            });
        }

        return false;

    });
});

