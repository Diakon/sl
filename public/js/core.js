$(document).ready(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    if (typeof $.fn.select2 != 'undefined') {
        $( 'select.select2').select2({
            language: 'ru',
        });
        $( 'select#id_anketa, select.id_anketa' ).select2({
            language: 'ru',
            ajax: {
                url: '/tuovAllowances/find/anketa',
                dataType: 'json',
                delay: 500,
                data: function (params) {
                    return {
                        q: params.term, // search term
                    };
                },
                processResults: function (data, params) {

                    return {
                        results: data
                    };
                },
                cache: true
            },
            minimumInputLength: 2,
            templateResult: function(node) {
                if (node.loading) return node.text;
                return node.value;
            },
            templateSelection: function (node) {
                return node.value || node.text;
            }
        });
    }

    if (typeof $.datepicker != 'undefined') {
        $( "input#datepicker, input.datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' }, $.datepicker.regional['ru']);

        var dates = $( "[name='date_from'], [name='date_to']" ).datepicker({
            onSelect: function( selectedDate ) {
                var option = this.name == "date_from" ? "minDate" : "maxDate",
                    instance = $( this ).data( "datepicker" ),
                    date = $.datepicker.parseDate(
                        instance.settings.dateFormat ||
                        $.datepicker._defaults.dateFormat,
                        selectedDate, instance.settings );
                dates.not( this ).datepicker( "option", option, date );
                $(this).trigger('change');
            },
            dateFormat: 'yy-mm-dd'
        });
    }
});