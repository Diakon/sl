var AppTuov = {
    edit: {
        timer: 0,

        dateRange: {
            pillDates: [],
            getDates: function (start, end) {
                var now = new Date(end);
                var dates = [];
                for (var d = new Date(start); d <= now; d.setDate(d.getDate() + 1)) {
                    var date = new Date(d).toISOString().slice(0,10);
                    if (dates.indexOf(date) == -1) {
                        dates.push(date);
                    }
                }
                return dates;
            },
            init: function () {
                if ($('#add_tab').size()<1) {
                    return;
                }

                var date_index = $('#tpl_row').find('table').find('[name^="date["]').closest('td').index()+1;
                $('#container').find('table').find('td:nth-child(' + date_index + '), th:nth-child(' + date_index + ')').hide();
                $('#tpl_row').find('table').find('td:nth-child(' + date_index + '), th:nth-child(' + date_index + ')').hide();

                var tr = $('.tab-content').find('table tbody tr');
                var dates = [];
                $('.nav-pills').html('');
                tr.each(function() {
                    var date = $(this).find('[name^="date["]').val();
                    if (date.length>0 && dates.indexOf(date) == -1) {
                        dates.push(date);
                    }
                });
                if (dates.length>0) {
                    dates.sort(function(a, b) {
                        a = new Date(a); b = new Date(b);
                        return a>b ? 1 : a<b ? -1 : 0;
                    });
                    for(var i=0; i<dates.length; i++){
                        AppTuov.edit.dateRange.setButtonClick(dates[i]);
                    }
                    setTimeout(function(){$('.nav-pills button:last').trigger('click')}, 100);
                } else {

                }
                AppTuov.edit.dateRange.pillDates = dates;

            },
            addDate: function (obj) {
                var item = $(obj);
                var dates = AppTuov.edit.dateRange.pillDates;
                var allowDates = AppTuov.edit.dateRange.getDates(item.data('date'), item.data('date_end'));

                var activeDate = $('.nav-pills button.active').text();
                var date = new Date(activeDate?activeDate:allowDates[0]);

                var i=0;
                var newTabDate = '';
                while(i<50) {
                    i++;

                    var date_string = date.toISOString().slice(0,10);
                    if (allowDates.indexOf(date_string) > -1) {
                        if (dates.indexOf(date_string) == -1) {
                            dates.push(date_string);
                            newTabDate = date_string;
                            break;
                        }
                        date = new Date(date.setDate(date.getDate() + 1));
                    } else {
                        date = new Date(allowDates[0]);
                    }
                }
                if (newTabDate.length>0) {
                    AppTuov.edit.dateRange.setButtonClick(newTabDate);
                    AppTuov.edit.init();

                    $('.nav-pills button:last').trigger('click');
                } else {
                    item.addClass('disabled');
                }
            },

            setButtonClick: function (date) {
                var dataRange = AppTuov.edit.dateRange;
                var button = $('<button />', {'text': date, 'class': 'btn btn-default', 'title':"dblLeftClick - редактировать дату"});
                $('.nav-pills').append($('<li>').append(button));
                var buttonEventClick = function(e) {
                    var item = $(this);
                    e.preventDefault();
                    $('.nav-pills').find('.active').removeClass('active');
                    item.addClass('active');
                    var date_filter = item.text();

                    $('#default_params').find('[name="date"]').val(date_filter);
                    var tr = $('.tab-content').find('table tbody tr');
                    tr.each(function() {
                        var date = $(this).find('[name^="date["]').val();
                        if (date != date_filter) {
                            $(this).hide();
                        } else {
                            $(this).show();
                        }
                    });
                    AppTuov.edit.totalSum();
                };
                button.off('click').on('click', buttonEventClick);
                button.off('dblclick').on('dblclick', function() {
                    var item = $(this);
                    var li = item.closest('li');
                    var oldDate = item.text();

                    var button = $('#add_tab');
                    var data = dataRange.getDates(button.data('date'), button.data('date_end'));

                    var s = $("<select />", {'class':"form-control"});
                    for(var val in data) {
                        if (dataRange.pillDates.indexOf(data[val]) == -1 || data[val]==oldDate) {
                            $("<option />", {value: data[val], text: data[val], selected: (data[val]==oldDate?true:false)}).appendTo(s);
                        }
                    }
                    if (s.find('option').size()<2) {
                        return;
                    }

                    item.off('click');
                    s.on('change', function() {
                        var date = $(this).val();
                        var tr = $('.tab-content').find('table tbody tr');
                        tr.each(function() {
                            $(this).find('[name^="date["]').filter(function(){
                                return $(this).val() == oldDate;
                            }).val(date);
                        });

                        var position = $.inArray(oldDate, dataRange.pillDates);
                        if ( ~position ) dataRange.pillDates.splice(position, 1);

                        dataRange.setButtonClick(date);
                        dataRange.pillDates.push(date);
                        $('.nav-pills').find('button:contains("'+date+'")').trigger('click');
                        li.remove();
                    }).on('blur', function (){
                        li.find('button').show().on('click', buttonEventClick);
                        $(this).remove();
                    });

                    li.append(s).find('button').hide();
                    s.focus();
                });

                if ( !$('.tab-content').find('table tbody tr').find('[name^="date["]').is('[value="' + date + '"]') ) {
                    var findDate = new Date(new Date(date).setDate(new Date(date).getDate() - 1)).toISOString().slice(0,10);
                    var tr = $('.tab-content').find('table tbody tr').filter(function (){
                        return $(this).find('[name^="date["]').val() == findDate;
                    });
                    if (tr.size()) {
                        var new_tr = tr.clone();
                        new_tr.find('[name^="deals["]').val('');
                        new_tr.find('[name^="date["]').val(date);

                        $('.tab-content').find('table tbody').append(new_tr);
                    }
                }
                $('#default_params').find('[name="date"]').val(date);
            }
        },



        getHours: function (start,end) {
            var dinner = parseFloat(AppTuov.edit.getDefaultParam('dinner'));
            var hours = '';
            if (end && start) {
                hours = end - start;
                if (hours>0) {hours = hours - dinner;}
                else if (hours<0) {hours = 24 - parseFloat(start) + parseFloat(end) - dinner;}
            }
            return hours;
        },

        recount: function (obj) {
            var form_default = $(obj).closest('form').find('input:visible, select:visible');
            form_default.each(function(){
                if ( typeof $(this).attr('readOnly') == 'undefined' ) {
                    $('[name="' + $(this).attr('name') + '[]"]').val($(this).val()).trigger('blur').trigger('change');
                }
            });

        },
        getDefaultParam: function (name) {
            if (typeof name == 'undefined' ) return;
            return $('#default_params').find('[name="' + name.replace('[]','') + '"]').val();
        },

        init: function () {
            $("input,select").focus(function() {
                prev_focus = focus;
                focus = $(this);
            });

            $('input:visible').off('change').on('change', function(){
               AppTuov.edit.totalSum();
            });

            $('#add').off('keyup').keyup(function(event){
                if (event.keyCode==13) $(this).trigger('click');
            });

            $('form').off('keydown').keydown(function(event){
                if (event.keyCode==13) event.preventDefault();
            });

            $(document).off('keydown').keydown(function(event){
                if (event.keyCode==192) {
                    event.preventDefault();
                    $('#add').focus();
                }
                if (event.keyCode==18) {
                    event.preventDefault();
                    $(prev_focus).focus();
                }
            });



            $('#container .fio').off('blur').on('blur',function(){
                setTimeout(function () {$('#hint').fadeOut(100);}, 100);
            });


            // блокируем подсчет кол-ва часов
            $('[name="hours[]"]').attr('readonly', 1).attr('tabindex', -1);

            // выходные блокируют все часы и выполненные работы в сделках
            $('[name="outlet[]"]').off('change').on('change', function (){
                var item = $(this);
                item.find('option').not(':selected').attr('selected', false);
                item.find("option:selected").attr('selected', 'selected');
                var $inputs = item.closest('tr').find('[name="start[]"], [name="end[]"], [name^="deals["]');
                if (item.val()>0) {
                    $inputs.each(function() {
                        $(this).data('value', $(this).val()).val('').trigger('blur');
                    });
                } else {
                    $inputs.each(function () {
                        $(this).val($(this).data('value')).trigger('blur');
                    });
                }
                $inputs.attr('readonly', item.val()>0);
            });

            // считает кол-во отработанных часов с учетом обеда (по умолчанию 1 час)
            $('[name="start[]"], [name="end[]"]').off('blur').on('blur', function () {
                var $tr = $(this).closest('tr');
                var $hours = $tr.find('[name="hours[]"]');
                $hours.val(AppTuov.edit.getHours($tr.find('[name="start[]"]').val(), $tr.find('[name="end[]"]').val())).trigger('change');

            });

            // нажимаем "добавить" если нет ниодного добавленного элемента
            if ($('#container tbody tr').size()<1) {
                setTimeout(function (){$('#add').trigger('click')}, 100);
            }

            $('#form_tuov_default [name="outlet"]').unbind('change').on('change', function () {
                var inputs = $('#form_tuov_default').find('[name="start"], [name="end"], [name^="deals["]');
                inputs.val('').attr('readonly',$(this).val()>0);
            });

            $('form#form_tuov_edit').unbind('submit').on('submit', function(){
                var form = $(this);

                var checkError = function (obj, is_error) {
                    if (is_error) {
                        $(obj).closest('div.form-group').addClass('has-error');
                    } else {
                        $(obj).closest('div.form-group').removeClass('has-error');
                    }
                    return is_error;
                };

                var errors = 0;
                form.find('[name="hours[]"]').each(function(){
                    errors += checkError(this, (
                        $(this).closest('tr').find('[name="outlet[]"]').val()<1 &&
                        (!/^\d+$/.test($(this).val()) && !/^\d+\.\d+$/.test($(this).val()))
                    ));
                });
                form.find('[name="start[]"], [name="end[]"]').each(function(){
                    errors += checkError(this, (
                        $(this).closest('tr').find('[name="outlet[]"]').val()<1 &&
                        (!/^\d+$/.test($(this).val()) && !/^\d+\.\d+$/.test($(this).val())
                        || $(this).val()>24)
                    ));
                });
                form.find('.id').each(function(){
                    errors += checkError(this, (!/^\d+$/.test($(this).val())) );
                });


                if (errors) {
                    alert('Ошибки в заполнении');
                    return false;
                } else return true;
            });
        },
        image: {
            show: function () {
                if ($('#doc_img')[0].width>0) {
                    $('#doc_container_main').removeClass('hidden');
                }
            },
            rotate: function (val) {
                var obj = $('#doc_img');
                var width = parseInt(obj.css('width'));
                var height = parseInt(obj.css('height'));
                var translate='';
                var angle = parseInt(obj.data('angle'))+val;

                if (width>height && (angle%180)!=0) {
                    var translateVal = width/2 - height/2;
                    translate = 'translateY('+translateVal+'px) ';
                }

                if (width<height && (angle%180)!=0) {
                    var translateVal = height/2 - width/2 ;
                    translate = 'translate('+translateVal+'px,-'+translateVal+'px) ';
                }



                obj.css( 'transform', translate+'rotate(' + angle + 'deg)' );
                obj.css( '-moz-transform', translate+'rotate(' + angle + 'deg)' );
                obj.css( '-webkit-transform', translate+'rotate(' + angle + 'deg)' );
                obj.css( '-o-transform', translate+'rotate(' + angle + 'deg)' );
                obj.css( '-ms-transform', translate+'rotate(' + angle + 'deg)' );
                obj.css('height',height);
                obj.css('width',width);

                obj.data('angle', angle);

            },
            zoom: function (val) {
                var obj = $('#doc_img');
                var width = parseInt(obj.css('width'))+val*40;
                if (width>400 && width<1256*2) {
                    obj.css('max-width',width+'px').css('width',width).css('height','auto');
                    AppTuov.edit.image.rotate(0);
                }
            },

        },
        add: function () {
            if ($('#add_tab:visible').size()>0 && $('.nav-pills button.active').size()<1) {
                return AppTuov.edit.dateRange.addDate($('#add_tab')[0]);
            }

            var newIndex = $('#container tbody tr:visible').size()+1;

            $('#tpl_row').find('label').text((newIndex<10?'0':'') + newIndex);
            var item = $('#tpl_row tbody').clone();
            item.find('input, select').attr('disabled',false);

            $('#container table tbody').prepend(item.html());
            AppTuov.edit.init();
            var obj = $('#container tbody tr:first');
            obj.find('select:visible, input').each(function(){
                $(this).val(AppTuov.edit.getDefaultParam($(this).attr('name'))).trigger('blur');
            });

            obj.find('.fio').focus();

            AppTuov.edit.totalSum();
        },
        remove: function (item) {
            $(item).closest('tr').remove();
            AppTuov.edit.totalSum();
        },
        totalSum: function () {
            $('.total_amount').each(function(){
                var $item = $(this);
                var input_items_name = $item.attr('name').replace('amount_','');
                var total_amount = 0;
                $('input[name^="'+input_items_name+'["]:visible').each(function(){
                    total_amount += (parseFloat($(this).val())>0 ? parseFloat($(this).val()) : 0);
                });
                $item.val(total_amount);
                if ($item.hasClass('hidden') && total_amount>0) {
                    $item.removeClass('hidden');
                }
                if (total_amount==0) {
                    $item.addClass('hidden');
                }
            });
        },

        hint: {
            init: function (obj) {
                var item = $(obj);

                var lookup = function () {
                    var position = item.position();
                    var hint = $('#hint');
                    hint.html('').show();
                    hint.css('top', position.top+34).css('left', position.left);
                    $.get('/tuovAllowances/find/anketa', {q: $(obj).val()}).done(function(data) {
                        var list = [];
                        for(d in data) {
                            var row = data[d];
                            list.push('<a href="#" onclick="AppTuov.edit.hint.enter(this);" class="list-group-item" data-id="' + row['id'] + '">' + row['value'] + '</a>');
                            if (d>5) {
                                break;
                            }
                        }
                        if(list.length>0) {
                            hint.html('<div class="list-group">' + list.join('') + '</div>');
                        }
                    });
                }

                $('input.isopen').removeClass('isopen');
                item.addClass('isopen');
                var e = window.event;
                if (e.which!=9 && e.which!=38 && e.which!=40 && e.which!=13 && item.val().length>0) {
                    clearTimeout(AppTuov.edit.timer);
                    AppTuov.edit.timer = setTimeout(function() {
                        lookup();
                    }, 400);
                }

                if (e.which==38) // нажимаем стрелку вверх
                    AppTuov.edit.hint.key_nav("last");
                if (e.which==40) // нажимаем стрелку вниз
                    AppTuov.edit.hint.key_nav("first");

                if (e.which==13) // нажимаем Enter
                {
                    if ($('.active').length) {
                        $('.active').closest('.list-group-item').trigger('click');
                    }
                }
            },
            enter: function (obj) {
                var id = $(obj).data('id');
                var fio = $(obj).html();
                var tr = $('#container').find('.isopen').closest('tr');

                tr.find('.fio').val(fio).trigger('blur');
                tr.find('.id').val(id);
                tr.find('input, select').eq(2).focus();
            },
            key_nav: function (str) {
                if (!$('.active').length) {
                    $('#hint div a:'+str).addClass('active');
                } else {
                    var obj = $('#hint').find('.active');

                    if (str=="last") {
                        obj = obj.prev();
                    } else {
                        obj = obj.next();
                    }
                    $('#hint').find('.active').removeClass('active');
                    $(obj).addClass('active');
                    if ($(obj).length) {
                        $(obj).addClass('active');
                    } else {
                        $('#hint div a:'+str).addClass('active');
                    }
                }
            }
        }
    },

    navigateTable: {
        init: function () {
            var self = AppTuov.navigateTable;
            $(document).keydown(function (e){
                switch (e.keyCode) {
                    case 38:
                    case 40:
                        e.preventDefault();
                        self.clickButton.vertical(e.keyCode);
                        break;
                    case 37:
                    case 39:
                        e.preventDefault();
                        self.clickButton.horizontal(e.keyCode);
                        break;
                    case 13:
                        e.preventDefault();
                        self.clickButton.enter();
                        break;
                }
            });

            var table = self.table;
            $(table.tableRows).on('dblclick', function(e){
                if (e.target.tagName != "I" && e.target.tagName != "A" && e.target.tagName != 'IMG' && e.target.tagName != 'INPUT' && e.target.tagName != 'U') {
                    var a = $(this).find('.glyphicon-edit').parent()[0];
                    if (a) {
                        a.click();
                    }
                }
            });
        },
        clickButton: {
            vertical: function(code) {
                var self = AppTuov.navigateTable;
                if (code == 40) {
                    return self.table.rows.next();
                }
                return self.table.rows.previous();
            },
            horizontal: function(code) {
                var self = AppTuov.navigateTable;
                if (code == 39) {
                    return self.paginate.next();
                }
                return self.paginate.previous();
            },
            enter: function() {
                var self = AppTuov.navigateTable;
                var table = self.table;
                $(table.tableRows).eq(table.tableCurrentRow).find('.glyphicon-edit').trigger('click');
            }
        },
        table: {
            tableCurrentRow: -1,
            tableRows: 'table.table-tuov-keys tbody tr',
            rows: {
                _init: function () {
                    var self = AppTuov.navigateTable.table;
                    var rows = self.rows;
                    if (self.tableCurrentRow > $(self.tableRows).size()-1) {
                        return self.tableCurrentRow = 0;
                    }
                    if (self.tableCurrentRow<0) {
                        return self.tableCurrentRow = $(self.tableRows).size()-1;
                    }
                    rows._current();
                },
                previous: function () {
                    var self = AppTuov.navigateTable.table;
                    var rows = self.rows;
                    self.tableCurrentRow--;
                    rows._init();
                },
                next: function () {
                    var self = AppTuov.navigateTable.table;
                    var rows = self.rows;
                    self.tableCurrentRow++;
                    rows._init();
                },
                _current: function () {
                    var self = AppTuov.navigateTable.table;
                    var tr = $(self.tableRows).removeClass('success').eq(self.tableCurrentRow).addClass('success');
                    var rowpos = tr.position();
                    $(document).scrollTop(rowpos.top - ($(window).height()/2));
                }
            }

        },
        paginate: {
            paginateCurrent: 'ul.pagination li.active',
            previous: function (){
                var self = AppTuov.navigateTable.paginate;
                var link = $(self.paginateCurrent).prev().not('.disabled').find('a');
                if(link.size()) {
                    link[0].click()
                }
            },
            next: function (){
                var self = AppTuov.navigateTable.paginate;
                var link = $(self.paginateCurrent).next().not('.disabled').find('a');
                if(link.size()) {
                    link[0].click()
                }
            }

        }
    },

    history: {
        init: function (){
            var history_url = function (id) {
                return $('table.table-tuov-keys').data('history_uri').replace('%7Bid%7D', id);
            }

            var modal = $('.history-modal-md');
            modal.on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget);
                var document_id = button.data('document_id');

                modal.find('.document_id').text(document_id);
                modal.find('.modal-body').html('');

                $.getJSON(history_url(document_id), function (data) {
                    var items = [];
                    items.push("<tr><th>Статус</th><th>Дата изменения</th><th>Пользователь</th></tr>");
                    $.each( data, function( key, val ) {
                        items.push( "<tr><td>" + val.status + "</td><td>" + val.datetime + "</td><td>" + val.username + "</td></tr>" );
                    });

                    $( "<table>", {
                        "class": "table",
                        html: items.join( "" )
                    }).appendTo( modal.find('.modal-body') );
                });
            })
        }
    }
}