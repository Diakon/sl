$(document).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        type: 'POST',
        url: '/edit/getphones',
        dataType: "json",
        data: {number:aon},
        success: function(data) {
            //Получаем ответ - строим таблицу
            var html = '<table class="duplicatetable" width="700px" cellspacing="0" cellpadding="0" border="1" align="center">';
            html += '<tbody>' +
            '<tr bgcolor="grey"><td class="duplicatetable"><b>Дата звонка</b></td><td class="duplicatetable"><b>Откуда</b></td><td class="duplicatetable"><b>Куда</b></td><td class="duplicatetable"><b>Результат</b></td><td class="duplicatetable"><b>Длительность</b></td></tr>';
            $.each(data, function( index, value ) {
                //Проверяем входящий/исходящий звонок
                var styleColor =  '';
                var channel = value['channel'].split('/');
                //console.log(channel);
                if ( ( (value['dst'].charAt(0)+value['dst'].charAt(1) == '98') || (value['dst'].charAt(0)+value['dst'].charAt(1)+value['dst'].charAt(2) == '370') || (value['dst'].charAt(0)+value['dst'].charAt(1)+value['dst'].charAt(2) == '380') ) && channel[0]=="SIP" ){
                    styleColor =  'bgcolor="yellow";';
                }


                html +='<tr '+styleColor+'>' +
                '<td class="duplicatetable">'+value['calldate']+'</td>' +
                '<td class="duplicatetable">'+value['src']+'</td>' +
                '<td class="duplicatetable">'+value['dst']+'</td>' +
                '<td class="duplicatetable">'+value['disposition']+'</td>' +
                '<td class="duplicatetable"><center>'+value['billsec']+' сек</center></td>' +
                '</tr>';
            });
            html += '</tbody>';
            html += '</table>';
            $('#phonesCallList').empty().append(html);
        }
    });



});

