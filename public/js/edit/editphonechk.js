$( document ).ready(function() {

    var country=0;
    var thisNumberExt = 0;

    // Считывает GET переменные из URL страницы и возвращает их как ассоциативный массив.
    function getUrlVars()
    {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for(var i = 0; i < hashes.length; i++)
        {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    var isNewRecord  = getUrlVars()["pid"];


    function chkNumber(number){
        //Россия
        countryType = 0;
        arr = [ "900", "901", "902", "903", "904", "905", "906", "908", "909", "910", "911", "912", "913", "914",
                "915", "916", "917", "918", "919", "920", "921", "922", "923", "924", "925", "926", "927", "928",
                "929", "930", "931", "932", "933", "934", "936", "937", "938", "939", "941", "950", "951", "952",
                "953", "954", "955", "956", "958", "960", "961", "962", "963", "964", "965", "966", "967", "968",
                "969", "970", "971", "980", "981", "982", "983", "984", "985", "987", "988", "989", "991", "992",
                "993", "994", "995", "996", "997", "999" ];
        if (arr.indexOf(number)!=-1 || number.charAt(0)=="8" || number.charAt(0)=="7"  ){ countryType = 1; }
        //Украина
        if (number == "380"){ countryType = 2; }
        //Беларуссия
        if (number == "375"){ countryType = 3; }

        return countryType;
    }

    function imageShow(country){
        //Россия
        if( country==1){ $(".countryPhone").empty().append("<img style='margin-left:10px;' src='/images/ru.jpg' />"); }
        //Украина
        else if (country==2){ $(".countryPhone").empty().append("<img style='margin-left:10px;' src='/images/ukr.jpg' />"); }
        //Беларуссия
        else if (country==3){ $(".countryPhone").empty().append("<img style='margin-left:10px;' src='/images/blr.jpg' />"); }
        //не определили
        else { $(".countryPhone").empty(); return country=0; }
    }


    if ($("#phoneChk").length>6){
        country = chkNumber($(this).val().charAt(0)+$(this).val().charAt(1)+$(this).val().charAt(2));
        imageShow(country);
    }

    $("#phoneChk").keyup(function(){
        if ($(this).val().length>2){
            country = chkNumber($(this).val().charAt(0)+$(this).val().charAt(1)+$(this).val().charAt(2));
            imageShow(country);
        }

        if ($(this).val().length>6 && isNewRecord==1){
            //Проверка на дубликат номера
            var numberChk = $(this).val();
            if (country==1){
                if ($(this).val().charAt(0)=="7"){ numberChk = $(this).val().substring(0,0) + "8" + $(this).val().substring(+1); }
                else if ($(this).val().charAt(0)=="9") { numberChk = "8"+$(this).val();  }
            }
            $.ajax({
                type: 'POST',
                url: './editajax.php',
                dataType: 'json',
                data: { 'phone': numberChk },
                success: function(data) {
                    $(".chkNumberExist").empty();
                    if (data=="record_exists"){
                        $(".chkNumberExist").append("<p style='color: red'>Такой номер уже есть в БД!</p>");
                        document.getElementById("mainsubmit").disabled = true;
                        thisNumberExt = 1;
                    }
                    else {
                        document.getElementById("mainsubmit").disabled = false;
                        thisNumberExt = 0;
                    }
                }

            });



        }
    });

    $("#phoneChk").blur(function(){

        if (country==1 && isNewRecord==1){
            if ($(this).val().charAt(0)=="7"){ $(this).val($(this).val().substring(0,0) + "8" + $(this).val().substring(+1)); }
            else if ($(this).val().charAt(0)=="9") { $(this).val("8"+$(this).val());  }
        }

    });

    function checkStatus(){
        var status = $('#customer_status').val();
    }

    //Проверка заполнения обязательных полей
    $("#mainsubmit").click(function(){
        var errMsg = "";

        if ($("#customer_status").val() == null){
            errMsg = errMsg +"\nВы не указали статус!";
        }

        if ( $("#customer_status").val()=="11" || $("#customer_status").val()=="12" ){
            if ( $("#lastnameField").val().length==0 ){
                errMsg = errMsg +"\nВы не указали фамилию!";
            }
            if ( $("#nameField").val().length==0 ){
                errMsg = errMsg +"\nВы не указали имя!";
            }
            if ( $("#otchField").val().length==0 ){
                errMsg = errMsg +"\nВы не указали отчество!";
            }
            if ( $("#vacancyField").val().length==0 ){
                errMsg = errMsg +"\nВы не указали вакансию!";
            }
            if ( $("#ageField").val().length==0 ){
                errMsg = errMsg +"\nВы не указали возраст!";
            }
        }
        if ( $("#phoneChk").val().length==0 ){
            errMsg = errMsg +"\nВы не указали номер телефона!";
        }
        if (errMsg!="") { alert(errMsg); return false; }
    });

    $("#phoneChk, #ageField").keydown(function(event) {
        if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
            (event.keyCode == 65 && event.ctrlKey === true) ||
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        }
        else {
            if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
                event.preventDefault();
            }
        }
    });

    $(document).on('click','#customer_status', function(){
        if (isNewRecord==1){
            if (thisNumberExt==1 ){
                document.getElementById("mainsubmit").disabled = true;
            } else {
                document.getElementById("mainsubmit").disabled = false;
                thisNumberExt = 0;
            }
        }
    });

    var e = jQuery.Event("keyup", { keyCode: 40 });
    jQuery("#phoneChk").trigger( e );
    $("#phoneChk").blur();

	
});