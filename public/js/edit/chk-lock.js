$(document).ready(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var lock = 0;

    //Проверяет блокирована ли запись
    function chkLock(){
        var url = (document.location.pathname).substring(1);
        $.ajax({
            url:     '/edit/ajax',
            type:     'POST',
            dataType: 'json',
            data: {url_lock:url},
            success: function(response) {
                $('#lock_block').empty();
                lock = 0;
                if ( response['userid'] !== null && response !== false ){
                    var html = '<div class="alert alert-info"><button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>' +
                        '<p>Данная запись заблокирована! С данной записью работает пользователь: '+response['fio']+' ['+response['username']+']</p><BR>';
                    html += '<ul>';
                    html += '<li>Телефон: '+response['phone']+'</li>';
                    html += '<li>E-mail: '+response['email']+'</li>';
                    html += '</ul>';
                    html +='</div>';
                    lock = 1;
                    $('#lock_block').append(html);
                }
            }
        });
    }
    chkLock();
    setInterval(function() { chkLock() }, 7000);

    $(document).on('click','#mainsubmit',function(){
        if (lock==1){
            alert('Запись заблокирована! С ней работает другой пользователь.');
            return false;
        }
    });

});

