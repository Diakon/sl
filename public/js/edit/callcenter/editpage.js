/**
 * Created by ciscofan on 18.08.14.
 */


function checkMetro() {
    var metro = $('#mos_metro_station').closest("tr");
    if ($('input#city').val() == "Москва") {
        metro.css('visibility', 'visible');

    } else {
        metro.css('visibility', 'collapse');
    }
}

function checkCustomerStatus() {
    /** Если стоит статус "придет" или "уже пришел" то показывает доп.блок */
    if ($('#customer_status').val() == 12 || $('#customer_status').val() == 25) {
        $('#trans_block').css('display', 'block');
        $('#inputField').closest("tr").css('visibility', 'visible');
    } else {
        $('#trans_block').css('display', 'none');
        $('#inputField').closest("tr").css('visibility', 'collapse');
        $('#inputField').val("");
    }

    /** Если стоит статус "не трогали" то обновить анкету нельзя. disable на кнопку */
    if ($('#customer_status').val() == 1 || $('#customer_status').val() == ""){
        $('input#mainsubmit').prop('disabled', true);
    }else{
        $('input#mainsubmit').prop('disabled', false);
    }

}

function key_nav(str) {  /** Created by Ivan Zavyalov. Navigation by keyboard keys at cities hints list */
    if (!$('.active').length) {
        $('#hint_block div:' + str).addClass('active');
    }
    else {
        var obj;
        if (str == "last") obj = $('.active').prev();
        else obj = $('.active').next();
        $('.active').removeClass('active');
        $(obj).addClass('active');
        if ($(obj).length) $(obj).addClass('active');
        else $('#hint_block div:' + str).addClass('active');
    }
}


function Init_Hint(id) {
    /** Created by Ivan Zavyalov. AJAX looking for similiar cities in DB for hint */

    $('form').keypress(function (e) {
        if (e.which == 13) return false;
    });

    $('#' + id).attr("autocomplete", "off");
    $('#' + id).blur(function () {
        $('#hint_block').fadeOut(200);
    });

    $('#' + id).keyup(function (e) {
        checkMetro();
        if (e.which != 9 && e.which != 38 && e.which != 40 && e.which != 13) {
            $('#' + id).attr("autocomplete", "off");
            $.get("/edit/ajax", 'name=' + encodeURIComponent($('#' + id).val()), function (data) {
                if ($("#hint_block").length) $("#hint_block").replaceWith(data); else $("#city_block").prepend(data);

                $("#hint_block div").click(function () {
                    $('#' + id).val($(this).html());
                    checkMetro();
                    $('#hint_block').fadeOut(200);
                });

                $("#hint_block div").mouseover(function () {
                    if ($('.active').length) {
                        $('.active').removeClass('active');
                    }
                    $(this).addClass('active');
                });

                $("#hint_block div").mouseout(function () {
                    $(this).removeClass('active');
                });

            });
        }

        if (e.which == 38) // нажимаем стрелку вверх
            key_nav("last");

        if (e.which == 40) // нажимаем стрелку вниз
            key_nav("first");

        if (e.which == 13) // нажимаем Enter
        {
            if ($('.active').length) {
                $('#city').val($('.active').html());
                checkMetro();
                $('#hint_block').fadeOut(200);
            }
        }

    });

}

$(document).ready(function () {

    checkMetro();
    checkCustomerStatus();
    Init_Hint('city');


    $('body').click(function () {
        var clickcount = $('input.clickcount').val();
        clickcount++;
        $('input.clickcount').val(clickcount);
        console.log('click1\n');

    });

    $('body').keypress(function () {
        var keypresscount = $('input.keypresscount').val();
        keypresscount++;
        $('input.keypresscount').val(keypresscount);
        console.log('press1\n');
    });


    $('#customer_status').change(checkCustomerStatus);

    //Автокомплит для ФИО
    $(document).on('keydown','.fioInput', function(){
        var idEllement = $(this).attr('id');
        $('#hint_'+idEllement).remove();
        $.ajax({
            type: 'POST',
            url: '/edit/ajax',
            dataType: "html",
            data: {fio:idEllement, name:$(this).val()},
            success: function(data) {
                $('.maintable').eq(0).append(data);
                $('#hint_'+idEllement).show();
            }
        });
    });
    $(document).on('click', '#hint_lastnameField div, #hint_nameField div, #hint_otchField div',function(){
        var idEllement = $(this).parent().attr('id');
        var valDiv = $(this).html();
        idEllement = idEllement.replace(/hint_/g, '');
        $('#'+idEllement).val(valDiv);
        $(this).parent().remove();
    });
    $(document).click(function(event) {
        if ($(event.target).closest("#hint_lastnameField", "#hint_nameField", "#hint_otchField").length) return;
        //$("#hint_lastnames").hide("slow");
        $('#hint_lastnameField').remove();
        $('#hint_nameField').remove();
        $('#hint_otchField').remove();
        event.stopPropagation();
    });

});