window.onload = function () {
    $('#bottom-brand').append('<a class="navbar-brand" href="#">' + mylogin + '</a>');

    if ((myip === "185.8.5.43") || (myip === "185.8.5.70")) {
        var socket = io.connect("http://192.168.200.200:3031");
    } else {
        var socket = io.connect("http://185.8.5.43:3031");
    }

    socket.emit('register', mylogin);
    var call_array = [];
    var reA = /A-(\d{1,})\s{0,}([\w\s]+){0,}/;
    var reZ = /Z-(\d{1,})\s{0,}([\w\s]+){0,}/;

    socket.on('answer-call', function (aon, displayname) {
        if ((reArray = reZ.exec(displayname)) != null) {
            window.location.replace("http://cloud.secondlab.ru/edit/update/" + reArray[1]);
        } else if ((reArray = reA.exec(displayname)) != null) {
            window.location.replace("http://vls.secondlab.ru/op/anketa.php?select=update_form&id=" + reArray[1]);
        } else {
            window.location.replace("http://cloud.secondlab.ru/edit/create?aon=" + aon);
        }
    });

    socket.on('incoming-call', function (aon, displayname) {
        if (call_array.length >= 5) {
            call_array.shift();
            $('#calls').find('ul').children().first().remove();
        }
        call_array.push([aon, displayname]);

        if ((reArray = reZ.exec(displayname)) != null) {
            if (typeof reArray[2] == "undefined") reArray[2] = "";
            $('#calls').find('ul').append('<li role="presentation" class="active"><a target="_blank" href="http://cloud.secondlab.ru/edit/update/' + reArray[1] + '">' + reArray[2] + ' Z-' + reArray[1] + '</a></li>');
        } else if ((reArray = reA.exec(displayname)) != null) {
            if (typeof reArray[2] == "undefined") reArray[2] = "";
            $('#calls').find('ul').append('<li role="presentation" class="active"><a target="_blank" href="http://vls.secondlab.ru/op/anketa.php?select=update_form&id=' + reArray[1] + '">' + reArray[2] + ' A-' + reArray[1] + '</a></li>');
        } else {
            //$('#calls').find('ul').append('<li role="presentation" class="active"><a href="#">' + aon + '</a></li>');
            $('#calls').find('ul').append('<li role="presentation" class="active"><a href="/edit/create?aon='+aon+'">' + aon + '</a></li>');
        }

    });
};