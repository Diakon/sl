$(document).ready(function(){
    $(document).on('mouseover', '.shwtooltip', function(){
        $(this).tooltip('show');
    });

    $("#userSelect").select2({language: 'ru'});


    $(document).on('click', '#sendButton', function(){
        var button = $(this);
        if ($("#body").val().length<10){
            alert ("Вы не описали причину обращения! Минимум 10 символов");
        }
        else {
            button.hide();
            var msg   = $('#sendForm').serialize();
            $.ajax({
                url:     '/support/ajax',
                type:     'POST',
                dataType: 'json',
                data: msg,
                success: function(response) {
                    if (response=="ok"){
                        $("#formshow").hide();
                        $("#success").show();
                    }
                    else if (response=="ssesion_err"){
                        alert("Ваша ссесия закрыта по таймауту. Войдите снова!");
                        window.location.href = "/"
                    }
                    else {
                        alert("Ошибка записи!\n Повторите попытку или обратитесь к администратору.");
                    }
                    $('#sendButton').show();
                },
                error: function(data) {
                    var json = data.responseJSON;
                    if (json) {
                        for (var key in json) {
                            alert(json[key]);
                        }
                        button.show();
                    }
                }
            });

        }
        return false
    });


    /*  Запрос с формы добавления пользователя */
    $(document).on('click', '#userFormAddBtn', function(){

        //Валидация
        if ( $('#user_fio').val().length == 0 || $('#user_alevel').val().length == 0  ){
            alert('Заполните обязательные поля (помечены звездочками)!');
            return false;
        }
        else {
            var msg   = $('#userFormAdd').serialize();
            $.ajax({
                url:     '/support/ajax',
                type:     'POST',
                dataType: 'json',
                data: msg,
                success: function(response) {

                    if (response=="ok"){
                        $('#adduserModal').modal('hide');
                        $("#formshow").hide();
                        $("#success").show();
                    }
                    else if (response=="ssesion_err"){
                        alert("Ваша ссесия закрыта по таймауту. Войдите снова!");
                        window.location.href = "/"
                    }
                    else {
                        alert("Ошибка записи!\n Повторите попытку или обратитесь к администратору.");
                    }

                },
                error: function(data) {
                    var json = data.responseJSON;
                    if (json) {
                        for (var key in json) {
                            alert(json[key]);
                        }
                    }
                }
            });
            return false;
        }



    });


    $("#applyFilter").click(function(){
        var data = $(this).closest('form').find("input,select").not('[name="_token"]').serialize();
        document.location.href = "http://" + site + "/support/listtiket?" + data;
    });

    $(document).on('click', '#complitedBtn', function(){
        $('#compliteModal').modal('hide');
        var id = $("#userModal").val();
        var type = $("#typeModal").val();
        var body = $("#bodycomplite").val();
        $.ajax({
            url:     '/support/ajax',
            type:     'POST',
            dataType: 'json',
            data: {id:id, type:type, body:body},
            success: function(response) {

                if (response=="ssesion_err"){
                    alert("Ваша ссесия закрыта по таймауту. Войдите снова!");
                    window.location.href = "/"
                }
                else  {
                    var s=location.pathname;
                    var page = s.substr(s.lastIndexOf("/")+1);
                    $("#block").load("http://"+site+"/support/"+page+""+siteid+" #block");
                }

            }
        });
    });

    $(document).on('click', '.shwtooltip', function(){
        var id = $(this).data("id");
        var type = $(this).data("status");
        if (type=="complite" || type=="proroguep"){
            $("#userModal").val(id);
            $(".title-modal").html((type=="complite")?"<h3>Завершение задачи</h3>":"<h3>Отложить задачу</h3>");
            $("#typeModal").val(type);
            $('#compliteModal').modal('show');

        }
        else {
            $.ajax({
                url:     '/support/ajax',
                type:     'POST',
                dataType: 'json',
                data: {id:id, type:type},
                success: function(response) {
                    if (response=="ssesion_err"){
                        alert("Ваша ссесия закрыта по таймауту. Войдите снова!");
                        window.location.href = "/"
                    }
                    else {

                        var s=location.pathname;
                        var page = s.substr(s.lastIndexOf("/")+1);
                        $("#block").load("http://"+site+"/support/"+page+""+siteid+" #block");

                    }
                }
            });
        }

    });

    $(document).on('click','#clickhistory', function(){
        $("#showhistory").slideToggle();
    });



    //Проверяем статус
    $(document).on('click','.openOrderURL', function(){
        var status = $(this).data('status');
        if (status==1){
            var id = $(this).data('id');
            $.ajax({
                url:     '/support/ajax',
                type:     'POST',
                dataType: 'json',
                data: {id:id, status:status}
            });
        }
    });


    //Обновляем страницу по таймеру, на проверку того, что ссесия не истекла
    //проверка, что ссеисия пользователя не окончилась
    function chkSsesion(){
        $.ajax({
            url:     '/support/chkauth',
            type:     'GET',
            dataType: 'json',
            data: {url:document.location.href},
            success: function(response) {
                if (response=="ssesion_err"){
                    alert("Ваша ссесия истекла! Вы будете перенаправлены на страницу авторизации.");
                    location.href = '/auth/login';
                }
            }
        });
    }
    //setInterval(function() { chkSsesion() }, 7000);



    //дедлайн
    $('.adddeadline').click(function(){
        $('#deadline_task_id').val($(this).data('id'));
        $('#deadlineModal').modal('show');
    });

    $(document).on('click', '#compliteDeadlinedBtn',function(){
        var timeDeadline = $('#deadline_time').val();
        var taskDeadline = $('#deadline_task_id').val();
        if (timeDeadline.length!=10){
            alert('Вы не установили время дедлайна!');
            return false;
        } else {
            $.ajax({
                url:     '/support/ajax',
                type:     'POST',
                dataType: 'json',
                data: { deadline_id:taskDeadline, deadline_time:timeDeadline },
                success: function(response) { location.reload(); }
            });
            $('#deadlineModal').modal('hide');
        }
    });

    if (typeof $.datepicker != 'undefined') $( ".datapiker_time" ).datepicker({monthNames: ['Январь', 'Февраль', 'Март', 'Апрель',
        'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
        'Октябрь', 'Ноябрь', 'Декабрь'],
        dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        firstDay: 1});

    $(document).on('click', '.compliteBtn', function(){
        var id = $(this).data('id');
        var status = $(this).data('status');

        $.ajax({
            url:     '/support/maintikets',
            type:     'POST',
            dataType: 'json',
            data: { confirm_id:id, confirm_status:status  },
            success: function(response) {
                alert('Ваш ответ обработан. Спасибо!');
                $("#block").load(document.location.href+" #block");
            }
        });
    });

    //задачу добавить в jiro
    $(document).on('click','#addToJiro',function(){
        $('#jiroModal').modal('show');
    });
    $(document).on('click','#addJiroBtn',function(){
        var id = $(this).data('id');
        var jiro_name = $('#jiro_name').val();
        var jiro_pass = $('#jiro_pass').val();
        var jiro_task = $('#type_task').val();
        if(jiro_name.length==0 || jiro_pass.length==0){
            alert('Вы не указали Ваш логин и/или пароль от JIRO!');
            return false;
        }  else {
            $('#addJiroBtn').hide();
            $.ajax({
                url:     '/support/addjiro',
                type:     'POST',
                dataType: 'json',
                data: { support_id:id, jiro_name:jiro_name, jiro_pass:jiro_pass, jiro_task:jiro_task,},
                success: function(response) {
                    if (response=="ok"){
                        alert('Заявка успешно добавлена в JIRA');
                        //Если поставили Запомнить - отправляю запрос на запись в куки
                        if ( $('#rememberPassJIRA').is(':checked') == true ){
                            $.ajax({
                                url:     '/support/addjiro',
                                type:     'POST',
                                dataType: 'json',
                                data: { jiro_name:jiro_name, jiro_pass:jiro_pass, jiro_remember:1 }
                            });
                        }
                        location.reload();
                    }
                    else { alert('Ошибка добавления, повторите снова или сообщите администратору!'); }
                },
                error: function(xhr, status, error) {
                    console.log(xhr); //console.log(status); console.log(error);
                    //alert('Ваша ссесия истекла! Авторизуйтесь!');
                    //location.href = '/auth/login';
                }
            });
            $('#jiroModal').modal('hide');
            $('#addJiroBtn').show();
        }


    });

});