$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function(){

    $(document).on('click','#mainsubmit', function(){
        $('#ajaxloader').show();
        $.ajax({
            url: '/reports/UnloadingAlphaBank/ajax',
            method: "POST",
            dataType: "json",
            encode: true,
            data: $("form").serialize(),
            success: function (response) {
                $('#ajaxloader').hide();
                $('#mainsubmit').hide();
                var html = '<a href="'+response+'" class="btn">Скачать</a>';
                $('#input_form').append(html);
            }
        });

        return false;

    });

});