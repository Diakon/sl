$(document).ready(function() {
    $('.card_expire').mask("99/99");
    $('.bik').mask("999999999");

    $("#object_filter, #card_status_filter, #banks_list_filter, #pers_status_list").select2();

    $(document).on('click', '.applyRow', function(){

        //Получаем данные по строкам
        var id_anketa = $(this).data('id');
        var arr = [];

        $(this).parent().parent().find('.rowData').each(function (index, value){
            var item = $(this);
            var table = item.data('table');
            var feeld = item.data('feeld');
            arr.push(table + '~' + feeld + '~' + $(this).val());
        });
        $.ajax({
            type: 'POST',
            url: '/bankcard/ajax',
            dataType: "html",
            data: {id_anketa:id_anketa, param:arr, type:1},
            success: function(data) {
            }
        });
    });

    $(document).on('click', '.applyToSelectRows',function(){
        var status = $('#status_to_all').val();
        $('table tbody input:checkbox:checked').each(function(){
            var parent = $(this).parent().parent();
            //Привеняем значения в строках
            if ( status.length>0 ){
                parent.find("select[name='card_status']").val(status);
            }
            parent.find('.applyRow').click();
        });
        return false;
    });


    $(document).on('click','.addNewBankBtn', function(){
        //Проверка полей
        var err = '';
        var bank_select = $('#new_banks_list_select').val();
        var bank_name = $('#new_bank_name').val();
        var bik_name = $('#new_bik_name').val();

        if ( bik_name.length>0 && (bank_select.length==0 && bank_name.length==0) ){
            err += 'Указывая новый БИК Вы должны либо выбрать банк из выпадающего списка, либо ввести название нового банка в поле ввода!\n';
        }
        if ( bik_name.length==0 && bank_name.length==0 ){
            err += 'Для сохранения данных укажите банк и(если не обходимо) БИК !\n';
        }
        if (err.length>0){
            alert(err);
            return false;
        } else {
            $.ajax({
                type: 'POST',
                url: '/bankcard/ajax',
                dataType: "html",
                data: {bank_select:bank_select, bik_name:bik_name, bank_name:bank_name, type:3},
                success: function(data) {
                    location.reload()
                }
            });
        }

    });

    $(document).on('change', '.bank_name_select', function(){
        var classBik = 'bank_'+$(this).val();
        var parent = $(this).parent().parent();

        parent.find('.bank_bik_name_select').val('0');
        parent.find('.bank_bik_name_select option').hide();
        parent.find(".bank_bik_name_select option[class='" + classBik + "']").show();
    });

    //При открытии страницы  - настраиваем SELECT БИКов
    $('table tbody .bank_bik_name_select').each(function(){
        var item = $(this);
        //Привеняем значения в строках
        var classBik = item.parent().parent().find('.bank_name_select').val();
        item.find('option').hide();
        item.parent().find(".bank_bik_name_select option[class='" + classBik + "']").show();
    });
    $('#new_bik_name').mask("999999999");
});

