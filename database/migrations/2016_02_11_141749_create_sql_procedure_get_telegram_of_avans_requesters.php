<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSqlProcedureGetTelegramOfAvansRequesters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
            CREATE PROCEDURE GET_TELEGRAM_OF_AVANS_REQUESTERS()
                NO SQL
            BEGIN
              (
                SELECT
                  a.object_id AS obj, a.user_id AS kemzakaz,
                  b.coordinator AS otvetstv,
                  c.telegram_id AS telegram,
                  d.name AS objname
                FROM tuov_advance_request AS a
                  LEFT JOIN objects AS b ON a.object_id=b.id
                  LEFT JOIN users AS c ON b.coordinator = c.userid
                  LEFT JOIN objects AS d ON a.object_id = d.id
                WHERE a.week=CONCAT(YEAR(CURDATE()),"-",CONCAT(WEEK(CURDATE())+1))
                  AND a.active = 1
                  AND c.telegram_id IS NOT NULL
                  AND (c.enable = 1 OR c.enable IS NULL)
              ) UNION (
                SELECT
                  a.object_id AS obj, a.user_id AS kemzakaz,
                  b.taskmaster_id AS otvetstv,
                  c.telegram_id AS telegram,
                  d.name AS objname
                FROM tuov_advance_request AS a
                  LEFT JOIN taskmaster_objects AS b ON a.object_id = b.object_id
                  LEFT JOIN users AS c ON b.taskmaster_id = c.userid
                  LEFT JOIN objects AS d ON a.object_id = d.id
                WHERE a.week = CONCAT(YEAR(CURDATE()),"-",CONCAT(WEEK(CURDATE())+1))
                  AND a.active = 1
                  AND c.telegram_id IS NOT NULL
                  AND (c.enable = 1 OR c.enable IS NULL)
              );
            END
SQL;

        $this->down();
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS GET_TELEGRAM_OF_AVANS_REQUESTERS');
    }
}
