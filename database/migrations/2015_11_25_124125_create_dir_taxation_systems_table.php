<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateDirTaxationSystemsTable extends Migration
{
    private $_tableName = 'dir_taxation_systems';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->_tableName)) {
            Schema::create($this->_tableName, function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
            });
            Db::table($this->_tableName)->insert([
                ['name' => 'ОСНО'],
                ['name' => 'УСН'],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->_tableName);
    }
}
