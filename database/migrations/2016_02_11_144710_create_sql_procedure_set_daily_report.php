<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSqlProcedureSetDailyReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
            CREATE PROCEDURE SET_DAILY_REPORT()
                NO SQL
            BEGIN
                DECLARE dailysumm INT DEFAULT(0);
                DECLARE monthmed INT DEFAULT(0);

                SELECT SUM(t.day_shift + t.night_shift) INTO dailysumm
                FROM daily_report t
                WHERE date = CURDATE()
                  AND NOT EXISTS (
                    SELECT *
                    FROM daily_report
                    WHERE date = CURDATE()
                      AND subdivision_id = t.subdivision_id
                      AND timestamp > t.timestamp
                  );

                SELECT (SUM(t.day_shift + t.night_shift) / DAY(CURDATE())) INTO monthmed
                FROM daily_report t
                WHERE MONTH(date) = MONTH(CURDATE())
                  AND YEAR(date) = YEAR(CURDATE())
                  AND NOT EXISTS (
                    SELECT *
                    FROM daily_report
                    WHERE date = t.date
                      AND subdivision_id = t.subdivision_id
                      AND timestamp > t.timestamp
                  );

                INSERT INTO report_daily_summary (id, dailysumm, monthlymed, datestamp) VALUES (NULL, dailysumm, monthmed, CURDATE());

                SELECT dailysumm, monthmed;
            END
SQL;

        $this->down();
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS SET_DAILY_REPORT');
    }
}
