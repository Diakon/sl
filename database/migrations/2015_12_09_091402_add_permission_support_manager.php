<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\Permission;

class AddPermissionSupportManager extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        (new Permission())->firstOrCreate([
            'name' => 'support-manager',
            'display_name' => 'Работа с заявками',
            'description' => 'Обработка тикетов  в службу поддержки'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::whereName('support-manager')->delete();
    }
}
