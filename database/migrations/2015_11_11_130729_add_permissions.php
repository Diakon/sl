<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\Permission;

class AddPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        (new Permission())->firstOrCreate([
            'name' => 'employment-requests',
            'display_name' => 'Все заявки',
            'description' => 'Заявки от будущих сотрудников и их редактирование'
        ]);
        (new Permission())->firstOrCreate([
            'name' => 'employment-agencies',
            'display_name' => 'Кадровые агенства',
            'description' => 'Управление кадровыми агенствами и их сотрудниками'
        ]);
        (new Permission())->firstOrCreate([
            'name' => 'tuov-allowance',
            'display_name' => 'Надбавки лин.бригадиров',
            'description' => 'Управление надбавками линейных бригадиров при расчете з/п, редактирование'
        ]);
        (new Permission())->firstOrCreate([
            'name' => 'deals',
            'display_name' => 'Виды сделок',
            'description' => 'Управление видами сделок'
        ]);
        (new Permission())->firstOrCreate([
            'name' => 'tuov-deals',
            'display_name' => 'Ставки сделок',
            'description' => 'Управление Ставками сделок'
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::whereName('employment-requests')->delete();
        Permission::whereName('employment-agencies')->delete();
        Permission::whereName('tuov-allowance')->delete();
        Permission::whereName('deals')->delete();
        Permission::whereName('tuov-deals')->delete();
    }
}
