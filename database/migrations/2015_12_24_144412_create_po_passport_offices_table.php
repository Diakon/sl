<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoPassportOfficesTable extends Migration
{
    private $_tableName = 'po_passport_offices';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->_tableName)) {
            Schema::create($this->_tableName, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('po_job_application')->index('po_job_application');
                $table->tinyInteger('status');
                $table->timestamp('start_date');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->_tableName);
    }
}
