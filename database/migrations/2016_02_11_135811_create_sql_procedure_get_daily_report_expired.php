<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSqlProcedureGetDailyReportExpired extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
            CREATE PROCEDURE GET_DAILY_REPORT_EXPIRED()
                NO SQL
            BEGIN
                SELECT `name`
                FROM `objects`
                WHERE `comments` LIKE 'в работе'
                  AND `name` NOT IN (
                    SELECT t.names as objects
                    FROM (SELECT SUM(t.`day_shift`+ t.`night_shift`) as sum, c.`name` AS names
                          FROM daily_report t
                          LEFT JOIN subdivision AS b ON t.`subdivision_id`=b.`id`
                          LEFT JOIN objects AS c ON b.`object_id`=c.`id`
                          WHERE date = CURDATE()
                            AND NOT EXISTS (SELECT * FROM daily_report WHERE date = CURDATE() AND subdivision_id = t.subdivision_id AND timestamp > t.`timestamp` )
                          GROUP BY c.`name`) t
                    WHERE t.sum > 0);
            END
SQL;

        $this->down();
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS GET_DAILY_REPORT_EXPIRED');
    }
}
