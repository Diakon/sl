<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePoJobVacancy extends Migration
{
    private $_tableName = 'po_job_vacancy';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->_tableName, function ($table) {
            $table->integer('parent_id')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->_tableName, function ($table) {
            $table->dropColumn('parent_id');
        });
    }
}
