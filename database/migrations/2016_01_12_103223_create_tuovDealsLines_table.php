<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTuovDealsLinesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tuov_deal_lines', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('deal_id');
			$table->integer('subdivision_id');
			$table->date('from');
			$table->date('to');
			$table->float('koef_min')->nullable();
			$table->float('koef_max')->nullable();
			$table->float('rate');
			$table->timestamps();
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tuov_deal_lines');
	}

}
