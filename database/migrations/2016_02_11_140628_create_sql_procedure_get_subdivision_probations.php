<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSqlProcedureGetSubdivisionProbations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
            CREATE PROCEDURE GET_SUBDIVISION_PROBATIONS()
                NO SQL
            BEGIN
                SELECT
                  subdivision_id AS 'ID подразделения',
                  name AS 'Подразделение',
                  object AS 'Объект',
                  probation_days AS 'Обяз. стаж.',
                  repeat_days AS 'Обяз. повт. стаж.',
                  object_days AS 'Обязю с таж. на объетк'
                FROM subdivision_probations AS SP
	              INNER JOIN subdivision AS S ON S.id = SP.subdivision_id;
            END
SQL;

        $this->down();
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS GET_SUBDIVISION_PROBATIONS');
    }
}
