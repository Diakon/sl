<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PoJobApplication extends Migration
{
    private $_tableName = 'po_job_application';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->_tableName)) {
            Schema::create($this->_tableName, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('job_vacancy_id');
                $table->integer('author_id')->unsigned();
                $table->tinyInteger('manage_costs')->unsigned();
                $table->integer('request')->unsigned();
                $table->decimal('vz_employee', 10, 2)->unsigned();
                $table->decimal('payment_us_no_nds', 10, 2)->unsigned();
                $table->integer('schedule_day')->unsigned();
                $table->integer('schedule_week')->unsigned();
                $table->integer('clock')->unsigned();
                $table->decimal('payed', 10, 2)->unsigned();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->_tableName);
    }
}
