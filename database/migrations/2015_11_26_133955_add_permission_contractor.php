<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\Permission;

class AddPermissionContractor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        (new Permission())->firstOrCreate([
            'name' => 'contractor',
            'display_name' => 'Контрагенты',
            'description' => 'Работа со списком контрагентов: добавление, редактирование, удаление'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::whereName('contractor')->delete();
    }
}
