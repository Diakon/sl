<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSqlProcedureGetUserSubdivisions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
            CREATE PROCEDURE GET_USER_SUBDIVISIONS(IN brig_id INT)
                NO SQL
            BEGIN
                (
                  SELECT DISTINCT(b.id)
                  FROM taskmaster_objects AS a
                    LEFT JOIN subdivision AS b ON a.object_id = b.object_id
                  WHERE b.subdivision_comments = "в работе" AND a.taskmaster_id = brig_id
                ) UNION DISTINCT (
                  SELECT d.id
                  FROM objects AS c
                    LEFT JOIN subdivision AS d ON c.id = d.object_id
                  WHERE d.subdivision_comments = "в работе" AND c.coordinator = brig_id
                ) UNION DISTINCT (
                  SELECT e.subdivision_id
                  FROM taskmaster_subdivisions AS e
                  WHERE e.taskmaster_id = brig_id
                );
            END
SQL;

        $this->down();
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS GET_USER_SUBDIVISIONS');
    }
}
