<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDealIdToTuovAllowance extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('tuov_allowance', function($table)
        {
            $table->integer('deal_id')->nullable();
            $table->integer('subdivision_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('tuov_allowance', function($table)
        {
            $table->dropColumn('deal_id');
            $table->dropColumn('subdivision_id');
        });
    }
}
