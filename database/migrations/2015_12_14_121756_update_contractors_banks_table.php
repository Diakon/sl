<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateContractorsBanksTable extends Migration
{
    private $_tableName = 'contractor_banks';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->_tableName, function ($table) {
            $table->tinyInteger('is_active');
            $table->dropColumn('inn_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->_tableName, function ($table) {
            $table->dropColumn('is_active');
            $table->integer('inn_id');
        });
    }
}
