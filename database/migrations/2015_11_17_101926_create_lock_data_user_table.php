<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLockDataUserTable extends Migration
{
    private $_tableName = 'lock_data_user';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->_tableName)) {
            Schema::create($this->_tableName, function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('user_id')->comment('Пользователь, который работает с таблицей');
                $table->string('route_name')->comment('роут таблицы');
                $table->timestamp('created_at')->default(\DB::raw('CURRENT_TIMESTAMP'));
                $table->timestamp('updated_at');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->_tableName);
    }
}
