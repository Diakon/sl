<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\Permission;

class AddPermissionBankCard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        (new Permission())->firstOrCreate([
            'name' => 'bank-card',
            'display_name' => 'Банковские карты',
            'description' => 'Работа с банковскими картами пользователей'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::whereName('bank-card')->delete();
    }
}
