<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDirDormitoryFmsTable extends Migration
{
    private $_tableName_dormitory = 'dir_dormitory_fms';
    private $_tableName_pers = 'pers_main';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->_tableName_dormitory)) {
            Schema::create($this->_tableName_dormitory, function(Blueprint $table)
            {
                $table->increments('id');
                $table->smallInteger('code');
                $table->integer('index');
                $table->string('region', 50);
                $table->string('district', 50);
                $table->string('city', 50);
                $table->string('street', 100);
                $table->string('house', 10);
                $table->string('building', 10);
            });
            $addresses = [
                '643,127204,Московская,,Долгопрудный,Дмитровское шоссе,167,',
                '643,143000,Московская,,Реутов,Железнодорожная,7,а',
                '643,140150,Московская,Раменский,Быково,Советская,19,',
                '643,142108,Московская,,Подольск,Курчатова,20,',
                '643,141241,Московская,,Пушкино,Рабочая,1,',
                '643,143444,Московская,,Красногорск,Ткацкой Фабрики,9,'
            ];
            \DB::table($this->_tableName_dormitory)->insert(array_map(function ($array) {
                $return = [];
                list(
                    $return['code'],$return['index'],$return['region'], $return['district'],
                    $return['city'],$return['street'],$return['house'],$return['building']) = explode(',',$array);
                return $return;
            },$addresses));
        }

        if (Schema::hasTable($this->_tableName_pers)) {
            Schema::table($this->_tableName_pers, function ($table) {
                $table->string('dormitory_fms_id')->nullable();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->_tableName_dormitory);
        if (Schema::hasTable($this->_tableName_pers)) {
            Schema::table($this->_tableName_pers, function ($table) {
                $table->dropColumn('dormitory_fms_id');
            });
        }
    }
}
