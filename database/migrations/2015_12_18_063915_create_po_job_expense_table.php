<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoJobExpenseTable extends Migration
{
    private $_tableName = 'po_job_expenses';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->_tableName)) {
            Schema::create($this->_tableName, function (Blueprint $table) {
                $table->increments('id');
                $table->integer('object_id')->index('object_id');
                $table->integer('author_id');
                $table->decimal('dormitory_cost',10,2);
                $table->decimal('transport_cost',10,2);
                $table->decimal('food_cost',10,2);
                $table->decimal('mk_cost',10,2);
                $table->decimal('ud_cost',10,2);
                $table->decimal('other_cost',10,2);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->_tableName);
    }
}
