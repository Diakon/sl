<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\Permission;

class AddPermissionAccountingReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        (new Permission())->firstOrCreate([
            'name' => 'accounting-report',
            'display_name' => 'Бухгалтерские выгрузки',
            'description' => 'Выгрузка отчетов для бухгалтерии'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::whereName('accounting-report')->delete();
    }
}
