<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ApiList extends Migration
{
    private $_tableNameApiList = 'api_list';
    private $_tableNameApiLog = 'api_log';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->_tableNameApiList)) {
            Schema::create($this->_tableNameApiList, function (Blueprint $table) {
                $table->increments('id', 11);
                $table->string('token', 100)->unique();
                $table->integer('status_api')->unsigned();
                $table->text('access');
                $table->timestamps();
            });
        }

        if (!Schema::hasTable($this->_tableNameApiLog)) {
            Schema::create($this->_tableNameApiLog, function (Blueprint $table) {
                $table->increments('id', 11);
                $table->text('data_processing');
                $table->integer('api_list_id')->unsigned();
                $table->string('user_ip', 25);
                $table->string('operation_code', 25);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->_tableNameApiList);
        Schema::dropIfExists($this->_tableNameApiLog);
    }
}
