<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePoJobApplicationTable extends Migration
{
    private $_tableName = 'po_job_application';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table($this->_tableName, function ($table) {
            $table->renameColumn('schedule_week', 'schedule_month');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->_tableName, function ($table) {
            $table->renameColumn('schedule_month', 'schedule_week');
        });
    }
}
