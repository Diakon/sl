<?php

use Illuminate\Database\Migrations\Migration;
use \App\Models\Permission;

class AddPermissionTuovDocument extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        (new Permission())->firstOrCreate([
            'name' => 'tuov-documents',
            'display_name' => 'Работа с ТУОВ',
            'description' => 'Обработка ТУОВ документов, их добавление'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::whereName('tuov-documents')->delete();
    }
}
