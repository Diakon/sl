<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SubdivisionProbations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
            ALTER TABLE `subdivision_probations`
            ADD COLUMN `date_from` DATE NULL DEFAULT NULL AFTER `object_days`;
            ALTER TABLE `subdivision_probations`
            ADD COLUMN `date_to` DATE NULL DEFAULT NULL AFTER `date_from`;
SQL;
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $sql = <<<SQL
         ALTER TABLE `subdivision_probations` DROP COLUMN `date_from`;
         ALTER TABLE `subdivision_probations` DROP COLUMN `date_to`;
SQL;
        DB::unprepared($sql);
    }
}
