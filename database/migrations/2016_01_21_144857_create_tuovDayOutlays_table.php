<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTuovDayOutlaysTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tuov_day_outlay', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('subdivision_id');
			$table->float('rate');
			$table->date('date_from');
			$table->date('date_to');
			$table->text('name');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tuov_day_outlay');
	}

}
