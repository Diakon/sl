<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSqlProcedureGetWorkersWithBadCards extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
            CREATE PROCEDURE GET_WORKERS_WITH_BAD_CARDS(IN oid INT)
                NO SQL
            BEGIN
                SELECT
                  PM.id_anketa as ID,
                  CONCAT(PM.firstname, ' ', PM.lastname) AS FIO,
                  CONCAT('http://vls.secondlab.ru/op/anketa.php?select=update_form&id=', PM.id_anketa) AS URL
                FROM `pers_status` AS PS
                  INNER JOIN pers_main AS PM ON PS.id_anketa = PM.id_anketa
                  INNER JOIN pers_bank AS PB ON PB.id_anketa = PM.id_anketa
                WHERE PS.status_name = 9
                  AND PS.status_subdivision IN (SELECT id FROM `subdivision` WHERE object_id = oid)
                  AND (CHAR_LENGTH(PB.card_number) < 5 OR CHAR_LENGTH(PB.account)<5 OR PB.bank_bik_id<1);
            END
SQL;

        $this->down();
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS GET_WORKERS_WITH_BAD_CARDS');
    }
}
