<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoObjectsTable extends Migration
{
    private $_tableName = 'po_objects';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->_tableName)) {
            Schema::create($this->_tableName, function (Blueprint $table) {
                $table->increments('id');
                $table->string('name',16);
                $table->string('address',193);
                $table->integer('dormitory_id');
                $table->tinyInteger('is_active');
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->_tableName);
    }
}
