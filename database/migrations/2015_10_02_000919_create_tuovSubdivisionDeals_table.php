<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTuovSubdivisionDealsTable extends Migration
{
	private $_tableName = 'tuovSubdivisionDeals';

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		if (!Schema::hasTable($this->_tableName)) {
			Schema::create($this->_tableName, function (Blueprint $table) {
				$table->increments('id');
				$table->integer('deal_id');
				$table->integer('subdivision_id');
				$table->date('from');
				$table->date('to');
				$table->timestamps();
				$table->softDeletes();
			});
		}
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists($this->_tableName);
	}

}
