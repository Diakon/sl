<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSqlProcedureGetDailyReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
            CREATE PROCEDURE GET_DAILY_REPORT()
                NO SQL
            BEGIN
                DECLARE dailysumm INT DEFAULT(0);
                DECLARE monthmed INT DEFAULT(0);
                DECLARE monthallmed INT DEFAULT(0);
                DECLARE tomorrowplan INT DEFAULT(0);

                SELECT SUM(t.`day_shift` + t.`night_shift`) INTO dailysumm FROM daily_report t WHERE date = CURDATE() AND NOT EXISTS (SELECT * FROM daily_report WHERE date = CURDATE() AND subdivision_id = t.subdivision_id AND timestamp > t.`timestamp` );
                SELECT (SUM(t.`day_shift` + t.`night_shift`)/DAY(CURDATE())) INTO monthmed FROM daily_report t WHERE MONTH(date) = MONTH(CURDATE()) AND YEAR(date) = YEAR(CURDATE()) AND NOT EXISTS (SELECT * FROM daily_report WHERE date = t.date AND subdivision_id = t.subdivision_id AND timestamp > t.`timestamp` );
                SELECT SUM(monthlymed) INTO monthallmed FROM `report_daily_summary` WHERE MONTH(datestamp)=MONTH(CURDATE()) AND YEAR(datestamp) = YEAR(datestamp);
                SELECT (380*31-(monthmed+monthallmed))/DAY(LAST_DAY(CURDATE()))-DAY(CURDATE()) INTO tomorrowplan;
                SELECT dailysumm,monthmed,tomorrowplan;
            END
SQL;

        $this->down();
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS GET_DAILY_REPORT');
    }
}
