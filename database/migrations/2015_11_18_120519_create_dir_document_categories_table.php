<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateDirDocumentCategoriesTable extends Migration
{
    private $_tableName = 'dir_document_categories';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->_tableName)) {
            Schema::create($this->_tableName, function(Blueprint $table)
            {
                $table->increments('id');
                $table->string('name');
            });
            Db::table($this->_tableName)->insert([
                ['name' => 'почасовка'],
                ['name' => 'аванс'],
                ['name' => 'штраф/премия'],
                ['name' => ''],
                ['name' => ''],
                ['name' => 'СИЗ'],
                ['name' => ''],
                ['name' => ''],
                ['name' => 'Сделка'],
                ['name' => 'Оф.зп'],
                ['name' => 'Акция'],
                ['name' => 'Сл.записка'],
                ['name' => 'Другое'],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->_tableName);
    }
}
