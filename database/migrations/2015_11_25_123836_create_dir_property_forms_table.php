<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class CreateDirPropertyFormsTable extends Migration
{
    private $_tableName = 'dir_property_forms';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->_tableName)) {
            Schema::create($this->_tableName, function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
            });
            Db::table($this->_tableName)->insert([
                ['name' => 'ООО'],
                ['name' => 'ПАО'],
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->_tableName);
    }
}
