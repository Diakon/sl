<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Models\DirDocumentCategory;

class EditDirDocumentCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        (new DirDocumentCategory())->firstOrCreate([
            'name' => 'Cделка (диапазон)'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DirDocumentCategory::whereName('Cделка (диапазон)')->delete();
    }
}
