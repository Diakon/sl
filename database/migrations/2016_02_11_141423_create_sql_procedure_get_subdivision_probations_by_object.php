<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSqlProcedureGetSubdivisionProbationsByObject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
            CREATE PROCEDURE GET_SUBDIVISION_PROBATIONS_BY_OBJECT(IN oid INT)
                NO SQL
            BEGIN
                SELECT
                  subdivision_id AS 'ID подразделения',
                  name AS 'Подразделение',
                  object AS 'Объект',
                  probation_days AS 'Обяз. стаж.',
                  repeat_days AS 'Обяз. повт. стаж.',
                  object_days AS 'Обязю стаж. на объетк'
                FROM subdivision_probations AS SP
	              INNER JOIN subdivision AS S ON S.id = SP.subdivision_id
                WHERE oid = S.object_id
                ORDER BY name;
            END
SQL;

        $this->down();
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS GET_SUBDIVISION_PROBATIONS_BY_OBJECT');
    }
}
