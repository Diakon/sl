<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSqlProcedureGetSoisk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $sql = <<<SQL
            CREATE PROCEDURE GET_SOISK()
                NO SQL
            BEGIN
                DECLARE dailysumm INT DEFAULT(0);

                SELECT COUNT(*) INTO dailysumm FROM `soiskateli` WHERE 1;
                SELECT `city`,count(`city`) AS kolvo, count(`city`)/dailysumm FROM `soiskateli` WHERE 1 GROUP BY `city` ORDER BY kolvo DESC;
            END
SQL;

        $this->down();
        DB::unprepared($sql);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE IF EXISTS GET_SOISK');
    }
}
