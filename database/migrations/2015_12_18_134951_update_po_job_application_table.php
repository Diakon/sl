<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePoJobApplicationTable extends Migration
{
    private $_tableName = 'po_job_application';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table($this->_tableName, function ($table) {
            $table->tinyInteger('object_id')->index('object_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table($this->_tableName, function ($table) {
            $table->dropColumn('object_id');
        });
    }
}
