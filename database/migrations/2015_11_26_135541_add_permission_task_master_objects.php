<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use \App\Models\Permission;

class AddPermissionTaskMasterObjects extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        (new Permission())->firstOrCreate([
            'name' => 'task-master-objects',
            'display_name' => 'Назначение координаторов',
            'description' => 'Работа со списком координаторов: добавление, редактирование, удаление'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Permission::whereName('task-master-objects')->delete();
    }
}
