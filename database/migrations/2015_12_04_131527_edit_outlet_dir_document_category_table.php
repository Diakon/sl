<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class EditOutletDirDocumentCategoryTable extends Migration
{
    private $_tableName = 'dir_document_categories';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table($this->_tableName)->where('id', 7)->update(['name' => 'Выходные']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table($this->_tableName)->where('id', 7)->update(['name' => '']);
    }
}
