[
    {
        "field":"deal_id:integer",
        "type":"select:1,2",
        "validations": "required"
    },
    {
        "field":"rate:float",
        "type":"text",
        "validations": "required"
    },
    {
        "field":"subdivision_id:integer",
        "type":"select:1,2",
        "validations": "required"
    },
    {
        "field":"from:date",
        "type":"text",
        "validations": "required"
    },
    {
        "field":"to:date",
        "type":"text",
        "validations": "required"
    }
]