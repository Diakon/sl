[
    {
        "field":"subdivision_id:integer",
        "type":"select:test,",
        "validations": "required"
    },
    {
        "field":"rate:float",
        "type":"text,",
        "validations": "required"
    },
    {
        "field":"date_from:date",
        "type":"text",
        "validations": "required"
    },
    {
        "field":"date_to:date",
        "type":"text",
        "validations": "required"
    },
    {
        "field":"name:text",
        "type":"text",
        "validations": "required"
    }

]