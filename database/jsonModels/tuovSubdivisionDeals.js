[
    {
        "field":"deal_id:integer",
        "type":"select:test,",
        "validations": "required"
    },
    {
        "field":"subdivision_id:integer",
        "type":"select:test,",
        "validations": "required"
    },
    {
        "field":"from:date",
        "type":"text",
        "validations": "required"
    },
    {
        "field":"to:date",
        "type":"text",
        "validations": "required"
    }
]