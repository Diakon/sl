<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Subdivision as Subdivision;
use App\Models\Deal as Deal;
use App\Models\TuovDealRates as TuovDealRates;
use Carbon\Carbon as Carbon;

class DatabaseSeeder extends Seeder
{

    protected $faker;

    public function __construct()
    {
        $this->faker = \Faker\Factory::create();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();


        //$faker = \Faker\Factory::create();
        //$this->command->info($faker->word);


        $this->call('DealsSeeder');
        $this->command->info('seeded');
    }

}

class DealsSeeder extends DatabaseSeeder
{
    public function run()
    {

        DB::table('tuov_deal_rates')->truncate();
        DB::table('tuov_deal_names')->truncate();
        DB::table('tuov_deals')->truncate();


        for ($i = 0; $i < 50; $i++) {
            $deal = Deal::create(['name' => $this->faker->word]);
        }


        $subs = Subdivision::getActiveSubdivisions();
        foreach ($subs as $sub_id => $name) {
            for ($i = 0; $i < 3; $i++) {
                $date1 = $this->faker->dateTimeThisYear->format('Y-m-d');
                $date2 = $this->faker->dateTimeBetween($date1)->format('Y-m-d');


                TuovDealRates::create(
                    [
                        'deal_id' => Deal::orderByRaw("RAND()")->first()['id'],
                        'subdivision_id' => $sub_id, 'rate' => $this->faker->randomFloat(4, 0.1, 1),
                        'from' => $date1, 'to' => $date2]);


            }
        }



    }
}

