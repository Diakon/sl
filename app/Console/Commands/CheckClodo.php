<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Storage;

class CheckClodo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clodo:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверка доступности clodo облака';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $this->comment('Clodo check run: ' . (count(Storage::disk('clodo')->getContainers())>0?'true':'false') );
        } catch(\Exception $e) {
            $message = sprintf('api.clodo.ru выдал ошибку: "%s"', $e->getMessage());
            $url = 'http://62.76.188.60:3421/telegram/info/message';
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_TIMEOUT, 5);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode(['secret' => "asdkdf89432jfqji", 'message' => $message]));
            curl_exec($curl);
            curl_close($curl);
            $this->error( $message );
        }

    }
}
