<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Response;

class AuthenticateWithBasicAuthOnce {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if ( !$this->auth->once($this->getBasicCredentials($request)) ) {
			$headers = ['WWW-Authenticate' => 'Basic'];
			return Response('Invalid credentials.', 401, $headers);
		}

		//Обновляем User даты last_date
		$user = $this->auth->user();
		$user->last_date = date('Y-m-d H:i:s');
		$user->save();

        return $next($request);
	}

	protected function getBasicCredentials($request)
	{
		return ['username' => $request->getUser(), 'password' => $request->getPassword(), 'enable' => 1];
	}
}
