<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where all API routes are defined.
|
*/
Route::controller('ko/object/{object}/expense', 'KoObjectExpenseController');

Route::group(['middleware' => ['auth.basic.once', 'ability:admin,']], function() {
    Route::match(['post','get'], 'password/reset', 'PasswordResetController@postIndex');

    Route::match(['post'], 'tuov/create', 'TuovController@postСreate');
    Route::match(['post'], 'support/create', 'SupportController@postСreate');
});

Route::resource("deals", "DealAPIController");
Route::resource("tuovDealRates", "TuovDealRatesAPIController");


Route::resource("tuovDealsLines", "TuovDealsLineAPIController");