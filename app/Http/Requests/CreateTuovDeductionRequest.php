<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\LuovDocument;
use Illuminate\Validation\Factory as ValidationFactory;

class CreateTuovDeductionRequest extends Request {

    public function __construct(ValidationFactory $validationFactory)
    {
        $validationFactory->extend(
            'myCheckExt',
            function ($attribute, $value, $parameters) {
                if ( !is_array($value) ) {
                    return false;
                }
                $validExcelCategory = (new LuovDocument)->validExcelCategory;
                foreach ($this->file($attribute) as $document) {
                    if (!$document) {
                        return false;
                    }
                    $ext = $document->guessClientExtension();
                    $valid_ext = ['png', 'jpg', 'jpeg'];
                    if (in_array($this->input('document_category', 1), $validExcelCategory)) {
                        $valid_ext = array_merge($valid_ext, ['xls', 'xlsx']);
                    }
                    if (!in_array($ext, $valid_ext)) {
                        return false;
                    }
                }
                return true;
            },
            'Поддерживаются следующие форматы файлов: png, jpg, jpeg для выбранного типа документа.'
        );
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return LuovDocument::$rules;
    }

    public function messages()
    {
        return LuovDocument::$messages;
    }

}
