<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\LuovDocument;
use App\Models\Tuov\TuovFabric;

class UpdateTuovDeductionRequest extends Request {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(TuovFabric $tuov)
    {
        $document = LuovDocument::find($this->id);
        if (!$document) {
            abort(404);
        }
        $form = $tuov->fabric($document)->buildForm($document);

        $rules = [];
        $nbr = count($this->input('anketa_id', [])) - 1;
        if ($nbr<0) {
            return $rules;
        }
        foreach(range(0, max($nbr,0)) as $index) {
            $rules['anketa_id.' . $index] = 'required|integer|exists:pers_main,id_anketa';
            foreach($form as $attribute_name => $data) {
                if( isset($data[3]) && isset($data[3]['rules'])) {
                    $rules[preg_replace('!\[(\w+)\]!si', '.$1', $attribute_name) . '.' . $index] = $data[3]['rules'];
                }
            }
        }
        return $rules;
    }

}
