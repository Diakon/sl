<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Contractor\Contractor;

class UpdatePoExpenseRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = [];
		foreach(['dormitory_cost', 'transport_cost', 'food_cost', 'mk_cost', 'ud_cost', 'other_cost'] as $val) {
			$rules[$val] = 'numeric|min:0';
		}
		return $rules;
	}
}
