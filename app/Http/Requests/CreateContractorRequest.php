<?php namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\Contractor\Contractor;

class CreateContractorRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		$rules = Contractor::$rules;

		if ($this->input('type', '1') == '1' || 1) {
			$addRules = [
				"ceo" => "string",
				"chief_accountant" => "string",
				"action_on_basis" => "string",
				"legal_address_index" => "string|min:4|max:20",
				"legal_address_city" => "string",
				"legal_address" => "string",
				"actual_address_index" => "string|min:4|max:20",
				"actual_address_city" => "string",
				"actual_address" => "string",
				"mail_address_index" => "string|min:4|max:20",
				"mail_address_city" => "string",
				"mail_address" => "string",
				"property_form" => "integer|exists:dir_property_forms,id",
				"inn" => "integer",
				"kpp" => "integer",
				"ogrn" => "integer",
				"taxation_system" => "integer|exists:dir_taxation_systems,id",
			];
			$rules += $addRules;
		}

		// проверка банковских счетов
		foreach ($this->input('banks.score', []) as $key=>$bank) {
			$rules['banks.score.' . $key] = 'required|integer|min:0';
			$rules['banks.bank_id.' . $key] = 'required|integer|min:0';
			$rules['banks.is_active.' . $key] = 'integer|in:0,1';
			$rules['banks.is_main.' . $key] = 'integer|in:0,1';
		}

		return $rules;
	}
}
