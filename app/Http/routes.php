<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::controller('auth', 'Auth\AuthController');
Route::get('pw/{token}', ['as' => 'password/token', 'uses' => 'Auth\PasswordController@getReset']);
Route::post('password', ['as' => 'password/reset', 'uses' => 'Auth\PasswordController@postReset']);


Route::group(['middleware' => ['auth']], function() {
    //Route::controller('users', 'UsersController');
    Route::get('anketa/{id}/fmsExcel', ['uses' => 'UsersController@fms']);
    Route::get('/', ['uses' => 'HomeController@index']);

    Route::group(['middleware' => ['ability:admin,']], function() {


        Route::group(['prefix' => 'po', 'namespace' => 'PO'], function() {
            Route::resource('objects', 'ObjectController');
            Route::get('objects/{id}/delete', [
                'as' => 'po.objects.delete',
                'uses' => 'ObjectController@destroy',
            ]);
            Route::group(['prefix' => 'object/{object}'], function() {
                Route::get('expense', ['as' => 'po.expense.index', 'uses' => 'ExpenseController@index']);
                Route::post('expense', ['as' => 'po.expense.update', 'uses' => 'ExpenseController@postIndex']);

                Route::get('applications', ['as' => 'po.applications.index','uses' => 'ApplicationController@index']);
                Route::get('applications/create', ['as' => 'po.applications.create','uses' => 'ApplicationController@create']);
                Route::post('applications/create', ['as' => 'po.applications.store','uses' => 'ApplicationController@store']);
                Route::get('applications/{id}/edit', ['as' => 'po.applications.edit','uses' => 'ApplicationController@edit']);
                Route::patch('applications/{id}/update', ['as' => 'po.applications.update','uses' => 'ApplicationController@update']);
                Route::get('applications/{id}/delete', ['as' => 'po.applications.delete','uses' => 'ApplicationController@destroy']);
            });
            Route::get('expenses', ['as' => 'po.expense.all.index', 'uses' => 'ExpenseController@all']);
            Route::get('applications', ['as' => 'po.applications.all.index','uses' => 'ApplicationController@all']);


            /*
            |--------------------------------------------------------------------------
            | Вакансии
            |--------------------------------------------------------------------------
            */
            Route::resource('vacancies', 'VacancyController');
            Route::get('vacancies/{id}/delete', [
                'as' => 'po.vacancies.delete',
                'uses' => 'VacancyController@destroy',
            ]);

            Route::resource('passportOffices', 'PassportOfficesController');

        });


        /*--------------------------------
        Cлужба Безопасности
        --------------------------------*/
        Route::resource('sbUsers', 'SbUsersController');
        Route::get('sbUsers/{id}/delete', ['as' => 'sbUsers.delete','uses' => 'SbUsersController@destroy']);

        Route::group(['prefix' => 'sb'], function() {
            Route::get('users', ['as' => 'sb.users', 'uses' => 'SbUsersController@index']);
        });

        Route::group(['prefix' => 'admin'], function() {
            Route::group(['prefix' => 'users'], function() {
                Route::match(['get','post'], '', ['as' => 'admin.users.index', 'uses' => 'AdminController@user']);
                Route::get('create', ['as' => 'admin.users.create', 'uses' => 'AdminController@userCreate']);
                Route::post('store', ['as' => 'admin.users.store', 'uses' => 'AdminController@userStore']);
                Route::get('{id}/delete', ['as' => 'admin.users.delete', 'uses' => 'AdminController@userDestroy']);
                Route::get('{id}/edit', ['as' => 'admin.users.edit', 'uses' => 'AdminController@userEdit']);
                Route::patch('{id}/update', ['as' => 'admin.users.update', 'uses' => 'AdminController@userUpdate']);

                Route::group(['prefix' => 'logs'], function() {
                    Route::match(['get','post'], 'auth', ['as' => 'admin.users.log', 'uses' => 'LoginLogController@report']);
                });
            });

            Route::get('applicationLogs', ['as' => 'admin.applicationLogs', 'uses' => '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index' ]);

            Route::group(['prefix' => 'roles'], function() {
                Route::get('', ['as' => 'admin.roles.index', 'uses' => 'AdminController@roles']);
                Route::get('create', ['as' => 'admin.roles.create', 'uses' => 'AdminController@rolesCreate']);
                Route::post('store', ['as' => 'admin.roles.store', 'uses' => 'AdminController@rolesStore']);
                Route::get('{id}/edit', ['as' => 'admin.roles.edit', 'uses' => 'AdminController@rolesEdit']);
                Route::patch('{id}/update', ['as' => 'admin.roles.update', 'uses' => 'AdminController@rolesUpdate']);
            });
        });
    });

    Route::group(['middleware' => ['ability:admin,employment-agencies']], function() {
        Route::resource('agencyStaffing', 'AgencyStaffingController');
        Route::get('agencyStaffing/{id}/delete', ['as' => 'agencyStaffing.delete','uses' => 'AgencyStaffingController@destroy']);

        Route::resource('agencyContacts', 'AgencyContactController');
        Route::get('agencyContacts/{id}/delete', ['as' => 'agencyContacts.delete', 'uses' => 'AgencyContactController@destroy']);
    });

    Route::group(['prefix' => 'scheduler'], function() {
        Route::get('', ['uses' => 'SchedulerController@index', 'middleware' => 'ability:admin,']);
        Route::post('ajax', ['uses' => 'SchedulerController@ajax']);
        Route::post('ajaxuploadfile', ['uses' => 'SchedulerController@ajaxuploadfile']);
        Route::post('ajaxdeletefile', ['uses' => 'SchedulerController@ajaxdeletefile']);
        Route::get('mytasks', ['uses' => 'SchedulerController@mytasks']);
        Route::post('mytasks', ['uses' => 'SchedulerController@mytasks']);
        Route::get('mytasks/show/{task_i?}', ['uses' => 'SchedulerController@showtasks']);
        Route::post('mytasks/show/{task_i?}', ['uses' => 'SchedulerController@showtasks']);
        Route::get('mytasks/showakt/{akt_i?}', ['uses' => 'SchedulerController@showakt']);
        Route::post('ajaxact', ['uses' => 'SchedulerController@ajaxact']);
    });

    Route::group(['prefix' => 'support', 'namespace' => 'Support'], function() {
        Route::get('addtiket', ['uses' => 'SupportController@addtiket']);
        Route::post('ajax', ['uses' => 'SupportController@ajax']);
        Route::post('ajaxform', ['uses' => 'SupportController@ajaxform']);

        Route::get('stattiket', ['uses' => 'SupportController@stattiket', 'middleware' => 'ability:admin,']);
        Route::group(['middleware' => 'ability:admin,support-manager'], function() {
            Route::get('listtiket', ['as' => 'support.listtiket', 'uses' => 'SupportController@listtiket']);
            Route::get('viewtiket', ['as' => 'support.viewtiket', 'uses' => 'SupportController@viewtiket']);
            Route::post('addjiro', ['uses' => 'SupportController@addjiro']);
            Route::get('formcomplite', ['uses' => 'SupportController@formcomplite']);
        });

        Route::post('chkauth', ['uses' => 'SupportController@chkauth']);

        Route::get('maintikets', ['uses' => 'SupportController@maintikets']);
        Route::post('maintikets', ['uses' => 'SupportController@maintikets']);
    });

    /*--------------------------------
    Отчет Fullbase
    --------------------------------*/
    Route::group(['middleware' => ['ability:admin,employment-requests']],function() {
        Route::match(['get','post'], '/fullbase', ['uses' => 'FullbaseController@index']);
        Route::get('/edit/create', ['uses' => 'EditController@create']);
        Route::get('/edit/update/{id?}', ['uses' => 'EditController@update']);
        Route::post('/edit/sms_send_addr', ['uses' => 'EditController@ajaxsms']);
        Route::post('/edit/getphones', ['uses' => 'EditController@getphones']);
        Route::post('/edit/formupdate', ['uses' => 'EditController@formupdate']);
        Route::get('/edit/ajax', ['uses' => 'EditController@ajax']);
        Route::post('/edit/ajax', ['uses' => 'EditController@ajax']);
        Route::post('/fullbase/feelds', ['uses' => 'FullbaseController@setFeeldsSoiskateli']);
    });

    /*------------------------
    Банковские карты
    ------------------------*/
    Route::group(['middleware' => ['ability:admin,bank-card']], function() {
        Route::group(['prefix' => 'bankcard',], function () {
            Route::get('', ['as' => 'bankcard.index', 'uses' => 'BankcardController@index']);
            Route::post('ajax', ['uses' => 'BankcardController@ajax']);
            Route::post('', ['uses' => 'BankcardController@index']);
        });
    });
    /*--------------------------------
    Контрагенты
    --------------------------------*/
    Route::group(['prefix' => 'contractor', 'middleware' => ['ability:admin,contractor']], function() {
        Route::match(['get', 'post'],'', ['as' => 'contractor.index', 'uses' => 'ContractorController@index']);
        Route::get('create', ['as' => 'contractor.create', 'uses' => 'ContractorController@create']);
        Route::post('store', ['as' => 'contractor.store', 'uses' => 'ContractorController@store']);
        Route::get('{id}/edit', ['as' => 'contractor.edit', 'uses' => 'ContractorController@edit']);
        Route::patch('{id}/update', ['as' => 'contractor.update', 'uses' => 'ContractorController@update']);
        Route::get('{id}/delete', ['as' => 'contractor.delete', 'uses' => 'ContractorController@destroy']);
    });

    /*-----------------------
    Анализ статистики FreePBX и вывод пропущенных вызовов
    ------------------------*/
    Route::group(['middleware' => ['ability:admin,employment-requests']], function() {
        Route::group(['prefix' => 'analyze-cc-noanswer',], function () {
            Route::get('', ['uses' => 'ReportNoAnswerController@index']);
        });
    });

    /*-----------------------
    Личный кабинет
    ------------------------*/
    Route::group(['prefix' => 'lcab/telegrams'], function() {
        Route::get('', ['uses' => 'TelegramController@index']);
        Route::post('newtelegramcode', ['uses' => 'TelegramController@newtelegramcode']);
    });

    /*-----------------------
    ТУОВ
    ------------------------*/
    Route::group(['prefix' => 'tuov', 'middleware' => ['ability:admin,tuov-documents']], function () {
        Route::match(['get', 'post'], '', ['as' => 'tuovDeduction.index', 'uses' => 'Tuov\TuovController@index']);
        Route::get('create', ['as' => 'tuovDeduction.create', 'uses' => 'Tuov\TuovController@create']);
        Route::get('{id}/delete', ['as' => 'tuovDeduction.delete', 'uses' => 'Tuov\TuovController@destroy']);
        Route::post('store', ['as' => 'tuovDeduction.store', 'uses' => 'Tuov\TuovController@store']);
        Route::get('{id}/edit', ['as' => 'tuovDeduction.edit', 'uses' => 'Tuov\TuovController@edit']);
        Route::patch('{id}/update', ['as' => 'tuovDeduction.update', 'uses' => 'Tuov\TuovController@update']);
        Route::get('{id}/history', ['as' => 'tuovDeduction.history', 'uses' => 'Tuov\TuovController@getHistory']);
        Route::get('image/{name}.{ext}', ['as' => 'tuovDeduction.image', 'uses' => 'Tuov\TuovController@getImage'])->where('name', '[A-Za-z0-9\._]+');

        Route::get('report', ['as' => 'tuov.report', 'uses' => 'Tuov\TuovController@report']);
    });

    Route::group(['middleware' => ['ability:admin,tuov-deals']], function() {
        Route::resource('tuovDealRates', 'TuovDealRatesController');
        Route::group(['prefix' => 'tuovDealRates'], function () {
            Route::get('{id}/delete', ['as' => 'tuovDealRates.delete', 'uses' => 'TuovDealRatesController@destroy']);
            Route::get('subdivision/{id}', ['as' => 'tuovDealRates.subdivision', 'uses' => 'TuovDealRatesController@subdivision']);
            Route::post('ajax', ['uses' => 'TuovDealRatesController@ajax']);
        });

        Route::group(['prefix' => 'tuov/rates'], function () {
            Route::get('/', ['as' => 'tuov.rates.index', 'uses' => 'Tuov\SubdivisionRatesController@index']);
            Route::get('create', ['as' => 'tuov.rates.create', 'uses' => 'Tuov\SubdivisionRatesController@create']);
            Route::post('store', ['as' => 'tuov.rates.store', 'uses' => 'Tuov\SubdivisionRatesController@store']);
            Route::get('{id}/edit', ['as' => 'tuov.rates.edit', 'uses' => 'Tuov\SubdivisionRatesController@edit']);
            Route::patch('{id}/update', ['as' => 'tuov.rates.update', 'uses' => 'Tuov\SubdivisionRatesController@update']);
            Route::get('{id}/delete', ['as' => 'tuov.rates.delete', 'uses' => 'Tuov\SubdivisionRatesController@destroy']);
        });
    });

    Route::group(['middleware' => ['ability:admin,tuov-allowance']], function() {
        Route::resource('tuovAllowances', 'TuovAllowanceController');
        Route::group(['prefix' => 'tuovAllowances'], function () {
            Route::get('{id}/delete', ['as' => 'tuovAllowances.delete', 'uses' => 'TuovAllowanceController@destroy']);
            Route::post('', ['uses' => 'TuovAllowanceController@index']);
            Route::post('store', ['as' => 'tuovAllowances.store', 'uses' => 'TuovAllowanceController@store']);

        });
    });
    //поиск профилей пользователей
    Route::get('tuovAllowances/find/anketa', ['as' => 'tuovAllowances.find', 'uses' => 'TuovAllowanceController@find']);

    Route::group(['middleware' => ['ability:admin,task-master-objects']], function() {
        Route::resource('taskmasterObjects', 'TaskmasterObjectsController');
        Route::group(['prefix' => 'taskmasterObjects'], function() {
            Route::get('{id}/delete', ['as' => 'taskmasterObjects.delete','uses' => 'TaskmasterObjectsController@destroy']);
        });
    });

    Route::group(['middleware' => ['ability:admin,task-master-objects']], function() {
        Route::resource('subdivisionProbations', 'SubdivisionProbationsController');
        Route::group(['prefix' => 'subdivisionProbations'], function() {
            Route::get('{id}/delete', ['as' => 'subdivisionProbations.delete','uses' => 'SubdivisionProbationsController@destroy']);
        });
    });

    Route::group(['middleware' => ['ability:admin,']], function() {
        Route::resource('subdivisions', 'SubdivisionController');
        Route::group(['prefix' => 'subdivisions'], function () {
            Route::get('{id}/delete', ['as' => 'subdivisions.delete', 'uses' => 'SubdivisionController@destroy']);
            Route::get('{id}/on', ['as' => 'subdivisions.on', 'uses' => 'SubdivisionController@on']);
        });
    });

    Route::group(['middleware' => ['ability:admin,deals']], function() {
        Route::resource('deals', 'DealController');
        Route::group(['prefix' => 'deals'], function () {
            Route::get('{id}/delete', ['as' => 'deals.delete', 'uses' => 'dealController@destroy']);
        });
    });

    Route::group(['middleware' => ['ability:admin,tuov-deals']], function() {

        Route::resource('tuovDealsLines', 'TuovDealsLineController');

        Route::get('tuovDealsLines/{id}/delete', [
            'as' => 'tuovDealsLines.delete',
            'uses' => 'TuovDealsLineController@destroy',
        ]);

    });

    // Дорожные
    Route::group(['middleware' => ['ability:admin,tuov-deals']], function() {

        Route::resource('tuovDayOutlays', 'TuovDayOutlayController');

        Route::get('tuovDayOutlays/{id}/delete', [
            'as' => 'tuovDayOutlays.delete',
            'uses' => 'TuovDayOutlayController@destroy',
        ]);
    });

    Route::resource('persMains', 'PersMainController');
    Route::group(['prefix' => 'persMains'], function() {
        Route::get('{id}/delete', ['as' => 'persMains.delete','uses' => 'PersMainController@destroy']);
    });

    Route::resource('objects', 'ObjectsController');
    Route::group(['prefix' => 'objects'], function() {
        Route::get('{id}/delete', ['as' => 'objects.delete','uses' => 'ObjectsController@destroy']);
    });

    /*
    Route::resource('dealRates', 'DealRateController');
    Route::group(['prefix' => 'dealRates'], function() {
        Route::get('{id}/delete', ['as' => 'dealRates.delete','uses' => 'dealRateController@destroy']);
    });


    Route::resource('subdivisionDeals', 'SubdivisionDealsController');
    Route::group(['prefix' => 'subdivisionDeals'], function() {
        Route::get('{id}/delete', ['as' => 'subdivisionDeals.delete','uses' => 'subdivisionDealsController@destroy']);
    });
    */


    Route::group(['middleware' => ['ability:admin,accounting-report']], function() {
        Route::resource('reports/UnloadingAlphaBank', 'Reports\UnloadingAlphaBankController');
        Route::group(['prefix' => '/reports/UnloadingAlphaBank'], function() {
            Route::post('ajax', [
                'as' => 'reports/UnloadingAlphaBank.ajax',
                'uses' => 'Reports\UnloadingAlphaBankController@ajax',
            ]);
        });
    });




});

Route::get('support/chkauth', [ 'uses' => 'Support\SupportController@chkauth']);


Route::group(['prefix' => 'sb'], function() {
    Route::get('', ['uses' => 'SbUsersController@showdata']);
    Route::post('auth', ['uses' => 'SbUsersController@auth']);
    Route::get('logout', ['uses' => 'SbUsersController@logout']);
});


Route::group(['prefix' => 'personal_area'], function() {
    Route::get('', ['uses' => 'PersonalAreaController@index']);
    Route::post('', ['uses' => 'PersonalAreaController@index']);
});


/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
*/

Route::get('api/test_view/{name}', ['as' => 'sb.users','uses' => 'ApiController@test']);

Route::group(['prefix' => 'api', 'namespace' => 'API', 'middleware' => ['addJsonAcceptHeader']], function ()

{
    Route::group(['prefix' => 'v1'], function ()
    {
        include_once Config::get('generator.path_api_routes');
    });
});


/*=======================
A P I
=======================*/
Route::resource('apis', 'ApiController');

Route::get('apis/{id}/delete', [
    'as' => 'apis.delete',
    'uses' => 'ApiController@destroy',
]);
Route::post('/api/{token?}', 'ApiController@processing');

Route::get('/cron/{token?}', 'CronController@index');

Route::resource('loginLogs', 'LoginLogController');
Route::get('loginLogs/{id}/delete', [
    'as' => 'loginLogs.delete',
    'uses' => 'LoginLogController@destroy',
]);










