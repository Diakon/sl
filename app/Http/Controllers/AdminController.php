<?php namespace  App\Http\Controllers;

use App;
use App\Http\Requests\Admin\CreateUserRequest;
use App\Http\Requests\Admin\UpdateUserRequest;
use App\Http\Requests\Admin\CreateRoleRequest;
use App\Http\Requests\Admin\UpdateRoleRequest;
use App\Libraries\Repositories\UserRepository;
use Illuminate\Http\Request;
use Zizaco\Entrust\Contracts\EntrustPermissionInterface;
use Zizaco\Entrust\Contracts\EntrustRoleInterface;
use Flash;

class AdminController extends Controller {

    private $roles;
    private $permissions;
    private $usersRepository;

    public function __construct(EntrustRoleInterface $roles, EntrustPermissionInterface $permissions, UserRepository $users) {
        $this->roles = $roles;
        $this->permissions = $permissions;
        $this->usersRepository = $users;
    }

    public function user(Request $request) {
        $users = $this->usersRepository->search($request->all());
        return view('admin.users.index')
            ->with('users', $users)->with('request', $request);
    }
    public function userDestroy($id) {
        $user = $this->usersRepository->find($id);

        if (!empty($user)) {
            $this->usersRepository->delete($id);
            Flash::success('user deleted successfully.');
        } else {
            Flash::error('user not found');
        }
        return redirect(route('admin.users.index'));
    }
    public function userCreate() {
        return view('admin.users.create')->with('roles', $this->roles);;
    }
    public function userStore(CreateUserRequest $request) {
        $user = $request->all();
        $user['password'] = hash("sha256", $user['password']);
        if ($new_user = $this->usersRepository->create($user)) {
            $this->roles->addUserRoles($new_user, $request->input('role', []));
        }
        Flash::success('Пользователь успешно добавлен!');
        return redirect(route('admin.users.index'));
    }
    public function userEdit($id) {
        $user = $this->usersRepository->find($id);
        if (empty($user)) {
            Flash::error('Пользователь не найден!');
            return redirect(route('admin.users.index'));
        }
        return view('admin.users.edit')->with('user', $user)->with('roles', $this->roles);
    }
    public function userUpdate(UpdateUserRequest $request, $id) {
        $user = $this->usersRepository->find($id);
        if (empty($user)) {
            Flash::error('Пользователь не найден!');
            return redirect(route('admin.users.index'));
        }
        $input = $request->all();
        if (empty($input['password'])){
            unset($input['password']);
        } else {
            $input['password'] = hash("sha256", $input['password']);
        }
        if ($this->usersRepository->updateRich($input, $id)) {
            $this->roles->addUserRoles($user, $request->input('role', []));
        }

        Flash::success('Данные пользователя ' . $input['username'] . ' успешно обновлены.');

        return redirect(route('admin.users.index'));
    }


    public function roles() {
        return view('admin.roles.index')->with('roles', $this->roles->with('perms'));
    }
    public function rolesCreate() {
        return view('admin.roles.create')->with('roles', $this->roles)->with('permissions', $this->permissions);
    }
    public function rolesStore(CreateRoleRequest $request) {
        if ($role = $this->roles->create($request->all()) ) {
            $role->perms()->sync($this->permissions->whereIn('id', $request->input('permissions', []))->get());
            Flash::success('Роль успешно добавлена!');
        }
        return redirect(route('admin.roles.index'));
    }
    public function rolesEdit($id) {
        return view('admin.roles.edit')->with('role', $this->getRoleById($id))->with('permissions', $this->permissions);
    }
    public function rolesUpdate(UpdateRoleRequest $request, $id) {
        $input = $request->all();
        unset($input['name']);
        $role = $this->getRoleById($id);
        if ($role->fill($input)->save()) {
            $role->perms()->sync($this->permissions->whereIn('id', $request->input('permissions', []))->get());
        }
        Flash::success('Роль ' . $input['display_name'] . ' успешно обновлена.');
        return redirect(route('admin.roles.index'));
    }

    private function getRoleById ($id) {
        $role = $this->roles->find($id);
        if (empty($role)) {
            App::abort(404, 'Role not found');
        }
        return $role;
    }
}


