<?php namespace App\Http\Controllers\API;

use Mitul\Controller\AppBaseController as AppBaseController;

class KoObjectExpenseController extends AppBaseController
{

    /**
     * Расходы на проживание
     * @param $object
     * @return mixed
     */
    public function getDormitory($object)
    {
        $rate = 30;
        $expense = 0;
        foreach ($object->applications as $app) {
            $expense += $app->calendar_days * $rate * $app->request;
        }
        //транспортные расходы = кол-во человек * кол-во дней * ставка
        return $this->sendResponse($expense, '');
    }

    /**
     * Расходы на транспорт
     * @param $object
     * @return mixed
     */
    public function getTransport($object)
    {
        $rate = 30;
        $expense = 0;
        foreach ($object->applications as $app) {
            $expense += $app->work_days * $rate * $app->request;
        }
        //транспортные расходы = кол-во человек * кол-во дней * ставка
        return $this->sendResponse($expense, '');
    }

    /**
     * Расходы на питание
     * @param $object
     * @return mixed
     */
    public function getFood($object)
    {
        $rate = 30;
        $expense = 0;
        foreach ($object->applications as $app) {
            $expense += $app->work_days * $rate * $app->request;
        }
        //транспортные расходы = кол-во человек * кол-во дней * ставка
        return $this->sendResponse($expense, '');
    }

    /**
     * Расходы на МК (мед книжка)
     * @param $object
     * @return mixed
     */
    public function getMk($object)
    {
        $rate = 1000;
        //расходы на документы = кол-во человек * ставка
        return $this->sendResponse($object->applications->sum('request') * $rate, '');
    }

    /**
     * Расходы на УД (удостоверение)
     * @param $object
     * @return mixed
     */
    public function getUd($object)
    {
        $rate = 1000;
        //расходы на документы = кол-во человек * ставка
        return $this->sendResponse($object->applications->sum('request') * $rate, '');
    }

    /**
     * Прочие расходы
     * @param $object
     * @return mixed
     */
    public function getOther($object)
    {
        $rate = 1000;
        //расходы на документы = кол-во человек * ставка
        return $this->sendResponse($object->applications->sum('request') * $rate, '');
    }
}