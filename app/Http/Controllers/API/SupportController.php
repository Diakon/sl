<?php namespace App\Http\Controllers\API;

use App\Libraries\Repositories\SupportRepository;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Symfony\Component\HttpFoundation\RedirectResponse;


class SupportController extends AppBaseController
{

    private $supportRepository;

    function __construct(SupportRepository $supportRepository){
        $this->supportRepository = $supportRepository;

    }

    /**
     * Добавление it заявки
     * @param Request $request
     * @return mixed
     */
    public function postСreate(Request $request)
    {
        $this->validate($request, $this->supportRepository->makeModel()->getRules('tiket'));

        $data = [
            'user_id' => $request->get('user_id'),
            'fromuser' => $request->get( 'fromuser' ),
            'department' => $request->get( 'department', 1),
            'type' => 1,
            'body' => $request->get( 'body' )
        ];
        $result = $this->supportRepository->addTiket($data);
        return $this->sendResponse($result, 'IT заявка ' . ($result?'успешно создана': 'не создана') );
    }
}