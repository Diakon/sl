<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\TuovDealRatesRepository;
use App\Models\TuovDealRates;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class TuovDealRatesAPIController extends AppBaseController
{
	/** @var  TuovDealRatesRepository */
	private $tuovDealRatesRepository;

	function __construct(TuovDealRatesRepository $tuovDealRatesRepo)
	{
		$this->tuovDealRatesRepository = $tuovDealRatesRepo;
	}

	/**
	 * Display a listing of the TuovDealRates.
	 * GET|HEAD /tuovDealRates
	 *
	 * @return Response
	 */
	public function index()
	{
		$tuovDealRates = $this->tuovDealRatesRepository->all();

		return $this->sendResponse($tuovDealRates->toArray(), "TuovDealRates retrieved successfully");
	}

	/**
	 * Show the form for creating a new TuovDealRates.
	 * GET|HEAD /tuovDealRates/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created TuovDealRates in storage.
	 * POST /tuovDealRates
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(TuovDealRates::$rules) > 0)
			$this->validateRequestOrFail($request, TuovDealRates::$rules);

		$input = $request->all();

		$tuovDealRates = $this->tuovDealRatesRepository->create($input);

		return $this->sendResponse($tuovDealRates->toArray(), "TuovDealRates saved successfully");
	}

	/**
	 * Display the specified TuovDealRates.
	 * GET|HEAD /tuovDealRates/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$tuovDealRates = $this->tuovDealRatesRepository->apiFindOrFail($id);

		return $this->sendResponse($tuovDealRates->toArray(), "TuovDealRates retrieved successfully");
	}

	/**
	 * Show the form for editing the specified TuovDealRates.
	 * GET|HEAD /tuovDealRates/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified TuovDealRates in storage.
	 * PUT/PATCH /tuovDealRates/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var TuovDealRates $tuovDealRates */
		$tuovDealRates = $this->tuovDealRatesRepository->apiFindOrFail($id);

		$result = $this->tuovDealRatesRepository->updateRich($input, $id);

		$tuovDealRates = $tuovDealRates->fresh();

		return $this->sendResponse($tuovDealRates->toArray(), "TuovDealRates updated successfully");
	}

	/**
	 * Remove the specified TuovDealRates from storage.
	 * DELETE /tuovDealRates/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->tuovDealRatesRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "TuovDealRates deleted successfully");
	}
}
