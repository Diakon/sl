<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\TuovSubdivisionDealsRepository;
use App\Models\TuovSubdivisionDeals;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class TuovSubdivisionDealsAPIController extends AppBaseController
{
	/** @var  TuovSubdivisionDealsRepository */
	private $tuovSubdivisionDealsRepository;

	function __construct(TuovSubdivisionDealsRepository $tuovSubdivisionDealsRepo)
	{
        $this->middleware('auth');
        $this->tuovSubdivisionDealsRepository = $tuovSubdivisionDealsRepo;
	}

	/**
	 * Display a listing of the TuovSubdivisionDeals.
	 * GET|HEAD /tuovSubdivisionDeals
	 *
	 * @return Response
	 */
	public function index()
	{
		$tuovSubdivisionDeals = $this->tuovSubdivisionDealsRepository->all();

		return $this->sendResponse($tuovSubdivisionDeals->toArray(), "TuovSubdivisionDeals retrieved successfully");
	}

	/**
	 * Show the form for creating a new TuovSubdivisionDeals.
	 * GET|HEAD /tuovSubdivisionDeals/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created TuovSubdivisionDeals in storage.
	 * POST /tuovSubdivisionDeals
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(TuovSubdivisionDeals::$rules) > 0)
			$this->validateRequestOrFail($request, TuovSubdivisionDeals::$rules);

		$input = $request->all();

		$tuovSubdivisionDeals = $this->tuovSubdivisionDealsRepository->create($input);

		return $this->sendResponse($tuovSubdivisionDeals->toArray(), "TuovSubdivisionDeals saved successfully");
	}

	/**
	 * Display the specified TuovSubdivisionDeals.
	 * GET|HEAD /tuovSubdivisionDeals/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$tuovSubdivisionDeals = $this->tuovSubdivisionDealsRepository->apiFindOrFail($id);

		return $this->sendResponse($tuovSubdivisionDeals->toArray(), "TuovSubdivisionDeals retrieved successfully");
	}

	/**
	 * Show the form for editing the specified TuovSubdivisionDeals.
	 * GET|HEAD /tuovSubdivisionDeals/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified TuovSubdivisionDeals in storage.
	 * PUT/PATCH /tuovSubdivisionDeals/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var TuovSubdivisionDeals $tuovSubdivisionDeals */
		$tuovSubdivisionDeals = $this->tuovSubdivisionDealsRepository->apiFindOrFail($id);

		$result = $this->tuovSubdivisionDealsRepository->updateRich($input, $id);

		$tuovSubdivisionDeals = $tuovSubdivisionDeals->fresh();

		return $this->sendResponse($tuovSubdivisionDeals->toArray(), "TuovSubdivisionDeals updated successfully");
	}

	/**
	 * Remove the specified TuovSubdivisionDeals from storage.
	 * DELETE /tuovSubdivisionDeals/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->tuovSubdivisionDealsRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "TuovSubdivisionDeals deleted successfully");
	}
}
