<?php namespace App\Http\Controllers\API;

use App\Http\Requests\CreateTuovDeductionRequest;
use App\Libraries\Repositories\TuovRepository;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Symfony\Component\HttpFoundation\RedirectResponse;


class TuovController extends AppBaseController
{

    private $tuovRepository;

    function __construct(TuovRepository $tuov){
        $this->tuovRepository = $tuov;

    }

    /**
     * @param CreateTuovDeductionRequest $request
     * @return mixed
     *
     * @example
        Content-Disposition: form-data; name="subdivision_id"
        79
        Content-Disposition: form-data; name="date"
        2016-02-03
        Content-Disposition: form-data; name="document_category"
        2
        Content-Disposition: form-data; name="shift"
        0
        Content-Disposition: form-data; name="document[]"; filename="luov_document.jpeg"
        Content-Type: image/jpeg
        Content-Disposition: form-data; name="user_id"
        1
     */
    public function postСreate(CreateTuovDeductionRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = $request->get('user_id');

        $model = $this->tuovRepository->create($data);
        if ($model instanceof RedirectResponse) {
            return $this->sendResponse($model->getTargetUrl(), 'Документ успешно добавлен');
        }
        $model->url = route('tuovDeduction.edit', $model->id);
        return $this->sendResponse($model, 'Документ успешно добавлен');
    }
}