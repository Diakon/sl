<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\DealRepository;
use App\Models\Deal;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class DealAPIController extends AppBaseController
{
	/** @var  DealRepository */
	private $dealRepository;

	function __construct(DealRepository $dealRepo)
	{
		$this->dealRepository = $dealRepo;
	}

	/**
	 * Display a listing of the Deal.
	 * GET|HEAD /deals
	 *
	 * @return Response
	 */
	public function index()
	{
		$deals = $this->dealRepository->all();

		return $this->sendResponse($deals->toArray(), "Deals retrieved successfully");
	}

	/**
	 * Show the form for creating a new Deal.
	 * GET|HEAD /deals/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created Deal in storage.
	 * POST /deals
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(Deal::$rules) > 0)
			$this->validateRequestOrFail($request, Deal::$rules);

		$input = $request->all();

		$deals = $this->dealRepository->create($input);

		return $this->sendResponse($deals->toArray(), "Deal saved successfully");
	}

	/**
	 * Display the specified Deal.
	 * GET|HEAD /deals/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$deal = $this->dealRepository->apiFindOrFail($id);

		return $this->sendResponse($deal->toArray(), "Deal retrieved successfully");
	}

	/**
	 * Show the form for editing the specified Deal.
	 * GET|HEAD /deals/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified Deal in storage.
	 * PUT/PATCH /deals/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var Deal $deal */
		$deal = $this->dealRepository->apiFindOrFail($id);

		$result = $this->dealRepository->updateRich($input, $id);

		$deal = $deal->fresh();

		return $this->sendResponse($deal->toArray(), "Deal updated successfully");
	}

	/**
	 * Remove the specified Deal from storage.
	 * DELETE /deals/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->dealRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "Deal deleted successfully");
	}
}
