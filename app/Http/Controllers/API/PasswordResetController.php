<?php namespace App\Http\Controllers\API;


use App\Libraries\Repositories\UserRepository;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Support\Facades\Password;

class PasswordResetController extends AppBaseController
{
    private $user;

    function __construct(UserRepository $user)
    {
        $this->user = $user;
    }


    public function postIndex(Request $request)
    {
        $this->validate($request, ['username'=>'required|min:3']);

        $user = $this->user->findBy('username',$request->input('username'));
        if (!$user) {
            abort(404);
        }
        return $this->sendResponse(route('password/token', Password::getRepository('user')->create($user)), '');
    }
}