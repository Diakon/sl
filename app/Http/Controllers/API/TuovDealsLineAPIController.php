<?php namespace App\Http\Controllers\API;

use App\Http\Requests;
use App\Libraries\Repositories\TuovDealsLineRepository;
use App\Models\TuovDealsLine;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class TuovDealsLineAPIController extends AppBaseController
{
	/** @var  TuovDealsLineRepository */
	private $tuovDealsLineRepository;

	function __construct(TuovDealsLineRepository $tuovDealsLineRepo)
	{
		$this->tuovDealsLineRepository = $tuovDealsLineRepo;
	}

	/**
	 * Display a listing of the TuovDealsLine.
	 * GET|HEAD /tuovDealsLines
	 *
	 * @return Response
	 */
	public function index()
	{
		$tuovDealsLines = $this->tuovDealsLineRepository->all();

		return $this->sendResponse($tuovDealsLines->toArray(), "TuovDealsLines retrieved successfully");
	}

	/**
	 * Show the form for creating a new TuovDealsLine.
	 * GET|HEAD /tuovDealsLines/create
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created TuovDealsLine in storage.
	 * POST /tuovDealsLines
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		if(sizeof(TuovDealsLine::$rules) > 0)
			$this->validateRequestOrFail($request, TuovDealsLine::$rules);

		$input = $request->all();

		$tuovDealsLines = $this->tuovDealsLineRepository->create($input);

		return $this->sendResponse($tuovDealsLines->toArray(), "TuovDealsLine saved successfully");
	}

	/**
	 * Display the specified TuovDealsLine.
	 * GET|HEAD /tuovDealsLines/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$tuovDealsLine = $this->tuovDealsLineRepository->apiFindOrFail($id);

		return $this->sendResponse($tuovDealsLine->toArray(), "TuovDealsLine retrieved successfully");
	}

	/**
	 * Show the form for editing the specified TuovDealsLine.
	 * GET|HEAD /tuovDealsLines/{id}/edit
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		// maybe, you can return a template for JS
//		Errors::throwHttpExceptionWithCode(Errors::EDITION_FORM_NOT_EXISTS, ['id' => $id], static::getHATEOAS(['%id' => $id]));
	}

	/**
	 * Update the specified TuovDealsLine in storage.
	 * PUT/PATCH /tuovDealsLines/{id}
	 *
	 * @param  int              $id
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$input = $request->all();

		/** @var TuovDealsLine $tuovDealsLine */
		$tuovDealsLine = $this->tuovDealsLineRepository->apiFindOrFail($id);

		$result = $this->tuovDealsLineRepository->updateRich($input, $id);

		$tuovDealsLine = $tuovDealsLine->fresh();

		return $this->sendResponse($tuovDealsLine->toArray(), "TuovDealsLine updated successfully");
	}

	/**
	 * Remove the specified TuovDealsLine from storage.
	 * DELETE /tuovDealsLines/{id}
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->tuovDealsLineRepository->apiDeleteOrFail($id);

		return $this->sendResponse($id, "TuovDealsLine deleted successfully");
	}
}
