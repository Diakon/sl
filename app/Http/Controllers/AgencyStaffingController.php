<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCardStaffingRequest;
use App\Http\Requests\UpdateCardStaffingRequest;
use App\Libraries\Repositories\CardStaffingRepository;
use Flash;
use DB;
//use Mitul\Controller\AppBaseController as Controller;
use Response;

class AgencyStaffingController extends Controller
{

	/** @var  CardStaffingRepository */
	private $cardStaffingRepository;

	function __construct(CardStaffingRepository $cardStaffingRepo)
	{
		$this->cardStaffingRepository = $cardStaffingRepo;
	}

	/**
	 * Display a listing of the CardStaffing.
	 *
	 * @return Response
	 */
	public function index()
	{
        $cardStaffings = $this->cardStaffingRepository->paginate(10);
		return view('agencyStaffing.index')
			->with('cardStaffings', $cardStaffings);
	}

	/**
	 * Show the form for creating a new CardStaffing.
	 *
	 * @return Response
	 */
	public function create()
	{
        $cardStaffing['card_staffing'] = $this->getContact(0);
        $cardStaffing['region_list'] = $this->getRegion();
		return view('agencyStaffing.create')->with('cardStaffing', $cardStaffing);
	}

	/**
	 * Store a newly created CardStaffing in storage.
	 *
	 * @param CreateCardStaffingRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCardStaffingRequest $request)
	{
		$input = $request->all();
        $input['author_id'] = \Auth::id();
		$cardStaffing = $this->cardStaffingRepository->create($input);

		Flash::success('agencyStaffing saved successfully.');

		return redirect(route('agencyStaffing.index'));
	}

	/**
	 * Display the specified CardStaffing.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$cardStaffing = $this->cardStaffingRepository->find($id);

		if(empty($cardStaffing))
		{
			Flash::error('agencyStaffing not found');

			return redirect(route('cardStaffings.index'));
		}

		return view('agencyStaffing.show')->with('cardStaffing', $cardStaffing);
	}

	/**
	 * Show the form for editing the specified CardStaffing.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$cardStaffing = $this->cardStaffingRepository->find($id);

		if(empty($cardStaffing))
		{
			Flash::error('agencyStaffing not found');

			return redirect(route('agencyStaffing.index'));
		}

        $cardStaffing['card_staffing'] = $this->getContact($id);
        $cardStaffing['region_list'] = $this->getRegion(((!empty($cardStaffing->region))?($cardStaffing->region):(null)));
		return view('agencyStaffing.edit')->with('cardStaffing', $cardStaffing);
	}

	/**
	 * Update the specified CardStaffing in storage.
	 *
	 * @param  int              $id
	 * @param UpdateCardStaffingRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateCardStaffingRequest $request)
	{
		$cardStaffing = $this->cardStaffingRepository->find($id);

		if(empty($cardStaffing))
		{
			Flash::error('CardStaffing not found');

			return redirect(route('cardStaffings.index'));
		}
        $input = $request->all();
        $input['author_id'] = \Auth::id();
		$cardStaffing = $this->cardStaffingRepository->updateRich($input, $id);

		Flash::success('agencyStaffing updated successfully.');

		return redirect(route('agencyStaffing.index'));
	}

	/**
	 * Remove the specified CardStaffing from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$cardStaffing = $this->cardStaffingRepository->find($id);

		if(empty($cardStaffing))
		{
			Flash::error('agencyStaffing not found');

			return redirect(route('agencyStaffing.index'));
		}

		$this->cardStaffingRepository->delete($id);

        //Контактным лицам, которые закрепдены за этой организацией проставляю card_staffing_id=0
        foreach (\DB::table('card_contact')->where('card_staffing_id', (int)$id)->get() as $data){
            \DB::table('card_contact')
                ->where('id', $data->id)
                ->update(array('card_staffing_id' => 0));
        }

		Flash::success('Запись успешно удалена. Не забудьте отредактировать записи о контактных лицах, которые были закреплены за этой организацией.');

		return redirect(route('agencyStaffing.index'));
	}

    private function getContact($staff_id){
        return \DB::table('card_contact')->where('card_staffing_id', (int)$staff_id)->get();
    }
    private function getRegion($id = null){

		$table_name = DB::connection()->table("information_schema.tables")->where('TABLE_NAME', 'like', 'KLADR')->value('TABLE_NAME');
		$table = DB::table($table_name);
        if (!empty($id)) {
			$model = $table->where('id', (int)$id);
		} else {
			$model = $table->where('socr', 'LIKE', 'респ')->orWhere('socr', 'LIKE', 'обл')->orWhere('socr', 'LIKE', 'край');
		}
        $result = array(0=>'Выбирите из списка');
        foreach ($model->get() as $data){
            $k = $data->id;
            $kladr = null;
            switch ($data->socr) {
                case "Респ":
                    $kladr = "Республика ".$data->name;
                    break;
                case "обл":
                    $kladr = $data->name." область";
                    break;
                case "край":
                    $kladr = $data->name." край";
                    break;
            }
            if (!empty($kladr)){ $result[$k] = $kladr; }
        }
        return $result;
    }
}
