<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateApiRequest;
use App\Http\Requests\UpdateApiRequest;
use App\Libraries\Repositories\ApiRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class ApiController extends AppBaseController
{

	/** @var  ApiRepository */
	private $apiRepository;

	function __construct(ApiRepository $apiRepo)
	{
		$this->apiRepository = $apiRepo;
	}

	public function test($name){
		//  /api/test_view/dopbank
		$uid =  \Auth::id();
		if ( empty($uid) ) {
			//ссесия истекла
			//пишем в куки текущую страницу для редиректа после авторизации
			\Cookie::queue('return_url', \Input::get( 'url' ), 120);
			return redirect('/auth/login');
			exit;
		}
		$this->data = $this->getData($name);

		return view('apis.testviews.'.$name,$this->data);


	}

	private function getData($name){
		$results = array();
		if ($name == 'dopbank'){
			//Заголовок
			$results['title'] = 'Доб. банк';
			//Список банков
			$results['banks'] = array();
			foreach ( \DB::table('dir_banks')->get() as $data ){
				$results['banks'][$data->bank_id] = $data->bank_name;
			}
			//Список расчетных счетов
			$results['inns'] = array();
			foreach ( \DB::table('pers_passport')->where('inn', '<>', '')->get() as $data ){
				$results['inns'][$data->id_anketa] = $data->inn;
			}

		}

		if ($name == 'orders'){
			$results['title'] = 'Экран `Заявка` (приходы)';
			//Списока вакансий
			$results['vakans'] = array('Упаковщица', 'Уборщик', 'Кладовщик', 'Шлифовщик', 'Комплектовщик', 'Сборщик', 'Газорезщик', 'Товаровед');

		}

		return $results;
	}









	/**
	 * Display a listing of the Api.
	 *
	 * @return Response
	 */
	public function index()
	{

        $this->checkAccess();
		$apis = $this->apiRepository->paginate(10);

		return view('apis.index')
			->with('apis', $apis);
	}

	/**
	 * Show the form for creating a new Api.
	 *
	 * @return Response
	 */
	public function create()
	{
        $this->checkAccess();
		return view('apis.create');
	}

	/**
	 * Store a newly created Api in storage.
	 *
	 * @param CreateApiRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateApiRequest $request)
	{
		$input = $request->all();

		$api = $this->apiRepository->create($input);

		Flash::success('Запись сохранена.');

		return redirect(route('apis.index'));
	}

	/**
	 * Display the specified Api.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$api = $this->apiRepository->find($id);

		if(empty($api))
		{
			Flash::error('Запись не найдена');

			return redirect(route('apis.index'));
		}

		return view('apis.show')->with('api', $api);
	}

	/**
	 * Show the form for editing the specified Api.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
        $this->checkAccess();
		$api = $this->apiRepository->find($id);

		if(empty($api))
		{
			Flash::error('Запись не найдена');

			return redirect(route('apis.index'));
		}

		return view('apis.edit')->with('api', $api);
	}

	/**
	 * Update the specified Api in storage.
	 *
	 * @param  int              $id
	 * @param UpdateApiRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateApiRequest $request)
	{
		$api = $this->apiRepository->find($id);

		if(empty($api))
		{
			Flash::error('Запись не найдена');

			return redirect(route('apis.index'));
		}

		$api = $this->apiRepository->updateRich($request->all(), $id);

		Flash::success('Запись обновлена');

		return redirect(route('apis.index'));
	}

	/**
	 * Remove the specified Api from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
        $this->checkAccess();
		$api = $this->apiRepository->find($id);

		if(empty($api))
		{
			Flash::error('Запись не найдена');

			return redirect(route('apis.index'));
		}

		$this->apiRepository->delete($id);

		Flash::success('Запись удалена.');

		return redirect(route('apis.index'));
	}

    private function checkAccess(){
        $uid =  \Auth::id();
        if (empty($uid)){ return \Redirect::to('auth/login'); exit; }
        if ( \DB::table('users')->where('userid', $uid)->first()->status != 15 ){ return \Redirect::to('/'); exit;  }
    }

    /**
     * Обработка API запроса
     */
    public function processing($token){
        //Проверяем токен - ищем какое API закреплено за этим токеном
        $model = new \App\Models\Api();
        $result = $model->processingAPI($token, $_POST);
        echo $result;
    }
}
