<?php namespace  App\Http\Controllers;

use Auth;
use User;
use Request;
use Hash;
use App\Http\Controllers\Controller;
use App\Models;
use Illuminate\Support\Facades\App;

class FullbaseController extends Controller {


    private $param;

    public function index(){

        $uid =  \Auth::id();


        $param = array();
        if (isset($_POST['_token'])){
            unset($_POST['_token']);
            //\Session::forget('filter_fullbase');
            $status = array();
            if (!isset($_POST['s1'])){
                if (isset($_POST['s2'])){$status[] = 1;}
                if (isset($_POST['s3'])){$status[] = 10;}
                if (isset($_POST['s5'])){$status[] = 20;}
                if (isset($_POST['s6'])){$status[] = 11;}
            }
            if (!empty($status)){
                $_POST['status'] = $status;
            }
            $param = $_POST;
            //\Session::put('filter_fullbase', $_POST);
        }


        //$param = \Session::get('filter_fullbase');
        if (!empty($param)){ $this->param = $param; }



        $this->data['rezerv_3day'] = $this->getDataRezerv();  //Через 3 дня должны выйти с резерва
        $this->data['callback30'] = $this->getCallback30();   //Обратный звонок (30 минут)
        $this->data['callback'] = $this->getCallback();       //Перезвонить за 14 дней
        $this->data['soiskateli'] = $this->getSoiskateli();      //Соискатели
        $this->data['status_user'] = $this->getStatusUser();  //Статусы пользователя
        $this->data['nation'] = $this->getNationality();      //Гражданство
        $this->data['param_filter'] = $param;
        $this->data['feelds'] = $this->getFeeldsSoiskateli();



        return view('fullbase.index',$this->data);
    }


    /** Выход из резерва через 3 дня */
    private function getDataRezerv(){

        $interval = date('Y-m-d',strtotime('-12 day', time()));
        $status_name = 11;

        $result = \DB::table('pers_status')
            ->leftJoin('pers_main', 'pers_status.id_anketa', '=', 'pers_main.id_anketa')
            ->leftJoin('objects', 'pers_status.object', '=', 'objects.id')
            ->leftJoin('domitory', 'pers_status.domitory', '=', 'domitory.id')
            ->leftJoin('pers_address', 'pers_status.id_anketa', '=', 'pers_address.id_anketa')
            ->select('pers_status.id_anketa', 'pers_status.status_date', 'pers_status.position',
                     'pers_main.lastname', 'pers_main.firstname', 'pers_main.patronymic', 'pers_main.speciality',
                     'objects.name as object', 'domitory.name as domitory', 'pers_address.phone_mobile'
            )
            ->where('pers_status.status_name', '=', $status_name)
            ->where('pers_status.status_date', '=', $interval)
            ->get();
        return $result;
    }

    /** Обратный звонок (30 минут) */
    private function getCallback30(){
        $result = \DB::table('soiskateli')->where('searchsource','=',150)->whereNull('customer_status')->get();
        return $result;
    }

    /** Перезвонить за 14 дней */
    private function getCallback(){

        $status = \DB::table('users')->where('userid', \Auth::id())->pluck('status');


        $result['table'] = \DB::table('soiskateli');


        $result['table'] = $result['table']->where(function ($query) {
            $query->where('modified_when', '>', 'DATE(CURDATE() - INTERVAL 14 DAY)')
                ->where('customer_status', 20)
                ->whereIn('customer_status', array(2,7,9))
                ->where('modified_when','>','DATE(CURDATE() - INTERVAL 4 DAY)')
                ->where('modified_when','<','DATE(CURDATE() - INTERVAL 2 HOUR)');
        });
        $result['table'] = $result['table']->where('access_level','<=',$status);


        $result['table'] = $result['table']->orderBy('modified_when', 'DESC')->paginate(100);


        return $result;
    }

    /** Соискатели */
    private function getSoiskateli(){

        $result['table'] = \DB::table('soiskateli');
        if (isset($this->param['lastname']) && !empty($this->param['lastname'])){
            $result['table'] = $result['table']->where(function ($query) {
                $query->where('lastname', 'LIKE', '%'.$this->param['lastname'].'%')
                    ->orWhere('firstname', 'LIKE', '%'.$this->param['lastname'].'%');
            });

        }
        if (isset($this->param['phone']) && !empty($this->param['phone'])){
            $result['table'] = $result['table']->where(function ($query) {
                $query->where('aon', 'LIKE', '%'.$this->param['phone'].'%');
            });
        }
        if (isset($this->param['vacancy']) && !empty($this->param['vacancy'])){
            $result['table'] = $result['table']->where(function ($query) {
                $query->where('vacancy', 'LIKE', '%'.$this->param['vacancy'].'%')
                    ->orWhere('comments', 'LIKE', '%'.$this->param['vacancy'].'%')
                    ->orWhere('city', 'LIKE', '%'.$this->param['vacancy'].'%');
            });
        }

        if (isset($this->param['s_pr_from']) && !empty($this->param['s_pr_from'])){
            $this->param['s_pr_from'] = date('Y-m-d', strtotime($this->param['s_pr_from']));


            $result['table'] = $result['table']->where(function ($query) {
                $query->where('kogda_pridet', '!=', "0000-00-00")
                    ->where('kogda_pridet', '>=', $this->param['s_pr_from']);
            });
        }

        if (isset($this->param['s_pr_to']) && !empty($this->param['s_pr_to'])){
            $this->param['s_pr_to'] = date('Y-m-d', strtotime($this->param['s_pr_to']));
            $result['table'] = $result['table']->where(function ($query) {
                $query->where('kogda_pridet', '!=', "0000-00-00")
                    ->where('kogda_pridet', '<=', $this->param['s_pr_to']);
            });
        }

        if (isset($this->param['status']) && !empty($this->param['status'])){
            $this->param['s_pr_from'] = ( isset($this->param['s_pr_from']) && !empty($this->param['s_pr_from']) )?$this->param['s_pr_from']:'2000-01-01';
            $this->param['s_pr_to'] = ( isset($this->param['s_pr_to']) && !empty($this->param['s_pr_to']) )?$this->param['s_pr_to']:date('Y-m-d');

            $result['table'] = $result['table']->where(function ($query) {
                $query->whereBetween('modified_when', array($this->param['s_pr_from'], $this->param['s_pr_to']))
                    ->whereIn('customer_status', $this->param['status']);
            });
        }

        $result['table'] = $result['table']->orderBy('created_when', 'DESC')->paginate(100);
        $result['table']->setPath('fullbase');


        return $result;
    }


    /** Статусы сотрудника */
    private function getStatusUser(){
        $result = array();
        foreach( \DB::table('statustable')->get() as $data ){
            $k = $data->status_id;
            $result[$k]['name'] = $data->status_name;
            $result[$k]['color'] = $data->colormark;
        }
        return $result;
    }

    /** Гражданство
     */
    private function getNationality(){
        $result = array();
        foreach( \DB::table('grazdanstvo')->get() as $data ){
            $k = $data->grazd_id;
            $result[$k] = $data->grazd_name;
        }
        return $result;
    }

    /** Возращаем массив необходимый для строительства таблиц Соискатели и Перезвонить за 14 дней
     */
    private   function getFeeldsSoiskateli(){

        //Получаем параметры из куки (какие поля показывать) - если нет, ставим дефолтные
        $nameFeelds = \Cookie::get('name_feelds');
        if (empty($nameFeelds)){
            $nameFeelds = array();
            $nameFeelds['id'] = 1;
            $nameFeelds['searchsource'] = 1;
            $nameFeelds['city'] = 1;
            $nameFeelds['created_when'] = 1;
            $nameFeelds['customer_status'] = 1;
            $nameFeelds['aon'] = 1;
            $nameFeelds['lastname'] = 1;
            $nameFeelds['firstname'] = 1;
            $nameFeelds['otch'] = 1;
            $nameFeelds['age'] = 1;
            $nameFeelds['grazd'] = 1;
            $nameFeelds['arrivaltime'] = 1;
            $nameFeelds['comments'] = 1;
        } else {
            $nameFeeldsArr = explode("|", $nameFeelds);
            $nameFeelds = array();
            foreach ($nameFeeldsArr as $key=>$val){
                $nameFeelds[$val] = 1;
            }
        }



        $result = array();

        $result['id']['name'] = 'ID';
        $result['searchsource']['name'] = 'Источник';
        $result['city']['name'] = 'Город';
        $result['created_when']['name'] = 'Заявка создана';
        $result['modified_when']['name'] = 'Заявка изменена';
        $result['customer_status']['name'] = 'Статус сотрудника';
        $result['gender']['name'] = 'Пол';
        $result['vacancy']['name'] = 'Вакансия';
        $result['aon']['name'] = 'Телефонный номер';
        $result['secondphone']['name'] = 'Телефонный номер 2';
        $result['email']['name'] = 'email';
        $result['lastname']['name'] = 'Фамилия';
        $result['firstname']['name'] = 'Имя';
        $result['otch']['name'] = 'Отчество';
        $result['age']['name'] = 'Возраст';
        $result['grazd']['name'] = 'Гражданство';
        $result['kogda_pridet']['name'] = 'Когда приедет (мы)';
        $result['arrivaltime']['name'] = 'Когда приедет (он)';
        $result['factor1']['name'] = 'Фактор 1';
        $result['factor2']['name'] = 'Фактор 2';
        $result['factor3']['name'] = 'Фактор 3';
        $result['ask4address']['name'] = 'Спросил адрес?';
        $result['interes_podrabotka']['name'] = 'Интерес: Подработка';
        $result['interes_vahta']['name'] = 'Интерес: Вахта';
        $result['interes_postrabota']['name'] = 'Интерес: Постоянка';
        $result['mos_metro_station']['name'] = 'Станция метро';
        $result['user_creator']['name'] = 'Кто добавил';
        $result['kemrabotal']['name'] = 'Кем работал';
        $result['comments']['name'] = 'Комментарий';

        foreach ($result as $key=>$val){
            if (isset($nameFeelds[$key])){
                $result[$key]['selected'] = 1;
            }
        }


        return $result;
    }

    /**
     * Пишет в куки настройки вывода полей в таблицах
     */
    public function setFeeldsSoiskateli(){
        if (isset($_POST['_token'])){
            unset($_POST['_token']);
            \Cookie::forget('name_feelds');
            $feelsd = array();
            foreach ($_POST as $key=>$val){
                $feelsd[] = $key;
            }
            if (!empty($feelsd)){
                $feelsd = implode("|", $feelsd);
                \Cookie::queue('name_feelds',$feelsd, 1200000);
                //\Cookie::forever('name_feelds', $feelsd);
            }
        }
        return redirect('/fullbase');
    }
}


