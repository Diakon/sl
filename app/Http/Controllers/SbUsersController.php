<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSbUsersRequest;
use App\Http\Requests\UpdateSbUsersRequest;
use App\Libraries\Repositories\SbUsersRepository;
use App\Models\Objects;
use Flash;
use Response;

class SbUsersController extends Controller
{

	/** @var  SbUsersRepository */
	private $sbUsersRepository;
    private $uid;
    private $objects_arr;

	function __construct(SbUsersRepository $sbUsersRepo)
	{
        $this->objects_arr = $this->getObjects();
		$this->sbUsersRepository = $sbUsersRepo;
	}

	/**
	 * Display a listing of the SbUsers.
	 *
	 * @return Response
	 */
	public function index()
	{
        $sbUsers = $this->sbUsersRepository->paginate(10);
        return view('sbUsers.index')
			->with('sbUsers', $sbUsers);
	}

	/**
	 * Show the form for creating a new SbUsers.
	 *
	 * @return Response
	 */
	public function create()
	{
        $sbUsers['objects_arr'] = $this->objects_arr;
		return view('sbUsers.create', array('sbUsers'=>$sbUsers));
	}

	/**
	 * Store a newly created SbUsers in storage.
	 *
	 * @param CreateSbUsersRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateSbUsersRequest $request)
	{
        $input = $request->all();

        $input['author_id'] = $this->uid;
        $input['password'] = md5($input['password']);

        $this->sbUsersRepository->create($input);

        Flash::success('Пользователь успешно добавлен!');

        return redirect(route('sb.users'));
	}

	/**
	 * Display the specified SbUsers.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$sbUsers = $this->sbUsersRepository->find($id);

		if(empty($sbUsers))
		{
			Flash::error('SbUsers not found');

			return redirect(route('sbUsers.index'));
		}

		return view('sbUsers.show')->with('sbUsers', $sbUsers);
	}

	/**
	 * Show the form for editing the specified SbUsers.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$sbUsers = $this->sbUsersRepository->find($id);

		if(empty($sbUsers))
		{
			Flash::error('Пользователь не найден!');

			return redirect(route('sbUsers.index'));
		}

        $sbUsers['objects_arr'] = $this->objects_arr;
		return view('sbUsers.edit')->with('sbUsers', $sbUsers);
	}

	/**
	 * Update the specified SbUsers in storage.
	 *
	 * @param  int              $id
	 * @param UpdateSbUsersRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateSbUsersRequest $request)
	{
        $sbUsers = $this->sbUsersRepository->find($id);

		if(empty($sbUsers))
		{
			Flash::error('Пользователь не найден');

			return redirect(route('sbUsers.index'));
		}

        $input = $request->all();

        if (empty($input['password'])){
            $input['password'] = \DB::table('sb_users')->where('id', $id)->first()->password;
        }
        else { $input['password'] = md5($input['password']); }

		$sbUsers = $this->sbUsersRepository->updateRich($input, $id);

		Flash::success('Данные пользователя '.$input['username'].' успешно обновлены.');

		return redirect(route('sbUsers.index'));
	}

	/**
	 * Remove the specified SbUsers from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$sbUsers = $this->sbUsersRepository->find($id);

		if(empty($sbUsers))
		{
			Flash::error('SbUsers not found');

			return redirect(route('sbUsers.index'));
		}

		$this->sbUsersRepository->delete($id);

		Flash::success('SbUsers deleted successfully.');

		return redirect(route('sbUsers.index'));
	}

    public function showdata(){
        $data = array();

        $sb_user = \Session::get('sb_user');
        if (empty($sb_user)){
            return view('sbUsers.auth');
        }
        //Получаем запись
        $sbUser = $this->sbUsersRepository->findBy('id', $sb_user);
        $id = (isset($_GET['id']))?((int)$_GET['id']):null;
        if (empty($id)){
            //Выводим список пользователей в объекте
            $model = \DB::table('pers_main')
                ->select('pers_main.id_anketa', 'pers_main.firstname', 'pers_main.lastname', 'pers_main.patronymic')
                ->leftJoin('pers_status', 'pers_status.id_anketa', '=', 'pers_main.id_anketa')
                ->where('pers_status.object', $sbUser->objects_id)
                ->where('pers_main.disabled', 0)
                ->where('pers_status.status_name', 9)
                ->orderBy('pers_main.lastname', 'asc')
                ->get();
            $render = 'sbUsers.indexlist';

        } else {
            //проверяем право на открытие данных о челе
            if ( !\DB::table('pers_status')->select('id_anketa')->where('object',$sbUser->object)->where('id_anketa', $id)->first() ){
                return \Redirect::to('/sb');
            }
            //Выводим данне о пользователе
            $model = \DB::table('pers_main')
                ->select('pers_main.*', 'pers_status.*', 'pers_passport.*', 'pers_address.*')
                ->leftJoin('pers_status', 'pers_status.id_anketa', '=', 'pers_main.id_anketa')
                ->leftJoin('pers_passport', 'pers_passport.id_anketa', '=', 'pers_main.id_anketa')
                ->leftJoin('pers_address', 'pers_address.id_anketa', '=', 'pers_main.id_anketa')
                ->where('pers_main.id_anketa', $id)
                ->first();

            //Вычисляю возраст по рожэению
            if(!empty($model->birth_date)){
                $y = date('Y',strtotime($model->birth_date));
                $m = date('m',strtotime($model->birth_date));
                $d = date('d',strtotime($model->birth_date));
                $model->age = $this->getAge($y, $m, $d);
            }
            $render = 'sbUsers.detallist';
        }

        return view($render)
            ->with('model', $model);

    }

    public function auth(){
        $input = $_POST;
        if (empty($input['username']) || empty($input['password'])){
            Flash::error('Поля `Логин` и `Пароль` не могут быть пустыми');
            return view('sbUsers.auth');
        }
        $model = \DB::table('sb_users')
            ->where('username', 'LIKE', $input['username'])
            ->where('password', (md5($input['password'])))
            ->where('active', 1)
            ->first();
        if (!isset($model->id)){
            Flash::error('Пользователь с таким логином/паролем не найден!');
            return view('sbUsers.auth');
        }

        \Session::put('sb_user', $model->id);
        return redirect('/sb');
    }

    public function logout(){
        \Session::forget('sb_user');
        return redirect('/sb');
    }


    private function getAge($y, $m, $d) {
        if($m > date('m') || $m == date('m') && $d > date('d'))
            return (date('Y') - $y - 1);
        else
            return (date('Y') - $y);
    }

    private function getObjects($id=null){
        if (empty($id)){
            $model = Objects::All();
        } else {
            $model = Objects::whereId($id)->get();
        }
        $arrayObj = [];
        foreach ($model as $data){
            $arrayObj[$data->id] =  $data->name . ' (' . $data->comments . ')';
        }
        return $arrayObj;
    }
}
