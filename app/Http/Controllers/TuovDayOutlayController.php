<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTuovDayOutlayRequest;
use App\Http\Requests\UpdateTuovDayOutlayRequest;
use App\Libraries\Repositories\TuovDayOutlayRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class TuovDayOutlayController extends AppBaseController
{

	/** @var  TuovDayOutlayRepository */
	private $tuovDayOutlayRepository;

	function __construct(TuovDayOutlayRepository $tuovDayOutlayRepo)
	{
		$this->tuovDayOutlayRepository = $tuovDayOutlayRepo;
	}

	/**
	 * Display a listing of the TuovDayOutlay.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tuovDayOutlays = $this->tuovDayOutlayRepository->paginate(10);

		return view('tuovDayOutlays.index')
			->with('tuovDayOutlays', $tuovDayOutlays);
	}

	/**
	 * Show the form for creating a new TuovDayOutlay.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('tuovDayOutlays.create');
	}

	/**
	 * Store a newly created TuovDayOutlay in storage.
	 *
	 * @param CreateTuovDayOutlayRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateTuovDayOutlayRequest $request)
	{
		$input = $request->all();

		$tuovDayOutlay = $this->tuovDayOutlayRepository->create($input);

		Flash::success('TuovDayOutlay saved successfully.');

		return redirect(route('tuovDayOutlays.index'));
	}

	/**
	 * Display the specified TuovDayOutlay.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$tuovDayOutlay = $this->tuovDayOutlayRepository->find($id);

		if(empty($tuovDayOutlay))
		{
			Flash::error('TuovDayOutlay not found');

			return redirect(route('tuovDayOutlays.index'));
		}

		return view('tuovDayOutlays.show')->with('tuovDayOutlay', $tuovDayOutlay);
	}

	/**
	 * Show the form for editing the specified TuovDayOutlay.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$tuovDayOutlay = $this->tuovDayOutlayRepository->find($id);

		if(empty($tuovDayOutlay))
		{
			Flash::error('TuovDayOutlay not found');

			return redirect(route('tuovDayOutlays.index'));
		}

		return view('tuovDayOutlays.edit')->with('tuovDayOutlay', $tuovDayOutlay);
	}

	/**
	 * Update the specified TuovDayOutlay in storage.
	 *
	 * @param  int              $id
	 * @param UpdateTuovDayOutlayRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateTuovDayOutlayRequest $request)
	{
		$tuovDayOutlay = $this->tuovDayOutlayRepository->find($id);

		if(empty($tuovDayOutlay))
		{
			Flash::error('TuovDayOutlay not found');

			return redirect(route('tuovDayOutlays.index'));
		}

		$this->tuovDayOutlayRepository->updateRich($request->all(), $id);

		Flash::success('TuovDayOutlay updated successfully.');

		return redirect(route('tuovDayOutlays.index'));
	}

	/**
	 * Remove the specified TuovDayOutlay from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$tuovDayOutlay = $this->tuovDayOutlayRepository->find($id);

		if(empty($tuovDayOutlay))
		{
			Flash::error('TuovDayOutlay not found');

			return redirect(route('tuovDayOutlays.index'));
		}

		$this->tuovDayOutlayRepository->delete($id);

		Flash::success('TuovDayOutlay deleted successfully.');

		return redirect(route('tuovDayOutlays.index'));
	}
}
