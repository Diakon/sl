<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTuovDealsLineRequest;
use App\Http\Requests\UpdateTuovDealsLineRequest;
use App\Libraries\Repositories\TuovDealsLineRepository;
use App\Models\Deal;
use App\Models\Subdivision;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class TuovDealsLineController extends AppBaseController
{

	/** @var  TuovDealsLineRepository */
	private $tuovDealsLineRepository;

	function __construct(TuovDealsLineRepository $tuovDealsLineRepo)
	{
		$this->tuovDealsLineRepository = $tuovDealsLineRepo;
        $this->activeSubdivisions = Subdivision::getActiveSubdivisions();
        $this->activeSubdivisionsWithNull = Subdivision::getActiveSubdivisions(1);
        $this->activeDeals = Deal::getActiveDeals();
	}

	/**
	 * Display a listing of the TuovDealsLine.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tuovDealsLines = $this->tuovDealsLineRepository->paginate(10);

		return view('tuovDealsLines.index',array(
            'tuovDealsLines'=> $tuovDealsLines,
            'activeSubdivisionsWithNull'=>$this->activeSubdivisionsWithNull,
            'activeSubdivisions'=>$this->activeSubdivisions,
            'activeDeals'=>$this->activeDeals,
            'selected'=>0,
            'collisions'=>[]
        ));
	}

	/**
	 * Show the form for creating a new TuovDealsLine.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('tuovDealsLines.create',array('activeSubdivisions'=>$this->activeSubdivisions,'activeDeals'=>$this->activeDeals));
	}

	/**
	 * Store a newly created TuovDealsLine in storage.
	 *
	 * @param CreateTuovDealsLineRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateTuovDealsLineRequest $request)
	{
		$input = $request->all();

		$tuovDealsLine = $this->tuovDealsLineRepository->create($input);

		Flash::success('TuovDealsLine saved successfully.');

		return redirect(route('tuovDealsLines.index'));
	}

	/**
	 * Display the specified TuovDealsLine.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$tuovDealsLine = $this->tuovDealsLineRepository->find($id);

		if(empty($tuovDealsLine))
		{
			Flash::error('TuovDealsLine not found');

			return redirect(route('tuovDealsLines.index'));
		}

		return view('tuovDealsLines.show')->with('tuovDealsLine', $tuovDealsLine);
	}

	/**
	 * Show the form for editing the specified TuovDealsLine.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$tuovDealsLine = $this->tuovDealsLineRepository->find($id);

		if(empty($tuovDealsLine))
		{
			Flash::error('TuovDealsLine not found');

			return redirect(route('tuovDealsLines.index'));
		}

		return view('tuovDealsLines.edit',array('tuovDealsLine'=> $tuovDealsLine, 'activeSubdivisions'=>$this->activeSubdivisions,'activeDeals'=>$this->activeDeals));
	}

	/**
	 * Update the specified TuovDealsLine in storage.
	 *
	 * @param  int              $id
	 * @param UpdateTuovDealsLineRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateTuovDealsLineRequest $request)
	{
		$tuovDealsLine = $this->tuovDealsLineRepository->find($id);

		if(empty($tuovDealsLine))
		{
			Flash::error('TuovDealsLine not found');

			return redirect(route('tuovDealsLines.index'));
		}

		$this->tuovDealsLineRepository->updateRich($request->all(), $id);

		Flash::success('TuovDealsLine updated successfully.');

		return redirect(route('tuovDealsLines.index'));
	}

	/**
	 * Remove the specified TuovDealsLine from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$tuovDealsLine = $this->tuovDealsLineRepository->find($id);

		if(empty($tuovDealsLine))
		{
			Flash::error('TuovDealsLine not found');

			return redirect(route('tuovDealsLines.index'));
		}

		$this->tuovDealsLineRepository->delete($id);

		Flash::success('TuovDealsLine deleted successfully.');

		return redirect(route('tuovDealsLines.index'));
	}
}
