<?php namespace App\Http\Controllers;

use Auth;
use App\User;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        //Если авторизован и в куки есть URL - переадресуем на страницу
        $redirect_url = \Cookie::get('return_url');
        if (\Auth::id() && !empty($redirect_url)){
            //удаляю кукис
            \Cookie::queue('return_url', '', time()-120);
            return \Redirect::to($redirect_url);
        }
		return view('home');
	}

}
