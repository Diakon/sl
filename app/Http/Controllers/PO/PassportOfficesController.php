<?php namespace App\Http\Controllers\PO;

use App\Http\Requests;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Illuminate\Http\Request;
use Response;

class PassportOfficesController extends AppBaseController
{

	/** @var  PassportOfficesRepository */
	private $passportOfficesRepository;

	function __construct()
	{
	}

	public function index(Request $request)
	{

		$passportOffices = \DB::table('po_job_application')->paginate(10);

		return view('po.passportOffices.index')
			->with('passportOffices', $passportOffices);
	}

}
