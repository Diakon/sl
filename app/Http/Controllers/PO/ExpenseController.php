<?php namespace App\Http\Controllers\PO;

use App\Http\Requests\UpdatePoExpenseRequest;
use App\Libraries\Repositories\PO\ExpenseRepository;
use App\Models\PO\Expense;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


class ExpenseController extends Controller
{

	private $expense;
	/** @var  $expenseRepository */
	private $expenseRepository;

	function __construct(Expense $expense, ExpenseRepository $expenseRepo)
	{
		$this->expense = $expense;
		$this->expenseRepository = $expenseRepo;
		view()->share('object', Route::current()->object);
	}

	/**
	 * Display a listing of the Contractor.
	 *
	 * @return Response
	 */
	public function index($object)
	{
		return view('po.expense.index')->withExpense($object->expense);
	}

	public function all()
	{
		return view('po.expense.all')->withExpenses($this->expenseRepository->all());
	}

	public function postindex(UpdatePoExpenseRequest $request, $object)
	{
		$params = array_merge(
			$request->only(['dormitory_cost', 'transport_cost', 'food_cost', 'mk_cost', 'ud_cost', 'other_cost']), ['author_id' => Auth::id()]
		);
		$expense = $object->expense;
		if ($expense) {
			$expense->update($params);
		} else {
			$object->expense()->save($this->expense->fill($params));
		}

		return redirect()->route('po.objects.show', $object->id);
	}
}
