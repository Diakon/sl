<?php namespace App\Http\Controllers\PO;

use App\Http\Requests;
use App\Http\Requests\PO\CreateObjectRequest;
use App\Http\Requests\PO\UpdateObjectRequest;
use App\Libraries\Repositories\PO\ObjectRepository;
use Flash;
use App\Http\Controllers\Controller;
use Response;

class ObjectController extends Controller
{

	/** @var  $objectRepository */
	private $objectRepository;

	function __construct(ObjectRepository $objectRepo)
	{
		$this->objectRepository = $objectRepo;
	}

	/**
	 * Display a listing of the Object.
	 *
	 * @return Response
	 */
	public function index()
	{
		$objects = $this->objectRepository->paginate(10);

		return view('po.objects.index')
			->with('objects', $objects);
	}

	/**
	 * Show the form for creating a new Object.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('po.objects.create');
	}

	/**
	 * Store a newly created Object in storage.
	 *
	 * @param CreatePO/ObjectRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateObjectRequest $request)
	{
		$input = $request->all();

		$object = $this->objectRepository->create($input);

		Flash::success('Object saved successfully.');

		return redirect(route('po.objects.index'));
	}

	/**
	 * Display the specified Object.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$object = $this->objectRepository->find($id);

		if(empty($object))
		{
			Flash::error('Object not found');

			return redirect(route('po.objects.index'));
		}

		return view('po.objects.show')->with('object', $object);
	}

	/**
	 * Show the form for editing the specified Object.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$object = $this->objectRepository->find($id);

		if(empty($object))
		{
			Flash::error('Object not found');

			return redirect(route('po.objects.index'));
		}

		return view('po.objects.edit')->with('object', $object);
	}

	/**
	 * Update the specified Object in storage.
	 *
	 * @param  int              $id
	 * @param UpdatePO/ObjectRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateObjectRequest $request)
	{
		$object = $this->objectRepository->find($id);

		if(empty($object))
		{
			Flash::error('Object not found');

			return redirect(route('po.objects.index'));
		}

		$this->objectRepository->updateRich($request->all(), $id);

		Flash::success('Object updated successfully.');

		return redirect(route('po.objects.index'));
	}

	/**
	 * Remove the specified Object from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$object = $this->objectRepository->find($id);

		if(empty($object))
		{
			Flash::error('Object not found');

			return redirect(route('pO/Objects.index'));
		}

		$this->objectRepository->delete($id);

		Flash::success('Object deleted successfully.');

		return redirect(route('po.objects.index'));
	}
}