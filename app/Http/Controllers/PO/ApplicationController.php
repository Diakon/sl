<?php namespace App\Http\Controllers\PO;

use App\Http\Requests;
use App\Http\Requests\CreateJobApplicationRequest;
use App\Http\Requests\UpdateJobApplicationRequest;
use App\Libraries\Repositories\JobApplicationRepository;
use Flash;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Response;

class ApplicationController extends Controller
{

	/** @var  JobApplicationRepository */
	private $jobApplicationRepository;

	function __construct(JobApplicationRepository $jobApplicationRepo)
	{
		$this->jobApplicationRepository = $jobApplicationRepo;
		view()->share('object', Route::current()->object);
	}

	/**
	 * Display a listing of the JobApplication.
	 *
	 * @return Response
	 */
	public function index($object)
	{
		return view('po.applications.index')
			->with('jobApplications', $object->applications);
	}

	public function all()
	{
		$jobApplications = $this->jobApplicationRepository->all();
		return view('po.applications.index')
			->with('jobApplications', $jobApplications);
	}

	/**
	 * Show the form for creating a new JobApplication.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('po.applications.create');
	}

	/**
	 * Store a newly created JobApplication in storage.
	 *
	 * @param CreateJobApplicationRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateJobApplicationRequest $request, $object)
	{
		$params = $request->all();
		$params['author_id'] = \Auth::id();

		$object->applications()->save($this->jobApplicationRepository->create($params));

		return redirect(route('po.applications.index', $object->id));
	}

	/**
	 * Display the specified JobApplication.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$jobApplication = $this->jobApplicationRepository->find($id);

		if(empty($jobApplication))
		{
			Flash::error('Записи не найдены');

			return redirect(route('po.applications.index'));
		}

		return view('jobApplications.show')->with('jobApplication', $jobApplication);
	}

	/**
	 * Show the form for editing the specified JobApplication.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($object, $id)
	{
		$jobApplication = $this->jobApplicationRepository->find($id);

		if(empty($jobApplication))
		{
			Flash::error('Запись не найдена');

			return redirect(route('po.applications.index', $object->id));
		}
		return view('po.applications.edit')
			->with('jobApplication', $jobApplication);
	}

	/**
	 * Update the specified JobApplication in storage.
	 *
	 * @param  int              $id
	 * @param UpdateJobApplicationRequest $request
	 *
	 * @return Response
	 */
	public function update($object, $id, UpdateJobApplicationRequest $request)
	{
		$jobApplication = $this->jobApplicationRepository->find($id);

		if(empty($jobApplication))
		{
			Flash::error('Запись не найдена');
			return redirect(route('po.applications.index', $object->id));
		}

		$data = $request->all();
		$data['author_id'] = \Auth::id();

		$this->jobApplicationRepository->updateRich($data, $id);

		Flash::success('Запись успешно обнавлена.');
		return redirect(route('po.applications.index', $object->id));
	}

	/**
	 * Remove the specified JobApplication from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($object, $id)
	{
		$jobApplication = $this->jobApplicationRepository->find($id);
		if(empty($jobApplication))
		{
			Flash::error('Запись не найдена');

			return redirect(route('po.applications.index', $object->id));
		}

		$this->jobApplicationRepository->delete($id);

		Flash::success('Запись удалена.');

		return redirect(route('po.applications.index', $object->id));
	}


}
