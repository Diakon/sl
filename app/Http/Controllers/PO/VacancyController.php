<?php namespace App\Http\Controllers\PO;

use App\Http\Requests;
use App\Http\Requests\CreateJobVacancyRequest;
use App\Http\Requests\UpdateJobVacancyRequest;
use App\Libraries\Repositories\JobVacancyRepository;
use App\Models\JobApplication;
use App\Models\JobVacancy;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class VacancyController extends AppBaseController
{

	/** @var  JobVacancyRepository */
	private $jobVacancyRepository;

	function __construct(JobVacancyRepository $jobVacancyRepo)
	{
		$this->jobVacancyRepository = $jobVacancyRepo;
	}

	/**
	 * Display a listing of the JobVacancy.
	 *
	 * @return Response
	 */
	public function index()
	{
		//$jobVacancies = $this->jobVacancyRepository->paginate(50);
		$jobVacancies = $this->jobVacancyRepository->makeModel()->tree();
		return view('po.vacancies.index')
			->with('jobVacancies', $jobVacancies);
	}

	/**
	 * Show the form for creating a new JobVacancy.
	 *
	 * @return Response
	 */
	public function create()
	{
		$parents = JobVacancy::parents()->lists('name','id');
		return view('po.vacancies.create')->withParents($parents);
	}

	/**
	 * Store a newly created JobVacancy in storage.
	 *
	 * @param CreateJobVacancyRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateJobVacancyRequest $request)
	{
		$input = $request->all();

		$jobVacancy = $this->jobVacancyRepository->create($input);

		Flash::success('Запись успешно создана.');

		if ($request->has('uri')) {
			return redirect($request->input('uri'));
		}
		return redirect(route('po.vacancies.index'));
	}

	/**
	 * Display the specified JobVacancy.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$jobVacancy = $this->jobVacancyRepository->find($id);

		if(empty($jobVacancy))
		{
			Flash::error('Нет вакансий');

			return redirect(route('po.vacancies.index'));
		}

		return view('po.vacancies.show')->with('jobVacancy', $jobVacancy);
	}

	/**
	 * Show the form for editing the specified JobVacancy.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$jobVacancy = $this->jobVacancyRepository->find($id);

		if(empty($jobVacancy))
		{
			Flash::error('Запись не найдена');

			return redirect(route('jobVacancies.index'));
		}
		$parents = JobVacancy::parents()->where('id', '<>', $id)->lists('name','id');

		return view('po.vacancies.edit')
			->with('jobVacancy', $jobVacancy)
			->withParents($parents);
	}

	/**
	 * Update the specified JobVacancy in storage.
	 *
	 * @param  int              $id
	 * @param UpdateJobVacancyRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateJobVacancyRequest $request)
	{
		$jobVacancy = $this->jobVacancyRepository->find($id);

		if(empty($jobVacancy))
		{
			Flash::error('Запись не найдена');

			return redirect(route('po.vacancies.index'));
		}

		$this->jobVacancyRepository->updateRich($request->all(), $id);

		Flash::success('Запись успешно обнавлена.');

		return redirect(route('po.vacancies.index'));
	}

	/**
	 * Remove the specified JobVacancy from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$jobVacancy = $this->jobVacancyRepository->find($id);

		if(empty($jobVacancy))
		{
			Flash::error('Ошибка удаления - запись не найдена');

			return redirect(route('po.vacancies.index'));
		}

		//Проверяю,что запись не используется
		if( JobApplication::whereIn('job_vacancy_id', $jobVacancy->childrens->lists('id')->push($jobVacancy->id))->first() )
		{
			Flash::error('Вакансия используется в заявках - удаление невозможно!');
			return redirect(route('po.vacancies.index'));
		}
		$this->jobVacancyRepository->delete($id);

		Flash::success('Запись удалена.');

		return redirect(route('po.vacancies.index'));
	}
}