<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTelegramRequest;
use App\Http\Requests\UpdateTelegramRequest;
use App\Libraries\Repositories\TelegramRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class TelegramController extends AppBaseController
{

	/** @var  TelegramRepository */
	private $telegramRepository;

	function __construct(TelegramRepository $telegramRepo)
	{
		$this->telegramRepository = $telegramRepo;
	}

	/**
	 * Display a listing of the Telegram.
	 *
	 * @return Response
	 */
	public function index()
	{
        $uid = \Auth::id();

        $telegrams = $this->telegramRepository->paginate(10);


        return view('telegrams.index')
            ->with('telegrams', $telegrams);

	}

	/**
	 * Show the form for creating a new Telegram.
	 *
	 * @return Response
	 */
	public function create()
	{

        $telegrams['uid'] = \Auth::id();
        $telegrams['users'] = $this->getUserList();


        return view('telegrams.create',  array('telegram'=>$telegrams));


	}

	/**
	 * Store a newly created Telegram in storage.
	 *
	 * @param CreateTelegramRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateTelegramRequest $request)
	{
		$input = $request->all();

		$telegram = $this->telegramRepository->create($input);

		Flash::success('Данные успешно сохранены.');

		return redirect(route('telegrams.index'));
	}

	/**
	 * Display the specified Telegram.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$telegram = $this->telegramRepository->find($id);

		if(empty($telegram))
		{
			Flash::error('Запись не найдена');

			return redirect(route('telegrams.index'));
		}

		return view('telegrams.show')->with('telegram', $telegram);
	}

	/**
	 * Show the form for editing the specified Telegram.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
        $telegrams = $this->telegramRepository->find($id);


        $telegrams['uid'] = \Auth::id();
        $telegrams['users'] = $this->getUserList();

        if(empty($telegrams))
		{
			Flash::error('Запись не найдена');

			return redirect(route('telegrams.index'));
		}

		return view('telegrams.edit')->with('telegram', $telegrams);
	}

	/**
	 * Update the specified Telegram in storage.
	 *
	 * @param  int              $id
	 * @param UpdateTelegramRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateTelegramRequest $request)
	{
		$telegram = $this->telegramRepository->find($id);

		if(empty($telegram))
		{
			Flash::error('Запись не найдена');

			return redirect(route('telegrams.index'));
		}

		$telegram = $this->telegramRepository->updateRich($request->all(), $id);

		Flash::success('Запись успешно обнавлена.');

		return redirect(route('telegrams.index'));
	}

	/**
	 * Remove the specified Telegram from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$telegram = $this->telegramRepository->find($id);

		if(empty($telegram))
		{
			Flash::error('Запись не найдена');

			return redirect(route('telegrams.index'));
		}

		$this->telegramRepository->delete($id);

		Flash::success('Запись удалена.');

		return redirect(route('telegrams.index'));
	}


    public function newtelegramcode(){
        if (isset($_POST)){
            $uid = \Auth::id();

            //Создаем новый 6ти значеный цифровой код
            $new_code = \App\Models\Telegram::generateDigitalCode(6);
            \DB::table('telegram')->where('user_id', (int)$uid)->delete();
            \DB::table('telegram')->insertGetId(array('user_id' => (int)$uid, 'digital_code' => $new_code));
            echo $new_code;
        }
    }


    private function getUserList($id = null){
        $request = \DB::table('users');
        if (!empty($id)){
            $request = $request->where('userid', (int)$id);
        }
        $request = $request->where('enable', 1);

        $result = array();
        foreach ($request->get() as $data){
            $k = $data->userid;
            $result[$k] = $data->fio.' ['.$data->username.']';
        }
        return $result;
    }


}
