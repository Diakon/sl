<?php namespace App\Http\Controllers;

class SchedulerController extends Controller
{
    protected $layout = "layouts.main";
    protected $data = array();
    public $model;

    public function __construct()
    {
        $this->modelScheduler = new \App\Models\Scheduler();
        $this->modelTasks = new \App\Models\SchedulerTasks();
        $this->modelComments = new \App\Models\SchedulerComments();
        $this->modelAct = new \App\Models\SchedulerAct();
    }


    public function index()
    {

        $uid =  \Auth::id();
        //получаю статус
        $ustatus = \DB::table('users')->where('userid', $uid)->first();
        //Доступ есть у админа
        if ($ustatus->status != 15){
            return view('scheduler.noaccess');
            exit;
        }

        $this->data['users'] = \App\Models\Scheduler::getAllUsers();
        $this->data['department'] = \App\Models\Scheduler::getDepartment();

		//return 'test';
        return view('scheduler.index',$this->data);
    }

    function  ajaxact(){
        $auth_user_id =  \Auth::id();

        if (isset($_POST['akt_id'])){
            $akt_id = (int)\Input::get('akt_id');
            $status = (int)\Input::get('status_val');
            \DB::table('scheduler_act')
                ->where('id', $akt_id)
                ->update(array('status' => $status));
            return \Response::json( 'ok' ); exit;
        }
        else if (isset($_POST['complite_act'])){
            $status = (int)\Input::get('status_val');
            $id = (int)\Input::get('id');
            $complite_act = (int)\Input::get('complite_act');
            if ($this->modelAct->completedAct($id, $complite_act)){ return \Response::json( 'ok' ); }
            else { return \Response::json( 'err' ); }
        }

    }

    function  ajax(){

        $auth_user_id =  \Auth::id();
        $step = \Input::get( 'step' );
        if (!empty($step)){
            $step = (int)$step;
            switch ( $step ) {
                case 1:
                    //1 шаг - выбрали пользователя
                    $arrayUser = \App\Models\Scheduler::returnUserData((int)\Input::get('id'));
                    \Session::put('order_user_id', (int)\Input::get('id'));
                    return \Response::json( $arrayUser );
                    break;
                case 2:
                    //Удаление подчиненности
                    if ( (\DB::table('users_orders')->where('id', (int)\Input::get('id'))->delete()) ){  return \Response::json( 'ok' ); }
                    else {  return \Response::json( 'error' ); }
                    break;
                case 3:
                    //Добавление подчиненности пользователей
                    $order_user_id = \Session::get('order_user_id');
                    if(empty($order_user_id)){ return \Response::json( 'end_session' );  }
                    else {
                        $user_id = (int)$order_user_id;
                        $type_order = \Input::get('type_order');
                        $rule = \Input::get('rule');
                        foreach (\Input::get('id') as $order_id){
                            //Проверяю, что такая подчиненность этому пользователю уже не добавлена, если добавлена - обновляю, если нет - создаю
                            $chkID = \DB::table('users_orders')->where('user_id', $user_id)->where('order_id', $order_id)->where('type_order', $type_order)->lists('id');

                            if (!empty($chkID)){
                                \DB::table('users_orders')
                                    ->where('id',(int)$chkID)
                                    ->update(array('rule' => $rule));
                                //mysql_query("UPDATE `users_orders` SET `rule`=".$rule." WHERE id=".(int)$chkID."");
                            }
                            else {
                                \DB::table('users_orders')->insert(
                                    array('user_id' => $user_id, 'order_id' => $order_id, 'type_order' => $type_order, 'rule' => $rule)
                                );
                               // mysql_query("INSERT INTO `users_orders` (`user_id`, `order_id`, `type_order`, `rule`) VALUES (".$user_id.", ".$order_id.", ".$type_order.", ".$rule.")");
                            }
                        }
                        return \Response::json( 'ok' );
                    }
                    break;
            }

        }
        elseif (!empty($_POST['task_id']) && !empty($_POST['comment'])){
            //Коментарий добавляем
            $task_id = \Input::get( 'task_id' );
            $comment = \Input::get( 'comment' );

            //удаляем все теги кроме ссылок, картинок и форматирования текста
            $pattern = "<p>, <br>, <strong>, <em>, <del>, <ul>, <li>, <ol>, <a>, <hr>, <img>, <h1>, <h2>, <h3>, <h4>, <h5>, <blockquote>, <pre>, <b>";
            $comment = strip_tags($comment, $pattern);

            $this->modelComments->setTask($task_id);
            $this->modelComments->setDescription($comment);
            $this->modelComments->setAuthor($auth_user_id);
            if ( $this->modelComments->addComment() ) { return \Response::json( 'ok' ); }
            else { return \Response::json( 'error' ); }
        }
        elseif (!empty($_POST['task_id']) && !empty($_POST['delete_flag']) && $_POST['delete_flag']==1){
            //Удаление задания/акта
            $task_id = \Input::get( 'task_id' );
            $type = (isset($_POST['type']))?\Input::get( 'type' ):1;

            //Проверяю право на удаление. Удалить можно только свои задания (исходящие) / акты
            if($type == 1) { $tmp_task = \DB::table('scheduler_tasks')->where('id', $task_id)->first(); }
            elseif ($type == 2){ $tmp_task = \DB::table('scheduler_act')->where('id', $task_id)->first(); }
            if ($tmp_task->user_id != $auth_user_id){ return \Response::json( 'noaccess' ); exit;  }
            else {
                if($type == 1) { $this->modelTasks->deleteTask((int)$task_id); }
                elseif ($type == 2){
                    $this->modelAct->deleteAct((int)$task_id);
                }
                return \Response::json( 'ok' ); exit;
            }
        }
        elseif (!empty($_POST['task_id']) && !empty($_POST['status_val']) ){
            //Изменение статуса заявки
            $task_id = \Input::get( 'task_id' );
            $status = \Input::get( 'status_val' );
            $this->modelTasks->setStatus($status);
            if ($this->modelTasks->updateTask($task_id)){ return \Response::json( 'ok' ); exit; }
            else { return \Response::json( 'err' ); exit; }
        }
        elseif (!empty($_POST['deleteworker'])){
            $task_id = \Input::get( 'id' );
            $type = \Input::get( 'type' );
            $worker_id = \Input::get( 'worker' );
            if ($this->modelTasks->deleteWorkerTask($task_id, $worker_id, $type)){ return \Response::json( 'ok' ); exit; }
            else { return \Response::json( 'err' ); exit; }

        }
        elseif (!empty($_POST['closeTask'])){
            //закрытие задачи из списка задач
            $id = (int)\Input::get( 'id' );
            $type = (int)\Input::get( 'type' );
            if ($type == 1){
                //Если задача
                $this->modelTasks->setCompleted(1);
                $this->modelTasks->updateTask($id);
            } elseif ($type == 2){
                //Если Акт
                $this->modelAct->completedAct($id, 1);

            }
        }


    }

    function  ajaxuploadfile(){
        $auth_user_id =  \Auth::id();

        $task_id = \Input::get( 'task_id' );
        $type = 1;
        if (isset($_POST['type'])){$type = (int)\Input::get( 'type' );}

        // \Session::forget('file_task_key'); //удалить из сессии
        $file_task_key = \Session::get('file_task_key');
        if (!isset($file_task_key)) {
            \Session::put('file_task_key.token', $task_id);  //запись в ссесию
            \Session::put('file_task_key.type', $type);
            \Session::put('file_task_key.file', array());
        }
        $uploaddir = "./uploads/";

        $file = $_FILES['file'];
        $file_type = substr($file["name"], strrpos($file["name"], '.')+1);
        $filename = md5(date("Y-m-d H:i:s")).".".$file_type;
        if ( move_uploaded_file($file["tmp_name"], $uploaddir  . $filename) ){
            \Session::push('file_task_key.file', $file['name'].'~~'.$filename);
            //print_r(\Session::get('file_task_key'));  //чтение

            //Если на странице редактирования (добавили к заданию файл, то пишем и в БД)
            if ((int)$task_id>0){
                $this->modelTasks->taskFile($task_id,1,$file['name'],$filename);
            }

            echo $file['name'].'|'.$filename;
        }
        else {
            echo "send_err";
        }

    }


    function  ajaxdeletefile(){
        $auth_user_id =  \Auth::id();

        if ($this->modelTasks->taskFile(null,2,null,$_POST['name_file'])){
            //Если есть запись о файле в ссессии - удаляем
            $keyDel = null;
            $fileArr = \Session::get('file_task_key');
            if (isset($fileArr)){
                foreach ($fileArr['file'] as $key=>$val){
                    $pieces = explode("~~", $val);
                    if (count($pieces)!=2 || $pieces[1]!=$_POST['name_file']){ continue;}
                    $keyDel = $key;
                }
                $task_data_file = \Session::get('file_task_key.file');
                unset($task_data_file[$keyDel]);
                \Session::set('file_task_key.file', $task_data_file);
            }
            echo "send_ok";
        }
        else { echo "send_err"; }
    }

    public function mytasks(){

        $uid =  \Auth::id();

        //получаю статус
        $data = \DB::table('users')->where('userid', $uid)->first();
        $ustatus = $data->status;
        $this->data['username'] = $data->username;

        if (isset($_POST['Addtasks'])){

            if (!empty($_POST['Addtasks']['burn'])){ $this->modelTasks->setBurn(1); }
            $this->modelTasks->setName($_POST['Addtasks']['name']);
            $this->modelTasks->setDescription($_POST['Addtasks']['description']);
            $this->modelTasks->setDeadline($_POST['Addtasks']['deadline']);
            $this->modelTasks->setTypeTask($_POST['Addtasks']['type_task']);
            if(!empty($_POST['Addtasks']['worker_department'])){$this->modelTasks->setWorkerDepartment($_POST['Addtasks']['worker_department']);}
            if(!empty($_POST['Addtasks']['worker_users'])){$this->modelTasks->setWorkerUsers($_POST['Addtasks']['worker_users']);}


            //Добавляем файлы если были прикреплены к заданию
            $fileArr = \Session::get('file_task_key');
            if (isset($fileArr) && $fileArr['type']==1){
                foreach ($fileArr['file'] as $key=>$val){
                    $pieces = explode("~~", $val);
                    if (count($pieces)!=2){ continue;}
                    $this->modelTasks->setAttachFiles($pieces[0], $pieces[1]);
                }
                \Session::forget('file_task_key');
            }

            $this->modelTasks->addTask();

        }

        if (isset($_POST['Addact'])){

            $this->modelAct->setName( $_POST['Addact']['name'] );

            $arrAct = array();

            //Задания - название
            foreach ($_POST['Addact']['task_name'] as $key => $value){
                $arrAct[$key]['name'] = current($value);
            }
            //Задания - дедлайн
            foreach ($_POST['Addact']['task_deadline'] as $key => $value){
                $arrAct[$key]['deadline'] = current($value);
            }
            //Задания - описание
            foreach ($_POST['Addact']['task_description'] as $key => $value){
                $arrAct[$key]['description'] = current($value);
            }
            //Задания - имполнители отделы
            if (!empty($_POST['Addact']['taskworker_department'])){
                foreach ($_POST['Addact']['taskworker_department'] as $key => $value){
                    $arrAct[$key]['worker_department'] = $value;
                }
            }
            //Задания - имполнители пользователи
            if (!empty($_POST['Addact']['taskworker_users'])){
                foreach ($_POST['Addact']['taskworker_users'] as $key => $value){
                    $arrAct[$key]['worker_users'] = $value;
                }
            }
            //Задания - флаг срочности
            if (!empty($_POST['Addact']['task_burn'])){
                foreach ($_POST['Addact']['task_burn'] as $key => $value){
                    $arrAct[$key]['burn'] = current($value);
                }
            }
            /*
            print_r($_POST);
            print_r($arrAct);die();
            */
            $this->modelAct->setTasks( $arrAct );

            //Добавляем файлы если были прикреплены к акту
            $fileArr = \Session::get('file_task_key');
            if (isset($fileArr) && $fileArr['type']==2){
                foreach ($fileArr['file'] as $key=>$val){
                    $pieces = explode("~~", $val);
                    if (count($pieces)!=2){ continue;}
                    $this->modelAct->setAttachFiles($pieces[0], $pieces[1]);
                }
                \Session::forget('file_task_key');
            }

            $this->modelAct->addAct();

        }

        $this->data['type_action'] = $this->modelTasks->type_action;
        $this->data['department_list'] = \App\Models\Scheduler::getDepartment();

        $user_list_data = array();
        $department_list_data = array();
        foreach ($this->modelTasks->getUserOrders($uid) as $data){
            if ($data->type_order == 1 && $data->rule==1){
                //подчененные отделы
                foreach (\App\Models\Scheduler::getDepartment($data->order_id) as $key=>$val){
                    $department_list_data[$key] = $val;
                }
            }
            elseif ($data->type_order == 2){
                //подчиненные пользователи (сотрудники)
                foreach (\App\Models\Scheduler::getAllUsers($data->order_id) as $key=>$val){
                    $user_list_data[$key] = $val['username'].' ['.$val['fio'].']';
                }
            }
        }

        //добавляем самого пользователя
        foreach (\App\Models\Scheduler::getAllUsers($uid) as $key=>$val){
            $user_list_data[$key] = $val['username'].' ['.$val['fio'].']';
        }

        $this->data['department_list_data'] = $department_list_data;  //Подчененные пользователю отделы
        $this->data['user_list_data'] = $user_list_data;  //Подчененные пользователю сотрудники

        //Весь список пользователей и департаментов
        $all_user_list_data = array();
        foreach (\App\Models\Scheduler::getAllUsers() as $key=>$val){
            $all_user_list_data[$key] = $val['username'].' ['.$val['fio'].']';
        }
        $this->data['all_user_list_data'] = $all_user_list_data;
        $all_department_list_data = array();
        foreach (\App\Models\Scheduler::getDepartment() as $key=>$val){
            $all_department_list_data[$key] =  $val;
        }
        $this->data['all_department_list_data'] = $all_department_list_data;

        $this->data['task_user_data'] = $this->modelTasks->getTasksUserData($uid);


        //Получаю список задач пользователя
        $result = $this->modelTasks->getTaskListUser($uid);
        $this->data['task_list'] = $result;


        return view('scheduler.mytasks',$this->data);

    }

    public function showtasks($task_id){

        $uid =  \Auth::id();
        //получаю статус
        $data = \DB::table('users')->where('userid', $uid)->first();
        $ustatus = $data->status;
        $this->data['username'] = $data->username;

        if (empty($task_id)){ return \Redirect::to('scheduler/mytasks'); exit; }
        $task_id = (int)$task_id;
        $this->data['task_id'] = $task_id;


        if (isset($_POST['Updatetasks'])){

            if (!empty($_POST['Updatetasks']['burn'])){ $this->modelTasks->setBurn(1); }
            if (!empty($_POST['Updatetasks']['name'])){ $this->modelTasks->setName($_POST['Updatetasks']['name']); }
            if (!empty($_POST['Updatetasks']['description'])){ $this->modelTasks->setDescription($_POST['Updatetasks']['description']); }
            if (!empty($_POST['Updatetasks']['deadline'])){ $this->modelTasks->setDeadline($_POST['Updatetasks']['deadline']); }
            if (!empty($_POST['Updatetasks']['status'])){ $this->modelTasks->setStatus($_POST['Updatetasks']['status']); }
            if (!empty($_POST['Updatetasks']['worker_department'])){ $this->modelTasks->setWorkerDepartment($_POST['Updatetasks']['worker_department']); }
            if (!empty($_POST['Updatetasks']['worker_users'])){ $this->modelTasks->setWorkerUsers($_POST['Updatetasks']['worker_users']); }
            if (!empty($_POST['Updatetasks']['completed'])){ $this->modelTasks->setCompleted($_POST['Updatetasks']['completed']); }

            if(!empty($_POST['Updatetasks']['taskworker_department'])){$this->modelTasks->setWorkerDepartment($_POST['Updatetasks']['taskworker_department']);}
            if(!empty($_POST['Updatetasks']['taskworker_users'])){$this->modelTasks->setWorkerUsers($_POST['Updatetasks']['taskworker_users']);}

            $this->modelTasks->updateTask($task_id);
        }


        //определяем права пользователя в работе над заявкой - хозяин может редактировать, другие- коментировать, менять статус
        $taskData = current(\DB::table('scheduler_tasks')->where('id', $task_id)->get());


        //Если задача часть Акта, определяю кто автор Акта
        if ($taskData->type_task == 2){
            $autor_id = \DB::table('scheduler_act')->where('id', $taskData->user_id)->first();
            $autor_id = $autor_id->user_id;
        } else { $autor_id = 0; }

        if ($uid == $taskData->user_id || $uid ==$autor_id){
            //автор заявки
            $this->data['owner_task'] = true;
            $this->data['type_action'] = $this->modelTasks->type_action;
        }
        else {
            //другой - можно коментировать
            $this->data['owner_task'] = false;
        }

        $this->data['status_arr'] = $this->modelTasks->status_arr;
        $this->data['task'] = current($this->modelTasks->getTaskByID($task_id)['task']);
        $this->data['task_files'] = ((isset($this->modelTasks->getTaskByID($task_id)['files']))?($this->modelTasks->getTaskByID($task_id)['files']):null);



        $workerArr = $this->modelTasks->getTaskWorkerByID($task_id);
        foreach ($workerArr['user'] as $data){
            $tmp_user = current(\DB::table('users')->where('userid', $data->worker_id)->get());
            $this->data['worker_task_user'][$tmp_user->userid] = $tmp_user->username;
        }
        foreach ($workerArr['departament'] as $data){
            $tmp_depart = current(\DB::table('dir_department')->where('id', $data->worker_id)->get());
            $this->data['worker_task_departament'][$tmp_depart->id] = $tmp_depart->name;
        }

        //Получаем коментарии к задаче
        $tmp_comments = array();
        foreach ($this->modelComments->getComment($task_id) as $data){
            $tmp_comments[$data->id]['author'] = current(\DB::table('users')->where('userid', $data->author_id)->get())->username;
            $tmp_comments[$data->id]['comment'] = $data->comment;
            $tmp_comments[$data->id]['created_at'] = $data->created_at;
        }
        $this->data['task_comments'] = $tmp_comments;


        //получаем пользователей/акты для добавления в задачу
        //Если задача - часть акта, то получаем все отделы/департаменты
        $departments_list = array();
        $users_list = array();
        if ($taskData->type_task == 2){
            foreach (\App\Models\Scheduler::getAllUsers() as $key=>$val){
                $users_list[$key] = $val['username'].' ['.$val['fio'].']';
            }
            foreach (\App\Models\Scheduler::getDepartment() as $key=>$val){
                $departments_list[$key] =  $val;
            }
        } else {
            foreach ($this->modelTasks->getUserOrders($uid) as $data){
                if ($data->type_order == 1 && $data->rule==1){
                    //подчененные отделы
                    foreach (\App\Models\Scheduler::getDepartment($data->order_id) as $key=>$val){
                        $departments_list[$key] = $val;
                    }
                }
                elseif ($data->type_order == 2){
                    //подчиненные пользователи (сотрудники)
                    foreach (\App\Models\Scheduler::getAllUsers($data->order_id) as $key=>$val){
                        $users_list[$key] = $val['username'].' ['.$val['fio'].']';
                    }
                }
            }
        }

        $this->data['departments_list'] = $departments_list;
        $this->data['users_list'] = $users_list;



        return view('scheduler.showtasks',$this->data);
    }



    public function showakt($akt_i){

        $uid =  \Auth::id();
        //получаю статус
        $data = \DB::table('users')->where('userid', $uid)->first();
        $ustatus = $data->status;
        $this->data['username'] = $data->username;

        $this->data['act_data'] = $this->modelAct->getAct((int)$akt_i);
        $this->data['status_arr'] = $this->modelTasks->status_arr;
        $this->data['owner_akt'] = (($uid==$this->data['act_data']['act']->user_id)?true:false);

        $tasks = array();
        $i = 0;
        foreach ($this->data['act_data']['tasks'] as $groupTasksData){

            foreach ( \DB::table('scheduler_tasks')->where('type_task', 2)->where('user_id', $akt_i)->where('name', $groupTasksData->name)->get() as $data ){
                $tasks[$i][$data->id]['name'] = $data->name;
                $tasks[$i][$data->id]['description'] = $data->description;
                $tasks[$i][$data->id]['status'] = $data->status;
                $tasks[$i][$data->id]['burn'] = $data->burn;
                $tasks[$i][$data->id]['deadline'] = $data->deadline;
                $tasks[$i][$data->id]['completed'] = $data->completed;
                $tasks[$i][$data->id]['created_at'] = $data->created_at;
            }
            ++$i;

        }
        $this->data['act_tasks'] = $tasks;


        return view('scheduler.showakt',$this->data);
    }

}
