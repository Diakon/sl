<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCardContactRequest;
use App\Http\Requests\UpdateCardContactRequest;
use App\Libraries\Repositories\CardContactRepository;
use Flash;
//use Mitul\Controller\AppBaseController as Controller;
use Response;

class AgencyContactController extends Controller
{

	/** @var  CardContactRepository */
	private $cardContactRepository;

	function __construct(CardContactRepository $cardContactRepo)
	{
		$this->cardContactRepository = $cardContactRepo;
	}

	/**
	 * Display a listing of the CardContact.
	 *
	 * @return Response
	 */
	public function index()
	{
		$cardContact = $this->cardContactRepository->paginate(10);
		return view('agencyContacts.index')
			->with('cardContacts', $cardContact);
	}

	/**
	 * Show the form for creating a new CardContact.
	 *
	 * @return Response
	 */
	public function create()
	{
        $k = (isset($_GET['id']))?((int)$_GET['id']):(null);
        $cardContact['card_staffing'] =$this->getOrganization($k);
        return view('agencyContacts.create')->with('cardContact', $cardContact);
	}

	/**
	 * Store a newly created CardContact in storage.
	 *
	 * @param CreateCardContactRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCardContactRequest $request)
	{
		$input = $request->all();

        $input['author_id'] = \Auth::id();

		$cardContact = $this->cardContactRepository->create($input);

		Flash::success('Запись создана.');

		return redirect(route('agencyContacts.index'));
	}

	/**
	 * Display the specified CardContact.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$cardContact = $this->cardContactRepository->find($id);

		if(empty($cardContact))
		{
			Flash::error('Запись не найдена');

			return redirect(route('agencyContacts.index'));
		}

		return view('agencyContacts.show')->with('cardContact', $cardContact);
	}

	/**
	 * Show the form for editing the specified CardContact.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$cardContact = $this->cardContactRepository->find($id);

		if(empty($cardContact))
		{
			Flash::error('Запись не найдена');

			return redirect(route('cardContacts.index'));
		}

        $cardContact['card_staffing'] =$this->getOrganization($cardContact->card_staffing_id);
		return view('agencyContacts.edit')->with('cardContact', $cardContact);
	}


	/**
	 * Update the specified CardContact in storage.
	 *
	 * @param  int              $id
	 * @param UpdateCardContactRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateCardContactRequest $request)
	{
		$cardContact = $this->cardContactRepository->find($id);

		if(empty($cardContact))
		{
			Flash::error('Запись не найдена');

			return redirect(route('cardContacts.index'));
		}

		$cardContact = $this->cardContactRepository->updateRich($request->all(), $id);

		Flash::success('Запись успешно обнавлена.');

		return redirect(route('agencyContacts.index'));
	}

	/**
	 * Remove the specified CardContact from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$cardContact = $this->cardContactRepository->find($id);

		if(empty($cardContact))
		{
			Flash::error('Запись не найдена');

			return redirect(route('agencyContacts.index'));
		}

		$this->cardContactRepository->delete($id);

		Flash::success('Запись успешно удалена.');

		return redirect(route('agencyContacts.index'));
	}

    private function getOrganization($id=null){
        if (empty($id)){ $model = \DB::table('card_staffing')->get(); }
        else { $model = \DB::table('card_staffing')->where('id', (int)$id)->get(); }
        $result = array(0=>'Выбирите из списка');
        foreach ($model as $data){
            $k = $data->id;
            $result[$k] = $data->official_name;
        }
        return $result;
    }
}
