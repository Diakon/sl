<?php namespace App\Http\Controllers;

use App\Libraries\Repositories\ContractorRepository;
use Illuminate\Http\Request;
use App\Http\Requests\CreateContractorRequest;
use Flash;
use App\Models;
use Illuminate\Support\Facades\DB;

class ContractorController extends Controller
{

	/** @var  contractorRepository */
	private $contractorRepository;

	function __construct(ContractorRepository $contractorRepository)
	{
		$this->contractorRepository = $contractorRepository;
		view()->share('dir_banks', DB::table('dir_banks')->lists('bank_name','bank_id'));
	}

	/**
	 * Display a listing of the Contractor.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		return view('contractor.index')
			->with('contractors', $this->contractorRepository->search($request->all()))
			->withRequest($request);
	}

	/**
	 * Show the form for creating a new Contractor.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('contractor.create');
	}

	/**
	 * Store a newly created Contractor in storage.
	 *
	 * @param CreateContractorRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateContractorRequest $request)
	{
		$input = $request->all();

		$this->contractorRepository->create($input);
		Flash::success('Запись создана.');
		return redirect(route('contractor.index'));
	}

	public function edit($id)
	{
		$contractor = $this->contractorRepository->find($id);

		if (empty($contractor)) {
			abort(404);
		}
        return view('contractor.edit')->with('contractor', $contractor);
	}

	/**
	 * Update the specified Contractor in storage.
	 *
	 * @param  int              $id
	 * @param CreateContractorRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateContractorRequest $request)
	{
		$this->contractorRepository->myupdateRich($request->all(), $id);
		Flash::success('Запись успешно обнавлена.');
		return redirect(route('contractor.index'));
	}

	public function destroy($id) {
		$contractor = $this->contractorRepository->find($id);
		if (empty($contractor)) {
			abort(404);
		}
		$this->contractorRepository->delete($id);
		Flash::success('Контрагент удален');
		return redirect(route('contractor.index'));
	}
}
