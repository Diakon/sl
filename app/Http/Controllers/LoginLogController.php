<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CreateLoginLogRequest;
use App\Http\Requests\UpdateLoginLogRequest;
use App\Libraries\Repositories\LoginLogRepository;
use Flash;
use Illuminate\Support\Facades\DB;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class LoginLogController extends AppBaseController
{

	/** @var  LoginLogRepository */
	private $loginLogRepository;

	function __construct(LoginLogRepository $loginLogRepo)
	{
		$this->loginLogRepository = $loginLogRepo;
	}

	/**
	 * Display a listing of the LoginLog.
	 *
	 * @return Response
	 */
	public function index()
	{
        $this->checkAccess();
		$loginLogs = $this->loginLogRepository->paginate(10);

		return view('loginLogs.index')
			->with('loginLogs', $loginLogs);
	}

	/**
	 * Show the form for creating a new LoginLog.
	 *
	 * @return Response
	 */
	public function create()
	{
        $this->checkAccess();
		return view('loginLogs.create');
	}

	/**
	 * Store a newly created LoginLog in storage.
	 *
	 * @param CreateLoginLogRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateLoginLogRequest $request)
	{
		$input = $request->all();

		$loginLog = $this->loginLogRepository->create($input);

		Flash::success('LoginLog saved successfully.');

		return redirect(route('loginLogs.index'));
	}

	/**
	 * Display the specified LoginLog.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$loginLog = $this->loginLogRepository->find($id);

		if(empty($loginLog))
		{
			Flash::error('LoginLog not found');

			return redirect(route('loginLogs.index'));
		}

		return view('loginLogs.show')->with('loginLog', $loginLog);
	}

	/**
	 * Show the form for editing the specified LoginLog.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
        $this->checkAccess();

		$loginLog = $this->loginLogRepository->find($id);

		if(empty($loginLog))
		{
			Flash::error('LoginLog not found');

			return redirect(route('loginLogs.index'));
		}

		return view('loginLogs.edit')->with('loginLog', $loginLog);
	}

	/**
	 * Update the specified LoginLog in storage.
	 *
	 * @param  int              $id
	 * @param UpdateLoginLogRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateLoginLogRequest $request)
	{
		$loginLog = $this->loginLogRepository->find($id);

		if(empty($loginLog))
		{
			Flash::error('LoginLog not found');

			return redirect(route('loginLogs.index'));
		}

		$loginLog = $this->loginLogRepository->updateRich($request->all(), $id);

		Flash::success('LoginLog updated successfully.');

		return redirect(route('loginLogs.index'));
	}

	/**
	 * Remove the specified LoginLog from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
        $this->checkAccess();

		$loginLog = $this->loginLogRepository->find($id);

		if(empty($loginLog))
		{
			Flash::error('LoginLog not found');

			return redirect(route('loginLogs.index'));
		}

		$this->loginLogRepository->delete($id);

		Flash::success('LoginLog deleted successfully.');

		return redirect(route('loginLogs.index'));
	}

    private function checkAccess(){
        $uid =  \Auth::id();
        if ( \DB::table('users')->where('userid', $uid)->first()->status != 15 ){ return \Redirect::to('/'); exit;  }
    }

    public function report(Request $request){
        $date_start = (new \DateTime($request->input('s_pr_from','now')))->format('Y-m-d');
		$date_end = (new \DateTime($request->input('s_pr_to','now')))->format('Y-m-d');

		$data = DB::table('login_log')
			->select(
				'username', 'id_user',
				DB::raw('date_format(min(created_at), "%H:%i:%s") as date_first'),
				DB::raw('date_format(max(created_at), "%H:%i:%s") as date_end'),
				DB::raw('date(created_at) as date')
			)
			->where('action', 0)
			->whereRaw('created_at between
				STR_TO_DATE(?, "%Y-%m-%d %H:%i:%s")  and
				STR_TO_DATE(concat(?, "23:59:59"), "%Y-%m-%d %H:%i:%s")')
			->addBinding($date_start)->addBinding($date_end)
			->groupby('username', DB::raw('date(created_at)'))
			->orderByRaw( 'date desc, username asc')
			->get();
        return view('loginLogs.report')->with('loginLog', $data)->withRequest($request);
    }
}
