<?php namespace  App\Http\Controllers;

use Auth;
use User;
use Request;
use Hash;
use App\Http\Controllers\Controller;
use App\Models;
use Illuminate\Support\Facades\App;
use  App\Traits\SendMailTrait;

class BankcardController extends Controller {

    use SendMailTrait;
    private $param;

    public function index(\Illuminate\Http\Request $request){

        if ($request->has('anketa_id')) {
            \App\Models\PersMain::findOrFail($request->input('anketa_id'));
        }

        ini_set('memory_limit', '550M');

        $param = array();
        if (isset($_POST['_token'])){
            //Фильтруем
            unset($_POST['_token']);
            $param = $_POST;
        }

        $this->data['table'] = $this->getBanksDataList($param);
        $this->data['object'] = $this->getObject();
        $this->data['cardStatus'] = $this->getCardStatus();
        $this->data['banks_list'] = $this->getBanks();
        $this->data['bik_list'] = $this->getBankBiks();
        $this->data['pers_status_list'] = $this-> getPersStatus();
        $this->data['dir_banks_bik'] = \DB::table('dir_banks_bik')->get();

        return view('bankcard.index',$this->data)->withRequest($request);
    }

    private function getBanksDataList($param = array()){

        $result = \DB::table('pers_main')
            ->leftJoin('pers_status', 'pers_status.id_anketa', '=', 'pers_main.id_anketa')
            ->leftJoin('pers_bank', 'pers_bank.id_anketa', '=', 'pers_main.id_anketa')
            ->leftJoin('dir_banks', 'dir_banks.bank_id', '=', 'pers_bank.card_number')
            ->leftJoin('dir_banks_bik', 'dir_banks_bik.id', '=', 'dir_banks.bank_bik')
            ->leftJoin('dir_card_status', 'dir_card_status.status_id', '=', 'pers_bank.card_status')
            ->select('pers_main.id_anketa','pers_main.firstname', 'pers_main.lastname', 'pers_main.patronymic', 'pers_status.object', 'pers_bank.card_number', 'pers_bank.card_status',
                      'pers_bank.card_expire', 'pers_bank.bank_name', 'pers_bank.account as account', 'pers_bank.bank_bik_id as bik', 'pers_status.position', 'pers_status.status_name as pers_status_name'
            );

        if (!empty($param)) {

            if (isset($param['id_anketa']) && (int)$param['id_anketa']!=0 ){
                $result = $result->where('pers_main.id_anketa', '=', (int)$param['id_anketa']);
            }
            if (isset($param['id_object']) && (int)$param['id_object']!=0 ){
                $result = $result->where('pers_status.object', '=', (int)$param['id_object']);
            }
            if (isset($param['card_number']) && (int)$param['card_number']!=0 ){
                $result = $result->where('pers_bank.card_number', 'LIKE', '%'.$param['card_number'].'%');
            }
            if (isset($param['id_object']) && (int)$param['id_object']!=0 ){
                $result = $result->where('pers_status.object', '=', (int)$param['id_object']);
            }
            if (isset($param['id_card_status'])){
                $tmp_arr = array();
                foreach ($param['id_card_status'] as $key=>$val){
                    if (empty($val)){continue;}
                    $tmp_arr[] = (int)$val;
                }
                if (!empty($tmp_arr)){ $result = $result->whereIn('pers_bank.card_status', $tmp_arr); }
            }
            if (isset($param['id_bank'])){
                $tmp_arr = array();
                foreach ($param['id_bank'] as $key=>$val){
                    if (empty($val)){continue;}
                    $tmp_arr[] = (int)$val;
                }
                if (!empty($tmp_arr)){ $result = $result->whereIn('pers_bank.bank_name', $tmp_arr); }
            }
            if (isset($param['pers_status_list'])){

                $tmp_arr = array();
                foreach ($param['pers_status_list'] as $key=>$val){
                    if (empty($val)){continue;}
                    $tmp_arr[] = $val;
                }
                if (!empty($tmp_arr)){ $result = $result->whereIn('pers_status.status_name', $tmp_arr); }

                /*
                foreach ($param['pers_status_list'] as $key=>$val){
                    if (empty($val)){continue;}
                    $result = $result->where('pers_status.position', 'LIKE', $val);
                }
                */
            }


        }

        $result = $result->where('pers_main.disabled', '=', 0)->orderBy('dir_card_status.status_name', 'asc')->paginate(100);

        return $result;
    }

    private function getObject($id = null){

        $object = \DB::table('objects');
        if (!empty($id)){ $object = $object->where('id', $id)->first(); }
        else { $object = $object->get(); }

        $result = array();
        $result[0] = '';
        foreach ($object as $data){
            $k = $data->id;
            $result[$k] = $data->name;
        }
        return (!empty($id))?$result[$id]:$result;
    }

    private function getCardStatus($id = null){

        $object = \DB::table('dir_card_status');
        if (!empty($id)){ $object = $object->where('status_id', $id)->first(); }
        else { $object = $object->get(); }

        $result = array();
        $result[0] = '';
        foreach ($object as $data){
            $k = $data->status_id;
            $result[$k] = $data->status_name;
        }
        return (!empty($id))?$result[$id]:$result;
    }

    private function getPersStatus(){
        $result = array();
        /*
        $status = \DB::table('pers_status')->where('position', '<>', '')->whereNotNull('position')->orderBy('position', 'asc')->groupBy('position')->get();
        foreach ($status as $data){
            $k = $data->position;
            $result[$k] = $data->position;
        }
        */

        $status = \DB::table('worker_status')->get();
        foreach ($status as $data){
            $k = $data->id;
            $result[$k] = $data->name;
        }

        return $result;
    }

    private function getBanks($id = null){

        $banks = \DB::table('dir_banks');
        if (!empty($id)){ $banks = $banks->where('bank_id', $id)->first(); }
        else { $banks = $banks->get(); }

        $result = array();
        $result[0] = '';
        foreach ($banks as $data){
            $k = $data->bank_id;
            $result[$k] = $data->bank_name;
        }
        return (!empty($id))?$result[$id]:$result;
    }

    private function getBankBiks($id = null){

        /*
        $banksBiks = \DB::table('dir_banks_bik');
        if (!empty($id)){ $banksBiks = $banksBiks->where('id', $id)->first(); }
        else { $banksBiks = $banksBiks->get(); }

        $result = array();
        foreach ($banksBiks as $data){
            $k = $data->dir_banks_id;
            $i =  $data->id;
            $result[$k][$i] = $data->bik;
        }
        */
        $banksBiks = \DB::table('dir_banks_bik');
        if (!empty($id)){ $banksBiks = $banksBiks->where('id', $id)->first(); }
        else { $banksBiks = $banksBiks->get(); }

        $result = array();
        $result[0] = '';
        foreach ($banksBiks as $data){
            $i =  $data->id;
            $result[$i] = $data->bik;
        }

        return (!empty($id))?$result[$id]:$result;
    }

    public function ajax(){

        if ($_POST){
            if(isset($_POST['id_anketa'])){ $id_anketa = (int)$_POST['id_anketa']; }

            if ( (int)$_POST['type'] == 1 ){
                $email_card = [];
                $param = array();
                //$param[0] - таблица, $param[1] - поле таблицы, $param[2] - значение
                foreach($_POST['param'] as $key=>$val){
                    $tmp_param = explode("~", $val);
                    $k = $tmp_param[1];
                    $param[$k][] = $tmp_param[0];
                    $param[$k][] = $tmp_param[2];
                }


                if ($param['bik'][0]=='dir_banks_bik' && !empty($param['bik'][1])){
                    //Пишем БИК
                    \DB::table('pers_bank')
                        ->where('id_anketa', $id_anketa)
                        ->update(array('bank_bik_id' => $param['bik'][1]));
                }
                unset($param['bik']);
                $i = 0;
                $card_number = null;
                foreach ($param as $key=>$val){
                    //Проверяем есть ли строка с такими данными, если нет создаем
                    if (\DB::table($val[0])->where('id_anketa', $id_anketa)->pluck('id_anketa')){
                        \DB::table($val[0])
                            ->where('id_anketa', $id_anketa)
                            ->update(array($key => $val[1]));
                    } else { \DB::table($val[0])->insertGetId(array('id_anketa' => $id_anketa, $key => $val[1])); }

                    if ( $val[0]=='pers_bank' && $key=='card_number' ){
                        $card_number = $val[1];
                    }
                    //Если карте проставлен статус "на возврат"  -  пишем в массив для отправки писем ФИО человека, карту
                    if ( $val[0]=='pers_bank' && $val[1]==6 && $key=='card_status' ){
                        $pers_main = \DB::table('pers_main')->where('id_anketa', $id_anketa)->first();
                        $email_card[$i]['fio'] = $pers_main->lastname.' '.$pers_main->firstname.' '.$pers_main->patronymic;
                        $email_card[$i]['card_number'] = (!empty($card_number))?$card_number:'не указана';
                        ++$i;
                    }
                }

                //Отправляяем уведомление fo@vlspro.ru
                if (!empty($email_card)){
                    $email = 'fo@vlspro.ru';
                    $bodyArr = array(
                        'title' => 'Был проставлен статус `на возврат` для следующих карт:',
                        'vals' => '',
                    );
                    foreach ($email_card as $k=>$v){
                        $bodyArr['vals'] .= ''.$v['fio'].' ['.$v['card_number'].']; ';
                    }
                    $this->sendMail($email, $bodyArr, 'Уведомление с сайта cloud.secondlab.ru', 'bankcard_ststus');
                }
            }
            if ( (int)$_POST['type'] == 2 ){
                $status = $_POST['status'];
                $param = $_POST['param'];
                foreach ($param as $key=>$id_anketa){
                    \DB::table('pers_bank')
                        ->where('id_anketa', $id_anketa)
                        ->update(array('card_status' => $status));
                }
            }
            if ( (int)$_POST['type'] == 3 ){
                //Пишем БАНК/БИК
                $_POST['bik_name'] = trim($_POST['bik_name']);
                $_POST['bank_name'] = trim($_POST['bank_name']);

                if ( (int)$_POST['bank_select'] == 0  && empty($_POST['bank_name'])  ){exit;}

                $bank_id = (int)$_POST['bank_select'];
                if (!empty($_POST['bank_name'])){
                    //Если указали в поле ввода название банка и такой банк есть - получаем его id из БД
                    $bank_id = \DB::table('dir_banks')->where('bank_name', 'LIKE', $_POST['bank_name'])->pluck('bank_id');
                    if (empty($bank_id)){ $bank_id = \ DB::table('dir_banks')->insertGetId(array('bank_name' => $_POST['bank_name'])); }
                }
                //Указали БИК, проверяем есть ли такой в БД, если нет - пишем его к указаному банку
                if (!empty($_POST['bik_name'])){
                    if (!\DB::table('dir_banks_bik')->where('bik', $_POST['bik_name'])->first()){
                        //Добавляем БИК
                        \DB::table('dir_banks_bik')->insertGetId(array('dir_banks_id' => $bank_id, 'bik' => $_POST['bik_name']));
                    }
                }
                //Писать в историю добавление справочника банков не надо
                echo 'ok';
                exit;
            }

            //Пишем в историю
            $this->addHistory(array('id_anketa'=>$id_anketa));

        }
        echo 'ok';
    }

    private function addHistory($param){

        $uid =  \Auth::id();

        //Генерируем массив значений для истории
        $tablesArr = ['pers_main','pers_address','pers_passport','pers_status','pers_bank','pers_alien','pers_action'];
        $arr = array();
        foreach ($tablesArr as $table){
            foreach (\DB::table($table)->where('id_anketa', $param['id_anketa'])->get() as $data){
                $arr += (array) $data;
            }
        }
        $tablesArr = ['pers_documents','pers_penalty','pers_bank'];
        foreach ($tablesArr as $table){
            foreach (\DB::table($table)->where('id_anketa', $param['id_anketa'])->get() as $data){
                foreach ((array) $data as $k=>$v){
                    if (!isset($arr[$k])){ continue; }
                    if (gettype($arr[$k])!='array') $arr[$k] = array();
                    array_push($arr[$k],$v);
                }
            }
        }

        $code = base64_encode(serialize($arr));

        $id = \DB::table('history')->insertGetId(
            array('id_anketa' => $param['id_anketa'], 'code' => $code, 'id_user'=>$uid)
        );
        return $id;

    }
}


