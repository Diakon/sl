<?php namespace App\Http\Controllers;

use App\Http\Requests\CreateTuovDealRatesRequest;
use App\Http\Requests\UpdateTuovDealRatesRequest;
use App\Libraries\Repositories\TuovDealRatesRepository;
use App\Models\Deal;
use App\Models\Subdivision;
use App\Models\Objects;
use Illuminate\Support\Facades\Auth;
use App\Models\TuovDealRates;
use Flash;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\View;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use Illuminate\Support\Facades\Validator;

class TuovDealRatesController extends AppBaseController
{

	/** @var  TuovDealRatesRepository */
	private $tuovDealRatesRepository;

	function __construct(TuovDealRatesRepository $tuovDealRatesRepo, Objects $objects)
	{
        $this->tuovDealRatesRepository = $tuovDealRatesRepo;

        View::creator(['tuovDealRates.index', 'tuovDealRates.create', 'tuovDealRates.edit'], function ($view) {
            $view->with('activeDeals', Deal::getActiveDeals())
                ->with('activeSubdivisions', Subdivision::getActiveSubdivisions());
        });
        View::creator(['tuovDealRates.index'], function ($view) use ($objects) {
            $view->with('activeDeals', Deal::getActiveDeals());

            $my_objects = [];
            $my_subdivision_ids = [];
            $userObjects = Auth::user()->getObjects();
            foreach ($objects->whereIn('id', $userObjects->pluck('id')->all())->get() as $obj) {
                foreach ($obj->subdivisions()->where('subdivision_comments','в работе')->get() as $sub) {
                    $my_objects[$obj->name][] = $sub;
                    $my_subdivision_ids[] = $sub->id;
                }
            }
            $view->with('objects', $my_objects);
            $view->with('my_collection_ids', $my_subdivision_ids);
        });

        Validator::extend(
            'tuov_deal_rates_date_range',
            function ($attribute, $value, $parameters, $validator) {
                $model = App()->make(\App\Models\TuovDealRates::class);

                $from = Request::get('from');
                $to = Request::get('to');
                $id = Request::get('id', 0);

                if ($id && $curent_rate = $model::find($id)) {
                    $deal_id = Request::get('deal_id', $curent_rate->deal_id);
                    $subdivision_id = Request::get('subdivision_id', $curent_rate->subdivision_id);
                } else {
                    $deal_id = Request::get('deal_id');
                    $subdivision_id = Request::get('subdivision_id');
                }

                $collision = $model
                    ->where('from', '<=', $to)
                    ->where('to', '>=', $from)
                    ->where('deal_id', $deal_id)
                    ->where('subdivision_id', $subdivision_id)
                    ->where('id', '<>', $id)
                    ->first();
                if ($collision) {
                    $validator->setCustomMessages([
                        'tuov_deal_rates_date_range' => sprintf('Диапазон дат имеют пересечения c id: <a href="%s" target="_blank">%s</a> c %s по %s', route('tuovDealRates.edit', $collision->id),
                            $collision->id,
                            $collision->from, $collision->to)
                    ]);
                    return false;
                }
                return true;
            }
        );
    }

    /**
     * Display a listing of the TuovDealRates.
     *
     * @return Response
     */
    public function index(\Illuminate\Http\Request $request)
    {

        $tuovDealRates = $this->tuovDealRatesRepository->search($request->all());
        return view('tuovDealRates.index', ['tuovDealRates'=> $tuovDealRates]);
    }


    /**
     * Display a listing of the TuovDealRates by subdivision.
     *
     * @return Response
     */
    public function subdivision($id)
    {
        if(!$id) {
            return redirect(route('tuovDealRates.index'));
        }
        $tuovDealRates = $this->tuovDealRatesRepository->search(['subdivision_id' => $id]);
        return view('tuovDealRates.index', ['tuovDealRates'=> $tuovDealRates]);

    }

	/**
	 * Show the form for creating a new TuovDealRates.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('tuovDealRates.create');
	}

	/**
	 * Store a newly created TuovDealRates in storage.
	 *
	 * @param CreateTuovDealRatesRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateTuovDealRatesRequest $request)
	{
		$input = $request->all();
        $this->tuovDealRatesRepository->create($input);
        Flash::success('TuovDealRates saved successfully.');
        return redirect(route('tuovDealRates.subdivision', [ 'id' => $input['subdivision_id'] ]));
	}

	/**
	 * Display the specified TuovDealRates.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$tuovDealRates = $this->tuovDealRatesRepository->find($id);

		if(empty($tuovDealRates))
		{
			Flash::error('TuovDealRates not found');

			return redirect(route('tuovDealRates.index'));
		}

		return view('tuovDealRates.show',array('tuovDealRates'=> $tuovDealRates));
	}

	/**
	 * Show the form for editing the specified TuovDealRates.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$tuovDealRates = $this->tuovDealRatesRepository->findOrFail($id);
        return view('tuovDealRates.edit',array('tuovDealRates'=> $tuovDealRates));
	}

	/**
	 * Update the specified TuovDealRates in storage.
	 *
	 * @param  int              $id
	 * @param UpdateTuovDealRatesRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateTuovDealRatesRequest $request)
	{
		$this->tuovDealRatesRepository->findOrFail($id);
		$this->tuovDealRatesRepository->updateRich($request->all(), $id);

		Flash::success('TuovDealRates updated successfully.');
        return redirect(route( 'tuovDealRates.subdivision', $request['subdivision_id'] ));
    }

	/**
	 * Remove the specified TuovDealRates from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$this->tuovDealRatesRepository->findOrFail($id);
		$this->tuovDealRatesRepository->delete($id);

		Flash::success('TuovDealRates deleted successfully.');
		return redirect(route('tuovDealRates.index'));
	}

    public function ajax(\Illuminate\Http\Request $request){
        $answer = 'ok';
        if (isset($_POST['type']) && isset($_POST['blue']) && isset($_POST['red'])){
            //Получаем ID записи с которой будем менять данные
            $key = $_POST['type'];
            $model_work = \DB::table('tuov_deal_rates')->where('id', $_POST[$key])->first();  //с этой моделью будем работать
            unset($_POST['type']);
            unset($_POST[$key]);
            foreach ($_POST as $key=>$val){
                $model_data = \DB::table('tuov_deal_rates')->where('id',$val)->first(); //это вторая модель - из нее будем получать данные для изменения данных в модели $model_work
            }

            if (!empty($model_work) && !empty($model_data)){
                //Делаем первую дату обрабатываемой записи раной последней даты второй модели + количество дней

                //Определяем последнюю дату обрабатываемой модели
                $last_date = (int)(date('d',( strtotime($model_work->to) - strtotime($model_work->from) )))-1;

                $update = array(
                    'from' => date('Y-m-d', strtotime($model_data->to.' +1 day')),
                    'to' => date('Y-m-d', strtotime($model_data->to.' +'.$last_date.' day'))
                );

                \DB::table('tuov_deal_rates')
                    ->where('id', $model_work->id)
                    ->update($update);
            }

        }
        if (isset($_POST['object']) && isset($_POST['from']) && isset($_POST['to'])){
            //Проверяем что для указанного объекта и дат нет пересечений (колизий)
            $answer = array();
            $model = \DB::table('tuov_deal_rates')->where('subdivision_id', $_POST['object'])->where('from', '>=', date('Y-m-d', strtotime($_POST['from'])))->where('to', '<=', date('Y-m-d', strtotime($_POST['to'])));
            if (isset($_POST['rates']) && !empty($_POST['rates'])){
                $model = $model->where('id','<>',(int)$_POST['rates']);
            }
            $i = 0;
            foreach ( $model->get() as $data ){
                $answer[$i]['id'] = $data->id;
                $answer[$i]['from'] = date('d-m-Y',strtotime($data->from));
                $answer[$i]['to'] = date('d-m-Y',strtotime($data->to));
                ++$i;
            }
        }
        if (isset($_POST['id']) && isset($_POST['rate']) && isset($_POST['from']) && isset($_POST['to'])){
            $this->validate($request, TuovDealRates::$rulesForAjaxUpdate);

            $this->tuovDealRatesRepository->findOrFail(Request::get('id'));
            $this->tuovDealRatesRepository->updateRich($request->all(), Request::get('id'));
        }

        return \Response::json( $answer );
    }
}
