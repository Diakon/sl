<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatePersMainRequest;
use App\Http\Requests\UpdatePersMainRequest;
use App\Libraries\Repositories\PersMainRepository;
use Flash;
use Response;

class PersMainController extends Controller
{

	/** @var  PersMainRepository */
	private $persMainRepository;

	function __construct(PersMainRepository $persMainRepo)
	{
		$this->persMainRepository = $persMainRepo;
	}

	/**
	 * Display a listing of the PersMain.
	 *
	 * @return Response
	 */
	public function index()
	{
		$persMains = $this->persMainRepository->paginate(10);

		return view('persMains.index')
			->with('persMains', $persMains);
	}

	/**
	 * Show the form for creating a new PersMain.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('persMains.create');
	}

	/**
	 * Store a newly created PersMain in storage.
	 *
	 * @param CreatePersMainRequest $request
	 *
	 * @return Response
	 */
	public function store(CreatePersMainRequest $request)
	{
		$input = $request->all();

		$persMain = $this->persMainRepository->create($input);

		Flash::success('PersMain saved successfully.');

		return redirect(route('persMains.index'));
	}

	/**
	 * Display the specified PersMain.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$persMain = $this->persMainRepository->find($id);

		if(empty($persMain))
		{
			Flash::error('PersMain not found');

			return redirect(route('persMains.index'));
		}

		return view('persMains.show')->with('persMain', $persMain);
	}

	/**
	 * Show the form for editing the specified PersMain.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$persMain = $this->persMainRepository->find($id);

		if(empty($persMain))
		{
			Flash::error('PersMain not found');

			return redirect(route('persMains.index'));
		}

		return view('persMains.edit')->with('persMain', $persMain);
	}

	/**
	 * Update the specified PersMain in storage.
	 *
	 * @param  int              $id
	 * @param UpdatePersMainRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdatePersMainRequest $request)
	{
		$persMain = $this->persMainRepository->find($id);

		if(empty($persMain))
		{
			Flash::error('PersMain not found');

			return redirect(route('persMains.index'));
		}

		$persMain = $this->persMainRepository->updateRich($request->all(), $id);

		Flash::success('PersMain updated successfully.');

		return redirect(route('persMains.index'));
	}

	/**
	 * Remove the specified PersMain from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$persMain = $this->persMainRepository->find($id);

		if(empty($persMain))
		{
			Flash::error('PersMain not found');

			return redirect(route('persMains.index'));
		}

		$this->persMainRepository->delete($id);

		Flash::success('PersMain deleted successfully.');

		return redirect(route('persMains.index'));
	}
}
