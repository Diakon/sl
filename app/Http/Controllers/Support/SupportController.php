<?php namespace App\Http\Controllers\Support;

use App\Libraries\Repositories\SupportRepository;
use Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Response;
use User;
use Request;
use Hash;
use Support;
use \Illuminate\Http\Request as HttpRequest;
use App\Http\Controllers\Controller;
use  App\Traits\SendMailTrait;


class SupportController extends Controller {

    use SendMailTrait;
    public $module = 'Support';
    public $statusArr = array(1=>"Новая", 2=>"В работе", 3=>"Закрыта", 4=>"Отложена", 5=>"Просмотрена");
    public $departmentArr = array(1=>"Информационный", 2=>"Финансовый", 3=>"ОП");

    /** @var  \App\Libraries\Repositories\SupportRepository */
    private $supportRepository;

    public function __construct(SupportRepository $supportRepository) {
        $this->supportRepository = $supportRepository;
        $this->model = $supportRepository->makeModel();

    }

    public function addtiket()
	{
        $uid =  \Auth::id();

        $this->data['user_status'] = \DB::table('users')->where('userid', $uid)->first()->status;
        $this->data['users'] = \App\Models\Scheduler::getAllUsers();
        $this->data['vahta'] = \App\Models\Support::getAllVahtovik();
        $this->data['users_status_list'] = \App\Models\Support::getUserStatus();


        return view('support.addtiket',$this->data);

	}

    public function stattiket(HttpRequest $request)
    {
        $model = \DB::table($this->model->getTable() . ' as s')->select('s.id', 's.status_tiket', 's.department', 't.created_at', 'ss.created_at as date_end', 'u.username', 't.body');
        $model = $model->leftjoin('support_tiket as t', 's.support_tiket_id', '=', 't.id');
        $model = $model->leftjoin('support_solution as ss', function ($join) {
            $join->on('s.support_solution_id', '=', 'ss.id')
                ->where('s.status_tiket','=', 3);
        });
        $model = $model->leftjoin('users as u', 't.user_id', '=', 'u.userid');
        $model = $model->orderby('s.id', SORT_DESC);


        $this->data['user_status'] = Auth()->user()->status;

        $status = (array)$request->get('status', [1,2,3,4,5]);

        $where_department = '';
        //Если не админ - видит только свой отдел
        if ($this->data['user_status'] == 10){
            //Бухгалтер
            $where_department = ' AND s.department = 2 ';
        }
        if ($this->data['user_status'] == 5 || $this->data['user_status'] == 3){
            //Оператор штатный; Оператор удаленщик
            $where_department = ' AND s.department = 3 ';
        }

        if ($request->has('status')) {
            $model = $model->where('s.status_tiket', $request->get('status'));
        }
        if ($request->has('department')) {
            $model = $model->where('department', $request->get('department'));
        }

        $times = json_decode($request->get('times', json_encode(['start' => Carbon::now()->format('d-m-Y'), 'end' => Carbon::now()->format('d-m-Y')])));
        if ($times) {
            $model = $model->whereBetween('t.created_at', [$times->start . " 00:00:00", $times->end . " 23:59:59"]);
        }

        if ($request->has('author_id')) {
            $model = $model->where('t.user_id', $request->get('author_id'));
        }

        if ($request->has('worker_id')) {
            $model = $model->where('ss.user_id', $request->get('worker_id'));
        }

        return view('support.stattiket',$this->data)
            ->with('supports', $model->paginate(100000))
            ->with('request', $request);
    }

    public function listtiket(HttpRequest $request)
    {
        $model = \DB::table($this->model->getTable() . ' as s')
            ->select(
                's.id', 's.status_tiket', 's.department', 's.disabled', 's.status_tiket as status',
                'v.id as is_vahta', 't.created_at', 't.type as tiket_type', 't.source', 't.body as request', 't.created_at as created',
                'ss.created_at as date_end',
                'pm.id_anketa as id_anketa', 'pm.firstname as pmfirstname', 'pm.lastname as pmlastname', 'pm.patronymic as pmmidlename',
                'u.username', 'u.fio', 'u.email');
        $model = $model->leftjoin('support_tiket as t', 's.support_tiket_id', '=', 't.id');
        $model = $model->leftjoin('support_solution as ss', function ($join) {
            $join->on('s.support_solution_id', '=', 'ss.id')
                ->where('s.status_tiket','=', 3);
        });
        $model = $model->leftjoin('users as u', 't.user_id', '=', 'u.userid');
        $model = $model->leftjoin('users_vahtovik as v', 't.id', '=', 't.u_id');
        $model = $model->leftjoin('pers_main as pm', 'pm.id_anketa', '=', 't.u_id');
        $model = $model->leftjoin('pers_status as ps', 'ps.id_anketa', '=', 'pm.id_anketa');
        $model = $model->orderby('s.id', SORT_DESC);


        $this->data['user_status'] = Auth()->user()->status;
        if ($request->has('status')) {
            $model = $model->where('s.status_tiket', $request->get('status'));
        }
        if ($request->has('department')) {
            $model = $model->where('department', $request->get('department'));
        }
        if ($request->has('times')) {
            $model = $model->whereBetween('t.created_at', [$request->get('times') . " 00:00:00", $request->get('times') . " 23:59:59"]);
        }
        if ($request->has('author') && $request->get('author')>0) {
            $model = $model->where('u.userid', $request->get('author'));
        }

        if ($this->data['user_status'] == 10){
            //Бухгалтер
            $model = $model->where('department', 2);
        }
        if ($this->data['user_status'] == 5 || $this->data['user_status'] == 3){
            //Оператор штатный; Оператор удаленщик
            $model = $model->where('department', 3);
        }
        if(!$request->exists('status')) {
            $model = $model->wherein('s.status_tiket', [1,5])->orwhere(function($query) {
                $query->where('s.status_tiket', 2);
                $query->wherein('s.id', function($query){
                    $query->select('support_id')
                        ->from('support_solution')
                        ->where('user_id', Auth::id());
                });
            });
        }

        $this->data['result'] = $model->paginate(30);

        //объекты
        $this->data['objects'] = \DB::table('objects')->orderBy('name')->lists('name','id');
        $this->data['statusArr'] = $this->statusArr;
        $this->data['departmentArr'] = $this->departmentArr;
        $this->data['filterAuthor'] = $this->getAuthor();

        return view('support.listtiket',$this->data)->with('request', $request);

    }

    //Проверяет кончилась ли ссесия
    public function  chkauth(){
        $uid =  \Auth::id();
        if ( empty($uid) ) {
            //ссесия истекла
            //пишем в куки текущую страницу для редиректа после авторизации
            \Cookie::queue('return_url', \Input::get( 'url' ), 120);
            return \Response::json( 'ssesion_err' );
            exit;
        }
        else { return \Response::json( 'ssesion_ok' ); exit; }
    }

    function ajax(HttpRequest $request){
        //Заявка с формы
        if ($request->has('typeorder')) {
            $typeorder = $request->get( 'typeorder', 1 );
            $data = [
                'user_id' => Auth::id(),
                'fromuser' => (Auth::user()->hasRole('admin') ? $request->get( 'fromuser' ) : null),
                'department' => $request->get( 'department', 1),
                'type' => $typeorder
            ];
            switch ($typeorder) {
                case '1':
                    // новая ит заявка
                    $rules = $this->model->getRules('tiket');
                    $data['body'] = $request->get( 'body' );
                    break;
                case '3':
                    //Заявка на создание пользователя
                    $rules = $this->model->getRules('adduser');
                    $data['body'] = 'Заявка на создание пользователя через форму';
                    $data['User'] = $request->get('User');
                    break;
                default:
                    return Response::json('error');
            }
            $this->validate($request, $rules);
            if ($data && $this->supportRepository->addTiket($data)) {
                return Response::json('ok');
            } else {
                return Response::json('error');
            }
        }

        //отрабатывае действие на форме списка заявок
        if ($request->has('id') && $request->has('type')) {
            $post_id = $request->get('id');
            $post_type = $request->get('type');
            $body = $request->get('body', '');

            if ($this->model->getAddSolution((int)$post_id, $post_type, Auth::id(), $body, true)) {
                //Если заявку закрыли - отправляю админу уведомление
                if (strcasecmp($post_type, 'complite') == 0) {
                    //получаю email автора заявки
                    $sql = "
                        SELECT u.email as email FROM support as s
                        LEFT JOIN support_tiket as st ON st.id = s.support_tiket_id
                        LEFT JOIN users as u ON u.userid = st.user_id
                        WHERE s.id = " . $post_id . "
                        LIMIT 1
                     ";
                    $result = \DB::select($sql);
                    $user_email = (!empty($result[0]['email'])) ? $result[0]['email'] : null;

                    $this->supportRepository->sendMailAdmin(body, 2, $user_email);
                }
                echo json_encode('ok');
            } else {
                echo json_encode('error');
            }

        }

        //Открыли заявку - если статус "Новая" - меняю на "Просмотрено"
        if (!empty($_POST['id']) && !empty($_POST['status'])) {
            $this->model->chkStatus($_POST['id'], $_POST['status']);
        }

        //Установть дедлайн для задания
        if (!empty($_POST['deadline_id']) && !empty($_POST['deadline_time'])) {
            $this->model->setDeadline($_POST['deadline_id'], $_POST['deadline_time']);
            return \Response::json('ok');
        }


    }

    //аякс на добавление действий с формы
    function ajaxform(){
        $uid =  \Auth::id();
        if ( empty($uid) ) {
            //ссесия истекла
            return \Response::json( 'ssesion_err' );
            exit;
        }
        $user_status = \DB::table('users')->where('userid', $uid)->first()->status;

        $id = \Input::get( 'id' );
        if (!empty($id)){
            $type_support = "complite";
            $body_support = "Задача закрыта";

            if (!empty($_POST['User'])){
                //добавляем пользователя
                if ( $this->model->getActionForm(1, $_POST['User']) ){
                    $this->model->getAddSolution($id, $type_support, ((int)$uid), $body_support, true);;
                    exit;
                }

            }

        } else {
            return \Response::json( 'error' );
        }


    }


    public function viewtiket(){
        $id = \Input::get( 'id');
        if (!$id) {
            abort(404);
        }
        $this->data['jira_auth'] = \Cookie::get('cloud_jira');

        $this->data['get_id'] = $id;
        $this->data['user_status'] = Auth()->user()->status;

        $sql = "
            SELECT s.id as `id`, s.dead_line as `dead_line`, u.username as `autor`,  u.fio as `autor_fio`, u.email as `autor_email`, st.u_id as `u_id`, u2.username as `from`, u2.fio as `from_fio`, u2.email as `from_email`,
            st.body as `requst`, st.data_user as `data_user`, st.created_at as `tiket_data_start`,
            u3.username as `answer`, u3.fio as `answer_fio`, u3.email as `answer_email`, s.support_solution_id as `anwer_id`, ss.solution as `solution`, s.status_tiket as `status`, s.department as `departament`, s.created_at as `soldata`
            FROM support as s
            LEFT JOIN support_tiket as st ON st.id = s.support_tiket_id
            LEFT JOIN support_solution as ss ON ss.id = s.support_solution_id
            LEFT JOIN users as u ON u.userid = st.user_id
            LEFT JOIN users as u2 ON u2.userid = st.u_id
            LEFT JOIN users as u3 ON u3.userid = ss.user_id
            WHERE s.id = ".$id."
            LIMIT 1
        ";
        foreach (\DB::select($sql) as $data){
            $this->data['id'] = $data->id;
            $this->data['autor'] = $data->autor;
            $this->data['autor_fio'] = $data->autor_fio;
            $this->data['autor_email'] = $data->autor_email;
            $this->data['u_id'] = $data->u_id;
            $this->data['from'] = $data->from;
            $this->data['from_fio'] = $data->from_fio;
            $this->data['from_email'] = $data->from_email;
            $this->data['requst'] = $data->requst;
            $this->data['data_user'] = $data->data_user;
            $this->data['tiket_data_start'] = $data->tiket_data_start;
            $this->data['answer'] = $data->answer;
            $this->data['answer_fio'] = $data->answer_fio;
            $this->data['answer_email'] = $data->answer_email;
            $this->data['anwer_id'] = $data->anwer_id;
            $this->data['solution'] = $data->solution;
            $this->data['status'] = $data->status;
            $this->data['departament'] = $data->departament;
            $this->data['soldata'] = $data->soldata;
            $this->data['dead_line'] = $data->dead_line;
        }


        //Проверяю - не вахтовик ли человек
        $sql = "
                            SELECT v.phone as vahta FROM support as s
                            LEFT JOIN support_tiket as t ON t.id = s.support_tiket_id
                            LEFT JOIN users_vahtovik as v ON v.id = t.u_id
                            WHERE s.id = ".($id)." AND t.source = 2
                            LIMIT 1
                            ";
        $this->data['vahta'] = null;
        foreach (\DB::select($sql) as $data){
            $this->data['vahta'] = $data->vahta;
        }


        $this->data['department'] = $this->departmentArr[(int)$this->data['departament']];
        $this->data['statusStr'] = $this->statusArr[(int)$this->data['status']];


        $sql = "
            SELECT u.username as username, u.fio as fio, u.email as email, ss.solution as solution, ss.created_at as created_at, ss.status_tiket as status  FROM support_solution as ss
            LEFT JOIN users as u ON u.userid = ss.user_id
            WHERE ss.support_id = ".((int)$id)."
          ";
        $historyArr = array();
        $i = 0;
        foreach (\DB::select($sql) as $data){
            $historyArr[$i]['date'] = date('d.m.y h:m:s', strtotime($data->created_at));
            $historyArr[$i]['fio'] = $data->fio;
            $historyArr[$i]['username'] = $data->username;
            $historyArr[$i]['status'] = $data->status;
            $historyArr[$i]['status'] = $this->statusArr[(int)$data->status];
            $historyArr[$i]['solution'] = $data->solution;
            ++$i;
        }
        $this->data['history'] = $historyArr;


        //Получаю есть ли в JIRO эта заявка
        $this->data['jiro'] = \DB::table('support_jira')
            ->leftJoin('users', 'users.userid', '=', 'support_jira.user_id')
            ->select('users.username as jiro_uname', 'support_jira.url_jiro as jiro_url', 'support_jira.created_at as jiro_created')
            ->where('support_jira.support_id', '=', $id)
            ->first();





        return view('support.viewtiket',$this->data);
    }



    public function formcomplite(){
        $id = \Input::get( 'id' );
        $this->data['get_id'] = $id;
        $uid =  \Auth::id();
        if (empty($uid)){
            return \Redirect::to('auth/login'); exit;
        }
        $this->data['user_status'] = \DB::table('users')->where('userid', $uid)->first()->status;
        if ( $this->data['user_status'] != 15 ) {
            return view('support.noaccess');
            exit;
        }

        $sql = "
            SELECT supt.`type` as support_type, supt.type_table_id as support_type_tbl, usr.username as authorname, usr.fio as authorfio, usr.phone as authorphone, sup.status_tiket as status_tiket FROM support as sup
            LEFT JOIN support_tiket as supt on supt.id = sup.support_tiket_id
            LEFT JOIN users as usr on usr.userid = supt.user_id
            WHERE sup.id = ".(int)$id."
            LIMIT 1
            ";
        foreach (\DB::select($sql) as $data){
            $this->data['support_type'] = $data->support_type;
            $this->data['support_type_tbl'] = $data->support_type_tbl;
            $this->data['authorname'] = $data->authorname;
            $this->data['authorfio'] = $data->authorfio;
            $this->data['authorphone'] = $data->authorphone;
            $this->data['status_tiket'] = $data->status_tiket;
        }

        foreach (\DB::select("SELECT fio, username, email, phone, lvl FROM support_type".(int)$this->data['support_type']) as $data){
            $this->data['tbl_fio'] = $data->fio;
            $this->data['tbl_username'] = $data->username;
            $this->data['tbl_email'] = $data->email;
            $this->data['tbl_phone'] = $data->phone;
            $this->data['tbl_lvl'] = $data->lvl;
        }

        $users_status = array();
        foreach (\DB::select("SELECT * FROM `users_status`") as $data){
            $users_status[$data->id] = $data->name;
        }
        $this->data['users_status'] = $users_status;

        //тип активыности формы
        switch ((int)$this->data['support_type']) {
            case 3:
                $this->data['activityName'] = " добавление нового пользователя ";
                break;
        }

        $this->data['pass'] = $this->model->genPass();

        return view('support.formcomplite',$this->data);
    }

    /**
     * Возращает: 900 - не правельный токен,  500 - не известная ошибка,  700 - ошибка записи в БД, 200- все прошло успешно
     */
    public function addtiketpost(HttpRequest $request){
        $error_code = '500';

        if ($request->exists( 'phone' ) && $request->exists( 'body' )){

            $phone = $request->get( 'phone' );              //телефон пользователя
            $body = $request->get( 'body' );                //тело заявки
            $department = $request->get( 'department', 1 ); //в какой отдел   1-инфо 2-фин 3-оп

            //Ищим пользователя с таким телефоном, если нет - ставим viles
            $user = \DB::table('users')->where('phone', 'LIKE', $phone)->first();
            if (empty($user)){
                //берем вилеса
                $user = \DB::table('users')->where('username', 'LIKE', 'vilis')->first();
            }

            //Создаем заявку
            $pattern = "<p>, <br>, <strong>, <em>, <del>, <ul>, <li>, <ol>, <a>, <hr>, <img>, <h1>, <h2>, <h3>, <h4>, <h5>, <blockquote>, <pre>, <b>";
            $body = strip_tags($body, $pattern);
            if ($this->model->getAddTiket($user->userid, 1, null, $body, (int)$department)) {
                $error_code = '200';
            } else {
                $error_code = '700';
            }
        }

        echo $error_code;
        exit;
    }

    public function maintikets(){
        $uid =  \Auth::id();

        if (isset($_POST['confirm_id']) && isset($_POST['confirm_status'])){
            $id = (int)$_POST['confirm_id'];
            $status = (int)$_POST['confirm_status'];
            $this->model->setConfirm($id, $status);
            echo json_encode("ok");
            exit;
        }

        $this->data['tables'] =  \DB::table('support')
            ->leftJoin('support_tiket', 'support_tiket.id', '=', 'support.support_tiket_id')
            ->leftJoin('support_solution', 'support_solution.id', '=', 'support.support_solution_id')
            ->leftJoin('users', 'users.userid', '=', 'support_tiket.user_id')
            ->where('support_tiket.user_id', $uid)
            ->where('support.status_tiket', 3)
            ->orderBy('support_tiket.created_at', 'DESC')
            ->select('support.id as id', 'users.fio as fio', 'users.email as email', 'support_tiket.body as request', 'support_solution.solution as solution', 'support.department as department', 'support.status_tiket as status', 'support_tiket.created_at as created', 'support.created_at as complite_date', 'support.confirm_author as confirm_author')
            ->paginate(100);


        $this->data['statusArr'] = $this->statusArr;
        $this->data['departmentArr'] = $this->departmentArr;

        return view('support.maintikets',$this->data);
    }

    /**
     * Список авторов заявок
     */
    private function getAuthor(){
        $dataReturn = array();
        $dataReturn = array(0=>'Автор заявки');
        foreach ( \DB::table('users')->join('support_tiket', 'support_tiket.user_id', '=', 'users.userid')->orderBy('fio', 'asc')->get() as $data ){
            $k = $data->userid;
            $dataReturn[$k] = $data->fio.' ['.$data->username.']';
        }
        return $dataReturn;
    }


    public function addjiro(){

        if ( isset($_POST['jiro_remember']) && (int)$_POST['jiro_remember']==1 ){
            //Пишем в куки
            $cookie = \Cookie::make('cloud_jira', array('login' => $_POST['jiro_name'], 'password' => $_POST['jiro_pass']), 1000);
            $response = \Response::json(array('status' => 'ok'));
            $response->headers->setCookie($cookie);
            return $response;
            exit;
        }

        $id = $_POST['support_id'];
        $login = $_POST['jiro_name'];
        $password = $_POST['jiro_pass'];
        $task = $_POST['jiro_task'];
        if ( $this->model->addTaskInJiro($id, $login, $password, $task) ){
            echo json_encode("ok");
        }
        else { echo json_encode("error"); }

        exit;
    }




    public function getttoken($pass){
        if (isset($pass) && $pass=='qwefF234sHFGBh754sgsdg46456ersadasdG'){
            echo csrf_token();
        }
    }

    public function test(){
        return view('support.test');
    }
}


