<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateObjectsRequest;
use App\Http\Requests\UpdateObjectsRequest;
use App\Libraries\Repositories\ObjectsRepository;
use Flash;
use Response;

class ObjectsController extends Controller
{

	/** @var  ObjectsRepository */
	private $objectsRepository;

	function __construct(ObjectsRepository $objectsRepo)
	{
		$this->objectsRepository = $objectsRepo;
	}

	/**
	 * Display a listing of the Objects.
	 *
	 * @return Response
	 */
	public function index()
	{
		$objects = $this->objectsRepository->paginate(10);

		return view('objects.index')
			->with('objects', $objects);
	}

	/**
	 * Show the form for creating a new Objects.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('objects.create');
	}

	/**
	 * Store a newly created Objects in storage.
	 *
	 * @param CreateObjectsRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateObjectsRequest $request)
	{
		$input = $request->all();

		$objects = $this->objectsRepository->create($input);

		Flash::success('Objects saved successfully.');

		return redirect(route('objects.index'));
	}

	/**
	 * Display the specified Objects.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$objects = $this->objectsRepository->find($id);

		if(empty($objects))
		{
			Flash::error('Objects not found');

			return redirect(route('objects.index'));
		}

		return view('objects.show')->with('objects', $objects);
	}

	/**
	 * Show the form for editing the specified Objects.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$objects = $this->objectsRepository->find($id);

		if(empty($objects))
		{
			Flash::error('Objects not found');

			return redirect(route('objects.index'));
		}

		return view('objects.edit')->with('objects', $objects);
	}

	/**
	 * Update the specified Objects in storage.
	 *
	 * @param  int              $id
	 * @param UpdateObjectsRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateObjectsRequest $request)
	{
		$objects = $this->objectsRepository->find($id);

		if(empty($objects))
		{
			Flash::error('Objects not found');

			return redirect(route('objects.index'));
		}

		$objects = $this->objectsRepository->updateRich($request->all(), $id);

		Flash::success('Objects updated successfully.');

		return redirect(route('objects.index'));
	}

	/**
	 * Remove the specified Objects from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$objects = $this->objectsRepository->find($id);

		if(empty($objects))
		{
			Flash::error('Objects not found');

			return redirect(route('objects.index'));
		}

		$this->objectsRepository->delete($id);

		Flash::success('Objects deleted successfully.');

		return redirect(route('objects.index'));
	}
}
