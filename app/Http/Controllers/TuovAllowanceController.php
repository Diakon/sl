<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CreateTuovAllowanceRequest;
use App\Http\Requests\UpdateTuovAllowanceRequest;
use App\Libraries\Repositories\TuovAllowanceRepository;
use Flash;
use App\Models\Deal;
use App\Models\Subdivision;
use App\Models;

class TuovAllowanceController extends Controller
{

	/** @var  TuovAllowanceRepository */
	private $tuovAllowanceRepository;
    private $model;

	function __construct(TuovAllowanceRepository $tuovAllowanceRepo)
	{
		$this->tuovAllowanceRepository = $tuovAllowanceRepo;
        $this->activeDeals = Deal::getActiveDeals(1);
        $this->activeSubdivisions = Subdivision::getActiveSubdivisions(1);
        $this->activeSubdivisionsWithNull = Subdivision::getActiveSubdivisions(1);
	}

	/**
	 * Display a listing of the TuovAllowance.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		return view('tuovAllowances.index')
			->with('tuovAllowances', $this->tuovAllowanceRepository->search($request->all()))
			->withRequest($request);
	}

	/**
	 * Show the form for creating a new TuovAllowance.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('tuovAllowances.create',array('activeSubdivisions'=>$this->activeSubdivisions,'activeDeals'=>$this->activeDeals));
	}

	/**
	 * Store a newly created TuovAllowance in storage.
	 *
	 * @param CreateTuovAllowanceRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateTuovAllowanceRequest $request)
	{
		$input = $request->all();
        if (!empty($input['from'])){$input['from'] = date('Y-m-d', strtotime($input['from']));}
        if (!empty($input['to'])){$input['to'] = date('Y-m-d', strtotime($input['to']));}
		$tuovAllowance = $this->tuovAllowanceRepository->create($input);

		Flash::success('Запись создана.');

		return redirect(route('tuovAllowances.index'));
	}

	/**
	 * Display the specified TuovAllowance.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$tuovAllowance = $this->tuovAllowanceRepository->find($id);

		if(empty($tuovAllowance))
		{
			Flash::error('TuovAllowance not found');

			return redirect(route('tuovAllowances.index'));
		}

		return view('tuovAllowances.show')->with('tuovAllowance', $tuovAllowance);
	}

	/**
	 * Show the form for editing the specified TuovAllowance.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
    public function find(Request $request){
		$name = $request->input('q', '');

		$sql_builder = \DB::table('pers_main as t1')
			->select('t1.id_anketa', 't1.firstname','t1.lastname','t1.patronymic', 't4.id', 't4.name as object', 't3.name as user_status', 't3.id as status_id')
			->join('pers_status as t2', 't1.id_anketa', '=', 't2.id_anketa')
			->join('worker_status as t3', 't3.id', '=', 't2.status_name')
			->join('objects as t4', 't4.id', '=', 't2.object')
			->orWhere(function($query) use ($name) {
				$query->orwhere('t1.id_anketa', 'like', $name . '%')
					->orwhereRaw("concat_ws(' ', t1.lastname, t1.firstname, t1.patronymic) like concat(?, '%')", [$name] )
					->orwhere('t1.id_anketa', $name);
			})
			->orderBy('t1.lastname');

		if ($request->has('id')) {
			$sql_builder->where('t1.id_anketa',$request->input('id', 0));
		}
		$arr = [0=>[],1=>[],2=>[]];
		foreach ($sql_builder->limit(50)->get() as $data ){
			$sort_order_status = ($data->status_id==9?0:($data->status_id==11?1:2));
            $arr[$sort_order_status][] = [
				'id' => $data->id_anketa,
				'value' => $data->lastname.' '.$data->firstname.' '.$data->patronymic.', '.$data->id_anketa.', ' . $data->object . ', ' . $data->user_status
			];
        }
		return response()->json(array_merge($arr[0],$arr[1],$arr[2]));
    }

	public function edit($id)
	{
        $tuovAllowance = $this->tuovAllowanceRepository->find($id);

		if(empty($tuovAllowance))
		{
			Flash::error('Запись не найдена');

			return redirect(route('tuovAllowances.index'));
		}
        return view('tuovAllowances.edit',array('tuovAllowance'=> $tuovAllowance, 'activeSubdivisions'=>$this->activeSubdivisions,'activeDeals'=>$this->activeDeals));
	}

	/**
	 * Update the specified TuovAllowance in storage.
	 *
	 * @param  int              $id
	 * @param UpdateTuovAllowanceRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateTuovAllowanceRequest $request)
	{
		$tuovAllowance = $this->tuovAllowanceRepository->find($id);

		if(empty($tuovAllowance))
		{
			Flash::error('Запись не найдена');

			return redirect(route('tuovAllowances.index'));
		}

        $input = $request->all();
        if (!empty($input['from'])){$input['from'] = date('Y-m-d', strtotime($input['from']));}
        if (!empty($input['to'])){$input['to'] = date('Y-m-d', strtotime($input['to']));}

		$tuovAllowance = $this->tuovAllowanceRepository->updateRich($input, $id);

		Flash::success('Запись успешно обнавлена.');

		return redirect(route('tuovAllowances.index'));
	}

	/**
	 * Remove the specified TuovAllowance from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$tuovAllowance = $this->tuovAllowanceRepository->find($id);

		if(empty($tuovAllowance))
		{
			Flash::error('Запись не найдена');

			return redirect(route('tuovAllowances.index'));
		}

		$this->tuovAllowanceRepository->delete($id);

		Flash::success('Запись удалена.');

		return redirect(route('tuovAllowances.index'));
	}
}
