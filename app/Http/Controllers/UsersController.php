<?php namespace App\Http\Controllers;

use App\Libraries\FmsExcel\Fms;
use App\Models\PersMain;
use Auth;
use App\User;
use DB;
use Request;
use Hash;


class UsersController extends Controller {

	
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */

	public function fms($id) {
		$model = PersMain::BelarusAccepted()->where('id_anketa',$id)->first();
		if (!$model) {
			abort(404);
		}
		if (!$model->dormitory) {
			$model->saveDormitory();
		}
		return (new Fms)->model($model)->create();
	}

	public function getAddForm()
	{
		$lists = User::getLists();
		return view('register',['lists'=>$lists]);
	}
	
	public function postAddForm()
	{
		$request = Request::all();
		$user = new User();
		$validator = $user->Validate($request);
		$user->fill($request);
		if ($validator->fails()) {
			$errors = $validator->errors()->all();	
			$lists = User::getLists();		
			return view('register',['lists'=>$lists,'user'=>$user,'errors'=>$errors]);
		}
		$user->password = Hash::make($user->password);
		$user->save();
		return redirect()->intended('/users');
	}
	 
	 
	public function getUpdateForm($id)
	{
		$user = User::find($id);
		$lists = User::getLists();
		return view('register',['lists'=>$lists,'user'=>$user]);
	
	} 
	
	public function postUpdateForm()
	{
		$request = Request::all();
		$user = User::find($request['id']);	
		$validator = $user->Validate($request);
		$user->fill($request);
		if ($validator->fails()) {
			$errors = $validator->errors()->all();	
			$lists = User::getLists();		
			return view('register',['lists'=>$lists,'user'=>$user,'errors'=>$errors]);
		}
		if (!$request['password'])
			unset($user->password);
		else
			$user->password = Hash::make($user->password);
			
		$user->save();
		return redirect()->intended('/users');
	}
	
	
	 
	public function getDelete($id)
	{
		$user = User::find($id);
		$user->delete();
		return redirect()->intended('/users');
	}
	 
	public function getIndex()
	{
		$users = User::all();
		$lists = User::getLists();
		return view('users',['lists'=>$lists,'users'=>$users]);
		
		//return print_r($lists);
		//return view('users');
		//return 'sdfsd';
	}

}
