<?php namespace  App\Http\Controllers;

use Auth;
use User;
use Request;
use Hash;
use App\Http\Controllers\Controller;
use App\Models;
use Illuminate\Support\Facades\App;


class ReportNoAnswerController extends Controller {


    public function index(){
        set_time_limit(300);
        $weekdayname = ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"];
        $this->data['dates_array'] = array();
        for($i=0;$i<=7;++$i){
            $k = date("Y-m-d",strtotime('-'.$i.' day'));
            $this->data['dates_array'][$k] = $weekdayname[(date("w",strtotime('-'.$i.' day')))];
        }

        $this->data['dates_no_answer_val'] = array();
        foreach ($this->data['dates_array'] as $date => $weekday){
            $customer_phones = $this->getNoAnswer($date);
            if ( !empty($customer_phones) ){
                foreach ($customer_phones as $phone){
                    $this->data['dates_no_answer_val'][$date][$phone] = $this->find_customer($phone);
                }
            } else {
                $this->data['dates_no_answer_val'][$date] = null;
            }
        }



        return view('reportnoanswer.index', $this->data);
    }


    function getNoAnswer($date){
        $tmp_array = ['ANSWERED'=>[], 'NO ANSWER' => []];

        $db_ext = \DB::connection('asterisk');
        $request = $db_ext->select('SELECT DISTINCT src, dst, disposition FROM cdr WHERE disposition in("NO ANSWER", "ANSWERED") AND calldate between "' . $date . '" and "' . $date . ' 23:59:59"');
        foreach ($request as $data){
            $tmp_array[$data->disposition][] = $this->format_number($data->src);
            if ($data->disposition != 'ANSWERED') {
                $tmp_array[$data->disposition][] = substr($data->dst, 1);
            }
        }
        return array_unique(array_diff($tmp_array['NO ANSWER'], $tmp_array['ANSWERED']));
        /*
        $results = array(

            '13' => '89197427453',
            '14' => '89164673991',
            '15' => '89857040052',
        );
        */
    }

    function format_number($phone_number)
    {
        $phone = preg_replace('/\D/', '', $phone_number);  // оставляем только цифры
        if ((!preg_match("/^380\d{9}$/", $phone)) AND (!preg_match("/^375\d{9}$/", $phone))) {
            if (!preg_match("/^8\d{10}$/", $phone)) {
                $phone = preg_replace('/.*(\d{10})$/', "8$1", $phone); // приводим в единый формат 8XXXXXXXXXX
            }
        }
        return $phone;
    }


    function find_customer($phone)
    {
        $query = \DB::table('pers_address')
            ->where('pers_address.phone_mobile', 'LIKE', '%' . $phone)
            ->orWhere('pers_address.phone_home', 'LIKE', '%' . $phone)
            ->orderBy('pers_address.id_anketa', 'DESC')
            ->first();

        if ($query) {
            return Models\PersMain::find($query->id_anketa);
        }

        $query = \DB::table('soiskateli')->where('aon', 'LIKE', '%' . $phone)->orderBy('id', 'DESC')->first();
        return $query;
    }

    /** Обрабатывает ajax запросы */
    function ajax(){

    }


}


