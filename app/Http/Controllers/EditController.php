<?php namespace  App\Http\Controllers;

use Auth;
use User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Hash;
use App\Http\Controllers\Controller;
use App\Models;
use Illuminate\Support\Facades\App;


use  App\Traits\SendMailTrait;
use  App\Traits\SendSMSTrait;

class EditController extends Controller {

    use SendMailTrait;
    use SendSMSTrait;

    public function create(Request $request){

        $uid =  \Auth::id();

        $this->data['soiskatel'] = $this->getSoiskatel(0);
        if(!empty($request->all())){
            $this->data['soiskatel'] = (object) array_merge((array) $this->data['soiskatel'], (array) $request->all());
        }

        //создаем пустые значения
        $result = array();
        foreach ($this->getSoiskatel(0) as $key=>$data){
            $result[$key]='';
        }


        $this->data['dublicate'] = ((int)$this->data['soiskatel']->aon>0) ? $this->gatCopyByPhone($this->data['soiskatel']->aon) : null;
        $this->data['metro_list'] = $this->getMetro();
        $this->data['country_list'] = $this->getCountry();
        $this->data['sources_list'] = $this->getSources();
        $this->data['factorwork_list'] = $this->getFactorWork();
        $this->data['status_list'] = $this->getStatus();

        return view('edit.index',$this->data);
    }

    public function update($id){
        $uid =  \Auth::id();

        $this->data['soiskatel'] = $this->getSoiskatel($id);
        $this->data['dublicate'] = ((int)$this->data['soiskatel']->aon>0) ? $this->gatCopyByPhone($this->data['soiskatel']->aon) : null;
        $this->data['soiskatel']->aon = $this->format_number($this->data['soiskatel']->aon);
        $this->data['metro_list'] = $this->getMetro();
        $this->data['country_list'] = $this->getCountry();
        $this->data['sources_list'] = $this->getSources();
        $this->data['factorwork_list'] = $this->getFactorWork();
        $this->data['status_list'] = $this->getStatus();
        $this->data['history'] = $this->getHistory($id);

        return view('edit.index',$this->data);
    }


    //получает данные с формы
    public function formupdate(){

        if (!$uid = \Auth::id()){exit;}
        $uid = \DB::table('users')->where('userid', $uid)->first();

        $clickcount = $_POST['clickcount'];
        $keypresscount = $_POST['keypresscount'];
        $check_customer_status = $_POST['check_customer_status'];
        $day = $_POST['day'];

        if (empty($_POST)){exit;}



        $pid = (int)$_POST['pid'];
        $soiskateli = \DB::table('soiskateli')->where('id', $pid)->first();

        //Если статус изменился - меняем user_creator
        if((!empty($_POST['pid'])) && $_POST["customer_status"] == $soiskateli->customer_status){  $user_creator = $soiskateli->customer_status; }else{ $user_creator = $uid; }
        if (empty($_POST["customer_status"])){ $_POST["customer_status"] = 0; }

        unset($_POST['_token']);
        unset($_POST['pid']);
        unset($_POST['clickcount']);
        unset($_POST['keypresscount']);
        unset($_POST['day']);
        unset($_POST['check_customer_status']);

        if ($pid>0){
            //Редактироване - обновляем запись
            \DB::table('soiskateli')
                ->where('id', $pid)
                ->update($_POST);
        } else {
            //Новая запись
            $pid = \DB::table('soiskateli')->insertGetId($_POST);
        }


        \DB::table('metrics')->insert(
            array('userid' => $uid->userid, 'username' => $uid->username, 'clickcount'=>$clickcount, 'keypress'=>$keypresscount)
        );

        //Отправляем СМС
        if ( $_POST["customer_status"] == 11 ){
            $this->sendSMS($_POST["aon"], "Подумайте и сообщите нам о своем решении по телефону 84956467166, " . $uid->fio . " VLS");
            $mesage = "Вы брали время подумать о работе вахтой в VLS. Вы приняли решение? Позвоните нам 84956467166, " . $uid->fio . ".";
            $when_to_send = date('Y-m-d', strtotime(date('Y-m-d')) + 60 * 60 * 24 * 5);
        } elseif ($_POST["customer_status"] == 9) {
            $this->sendSMS($_POST["aon"], "Мы не смогли до Вас дозвониться! Если Вы еще ищете работу позвоните по телефону: 84956467166, " . $uid->fio . " VLS");
            $mesage = null;
            $when_to_send = date('Y-m-d', strtotime(date('Y-m-d')) + 60 * 60 * 24 * 5);
        } elseif ($_POST["customer_status"] == 12) {
            $this->sendSMS($_POST["aon"], "Спасибо! Мы ожидаем " . $day . " Вас на собеседование по адресу: Москва, м.Октябрьская(кольцевая), Ленинский проспект, д.6 стр.7 Наш телефон: 84956467166, " . $uid->fio . " VLS");
            $mesage = "Напоминаем что завтра ожидаем Вас на собеседовании, по адресу: Москва, м.Октябрьская(кольцевая), Ленинский проспект, д.6 стр.7 Наш телефон: 84956467166, " . $uid->fio . ". Если у Вас что-то изменилось, пожалуйста позвоните нам.";
            $when_to_send = date('Y-m-d', strtotime($_POST["day"]) - 60 * 60 * 24 * 1);
        }

        if (preg_match("/^8\d{10}$/", $_POST["aon"]) && !empty($mesage)){
            $sms_queue = \DB::table('vls_ko_sms_queue')->where('phone', $_POST["aon"])->where('when_to_send', $when_to_send)->get();
            if (count($sms_queue)==0) {
                \DB::table('vls_ko_sms_queue')->insert(
                    array('when_created' => date('Y-m-d H-i-s'), 'when_to_send' => $when_to_send, 'phone'=>$_POST["aon"], 'message'=>$mesage, 'id_ankety'=>$pid, 'sended'=>0)
                );
            }
        }

        $what_status = \DB::table('statustable')->where('status_id', $_POST["customer_status"])->pluck('status_name');
        $anketa_message = 'Анкета обновлена пользователем ' . $uid->username . ', установлен статус ' . $what_status . '.';
        if (!\DB::table('anketa_history')->insert(
            array('id_ankety' => $pid, 'user' => $uid->username, 'message' => $anketa_message, 'status_set' => $_POST["customer_status"], 'when' => date('Y-m-d H-i-s') )
        )){
            die('<br><font color=red>Не удалось сделать запись в историю анкеты</font><br>');
        }

        \Flash::message('Запись успешно сохранена!');
        return redirect('/edit/update/'.$pid);
    }


    /** Отправка СМС на телефон */
    function ajaxsms(){
        $phone = \Input::get( 'aon' );
        $firstname = \Input::get( 'firstname' );
        $body = 'ВЛС. Метро Октябрьская(кольцевая), Ленинский пр-т д.6 стр.7. Выйти из метро, направо, идти до Горного универа, далее, следуя по белым стрелкам на асфальте дойти до нашего офиса.';

        $this->sendSMS($phone, $body);

        return \Response::json( 'ok' );
    }

    /** Получение списка вызовов */
    function getphones(){
        $number = \Input::get( 'number' );
        $number = substr($number, 1); //Номер без первого символа

        \DB::connection('asterisk')->table('cdr')-> first();
        $db_ext = \DB::connection('asterisk');
        //$result = $db_ext->table('cdr')-> first();
        $result = $db_ext->select('SELECT * FROM `cdr` WHERE `src` LIKE "%' . $number . '" OR `dst` LIKE "%' . $number . '" ORDER BY `calldate` DESC LIMIT 50;');
        return response()->json($result);
    }

    /** Обрабатывает ajax запросы */
    function ajax(){
        $result = '';
        if ($_GET){
            if (isset($_GET['name']) && !empty($_GET['name'])){
                $name = $_REQUEST['name'];
                $request = \DB::table('city')->where('name', 'LIKE', '%'.$name.'%')->skip(0)->take(6)->get();
                if (!empty($request)){
                    $result = '<div id="hint_block">';
                    foreach ($request as $data){
                        $result .= '<div>'.$data->name.'</div>';
                    }
                    $result .= '</div>';
                }

            }
        }
        //ФИО
        if ($_POST && isset($_POST['fio'])){
            $request = array();
            $name = $_REQUEST['name'];
            $id = '';
            switch ($_POST['fio']) {
                case 'lastnameField':
                    $request = \DB::table('dir_lastnames')->where('lastname', 'LIKE', '%'.$name.'%')->take(6)->get();
                    break;
                case 'nameField':
                    $request = \DB::table('dir_firstnames')->where('firstname', 'LIKE', '%'.$name.'%')->take(6)->get();
                    break;
                case 'otchField':
                    $request = \DB::table('dir_middlenames')->where('middlename', 'LIKE', '%'.$name.'%')->take(6)->get();
                    break;
            }

            if (!empty($request)){
                $id = 'hint_'.$_POST['fio'];
                $result = '<div id="'.$id.'">';
                foreach ($request as $data){
                    $result .= '<div>';
                    if (isset($data->lastname)){ $result .= $data->lastname; }
                    elseif (isset($data->middlename)){ $result .= $data->middlename; }
                    else { $result .= $data->firstname; }
                    $result .= '</div>';
                }
                $result .= '</div>';
            }
        }

        //Проверка что запись не заблокирована
        if ($_POST && isset($_POST['url_lock'])){
            $result = json_encode( \App\Models\LockDataUser::chkLockRowUser($_POST['url_lock']) );  //Если false - значит с записью никто не работает, иначе - возращает объект с данными
        }


        echo $result;
    }











    /** Получить данные о соискателе по его ID */
    private function getSoiskatel($id){

        if ($id == 0){
            //Возращаем пустой объект - только заголовки
            $requsts = \DB::table('soiskateli')
                ->leftJoin('soiskateli_utm', 'soiskateli.utm_id', '=', 'soiskateli_utm.id')
                ->select('soiskateli.*','soiskateli_utm.key','soiskateli_utm.source')
                ->first();
            $result = array();
            foreach ($requsts as $key=>$data){
                $result[$key]='';
            }
            $result = (object)$result;
        } else {
            $result = \DB::table('soiskateli')
                ->leftJoin('soiskateli_utm', 'soiskateli.utm_id', '=', 'soiskateli_utm.id')
                ->select('soiskateli.*','soiskateli_utm.key','soiskateli_utm.source')
                ->where('soiskateli.id', '=', (int)$id)
                ->first();
        }
        return $result;
    }

    /** Поиск дубликатов по номеру телефона */
    private function gatCopyByPhone($phone){
        $result = \DB::table('soiskateli')
            ->select('id','customer_status','comments', 'created_when as whendate')
            ->where('aon', '=', $phone)
            ->get();
        return $result;
    }

    /** Приводит телефонный номер в виду 8XXXXXXXXXX */
    private function format_number($phone_number)
    {
        $phone = preg_replace('/\D/', '', $phone_number); // оставляем только цифры
        if ((!preg_match("/^380\d{9}$/", $phone)) AND (!preg_match("/^375\d{9}$/", $phone))) {
            if (!preg_match("/^8\d{10}$/", $phone)) {
                $phone = preg_replace('/.*(\d{10})$/', "8$1", $phone);
            }
        }
        return $phone;
    }

    /** Возращает список станций метро */
    private function getMetro(){
        $requsts = \DB::table('dir_mos_metro')->get();
        $result = array();
        foreach ($requsts as $data){
            $k = $data->id;
            $result[$k] = $data->station_name;
        }
        return $result;
    }

    /** Список стран в БД */
    private function getCountry(){
        $requsts = \DB::table('grazdanstvo')->orderBy('grazd_id', 'ASC')->get();
        $result = array();
        foreach ($requsts as $data){
            $k = $data->grazd_id;
            $result[$k] = $data->grazd_name;
        }
        return $result;
    }

    /** Список источников поиска (откуда пришли на сайт) */
    private function getSources(){
        $requsts = \DB::table('source_table')->orderBy('source_id', 'ASC')->get();
        $result = array();
        foreach ($requsts as $data){
            $k = $data->source_id;
            $result[$k] = $data->source_name;
        }
        return $result;
    }

    /** Фактор выбора работы */
    private function getFactorWork(){
        $requsts = \DB::table('factor_table')->orderBy('factor_id', 'ASC')->get();
        $result = array();
        foreach ($requsts as $data){
            $k = $data->factor_id;
            $result[$k] = $data->factor_name;
        }
        return $result;
    }

    /** Возращает статусы */
    private function getStatus(){
        $requsts = \DB::table('statustable')->orderBy('status_id', 'ASC')->get();
        $result = array();
        foreach ($requsts as $data){
            $k = $data->status_id;
            $result[$k] = $data->status_name;
        }
        return $result;
    }

    /** Возращает историю работы с заявкой */
    private function getHistory($id){
        $result = \DB::table('anketa_history')->where('id_ankety', '=', (int)$id)->get();
        return $result;
    }



}


