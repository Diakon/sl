<?php namespace App\Http\Controllers\Auth;

use Auth;
use Hash;
//use Illuminate\Routing\Controller;
use \Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\LoginLog;
use App\User;

class AuthController extends Controller {


	public function getLogin(Request $request)
	{
		if (Auth::check())
			return redirect()->intended('/');
		else
			return view('auth.login',['message'=>''])->withRequest($request);
	}
	
	
	public function postLogin(Request $request)
	{
		$username = $request->input('username');
		$password = $request->input('password');
		if ($username && $password) {
			if (Auth::attempt(['username' => $username, 'password' => $password, 'enable' => 1], $request->input('remember')))
			{
                //пишем в login_log
                LoginLog::addToLog(\Auth::id(), 0);

                //Обновляем User даты last_date
                User::where('userid', \Auth::id())->update(array('login_date' => date('Y-m-d H:i:s')));
                
				return redirect()->intended('/');
			}		
		}
		return view('auth.login',['message'=>'Ошибка авторизации!'])->withRequest($request);
	}
	
	
	public function getLogout() 
	{

        //пишем в login_log
        LoginLog::addToLog(\Auth::id(), 1);

		Auth::logout();
		return redirect()->intended('/');
	}

}


