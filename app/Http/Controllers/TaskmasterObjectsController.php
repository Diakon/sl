<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateTaskmasterObjectsRequest;
use App\Http\Requests\UpdateTaskmasterObjectsRequest;
use App\Libraries\Repositories\TaskmasterObjectsRepository;
use Flash;
use Illuminate\Support\Facades\DB;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;
use App\Models\Objects;
use App\User;

class TaskmasterObjectsController extends AppBaseController
{
    public $activeObjects;
    public $activeUsers;

	/** @var  TaskmasterObjectsRepository */
	private $taskmasterObjectsRepository;

	function __construct(TaskmasterObjectsRepository $taskmasterObjectsRepo)
	{
		$this->taskmasterObjectsRepository = $taskmasterObjectsRepo;
        $this->activeObjects = Objects::getActiveObjects();
        $this->activeUsers = User::getActiveUsersList();

	}

	/**
	 * Display a listing of the TaskmasterObjects.
	 *
	 * @return Response
	 */
	public function index()
	{
		$taskmasterObjects = $this->taskmasterObjectsRepository->paginate(10);

        return view('taskmasterObjects.index',array('taskmasterObjects'=> $taskmasterObjects,'activeUsers'=>$this->activeUsers));

	}

	/**
	 * Show the form for creating a new TaskmasterObjects.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('taskmasterObjects.create',array('activeObjects'=>$this->activeObjects,'activeUsers'=>$this->activeUsers));
	}

	/**
	 * Store a newly created TaskmasterObjects in storage.
	 *
	 * @param CreateTaskmasterObjectsRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateTaskmasterObjectsRequest $request)
	{
		$input = $request->all();

		$taskmasterObjects = $this->taskmasterObjectsRepository->create($input);

		Flash::success('TaskmasterObjects saved successfully.');

		return redirect(route('taskmasterObjects.index'));
	}

	/**
	 * Display the specified TaskmasterObjects.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$taskmasterObjects = $this->taskmasterObjectsRepository->find($id);

		if(empty($taskmasterObjects))
		{
			Flash::error('TaskmasterObjects not found');

			return redirect(route('taskmasterObjects.index'));
		}

		return view('taskmasterObjects.show',array('taskmasterObjects'=> $taskmasterObjects,'activeObjects'=>$this->activeObjects,'activeUsers'=>$this->activeUsers));;
	}

	/**
	 * Show the form for editing the specified TaskmasterObjects.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$taskmasterObjects = $this->taskmasterObjectsRepository->find($id);

		if(empty($taskmasterObjects))
		{
			Flash::error('TaskmasterObjects not found');

			return redirect(route('taskmasterObjects.index'));
		}

		return view('taskmasterObjects.edit',array('taskmasterObjects'=> $taskmasterObjects,'activeObjects'=>$this->activeObjects,'activeUsers'=>$this->activeUsers));;
	}

	/**
	 * Update the specified TaskmasterObjects in storage.
	 *
	 * @param  int              $id
	 * @param UpdateTaskmasterObjectsRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateTaskmasterObjectsRequest $request)
	{
		$taskmasterObjects = $this->taskmasterObjectsRepository->find($id);

		if(empty($taskmasterObjects))
		{
			Flash::error('TaskmasterObjects not found');

			return redirect(route('taskmasterObjects.index'));
		}

		$taskmasterObjects = $this->taskmasterObjectsRepository->updateRich($request->all(), $id);

		Flash::success('TaskmasterObjects updated successfully.');

		return redirect(route('taskmasterObjects.index'));
	}

	/**
	 * Remove the specified TaskmasterObjects from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$taskmasterObjects = $this->taskmasterObjectsRepository->find($id);

		if(empty($taskmasterObjects))
		{
			Flash::error('TaskmasterObjects not found');

			return redirect(route('taskmasterObjects.index'));
		}

		$this->taskmasterObjectsRepository->delete($id);

		Flash::success('TaskmasterObjects deleted successfully.');

		return redirect(route('taskmasterObjects.index'));
	}

}
