<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSubdivisionProbationsRequest;
use App\Http\Requests\UpdateSubdivisionProbationsRequest;
use App\Libraries\Repositories\SubdivisionProbationsRepository;
use Flash;
use Mitul\Controller\AppBaseController as AppBaseController;
use Response;

class SubdivisionProbationsController extends AppBaseController
{

	/** @var  SubdivisionProbationsRepository */
	private $subdivisionProbationsRepository;

	function __construct(SubdivisionProbationsRepository $subdivisionProbationsRepo)
	{
		$this->subdivisionProbationsRepository = $subdivisionProbationsRepo;
	}

	/**
	 * Display a listing of the SubdivisionProbations.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->data['subdivisionProbations'] = $this->subdivisionProbationsRepository->paginate(20);
		$this->data['subdivisions'] = $this->getSubdivisions();

		return view('subdivisionProbations.index',$this->data);

	}

	/**
	 * Show the form for creating a new SubdivisionProbations.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->data['subdivisions'] = $this->getSubdivisions();
		return view('subdivisionProbations.create',$this->data);
	}

	/**
	 * Store a newly created SubdivisionProbations in storage.
	 *
	 * @param CreateSubdivisionProbationsRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateSubdivisionProbationsRequest $request)
	{
		$input = $request->all();

		$subdivisionProbations = $this->subdivisionProbationsRepository->create($input);

		Flash::success('Данные успешно сохранены.');

		return redirect(route('subdivisionProbations.index'));
	}

	/**
	 * Display the specified SubdivisionProbations.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$subdivisionProbations = $this->subdivisionProbationsRepository->find($id);

		if(empty($subdivisionProbations))
		{
			Flash::error('SubdivisionProbations not found');

			return redirect(route('subdivisionProbations.index'));
		}

		return view('subdivisionProbations.show')->with('subdivisionProbations', $subdivisionProbations);
	}

	/**
	 * Show the form for editing the specified SubdivisionProbations.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$this->data['subdivisionProbations'] = $this->subdivisionProbationsRepository->find($id);
		$this->data['subdivisions'] = $this->getSubdivisions();

		if(empty($this->data['subdivisionProbations']))
		{
			Flash::error('Запись не найдена');

			return redirect(route('subdivisionProbations.index'));
		}

		return view('subdivisionProbations.edit', $this->data);
	}

	/**
	 * Update the specified SubdivisionProbations in storage.
	 *
	 * @param  int              $id
	 * @param UpdateSubdivisionProbationsRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateSubdivisionProbationsRequest $request)
	{
		$subdivisionProbations = $this->subdivisionProbationsRepository->find($id);

		if(empty($subdivisionProbations))
		{
			Flash::error('Запись не найдена');

			return redirect(route('subdivisionProbations.index'));
		}

		$this->subdivisionProbationsRepository->updateRich($request->all(), $id);

		Flash::success('Запись успешно сохранена.');

		return redirect(route('subdivisionProbations.index'));
	}

	/**
	 * Remove the specified SubdivisionProbations from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$subdivisionProbations = $this->subdivisionProbationsRepository->find($id);

		if(empty($subdivisionProbations))
		{
			Flash::error('Запись не найдена');

			return redirect(route('subdivisionProbations.index'));
		}

		$this->subdivisionProbationsRepository->delete($id);

		Flash::success('Запись удалена.');

		return redirect(route('subdivisionProbations.index'));
	}

	private function getSubdivisions($id = null){
		$arr = [];
		foreach (\DB::table('subdivision')->get() as $data){
			$arr[$data->id] = $data->name;
		}
		return ((!empty($id))?($arr[$id]):($arr));
	}
}
