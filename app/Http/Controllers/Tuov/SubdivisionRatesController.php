<?php namespace App\Http\Controllers\Tuov;

use App\Http\Controllers\Controller;
use App\Libraries\Repositories\TuovSubdivisionRatesRepository;
use App\Models\Deal;
use App\Models\Subdivision;
use App\Models\SubdivisionRates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Laracasts\Flash\Flash;

class SubdivisionRatesController extends Controller
{
    private $ratesRepository;

    function __construct(TuovSubdivisionRatesRepository $ratesRepository){
        $this->ratesRepository = $ratesRepository;

        View::creator(['tuov.rates.index', 'tuov.rates.create', 'tuov.rates.edit'], function ($view) {
            $view/*->with('activeDeals', Deal::getActiveDeals())*/
                 ->with('activeSubdivisions', Subdivision::getActiveSubdivisions());
        });
    }

    public function index(Request $request) {

        $rates = $this->ratesRepository->search($request->all());

        return view('tuov.rates.index')
            ->with('rates', $rates)
            ->withRequest($request);
    }

    public function create()
    {
        return view('tuov.rates.create');
    }

    public function store(Request $request) {
        $request->merge(['user_id' => Auth::id()]);
        $input = $request->all();
        $this->validate($request, SubdivisionRates::$rules);

        $this->ratesRepository->create($input);
        Flash::success('TuovSubdivisionRates saved successfully.');
        return redirect(route('tuov.rates.index', [ 'subdivision_id' => $input['subdivision_id'] ]));
    }


    public function edit($id)
    {
        return view('tuov.rates.edit',
            ['rate'=> $this->ratesRepository->findOrFail($id)]
        );
    }

    public function update($id, Request $request)
    {
        $request->merge(['user_id' => Auth::id()]);
        $input = $request->all();
        $this->validate($request, SubdivisionRates::$rules);

        $this->ratesRepository->updateRich($input, $id);
        Flash::success('TuovSubdivisionRates updated successfully.');
        return redirect(route( 'tuov.rates.index', [ 'subdivision_id' => $request->subdivision_id] ));
    }


    /**
     * Remove the specified from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $rate = $this->ratesRepository->findOrFail($id);
        $this->ratesRepository->delete($id);

        Flash::success('TuovSubdivisionRates deleted successfully.');
        return redirect(route( 'tuov.rates.index', [ 'subdivision_id' => $rate->subdivision_id] ));
    }
}