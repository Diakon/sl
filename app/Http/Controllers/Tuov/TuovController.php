<?php namespace App\Http\Controllers\Tuov;

use App\Http\Requests\CreateTuovDeductionRequest;
use App\Http\Requests\UpdateTuovDeductionRequest;
use App\Libraries\Repositories\TuovRepository;
use App\Models\LuovDocument;
use App\Models\Objects;
use App\Models\Tuov\TuovFabric;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\LuovDocumentsHistory;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Controller;
use Laracasts\Flash\Flash;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Illuminate\Support\Facades\Redis;

class TuovController extends Controller
{
    private $tuovRepository;
    private $tuov_fabric;

    function __construct(TuovRepository $tuov, TuovFabric $tuov_fabric){
        $this->tuovRepository = $tuov;
        $this->tuov_fabric = $tuov_fabric;
    }

    public function index(Request $request) {
        return view('tuov.index')
            ->with('documents', $this->tuovRepository->search($request->all()))
            ->withRequest($request)
            ->withCharts($this->tuovRepository->makeModel()->getChartsData());
    }

    public function create() {
        return view('tuov.create')->withExcelType($this->tuovRepository->getValidExcelCategory());
    }

    public function destroy($id, LuovDocumentsHistory $history) {
        $document = $this->tuovRepository->find($id);
        if (!$document || LuovDocument::STATUS_DELETE == $document->document_status) {
            abort(404);
        }
        $history->add($document, Auth::id(), LuovDocument::STATUS_DELETE);
        return redirect()->back();
    }

    public function store(CreateTuovDeductionRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();

        $model = $this->tuovRepository->create($data);
        if ($model instanceof RedirectResponse) {
            return $model;
        }
        if (!$model || $model->document_status == LuovDocument::STATUS_PROCESSED) {
            return redirect(route('tuovDeduction.index'));
        }
        return redirect(route('tuovDeduction.edit', $model->id));
    }

    public function edit($id, LuovDocumentsHistory $history) {
        $document = $this->tuovRepository->findOrFail($id);
        if (LuovDocument::STATUS_DELETE == $document->document_status) {
            Flash::warning('Данный документ удален');
            return redirect()->back();
        }

        if (Gate::allows('update', $document)) {
            $history->add($document, Auth::id(), LuovDocument::STATUS_OPEN);
        }

        $tuov_fabric = $this->tuov_fabric->fabric($document);

        return view('tuov.edit')
            ->with('deduction', $document)
            ->with('rows', $tuov_fabric->search($document->id))
            ->with('buildForm', $tuov_fabric->buildForm($document));

    }

    public function update($id, UpdateTuovDeductionRequest $request, LuovDocumentsHistory $history) {
        $deduction = $this->tuovRepository->findOrFail($id);

        $this->authorize('update', $deduction);


        $model = $this->tuov_fabric->fabric($deduction);
        $form = $model->buildForm($deduction);

        $data = [];
        foreach ($request->input('anketa_id', []) as $key=>$anketa_id) {
            $row = [];
            foreach (array_keys($form) as $field) {
                $row[$field] = str_replace(',', '.', $request->input(preg_replace('!\[(\w+)\]!si', '.$1', $field) . '.' . $key));
            }
            $row['anketa_id'] = $anketa_id;
            $row['document_id'] = $deduction->id;

            $data[] = $row;
        }

        if ($model->updateForm($data, $deduction->id)) {
            $history->add($deduction, Auth::id() , LuovDocument::STATUS_PROCESSED);
        }
        return redirect(route('tuovDeduction.index'));
    }

    public function getHistory($id) {
        $document = $this->tuovRepository->findOrFail($id);
        $results = [];
        foreach ($document->history as $history) {
            $results[] = [
                'status'   => $history->document_status,
                'datetime' => $history->date,
                'username' => $history->user->username
            ];
        }
        return response()->json($results);
    }

    public function getImage($name, $ext) {
        $expiresAt = \Carbon\Carbon::now()->addHours(24);
        $file_name = $name . '.' . $ext;
        return response(Cache::store('file')->remember($file_name, $expiresAt , function() use ($file_name) {
            try {
                return Storage::disk('clodo')->readFileFromClodo((new LuovDocument)->getContainer(), $file_name);
            } catch(Exception $e) {
                abort(404, $e->getMessage());
            }
        }))->header('Content-Type', $ext);
    }

    public function report(Request $request){

        $user = Auth::user();
        $userObjects = $user->getObjects();
        $objects = $allObjects = Objects::getActiveObjects();

        foreach($userObjects as $object){
            unset($objects[$object->id]);
        }
        foreach($objects as $id=>$object){
            $userObjects->push($objects[$id]);
        }


        $data = $request->all();
        $fileName = '';

        if(empty($data['action'])){
            $data['action'] = 'bySubdivision';
        }

        if(isset($data['oid'])){

            $key = "content:{$data['oid']}:{$data['from']}:{$data['to']}:{$data['action']}";
            if (Redis::exists($key)){
                $data['content'] = Redis::get($key);
                $fileName = "http://vls.secondlab.ru/tuov/table_new/reports/excel/{$data['oid']}/{$data['from']}-{$data['to']}/{$allObjects[$data['oid']]->name}-{$data['from']}-{$data['action']}.xlsx";

            }
        }else{
            $data['oid'] = null;
            $data['from'] = date("Y-m-d", time() - 14 * 60 * 60 * 24);
            $data['to'] = date("Y-m-d");
        }

        return view('tuov.report')->withObjects($userObjects)
            ->withFilename($fileName)
            ->with('data',$data);
    }
    
}