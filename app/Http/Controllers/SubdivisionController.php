<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSubdivisionRequest;
use App\Http\Requests\UpdateSubdivisionRequest;
use App\Libraries\Repositories\SubdivisionRepository;
use Flash;
use Illuminate\Http\Request;
use Response;


class SubdivisionController extends Controller
{

	/** @var  SubdivisionRepository */
	private $subdivisionRepository;
    public $model;
    private $objects_arr;

	function __construct(SubdivisionRepository $subdivisionRepo)
	{
        $this->objects_arr = $this->getObjects();
        $this->model = new \App\Models\Subdivision;
		$this->subdivisionRepository = $subdivisionRepo;
	}

	/**
	 * Display a listing of the Subdivision.
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$subdivisions = $this->subdivisionRepository->search($request->all());

		return view('subdivisions.index')
			->with('subdivisions', $subdivisions)
			->withRequest($request)
			->withObjects($this->objects_arr);
	}

	/**
	 * Show the form for creating a new Subdivision.
	 *
	 * @return Response
	 */
	public function create()
	{
        $subdivision['status'] = $this->model->getStatus();
        $subdivision['objects_arr'] = $this->objects_arr;
		return view('subdivisions.create',  array('subdivision'=>$subdivision));
	}

	/**
	 * Store a newly created Subdivision in storage.
	 *
	 * @param CreateSubdivisionRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateSubdivisionRequest $request)
	{
		$input = $request->all();

        $input['object'] = current($this->getObjects($input['object_id'],2));
        $input['subdivision_comments'] = $this->model->getStatus((int)$input['subdivision_comments']);

		$subdivision = $this->subdivisionRepository->create($input);

		Flash::success('Subdivision saved successfully.');

		return redirect(route('subdivisions.index'));
	}

	/**
	 * Display the specified Subdivision.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$subdivision = $this->subdivisionRepository->find($id);

		if(empty($subdivision))
		{
			Flash::error('Subdivision not found');

			return redirect(route('subdivisions.index'));
		}

		return view('subdivisions.show')->with('subdivision', $subdivision);
	}

	/**
	 * Show the form for editing the specified Subdivision.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function edit($id)
	{
		$subdivision = $this->subdivisionRepository->find($id);

		if(empty($subdivision))
		{
			Flash::error('Subdivision not found');

			return redirect(route('subdivisions.index'));
		}

        $subdivision['status'] = $this->model->getStatus();
        $subdivision['status_selected'] = $this->model->getStatus($subdivision->subdivision_comments);
        $subdivision['objects_arr'] = $this->objects_arr;
		return view('subdivisions.edit')->with('subdivision', $subdivision);
	}

	/**
	 * Update the specified Subdivision in storage.
	 *
	 * @param  int              $id
	 * @param UpdateSubdivisionRequest $request
	 *
	 * @return Response
	 */
	public function update($id, UpdateSubdivisionRequest $request)
	{
		$subdivision = $this->subdivisionRepository->find($id);

		if(empty($subdivision))
		{
			Flash::error('Подразделение не найдено!');

			return redirect(route('subdivisions.index'));
		}

        $input = $request->all();
        $input['object'] = current($this->getObjects($input['object_id'],2));
        $input['subdivision_comments'] = $this->model->getStatus((int)$input['subdivision_comments']);

		$subdivision = $this->subdivisionRepository->updateRich($input, $id);

		Flash::success('Запись о подразделение успешно изменена.');

		return redirect(route('subdivisions.index'));
	}

	/**
	 * Remove the specified Subdivision from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$subdivision = $this->subdivisionRepository->find($id);

		if(empty($subdivision))
		{
			Flash::error('Подразделение не найдено!');

			return redirect(route('subdivisions.index'));
		}

		//$this->subdivisionRepository->delete($id);

        $status = $this->model->getStatus(1);
        \DB::table($this->model->table)
            ->where('id', (int)$id)
            ->update(array('subdivision_comments' => $status));

		Flash::success('Подразделению статус изменен на "не в работе"');

		return redirect(route('subdivisions.index'));
	}

    public function on($id)
    {
        $subdivision = $this->subdivisionRepository->find($id);

        if(empty($subdivision))
        {
            Flash::error('Подразделение не найдено!');

            return redirect(route('subdivisions.index'));
        }

        $status = $this->model->getStatus(2);
        \DB::table($this->model->table)
            ->where('id', (int)$id)
            ->update(array('subdivision_comments' => $status));

        Flash::success('Подразделению статус изменен на "в работе"');

        return redirect(route('subdivisions.index'));
    }

    private function getObjects($id=null, $solution = 1){
        if (empty($id)){
            $model = \DB::table('objects')->orderby('name')->get();
        } else {
            $model = \DB::table('objects')->where('id', $id)->get();
        }
        $arrayObj = array();
        foreach ($model as $data){
            $arrayObj[$data->id] =  $data->name.(($solution==1)?(' ('.$data->comments.')'):'');
        }
        return $arrayObj;
    }
}
