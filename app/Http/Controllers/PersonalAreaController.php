<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateSbUsersRequest;
use App\Http\Requests\UpdateSbUsersRequest;
use App\Libraries\Repositories\SbUsersRepository;
use App\Models\Objects;
use Illuminate\Http\Request;
use Flash;
use Response;


class PersonalAreaController extends Controller
{

	public function index(Request $request)
	{
		$uid =  \Auth::id();

		$this->data['period_history'] = 14;
		if ($request->all()){
			$requests = $request->all();
			if (!empty($requests['s_pr_from'])){
				$difference = intval(abs(
					strtotime(((!empty($requests['s_pr_to']))?($requests['s_pr_to']):(date('Y-m-d')))) - strtotime($requests['s_pr_from'])
				));
				$this->data['period_history'] = $difference / (3600 * 24);
			}


		}

		$this->data['crd_status']['BUSY'] = 'Занято';
		$this->data['crd_status']['NO ANSWER'] = 'Нет ответа';
		$this->data['crd_status']['ANSWERED'] = 'Успешно';
		$this->data['crd_status']['FAILED'] = 'Неисправность';

        $this->data['user'] = \DB::table('users')->where('userid', $uid)->first();

		\DB::connection('asterisk')->table('cdr')-> first();
		$db_ext = \DB::connection('asterisk');
		$this->data['calls_out'] = $db_ext->select('SELECT `disposition` , count(*) as count_row, sum(`duration`) as sum_duration, sum(`billsec`) as sum_billsec FROM `cdr` WHERE `clid`="' . $this->data['user']->myextension . '" AND DATE(`calldate`) = CURDATE() and `channel` LIKE "SIP/' . $this->data['user']->myextension . '%" GROUP BY `disposition`');
		$this->data['calls_inc'] = $db_ext->select('SELECT `disposition` , count(*) as count_row, sum(`duration`) as sum_duration, sum(`billsec`) as sum_billsec FROM `cdr` WHERE DATE(`calldate`) = CURDATE() and `dstchannel` LIKE "SIP/' . $this->data['user']->myextension . '%" GROUP BY `disposition`;');


		$this->data['anketa_history'] = \DB::table('anketa_history')
			->select('id_ankety', 'status_set')
			->select(\DB::raw('count(DISTINCT `id_ankety`, `status_set`) as count_history'))
			->where('user', $this->data['user']->username)
			->where('when', 'LIKE', date('Y-m-d').'%')
			->groupBy('status_set')
			->get();


		switch (date('D', time())) {
			case "Fri":
				$interval  = 3;
				$this->data['when_com'] = "в понедельник";
				break;
			case "Sat":
				$interval  = 2;
				$this->data['when_com'] = "в понедельник";
				break;
			default:
				$interval  = 1;
				$this->data['when_com'] = "в завтра";
		}


		$this->data['soiskateli'] = \DB::table('soiskateli')
			->where('customer_status',12)
			->where('user_creator',$this->data['user']->userid)
			->whereBetween('kogda_pridet',array((date('Y-m-d')),(date('Y-m-d',strtotime('+'.$interval.' day')))))
			->get();
		$this->data['soiskateli_count'] = \DB::table('soiskateli')
			->where('user_creator', 12)
			->where('user_creator', $this->data['user']->userid)
			->whereBetween('kogda_pridet',array((date('Y-m-d')),(date('Y-m-d',strtotime('+'.$interval.' day')))))
			->count();


		$this->data['tabel_history'] = \DB::table('anketa_history')
			->where('user',$this->data['user']->username)
			->whereBetween('when',array((date('Y-m-d',strtotime('-'.$this->data['period_history'].' day'))),(date('Y-m-d'))))
			->orderBy('when', 'desc')
			->get();

		$id_ankety = array();
		foreach ($this->data['tabel_history'] as $data){
			$id_ankety[] = $data->id_ankety;
		}
		if (empty($id_ankety)){ $id_ankety[] = null; }

		$this->data['tabel_history_14'] = \DB::table('soiskateli')->whereIn('id', $id_ankety)->orderBy('created_when', 'desc')->get();


		$this->data['customer_status_array'] = array();
		foreach (\DB::table('statustable')->get() as $data){
			$this->data['customer_status_array'][$data->status_id]['name'] = $data->status_name;
			$this->data['customer_status_array'][$data->status_id]['color'] = $data->colormark;
		}


		$this->data['nation_array'] = array();
		foreach (\DB::table('grazdanstvo')->get() as $data){
			$this->data['nation_array'][$data->grazd_id] = $data->grazd_name;
		}


		return view('personalArea.index',$this->data);
	}



}
