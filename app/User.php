<?php namespace App;

use App\Models\Objects;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Validator;
use DB;
use Zizaco\Entrust\Traits\EntrustUserTrait;


class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;
    use EntrustUserTrait;

	const ROLE_BLOCKED = 0;
	const ROLE_USER = 1;
	const ROLE_MANAGER = 2;
	const ROLE_ADMIN = 10;

    public $subdivisions;
    public $objects;
    /**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';
	protected $primaryKey = 'userid';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['username', 'email', 'password','fio','phone','myextension','status','enable'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public static $rules =
	[
		'create' =>[
			'username' => 'required|unique:users,username',
			'fio' => 'required',
			'phone'=> 'max:100',
			'email' => 'email',
			'enable' => 'required|in:0,1',
			'password' => 'required|min:6|confirmed',
			'role' => 'array'
		],
		'update' => [
			'username' => 'required|exists:users,username',
			'fio' => 'required',
			'phone'=> 'max:100',
			'email' => 'email',
			'enable' => 'required|in:0,1',
			'password' => 'confirmed',
			'role' => 'array'
		]
	];

	public static $messages = [
		'username.required' => 'Введите Логин',
		'fio.required' => 'Введите ФИО',
		'password.required' => 'Введите Пароль',
		'confirmed' => 'Пароли не совпадают'
	];

	protected static function convertToDir($array) {
		$result = array();
		foreach($array as $value) 
		$result[$value->id] = $value->name; 
		return $result;
	}
	
	public static function getLists() {
		$data = array();
		$data['users_status'] =  self::convertToDir(DB::select('select * from users_status ORDER BY ID'));
		$data['enable'] = array('0'=>'нет','1'=>'да');
		return $data;
	}		
	
	public function Validate($request) {
	
		$rule = self::$rules;
		
		$messages = self::$messages;
		
		if ($request['id'] && !$request['password']) {
			unset($rule['password']);		
		}
		
		return Validator::make($request,$rule,$messages);	
	}
	
    public static function getActiveUsersList($uid = null){
        $users = User::where('enable','=','1');
        if (empty($uid)){
            $users = $users->orderBy('username', 'asc')->get();
            $activeUsers = [];
            foreach($users as $user){
                $activeUsers[$user->userid] = $user->username;
            }
        } else {
            $activeUsers = $users->where('userid','=',$uid)->first();
        }

        return $activeUsers;
    }

	/**
	 * Label for enable status
	 * @return string
	 */
	public function getEnableLabelAttribute() {
		return ($this->enable==1?'да':'нет');
	}


	public function owns($related)
	{
		return $this->id == $related->user_id;
	}

    public function getObjects(){
        $uid = $this->userid;

        $objects = new Collection();

        $obs = DB::select("SELECT * FROM objects WHERE coordinator={$uid} AND comments ='в работе'");

        foreach($obs as $oid){
            $objects->push($oid);
        }

        $obs = DB::select("SELECT * FROM objects WHERE id IN (SELECT object_id FROM taskmaster_objects WHERE taskmaster_id = $uid) AND comments ='в работе'");

        foreach($obs as $oid){
            $objects->push($oid);
        }

        return $objects;
    }
}
