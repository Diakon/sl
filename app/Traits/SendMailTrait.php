<?php namespace App\Traits;

use App\Models\Sendmail;

trait SendMailTrait
{

    //Отправляет письмо (сразу, минуя запись в таблицу рассылки) $email - емайл получателя, $data - массив с данными в темплейте, $from - от когого (имя), $$template = название темплейта (темплейты лежат в /resources/views/emails)
    public function sendMail($email = null, $data = null, $from = null, $template=null){
        if (empty($email)){ exit; }
        $mailer = new Sendmail();
        $mailer->setLayout($template);
        $mailer->setData($data);
        if (!empty($from)){$mailer->setFromName($from);}
        $mailer->setDestination($email);
        $mailer->sendMessage();
    }


    //пишет письмо в таблицу очереди рассылки. $email=емйл получателя письма, $data-массив данных темплейта, $title-заголовок, $template-шаблон письма
    /**
     * @param null $email
     * @param array $data
     * @param string $title
     * @param null $template
     */
    public function sendMailDelivery($email = null, $data = array(), $title="", $template=null){
        $modelEmail = new \App\Models\EmailDelivery();
        $modelEmail->setTemplate($template);
        $modelEmail->setRecipient($email);
        $modelEmail->setTitle($title);
        $modelEmail->setBody($data);
        $modelEmail->addRecord();
    }

}
