<?php namespace App\Traits;


trait SendSMSTrait
{

    /**
    Отправляет СМС
     * $phones - телефоный, либо 1 телефон, либо массив телефонов вида array('XXXXXXXXX', 'XXXXXXXXX');
     * $smsbody - текст сообщения
    **/
    public function sendSMS($phones, $smsbody){
        require_once \public_path()."/sms/smsc_api.php";

        if ( is_array($phones) ){
            $phones = implode(",", $phones);
        }

        $result = send_sms($phones, $smsbody, $translit = 0, $time = 0, $id = 0, $format = 0, $sender = "79168877777");
    }


}
