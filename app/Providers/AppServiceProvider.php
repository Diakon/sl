<?php namespace App\Providers;

use App\Events\documentChanged;
use App\Models\LuovDocument;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Rap2hpoutre\LaravelLogViewer\LaravelLogViewerServiceProvider;
use Event;

class AppServiceProvider extends ServiceProvider {

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		// регистрация провайдеров в обход config/app.php
		App::register(ComposerServiceProvider::class);
		App::register(LaravelLogViewerServiceProvider::class);
		App::register(PasswordResetServiceProvider::class);
		App::register(AuthServiceProvider::class);

		// добавление aliases в обход config/app.php
		$this->app->booting(function()
		{
			$loader = \Illuminate\Foundation\AliasLoader::getInstance();
			$loader->alias('Gate', \Illuminate\Support\Facades\Gate::class);
		});



		$this->app->bind(
			\Illuminate\Contracts\Auth\Registrar::class,
			\App\Services\Registrar::class
		);
		$this->app->bind('Zizaco\Entrust\Contracts\EntrustRoleInterface', Config('entrust.role'));
		$this->app->bind('Zizaco\Entrust\Contracts\EntrustPermissionInterface', Config('entrust.permission'));
	}

}
