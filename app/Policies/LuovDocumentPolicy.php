<?php

namespace App\Policies;

use App\Models\LuovDocument;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class LuovDocumentPolicy
{
    use HandlesAuthorization;

    /**
     * Проверяет есть ли у пользователя доступ к редактированию документа
     * (только админ либо тот кто установил статус "Обработан")
     * @param \App\User $user
     * @param \App\Models\LuovDocument $document
     * @return bool
     */
    public function update (User $user, LuovDocument $document)
    {
        // доступ админу
        if ($user->hasRole('admin')) {
            return true;
        }
        // доступ если не обработано
        if ($document->document_status != LuovDocument::STATUS_PROCESSED) {
            return true;
        }
        $firstLog = $document->history()->where('document_status', LuovDocument::STATUS_PROCESSED)->first();
        // доступ первому кто установил статус "Обработано"
        if (!$firstLog || $firstLog->user_id == $user->getKey() ) {
            return true;
        }
        return false;
    }
}
