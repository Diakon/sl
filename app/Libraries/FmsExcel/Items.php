<?php namespace App\Libraries\FmsExcel;

class Items
{
    public function getFamily() {
        return [
            'W69', 'AA69', 'AE69', 'AI69', 'AM69', 'AQ69', 'AU69', 'AY69', 'BC69', 'BG69', 'BK69', 'BO69',
            'BS69', 'BW69', 'CA69', 'CE69', 'CI69', 'CM69', 'CQ69', 'CU69', 'CY69', 'DC69', 'DG69', 'DK69',
            'DO69', 'DS69', 'DW69', 'EA69', 'EE69', 'EI69', 'EM69', 'EQ69', 'EU69', 'EY69', 'FC69',

            'W71', 'AA71', 'AE71', 'AI71', 'AM71', 'AQ71', 'AU71', 'AY71', 'BC71', 'BG71', 'BK71', 'BO71',
            'BS71', 'BW71', 'CA71', 'CE71', 'CI71', 'CM71', 'CQ71', 'CU71', 'CY71', 'DC71', 'DG71', 'DK71',
            'DO71', 'DS71', 'DW71', 'EA71', 'EE71', 'EI71', 'EM71', 'EQ71', 'EU71', 'EY71', 'FC71'
        ];
    }

    public function getNation() {
        return [
            'AA74', 'AE74', 'AI74', 'AM74', 'AQ74', 'AU74', 'AY74', 'BC74', 'BG74', 'BK74', 'BO74',
            'BS74', 'BW74', 'CA74', 'CE74', 'CI74', 'CM74', 'CQ74', 'CU74', 'CY74', 'DC74', 'DG74', 'DK74',
            'DO74', 'DS74', 'DW74', 'EA74', 'EE74', 'EI74', 'EM74', 'EQ74', 'EU74', 'EY74', 'FC74'
        ];
    }

    public function getBirthDate() {
        return [
            'AE77', 'AI77', 'AU77', 'AY77', 'BG77', 'BK77', 'BO77', 'BS77'
        ];
    }

    public function getGender() {
        return [
            'DC77', 'DW77'
        ];
    }

    public function getIdentityDocument() {
        return [
            'BC80', 'BG80', 'BK80', 'BO80', 'BS80', 'BW80', 'CA80', 'CE80', 'CI80', 'CM80', 'CQ80',
            'DC80', 'DG80', 'DK80', 'DO80',
            'DW80', 'EA80', 'EE80', 'EI80', 'EM80', 'EQ80', 'EU80', 'EY80', 'FC80',
        ];
    }

    public function getTenureTo() {
        return [
            'AQ95', 'AU95',
            'BG95', 'BK95',
            'BS95', 'BW95', 'CA95', 'CE95'
        ];
    }

    public function getDeparture() {
        return [
            'DO69', 'DS69',
            'EE69', 'EI69',
            'EQ69', 'EU69', 'EY69', 'FC69'
        ];
    }

    public function getRegion() {
        return [
            'AE83', 'AI83', 'AM83', 'AQ83', 'AU83', 'AY83', 'BC83', 'BG83', 'BK83', 'BO83',
            'BS83', 'BW83', 'CA83', 'CE83', 'CI83', 'CM83', 'CQ83', 'CU83', 'CY83', 'DC83', 'DG83', 'DK83',
            'DO83', 'DS83', 'DW83', 'EA83', 'EE83', 'EI83', 'EM83', 'EQ83', 'EU83', 'EY83', 'FC83'
        ];
    }

    public function getDistrict() {
        return [
            'W86', 'AA86', 'AE86', 'AI86', 'AM86', 'AQ86', 'AU86', 'AY86', 'BC86', 'BG86', 'BK86', 'BO86',
            'BS86', 'BW86', 'CA86', 'CE86', 'CI86', 'CM86', 'CQ86', 'CU86', 'CY86', 'DC86', 'DG86', 'DK86',
            'DO86', 'DS86', 'DW86', 'EA86', 'EE86', 'EI86', 'EM86', 'EQ86', 'EU86', 'EY86', 'FC86',
        ];
    }

    public function getCity() {
        return [
            'AE88', 'AI88', 'AM88', 'AQ88', 'AU88', 'AY88', 'BC88', 'BG88', 'BK88', 'BO88',
            'BS88', 'BW88', 'CA88', 'CE88', 'CI88', 'CM88', 'CQ88', 'CU88', 'CY88', 'DC88', 'DG88', 'DK88',
            'DO88', 'DS88', 'DW88', 'EA88', 'EE88', 'EI88', 'EM88', 'EQ88', 'EU88', 'EY88', 'FC88',
        ];
    }

    public function getStreet() {
        return [
            'W91', 'AA91', 'AE91', 'AI91', 'AM91', 'AQ91', 'AU91', 'AY91', 'BC91', 'BG91', 'BK91', 'BO91',
            'BS91', 'BW91', 'CA91', 'CE91', 'CI91', 'CM91', 'CQ91', 'CU91', 'CY91', 'DC91', 'DG91', 'DK91',
            'DO91', 'DS91', 'DW91', 'EA91', 'EE91', 'EI91', 'EM91', 'EQ91', 'EU91', 'EY91', 'FC91',
        ];
    }

    public function getHouseAndBuilding() {
        return [
            'S93', 'W93', 'AA93', 'AE93',
            'AQ93', 'AU93', 'AY93', 'BC93'
        ];
    }
}