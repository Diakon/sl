<?php namespace App\Libraries\FmsExcel;

use Illuminate\Database\Eloquent\Model;

class Fms
{
    private $model;
    private $excel2;
    private $words;
    private $items;

    public function __construct() {
        $this->init();
        $this->getWords();
        $this->items = new Items();
    }
    private function init() {
        $this->excel2 = \PHPExcel_IOFactory::createReader('Excel5');
        $this->excel2 = $this->excel2->load(dirname(__FILE__) . '/fms.xls');
    }

    public function model(Model $model) {
        $this->model = $model;
        return $this;
    }
    private function getWords() {
        $abc = [];
        foreach (range(chr(0xC0), chr(0xDF)) as $b) {
            $abc[] = iconv('CP1251', 'UTF-8', $b);
        }
        $this->words = array_merge(
            $this->getWordsCoordinate(range('A', 'Y'), 11),
            $this->getWordsCoordinate(range(0, 9), 11, true),
            $this->getWordsCoordinate($abc, 9)
        );
    }
    private function getWordsCoordinate($words, $row, $is_float=false) {
        $foundInCells = [];
        foreach ($this->excel2->getWorksheetIterator() as $worksheet) {
            $ws = $worksheet->getTitle();
            $row = $worksheet->getRowIterator($row)->current();
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(true);
            foreach ($cellIterator as $cell) {
                foreach ($words as $value) {
                    if ($cell->getValue() === $value || ($is_float && $cell->getValue() === (float)$value)) {
                        $foundInCells[$value] = "". $ws . "!".$cell->getCoordinate();
                    }
                }
            }
            break;
        }
        return $foundInCells;
    }

    public function create() {

        $status_date = (new \DateTime($this->model->status->status_date))->modify('+1 year -6 day')->format('dmY');

        $this->setText($this->items->getFamily(), join('  ', [$this->model->lastname, $this->model->firstname, $this->model->patronymic]));
        $this->setText($this->items->getNation(), 'республика беларусь');
        $this->setText($this->items->getBirthDate(), date('dmY',strtotime($this->model->birth_date)));
        $this->setText($this->items->getGender(), ($this->model->gender>0 ? 'X' : '_X') );
        $this->setText($this->items->getIdentityDocument(), join('', [
            $this->utf_8_sprintf("%- 11s", 'паспорт'),
            $this->utf_8_sprintf("%- 4s", $this->model->passport->series),
            $this->model->passport->number
        ]));
        $this->setText($this->items->getTenureTo(), $status_date);
        $this->setText($this->items->getDeparture(), $status_date, 1);

        $this->setText($this->items->getRegion(), $this->model->dormitory->region);
        $this->setText($this->items->getDistrict(), $this->model->dormitory->district);
        $this->setText($this->items->getCity(), $this->model->dormitory->city);
        $this->setText($this->items->getStreet(), $this->model->dormitory->street);
        $this->setText($this->items->getHouseAndBuilding(),
            $this->utf_8_sprintf("%- 4s", $this->model->dormitory->house) . $this->model->dormitory->building
        );







        $objWriter = \PHPExcel_IOFactory::createWriter($this->excel2, 'Excel5');
        $this->excel2->setActiveSheetIndex(0);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="yved_o_pribitii_' . $this->model->id_anketa . '.xls"');
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');

    }


    public function setText($coordinates, $word, $list_index=0) {
        $chars = $this->str_split_unicode(mb_strtoupper($word));
        foreach ($coordinates as $coordinate) {
            $char = array_shift($chars);
            if ($char == null || empty($this->words[$char])) {
                continue;
            }
            $this->excel2->setActiveSheetIndex($list_index)->setCellValue($coordinate, '=' . $this->words[$char]);
        }
    }





    private function str_split_unicode($str, $l = 0) {
        if ($l > 0) {
            $ret = array();
            $len = mb_strlen($str, "UTF-8");
            for ($i = 0; $i < $len; $i += $l) {
                $ret[] = mb_substr($str, $i, $l, "UTF-8");
            }
            return $ret;
        }
        return preg_split("//u", $str, -1, PREG_SPLIT_NO_EMPTY);
    }

    private function utf_8_sprintf ($format) {
        $args = func_get_args();

        for ($i = 1; $i < count($args); $i++) {
            $args [$i] = iconv('UTF-8', 'ISO-8859-5', $args [$i]);
        }

        return iconv('ISO-8859-5', 'UTF-8', call_user_func_array('sprintf', $args));
    }
}
