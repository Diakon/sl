<?php namespace App\Libraries\Clodo;

class ClodoClient
{
    protected $connection;

    protected $auth;

    function __construct($accessName, $accessKey, $url) {
        $this->auth = new \CF_Authentication($accessName, $accessKey, null, $url);
        $this->auth->authenticate();
        $this->connection = new \CF_Connection($this->auth);
    }

    /**
     * @param $old_file  - файл на сервере
     * @param $new_file - имя файла на clodo
     * @param $container  - папка на клодо куда скопировать $new_file
     */
    public function copyfile($old_file, $new_file, $container) {
        $files = $this->connection->create_container($container);
        $object = $files->create_object($new_file);
        $object->load_from_filename($old_file);
        return true;
    }

    public function deletefile($file_name, $container) {
        $files = $this->connection->get_container($container);
        $files->delete_object($file_name);
        return true;
    }

    /**
     * Возращает объек состоящий из данных о контейнерах (папках) на клоде
     */
    public function getContainers() {
        return $this->connection->get_containers();
    }

    public function getObjects($container) {
        $get_container = $this->connection->get_container($container);
        return $get_container->get_objects();
    }

    /**
     * Сохраняет файл из хранилища клодо на сервере в локальный файл
     *  $local_filename - имя локального файла на сервере куда будет загружен файл с клодо
     *  $container - контейнер на клодо где лежит файл
     *  $clodo_filename - имя локального файла на клодо
     */
    public function loadFileFromClodo($local_filename, $container, $clodo_filename){
        $files = $this->connection->get_container($container);
        $doc = $files->get_object($clodo_filename);
        $doc->save_to_filename($local_filename);

        return true;
    }

    /**
     * Читает файл из хранилища клодо на сервере в локальный файл
     *  $container - контейнер на клодо где лежит файл
     *  $clodo_filename - имя локального файла на клодо
     */
    public function readFileFromClodo($container, $clodo_filename){
        $files = $this->connection->get_container($container);
        $doc = $files->get_object($clodo_filename);
        return $doc->read();

    }
}