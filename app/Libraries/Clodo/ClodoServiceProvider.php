<?php namespace App\Libraries\Clodo;

use Illuminate\Support\ServiceProvider;
use Storage;


class ClodoServiceProvider extends ServiceProvider {

    public function boot()
    {
        Storage::extend('clodo', function($app, $config) {
            return new ClodoClient($config['username'], $config['key'], $config['url']);
        });
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        // TODO: Implement register() method.
    }
}
