<?php namespace App\Libraries\MyMenu;

use Illuminate\Support\Facades\Route;
use \Menu\Items\ItemList as vespakoenItemList;
use \Menu\Items\Contents\Link;


class ItemList extends vespakoenItemList
{
    public function addIfCan($url, $value, $children = null, $linkAttributes = array(), $itemAttributes = array(), $itemElement = null)
    {
        $permissions_by_routes= [];
        $route_current = null;

        if (isset($url['route'])) {
            $route_current = Route::getRoutes()->getByName($url['route']);
        } else {
            foreach (\Route::getRoutes() as $route) {
                if (!in_array('GET', $route->getMethods())) {
                    continue;
                }
                if ($route->getPath() == $url) {
                    $route_current = $route;
                    break;
                }
            }
        }
        if ($route_current) {
            foreach (Route::gatherRouteMiddlewares($route_current) as $middleware) {
                if (strpos($middleware, 'EntrustAbility') !== false) {
                    list(, $parameters) = array_pad(explode(':', $middleware, 2), 2, []);
                    if (is_string($parameters)) {
                        $parameters_array = explode(',', $parameters);
                    }
                    $permissions_by_routes = $parameters_array;
                    if (Auth()->user()->hasRole('admin')) {
                        $linkAttributes['title'] = sprintf('роли: %s права: %s', $parameters_array[0], $parameters_array[1]);
                    }
                    break;
                }
            }
            $url = $route_current->getPath();
        }

        if (
            ( count($permissions_by_routes) && Auth()->user()->ability($permissions_by_routes[0], $permissions_by_routes[1]) )
            or
            ( (is_object($children) && $children->getChildren()) || $this->hasParent() )
        ) {
            $content = new Link($url, $value, $linkAttributes);
            $this->addContent($content, $children, $itemAttributes, $itemElement);
        }
        return $this;
    }
}