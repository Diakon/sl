<?php namespace App\Libraries\MyMenu;

use Menu\Menu as vespakoenMenu;
use Menu\MenuHandler as vespakoenMenuHandler;

class Menu extends vespakoenMenu
{
    public static function handler($names = '', $attributes = array(), $element = 'ul')
    {
        $names = (array) $names;

        $itemLists = array();
        // Create a new ItemList instance for the names that don't exist yet
        foreach ($names as $name) {
            if (!array_key_exists($name, static::$itemLists)) {
                $itemList = new ItemList(array(), $name, $attributes, $element);
                static::setItemList($name, $itemList);
            }
            else {
                $itemList = static::getItemList($name);
            }

            $itemLists[] = $itemList;
        }
        // Return a Handler for the item lists
        return new vespakoenMenuHandler($itemLists);
    }

    public static function items($name = null, $attributes = array(), $element = 'ul')
    {
        return new ItemList(array(), $name, $attributes, $element);
    }
}