<?php namespace App\Libraries\MyMenu;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;

class MenuComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $menu = Menu::handler('mailbox')->addClass('nav navbar-nav navbar-left');
        if ( Auth::user() ){
            // items
            $menu->add('#', 'IT-заявки', Menu::items()
                ->add('support/addtiket', 'Создать заявку')
                ->addIfCan('support/listtiket', 'Просмотр заявок')
                ->addIfCan('support/stattiket', 'Отчет по ИТ-заявкам')
            );

            $menu->add('#', 'Планировщик', Menu::items()
                ->addIfCan('scheduler', 'CRUD Орг.структура')
                ->add('scheduler/mytasks', 'Мои задачи')
            );

            $menu->addIfCan('#', 'Отдел персонала', Menu::items()
                ->addIfCan(['route'=>'agencyStaffing.index'], 'Организации')
                ->addIfCan(['route'=>'agencyContacts.index'], 'Контактные лица')
                ->addIfCan('fullbase', 'Заявки')
                ->addIfCan('analyze-cc-noanswer', 'Анализ пропущенных звонков')
            );

            $menu->addIfCan('#', 'ТУОВ', Menu::items()
                ->addIfCan('tuovAllowances', 'CRUD Лин.бригадиры')
                ->addIfCan('taskmasterObjects', 'CRUD доп.Координаторы')
                ->addIfCan('subdivisionProbations', 'CRUD Стажировочные дни на подразделениях')
                ->addIfCan('deals', 'CRUD Сделки')
                ->addIfCan(['route' => 'tuovDayOutlays.index' ], 'CRUD Дорожные')
                ->addIfCan('subdivisions', 'CRUD Подразделения')
                ->addIfCan('tuovDealRates', 'CRUD Ставки для сделок')
                ->addIfCan(['route' => 'tuov.rates.index' ], 'CRUD Ставки для почасовок')
                ->addIfCan(['route' => 'tuovDeduction.index' ], 'CRUD Документы')
                ->addIfCan(['route' => 'tuov.report' ], 'ТУОВ')
            );

            $menu->add('#', 'СБ', Menu::items()
                ->add('sb', 'Войти')
                ->addIfCan('sb/users', 'Пользователи')
            );

            $menu->addIfCan('#', 'Бухгалтерия', Menu::items()
                ->addIfCan('bankcard', 'Список банковских карт')
                ->addIfCan('contractor', 'Контрагенты')
                ->addIfCan(['route' => 'po.objects.index'], 'PO Объекты')
                ->addIfCan(['route' => 'po.applications.all.index'], 'PO Заявки (приходы)')
                ->addIfCan(['route' => 'po.expense.all.index'], 'PO Расходы')
                ->addIfCan(['route' => 'po.passportOffices.index'], 'PO Паспорта подразделений')
            );

            $menu->addIfCan('#', 'Выгрузки', Menu::items()
                ->addIfCan('reports/UnloadingAlphaBank', 'Выгрузка по Альфабанку')
            );

            $menu->addIfCan('#', 'Администратор', Menu::items()
                ->addIfCan('admin/users', 'Пользователи')
                ->addIfCan('admin/roles', 'Роли и права')
                ->addIfCan('admin/applicationLogs', 'Логи приложения')
                ->addIfCan(['route' => 'admin.users.log'], 'Логи авторизации')

            );
        }

        $menu->getAllItems()->map(
            function ($item) {
                if ($item->hasActiveChild()) {$item->addClass('active');}
                if($item->hasChildren()){
                    // Add a class to the LI
                    $item->addClass('dropdown');
                    // Add a class to the A
                    $item->getContent()
                        ->addClass('dropdown-toggle')
                        ->setAttribute('data-toggle', 'dropdown');
                    // Add a class to the UL
                    $item->getChildren()->addClass('dropdown-menu');

                    $a = $item->getContent()->getValue();

                    $item->getContent()->setValue("$a <b class=\"caret\"></b>");
                }
            }
        );

        $view->with('menu', $menu);
    }
}