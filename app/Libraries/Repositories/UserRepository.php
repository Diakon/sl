<?php namespace App\Libraries\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use Schema;

class UserRepository extends Repository
{

    /**
    * Configure the Model
    *
    **/
    public function model()
    {
      return 'App\User';
    }

    public function search($input)
    {

        $this->applyCriteria();
        $model = $this->model;

        if (!empty($input)) {
            $columns = Schema::getColumnListing($model->getTable());

            foreach ($columns as $attribute) {
                if (isset($input[$attribute]) && (is_numeric($input[$attribute]) || !empty($input[$attribute]))) {
                    $model = $model->where($attribute, 'like', $input[$attribute].'%');
                }
            }
        }
        return $model->orderBy('username', 'desc')->paginate(30);
    }
}
