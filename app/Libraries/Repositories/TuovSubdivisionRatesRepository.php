<?php namespace App\Libraries\Repositories;

use App\Models\TuovDealRates;
use Bosnadev\Repositories\Console\Commands\Creators\CriteriaCreator;
use Bosnadev\Repositories\Eloquent\Repository;
use Schema;
use Symfony\Component\HttpKernel\Exception\HttpException;

class TuovSubdivisionRatesRepository extends Repository
{

    /**
    * Configure the Model
    *
    **/
    public function model()
    {
      return \App\Models\SubdivisionRates::class;
    }

	public function search($input, $perPage=20)
    {
        $query = $this->model->query();
        if (!empty($input)) {
            foreach(Schema::getColumnListing($this->model->getTable()) as $attribute) {
                if(isset($input[$attribute]) and !empty($input[$attribute]))
                {
                    if (Schema::getColumnType($this->model->getTable(), $attribute) == 'integer') {
                        $query->where($attribute, $input[$attribute]);
                    } else {
                        $query->where($attribute, 'like', $input[$attribute] . '%');
                    }
                    $attributes[$attribute] = $input[$attribute];
                }
                else
                {
                    $attributes[$attribute] =  null;
                }
            }
        }
        $query->with('subdivision')->orderBy('subdivision_id','ASC')->orderBy('date_from','ASC');

        return $query->paginate($perPage);
    }

    public function findOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(404, "Ставка не найдена");
        }

        return $model;
    }

    public function apiDeleteOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(1001, "TuovDealRates not found");
        }

        return $model->delete();
    }
}
