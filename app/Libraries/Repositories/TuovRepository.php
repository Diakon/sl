<?php namespace App\Libraries\Repositories;

use App\Http\Requests\CreateTuovDeductionRequest;
use App\Http\Requests\Request;
use App\Models\LuovDocument;
use App\Models\LuovDocumentsHistory;
use App\Models\PersMain;
use App\Models\Tuov\TuovFabric;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Schema;

class TuovRepository extends Repository
{
    use \App\Traits\SendMailTrait;

    /**
    * Configure the Model
    *
    **/
    public function model()
    {
      return \App\Models\LuovDocument::class;
    }

    public function getValidExcelCategory()
    {
        return $this->model->validExcelCategory;
    }

	public function search($input)
    {

        $this->applyCriteria();
        $model = $this->model;

        if (!empty($input)) {
            $columns = Schema::getColumnListing($model->getTable());
            foreach ($columns as $attribute) {
                if ($attribute == 'date') {
                    foreach (['>='=>'from', '<='=>'to'] as $condition=>$name) {
                        $key = $attribute . '_' . $name;
                        if (!empty($input[$key])) {
                            $model = $model->where($attribute, $condition, $input[$key]);
                        }
                    }
                } elseif (isset($input[$attribute]) and (is_numeric($input[$attribute]) || !empty($input[$attribute]))) {
                    $model = $model->where($attribute, $input[$attribute]);
                }
            }
        }
        return $model->with('subdivision','user', 'category')->orderby('id', 'desc')->paginate(40);
    }

    public function findOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(404, "Документ не найден");
        }

        return $model;
    }

    public function create(array $data) {
        $tuov_fabric = app()->make(TuovFabric::class);
        $persMain = app()->make(PersMain::class);
        $history = app()->make(LuovDocumentsHistory::class);
        $request = app()->make(CreateTuovDeductionRequest::class);
        $user_id = $data['user_id'];

        $model = parent::create($data);
        if ($model) {
            $images = [];
            foreach ($request->file('document') as $document) {
                $ext = $document->guessClientExtension();
                if (in_array($ext, ['xls', 'xlsx'])) {
                    $excel = \Excel::load($document);
                    $data = [];
                    $anketaNotFounds = [];
                    foreach($excel->toObject() as $row) {
                        $row->id = intval($row->id);
                        if (!$persMain->find($row->id)) {
                            $anketaNotFounds[] = json_encode($row->all(), JSON_UNESCAPED_UNICODE);
                            continue;
                        }
                        $data[] = [
                            'anketa_id' => intval($row->id),
                            'document_id' => $model->id,
                            'sum' => floatval($row->summa)
                        ];
                    }

                    if (count($data) && $tuov_fabric->fabric($model)->updateForm($data)) {
                        $history->add($model, $user_id , LuovDocument::STATUS_PROCESSED);

                        Storage::disk('clodo')->copyfile($document, $model->filename($ext), $model->getContainer());
                    }

                    if (count($anketaNotFounds)) {
                        $v = Validator::make($request->all(), []);
                        $v->errors()->add('document', 'Не найдены следующие анкеты в Excel файле: <br/>' . implode(', ', $anketaNotFounds) );

                        $this->sendMailDelivery('fo@vlspro.ru', [
                            'title'=> 'Анкета пользователя не найдена в Excel Луов',
                            'users' => $anketaNotFounds,
                            'document' => $model
                        ], 'Анкета пользователя не найдена в Excel Луов', 'tuov_anketa_not_found');
                        return redirect(route('tuovDeduction.edit', $model->id))->withErrors($v);
                    }
                    break;

                } else {
                    switch($document->getMimeType()) {
                        case 'image/png':
                            $images[] = imagecreatefrompng($document);
                            break;
                        case 'image/jpeg':
                            $images[] = imagecreatefromjpeg($document);
                            break;
                    }
                }
            }

            if (count($images)) {
                $mainWidth = 0;
                $mainHeight = 0;
                foreach($images as $image){
                    $sy = imagesy($image);
                    $sx = imagesx($image);
                    $mainHeight+=$sy;
                    $mainWidth = max($mainWidth,$sx);
                }
                $mainImage = imagecreatetruecolor($mainWidth,$mainHeight);
                $y_now = 0;
                foreach($images as $image){
                    $sy = imagesy($image);
                    $sx = imagesx($image);
                    imagecopyresampled($mainImage,$image,0,$y_now,0,0,$sx,$sy,$sx,$sy);
                    $y_now+=$sy;
                }

                imagejpeg($mainImage,$document->getPathname());
                Storage::disk('clodo')->copyfile($document->getPathname(), $model->filename('jpg'), $model->getContainer());
            }
        }
        $model->makeVisible(['id']);
        return $model;
    }
}
