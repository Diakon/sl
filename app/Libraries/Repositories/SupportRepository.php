<?php namespace App\Libraries\Repositories;

use App\Http\Requests\Request;
use App\User;
use Bosnadev\Repositories\Eloquent\Repository;
use Illuminate\Support\Facades\App;
use Schema;

class SupportRepository extends Repository
{
    use \App\Traits\SendMailTrait;

    /**
    * Configure the Model
    *
    **/
    public function model()
    {
      return \App\Models\Support::class;
    }

    public function addTiket (array $data)
    {
        $user = App::make(\App\User::class)->findOrFail($data['user_id']);
        $fromuser = $data['fromuser'];
        $body = $data['body'];
        $department = $data['department'];
        $type = (!empty($data['type'])?$data['type']:null);
        $form_user = (!empty($data['User'])?$data['User']:null);

        $source = 1;
        //заявка от вахтовика
        if (!empty($fromuser) && (preg_match('/^vahta/', $fromuser, $matches)) ) {
            $source = 2;
            $fromuser = (int)(str_replace('vahta', '', $fromuser));
        }

        $fromuid = ( !empty($fromuser) && (int)$fromuser>0 )?(int)$fromuser:null;

        //удаляем все теги кроме ссылок, картинок и форматирования текста
        $pattern = "<p>, <br>, <strong>, <em>, <del>, <ul>, <li>, <ol>, <a>, <hr>, <img>, <h1>, <h2>, <h3>, <h4>, <h5>, <blockquote>, <pre>, <b>";
        $body = strip_tags($body, $pattern);
        if ($this->model->getAddTiket($user->getKey(), $source, $fromuid, $body, $department, $type, $form_user)){
            //Если заявка в ID отдел - отправляю уведомление на почту  пользователя
            if ((int)$department ==1 ){
                $emailcontent = "<h1>Добавлена заявка от " . $user->username . "</h1>";
                $emailcontent .= "<h2>Дата: " . date('Y-m-d') . "</h2>";
                $emailcontent .= "<h2>Как связаться с " . $user->fio . " (" . $user->username . "):</h2>";
                $emailcontent .= '<table border=0><tr><td>Телефон: </td><td>' . $user->phone . '</td></tr><tr><td>Telegram: </td><td><img src="http://bigxoomtv.com/wp-content/uploads/2014/02/telegram_icon.jpg" width="25px" height="25px"> ' . $user->phone . '</td></tr><tr><td>E-mail: </td><td>' . $user->email . '</td></tr></table><br>';
                $emailcontent .= '<table border=0><tr><td><h2>Текст заявки:</h2></td></tr><tr><td style="text-align: justify;">' . $body . '</td></tr></table>';
                $this->sendMailAdmin($emailcontent);
            }
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Отправка писем уведомлений админам (некоторым)
     * @param null $content
     * @param int $type - тип уведомления: 1 - новая заявка в ИНФО отдел, 2- закрытие заявки
     * @param null $replayTo
     */
    public function sendMailAdmin($content = null, $type=1, $replayTo = null){

        $def_body = "В информационный отдел зарегистрирована новая заявка"."\n";
        if ($type==2){ $def_body = "Заявка закрыта"."\n"; }

        $adminEmail = array("lozovski@vlspro.ru", "krylov@vlspro.ru", "sklyarov_petr@mail.ru", "forrestgtt@gmail.com");

        $bodyArr = [
            'title'=>'Новая заявка',
            'description'=>((!empty($content))?($content):($def_body)),
            'date'=>date('d-m-Y, H:i:s'),
        ];
        foreach($adminEmail as $email){
            $this->sendMail($email, $bodyArr, 'Уведомление от службы поддержки', 'support_admin');
        }

    }
}
