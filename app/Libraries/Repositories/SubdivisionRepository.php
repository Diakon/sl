<?php namespace App\Libraries\Repositories;

use App\Models\Objects;
use App\Models\Subdivision;
use Bosnadev\Repositories\Eloquent\Repository;
use Schema;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SubdivisionRepository extends Repository
{

    /**
    * Configure the Model
    *
    **/
    public function model()
    {
      return 'App\Models\Subdivision';
    }

	public function search($input)
    {

        $this->applyCriteria();
        $model = $this->model;

        if (!empty($input)) {
            $columns = Schema::getColumnListing($model->getTable());
            foreach ($columns as $attribute) {

                if ($attribute == 'object' && !empty($input[$attribute])) {
                    $model = $model->where($attribute, Objects::findOrFail($input[$attribute])->name);
                } elseif (isset($input[$attribute]) and (is_numeric($input[$attribute]) || !empty($input[$attribute]))) {
                    if (Schema::getColumnType($this->model->getTable(), $attribute) == 'integer') {
                        $model = $model->where($attribute, $input[$attribute]);
                    } else {
                        $model = $model->where($attribute, 'like', '%' . $input[$attribute] . '%');
                    }
                }
            }
        }

        return $model->paginate(50);
    }

    public function apiFindOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(1001, "Subdivision not found");
        }

        return $model;
    }

    public function apiDeleteOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(1001, "Subdivision not found");
        }

        return $model->delete();
    }
}
