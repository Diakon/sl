<?php namespace App\Libraries\Repositories\PO;

use App\Models\PO\Expense;
use Bosnadev\Repositories\Eloquent\Repository;
use Schema;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ExpenseRepository extends Repository
{

    /**
     * Configure the Model
     *
     **/
    public function model()
    {
        return Expense::class;
    }

    public function search($input)
    {
        $query = Expense::query();

        $columns = Schema::getColumnListing((new Expense)->getTable());
        $attributes = array();

        foreach($columns as $attribute)
        {
            if(isset($input[$attribute]) and !empty($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] = $input[$attribute];
            }
            else
            {
                $attributes[$attribute] =  null;
            }
        }

        return [$query->get(), $attributes];
    }

    public function apiFindOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(1001, "JobApplication not found");
        }

        return $model;
    }

    public function apiDeleteOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(1001, "JobApplication not found");
        }

        return $model->delete();
    }
}
