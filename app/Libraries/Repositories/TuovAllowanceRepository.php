<?php namespace App\Libraries\Repositories;

use App\Models\TuovAllowance;
use Bosnadev\Repositories\Eloquent\Repository;
use Schema;
use Symfony\Component\HttpKernel\Exception\HttpException;

class TuovAllowanceRepository extends Repository
{


    /**
    * Configure the Model
    *
    **/
    public function model()
    {
      return 'App\Models\TuovAllowance';
    }

	public function search($input)
    {

        $this->applyCriteria();
        $model = $this->model;

        if (!empty($input)) {
            $columns = Schema::getColumnListing($model->getTable());
            foreach ($columns as $attribute) {
                if (isset($input[$attribute]) and !empty($input[$attribute])) {
                    if ($attribute == 'from' || $attribute == 'to') {
                        $input[$attribute] = date('Y-m-d', strtotime($input[$attribute]));
                    }
                    $model = $model->where($attribute, $input[$attribute]);
                }
            }
        }
        return $model->with('anketa')->paginate(20);
    }

    public function apiFindOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(1001, "TuovAllowance not found");
        }

        return $model;
    }

    public function apiDeleteOrFail($id)
    {
        $model = $this->find($id);

        if(empty($model))
        {
            throw new HttpException(1001, "TuovAllowance not found");
        }

        return $model->delete();
    }
}
