<?php namespace App\Libraries\Repositories;

use Bosnadev\Repositories\Eloquent\Repository;
use Schema;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ContractorRepository extends Repository
{


    /**
    * Configure the Model
    *
    **/
    public function model()
    {
      return 'App\Models\Contractor\Contractor';
    }

	public function search($input)
    {

        $this->applyCriteria();
        $model = $this->model;

        if (!empty($input)) {
            $columns = Schema::getColumnListing($model->getTable());
            foreach ($columns as $attribute) {
                if (isset($input[$attribute]) and !empty($input[$attribute])) {
                    if (is_numeric($input[$attribute])) {
                        $model = $model->where($attribute, $input[$attribute]);
                    } else {
                        $model = $model->where($attribute, 'like', $input[$attribute] . '%');
                    }
                }
            }
        }
        return $model->with('types')->paginate(20);
    }
    public function myupdateRich(array $data, $id) {
        if (!($model = $this->model->find($id))) {
            abort(404);
        }
        return $model->myfill($data)->save();
    }
}
