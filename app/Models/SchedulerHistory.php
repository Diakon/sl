<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchedulerHistory extends Model
{

    /*
     *  $type - указывает тип действия пользоватлея в задаче
     *  1-создали задачу, 2-оставили комментарий, 3-изменили статус, 4-закрыли задачу, 5-удалили задачу, 6-изменение в задаче, 7-удалили исполнителя, 8-делегировали задачу
     *
     */
    public $user_id = null;
    public $action = null;
    public $tasks_id = null;
    public $type = null;
    public $type_task = 1;  //1-задача 2-акт

    public function __construct()
    {
        parent::__construct();
    }

    public function setUser($id)
    {
        $this->user_id = $id;
        return true;
    }

    public function setAction($action)
    {
        $this->action = $action;
        return true;
    }

    public function setTasks($tasks_id)
    {
        $this->tasks_id = $tasks_id;
        return true;
    }

    public function setType($type)
    {
        $this->type = $type;
        return true;
    }

    public function setTypeTask($type_task)
    {
        $this->type_task = $type_task;
        return true;
    }

    public function addHistory()
    {

        if (empty($this->user_id) || empty($this->action) || empty($this->type)) {
            return false;
        }
        //Создаем задание в таблице scheduler_history
        $data = ['user_id' => $this->user_id, 'action' => $this->action, 'type' => (int)$this->type, 'type_task' => (int)$this->type_task];

        if (!empty($this->tasks_id)) {
            $data['tasks_id'] = $this->tasks_id;
        }

        $id = \DB::table('scheduler_history')->insertGetId(
            $data
        );
        return true;
    }


}
