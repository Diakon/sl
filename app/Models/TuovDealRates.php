<?php namespace App\Models;

use DateTime as DateTime;
use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TuovDealRates extends Model
{
    use SoftDeletes;

	public $table = "tuov_deal_rates";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "deal_id",
		"rate",
		"subdivision_id",
		"from",
		"to"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "deal_id" => "integer",
		"rate" => "float",
		"subdivision_id" => "integer"
    ];

	public static $rules = [
	    "deal_id" => "required",
		"rate" => "required|numeric|min:0.01",
		"subdivision_id" => "required|integer",
		"from" => "required|date|tuov_deal_rates_date_range",
		"to" => "required|date"
	];

    public static $rulesForAjaxUpdate = [
        "rate" => "required|numeric|min:0.01",
        "from" => "required|date|tuov_deal_rates_date_range",
        "to" => "required|date"
    ];

    static function checkCollisions($sid){
        $tuovDealRates = TuovDealRates::where('subdivision_id', $sid)->get();
        $deals = [];
        foreach($tuovDealRates as $dealRate){
            $deals[$dealRate->deal_id][] = $dealRate;
        }

        $collisionReport = [];
        foreach($deals as $deal_id =>$dealsData){

            foreach($dealsData as $dealRate){
                $deal = $dealRate;
                foreach($dealsData as $nextDeal){
                    if($deal==$nextDeal) continue;
                    $date1start = new DateTime($deal->from);
                    $date1end = new DateTime($deal->to);
                    $date2start = new DateTime($nextDeal->from);
                    $date2end = new DateTime($nextDeal->to);

                    if( (($date1start<=$date2end)&&($date1end>=$date2start)) ){
                        $collisionReport[min($deal->id,$nextDeal->id)][] = max($deal->id,$nextDeal->id);
                    }

                }
            }
        }
        foreach($collisionReport as $deal_id=>$deal_arr){
            $collisionReport[$deal_id] = array_unique($deal_arr);
        }
        return $collisionReport;

    }


    public function subdivision()
    {
        return $this->belongsTo(\App\Models\Subdivision::class);
    }

    public function deal()
    {
        return $this->belongsTo(\App\Models\Deal::class);
    }
}
