<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class TuovAllowance extends Model
{
    
	public $table = "tuov_allowance";
    public $timestamps = false;  //отключает необходимость в поле updated_at - иначе требует
    

	public $fillable = [
	    "id",
		"id_anketa",
		"allowance",
		"from",
		"to",
        "deal_id",
        "subdivision_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"id_anketa" => "integer",
		"allowance" => "float"
    ];

    public static $rules = [
        'id_anketa' => "required",
        'allowance' => "required",
        'from' => "required",
        'to' => "required",
    ];

    public function anketa()
    {
        return $this->hasOne('App\Models\PersMain', 'id_anketa', 'id_anketa');
    }
}
