<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class PersMain extends Model
{
	public function passport()
	{
		return $this->hasOne('App\Models\PersPassport', 'id_anketa', 'id_anketa');
	}
	public function status()
	{
		return $this->hasOne('App\Models\PersStatus', 'id_anketa', 'id_anketa');
	}
	public function dormitory()
	{
		return $this->belongsTo('App\Models\DirDormitoryFms', 'dormitory_fms_id');
	}

	public function advance()
	{
		return $this->hasMany('App\Models\TuovAdvance', 'id_anketa', 'anketa_id');
	}
    
	public $table = "pers_main";

	protected $primaryKey = 'id_anketa';

	public $timestamps = false;

	public $fillable = [
	    "id_anketa",
		"old_id",
		"create_date",
		"dog_num",
		"meet_date",
		"firstname",
		"lastname",
		"patronymic",
		"nation",
		"birth_date",
		"birth_place",
		"age",
		"gender",
		"family_status",
		"education",
		"speciality",
		"find_with",
		"subdivision",
		"criminal",
		"to_army",
		"in_army",
		"pmo",
		"criminal_state",
		"criminal_status",
		"medical_type",
		"medical_status",
		"medical_expire",
		"disabled",
		"disable_date",
		"badge_number",
		"no_money",
		"no_sim"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id_anketa" => "integer",
		"old_id" => "integer",
		"dog_num" => "string",
		"firstname" => "string",
		"lastname" => "string",
		"patronymic" => "string",
		"nation" => "string",
		"birth_place" => "string",
		"age" => "boolean",
		"gender" => "boolean",
		"family_status" => "boolean",
		"education" => "boolean",
		"speciality" => "string",
		"find_with" => "string",
		"subdivision" => "string",
		"criminal" => "boolean",
		"to_army" => "boolean",
		"in_army" => "boolean",
		"pmo" => "string",
		"criminal_state" => "string",
		"criminal_status" => "boolean",
		"medical_type" => "boolean",
		"medical_status" => "boolean",
		"disabled" => "boolean",
		"badge_number" => "integer",
		"no_money" => "boolean",
		"no_sim" => "boolean"
    ];

	public static $rules = [
	    
	];

	/**
	 * Анкета белоруса имеющего статус "Принят"
	 * @param $query
	 * @return mixed
	 */
	public function scopeBelarusAccepted($query)
	{
		return $query->whereNation(2)->whereHas('status', function($q) {
			$q->where('status_name', 9);
		});
	}

	public function saveDormitory() {
		$this->dormitory()->associate(DirDormitoryFms::all()->random(1))->save();
	}

	public function getFioAttribute() {
		return sprintf('%s %s %s', $this->lastname, $this->firstname, $this->patronymic);
	}
}
