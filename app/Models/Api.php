<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Api extends Model
{
    
	public $table = "api_list";
    public $timestamps = false;  //отключает необходимость в поле updated_at - иначе требует

    public $model;

    //Возращает: 900 - не правельный токен,  500 - не известная ошибка,  700 - ошибка записи в БД, 200- все прошло успешно, 800 - не верные данные переданы для обработки
    public $resultArr = ['err_token'=>900, 'unknown_error'=>500, 'write_error'=>700, 'success'=>200, 'data_error'=>800];

	public $fillable = [
	    "id",
		"token",
		"status_api",
		"access",
		"created_at"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"token" => "string",
		"status_api" => "boolean",
		"access" => "string"
    ];

    public static $rules = [
        'create' => [
            'token' => "required|min:10|unique:api_list,token",
            'status_api' => "required",
            'access' => "required",
        ],
        'update' => [
            'token' => "required|min:10",
            'status_api' => "required",
            'access' => "required",
        ]
    ];


    public static $messages = array(
        'required' => "Поле :attribute обязательно для заполнения!",
        'unique' => "Токен :attribute уже существует!",
        'same' => "Пароли не совпадают!",
        'min' => "Длина поля :attribute должна быть минимум :min символов!"
    );


    /**
     * $token - токен переданный  для проверки
     * $param - параметры для апи
     */
    public function processingAPI( $token, $param = array() )
    {
        $modelToken = \DB::table($this->table)->where('token', $token)->where('status_api', 1)->first();
        if (empty($modelToken)){ return $this->resultArr['err_token']; }
        //Токен верный - обрабатывем API в соответствии с указаным в access

        $operation_result = null;
        switch ($modelToken->access) {
            case 'addNewITApplication':
                $operation_result = $this->addNewITApplication($param);
                break;
        }



        //Пишем в историю
        $this->apiLog($_SERVER["REMOTE_ADDR"], $modelToken->id, $param, $operation_result);
        return $operation_result;
    }


    /**
     * Новая IT заявка
     * @param $param
     */
    private function addNewITApplication($param){
        if ( !is_array($param) || !isset($param['phone']) || !isset($param['body']) || empty($param['phone']) || empty($param['body']) ){ return $this->resultArr['data_error']; }
        $department = ((isset($param['department']) && (int)$param['department']>0)?((int)$param['department']):1);  //в какой отдел   1-инфо 2-фин 3-оп
        //Ищим пользователя с таким телефоном, если нет - ставим vilis
        $user = \DB::table('users')->where('phone', 'LIKE', $param['phone'])->first();
        if (empty($user)){
            //берем вилеса
            $user = \DB::table('users')->where('username', 'LIKE', 'vilis')->first();
        }
        //Создаем заявку
        $pattern = "<p>, <br>, <strong>, <em>, <del>, <ul>, <li>, <ol>, <a>, <hr>, <img>, <h1>, <h2>, <h3>, <h4>, <h5>, <blockquote>, <pre>, <b>";
        $body = strip_tags($param['body'], $pattern);
        $this->model = new \App\Models\Support();
        if ($this->model->getAddTiket($user->userid, 1, null, $body, (int)$department)){ return $this->resultArr['success']; }
        else {  return $this->resultArr['write_error'];  }
        return $this->resultArr['unknown_error'];
    }

    /**
     * Запись в логи
     * @param $ip   - ip с которго зашел пользователь
     * @param $api  - ID записи из таблицы api_list_id с котороым работали
     * @param $data_processing   -  данные которые переданы в апи
     * @param $code   -  код операции 900 - не правельный токен,  500 - не известная ошибка,  700 - ошибка записи в БД, 200- все прошло успешно, 800 - не верные данные переданы для обработки
     */
    private function apiLog($ip, $api, $data_processing, $code){
        $data_processing = base64_encode(serialize($data_processing));
        $id = \DB::table('api_log')->insertGetId(
            array('api_list_id' => $api, 'user_ip' => $ip, 'data_processing' => $data_processing, 'operation_code' => $code)
        );
        return $id;
    }

}
