<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DirDormitoryFms extends Model
{
	public function main()
	{
		return $this->hasMany('App\Models\PersMain', 'id', 'dormitory_fms_id');
	}
    
	public $table = "dir_dormitory_fms";


}
