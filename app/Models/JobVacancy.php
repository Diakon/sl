<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class JobVacancy extends Model
{
	protected static function boot() {
		parent::boot();

		static::deleting(function($vacancy) {
			$vacancy->children()->delete();
		});
	}

	public $table = "po_job_vacancy";

	public $fillable = [
	    "id",
		"name",
		"parent_id",
		"updated_at",
		"created_at"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"name" => "string",
		"parent" => "integer"
    ];

	public static $rules = [
		'create' => [
			'name' => "required|unique:po_job_vacancy",
			'parent_id' => "min:0",
		],
		'update' => [
			'name' => "required",
			'parent_id' => "min:0",
		]
	];
	public static $messages = array(
		'required' => "Поле `Вакансия` обязательно для заполнения!",
		'unique' => "Такая вакансия уже существует!",
		'same' => "Пароли не совпадают!",
		'min' => "Длина поля :attribute должна быть минимум :min символов!",
		'exists' => "Объекта :attribute не существует!"
	);

	/**
	 * ЗАпрос только для категорий верхнего уровня
	 *
	 * @return \Illuminate\Database\Eloquent\Builder
	 */
	public function scopeParents($query)
	{
		return $query->where('parent_id', 0);
	}

	/**
	 * Список вакансий для формирования Form::select()
	 * @return array
	 */
	public static function lists()
	{
		$lists = [];
		foreach (self::tree() as $tree) {
			foreach ($tree->childrens as $children) {
				$lists[$tree->name][$children->id] = $children->name;
			}
		}
		return $lists;
	}

	public function scopeTree()
	{
		return static::with('childrens')->where('parent_id', '=', 0)->get();
	}



	public function parent()
	{
		return $this->belongsTo(self::class, 'parent_id');
	}
	public function childrens()
	{
		return $this->hasMany(self::class, 'parent_id', 'id');
	}
}
