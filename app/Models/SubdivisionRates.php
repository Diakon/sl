<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SubdivisionRates extends Model
{
    protected $nullable = ['rank'];

    protected static function boot() {
        parent::boot();

        /**
         * Отмечает значение по умолчанию (всегда должно быть установлено у одной из записей для подразделения)
         */
        $check_default = function ($item) {
            $rows = self::whereSubdivisionId($item->subdivision_id)->whereDefault(1)->get();
            if (!$rows->count()) {
                if ($model = self::whereSubdivisionId($item->subdivision_id)->first()) {
                    $model->default=1;
                    $model->save();
                }
            } elseif($rows->count()>1) {
                self::whereSubdivisionId($item->subdivision_id)->where('id', '<>', $item->id)->update(['default' => 0]);
            }
        };
        static::deleted($check_default);
        static::created($check_default);
        static::updated($check_default);
        static::saving(function($model) {
            foreach ($model->attributes as $key => &$value) {
                if (in_array($key, $model->nullable)) {
                    empty($value) and $value = null;
                }
            }
        });
    }

    public $timestamps = false;
    protected $dates = ['timestamp'];

    public static $rules = [
        "position" => 'required|min:2',
        "rate" => 'required|min:0.01',
        "rank" => 'integer',
        "date_from" => 'required|date',
        "date_to" => 'required|date',
        "default" => 'required|in:0,1',
        "without_allowance" => 'required|in:0,1',
        "subdivision_id" => 'required|integer',
        "user_id" => 'required|integer',
    ];

    public $fillable = [
        "position",
        "rate",
        "rank",
        "date_from",
        "date_to",
        "default",
        "without_allowance",
        "subdivision_id",
        "user_id"
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "rate" => "float",
        "date_from" => "string",
        "date_to" => "string",
        "user_id" => "integer",
        "subdivision_id" => "integer"
    ];

    public function subdivision()
    {
        return $this->belongsTo(\App\Models\Subdivision::class);
    }
}
