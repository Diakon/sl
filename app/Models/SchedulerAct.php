<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchedulerAct extends Model   {


    public $name = null;
    public $task = array();
    public $attach_files = array();


	public function __construct() {
		parent::__construct();
	}

    public function setName($name){ $this->name = $name; return true; }
    public function setTasks($task){ $this->task = $task; }
    public function setAttachFiles($file_name, $url_to_file){ $this->attach_files[] = array('file_name'=>$file_name, 'url_to_file'=>$url_to_file); return true; }

    public function addAct(){
        if (empty($this->name) || empty($this->task)){ return false; exit; }

        $modelTasks = new \App\Models\SchedulerTasks();

        //Создаем новый Акт
        $act_id = \DB::table('scheduler_act')->insertGetId(
            array('user_id' => \Auth::id(), 'name' => $this->name, 'status' => 0 )
        );
        //Создаем задаия акта
        foreach ($this->task as $key=>$value){
            if (!empty($value['burn'])) { $modelTasks->setBurn(1); }
            $modelTasks->setName($value['name']);
            $modelTasks->setDescription($value['description']);
            $modelTasks->setDeadline($value['deadline']);
            $modelTasks->setTypeTask(2); //2 - тип задания - часть акта

            if(!empty($value['worker_department'])){$modelTasks->setWorkerDepartment($value['worker_department']);}
            if(!empty($value['worker_users'])){$modelTasks->setWorkerUsers($value['worker_users']);}

            $modelTasks->addActTasks($act_id);
        }


        //Грузим файлы, если добавлены в Акт
        if (!empty($this->attach_files)){
            foreach ($this->attach_files as $key=>$val){
                $this->taskFile($act_id, 1, $val['file_name'], $val['url_to_file']);
            }
        }

        $this->clearData();

        //Пишем в историю
        $modelHistory = new \App\Models\SchedulerHistory();
        $modelHistory->setUser(\Auth::id());
        $modelHistory->setTasks($act_id);
        $modelHistory->setType(1);
        $modelHistory->setTypeTask(2);
        $modelHistory->setAction( 'Пользователь '.(\DB::table('users')->where('userid', \Auth::id())->first()->username).' создал новый Акт ['.$act_id.']' );
        $modelHistory->addHistory();
    }

    /*
     * $acrions - тип действия с файлом
     * 1 - добавление файла
     * 2 - удаление файла
     * $file_name - имя файла, которое было у файла на компе пользователя
     * $url_to_file - имя файла, которое он получил после загрузки на сервер (меняем имя на свое, т.к. могут быть проблеммы из-за руских имен файлов)
     * $type - файл относится к заданию или акту 1- задание, 2-акт
     */
    public function taskFile($id_akt = null, $acrions = null, $file_name = null, $url_to_file = null, $type = 2){
        //добавление файла
        if ($acrions == 1){
            if (empty($id_akt) || empty($file_name) || empty($url_to_file)){ return false; exit;}
            $file_id = \DB::table('scheduler_file')->insertGetId(
                array('task_id' => $id_akt, 'file_name' => $file_name, 'url_to_file' => 'uploads/'.$url_to_file, 'type_task' => $type)
            );
            if (isset($file_id)){ return true; }
            else { return false; }
        }
        //Удаление файла
        if ($acrions == 2){
            if (empty($url_to_file)){ return false; exit;}
            //Проверяем, есть ли запись в БД
            $chkRecords = \DB::table('scheduler_file')->where('url_to_file', 'uploads/'.$url_to_file)->get();
            if (!empty($chkRecords)){ \DB::table('scheduler_file')->where('url_to_file', 'uploads/'.$url_to_file)->delete(); }
            if (unlink(\public_path().'/uploads/'.$url_to_file)){ return true;}
            else { return false; }
        }
    }

    public function getAct($id = null, $sort = "ASC"){
        if (empty($id)){ return false; exit; }
        $data['act'] = current(\DB::table('scheduler_act')->where('id',$id)->get());
        $data['tasks'] = \DB::table('scheduler_tasks')->where('type_task',2)->where('user_id',$id)->orderBy('id', $sort)->groupBy('user_id', 'name')->get();
        return $data;
    }

    //  Закрытие/открытие акта и подчиненных ему задач, $action = 1 - закрыть, $action = 2 - открыть
    public function completedAct($id = null, $action = 1){
        if ( empty($id) || empty($action) ){ return false; }
        $updateArr = array();
        switch ($action) {
            case 1:
                //закрывают
                //Закрываю подчиненные задачи
                $updateArr['task'] = array('status' => 100, 'completed' => 1, 'update_at'=>date("Y-m-d H:i:s"));
                //Закрываю акт
                $updateArr['act'] = array('status' => 100, 'completed' => 1, 'update_at'=>date("Y-m-d H:i:s"));
                break;
            case 2:
                //открывают павторно
                //Открываю подчиненные задачи
                $updateArr['task'] = array('completed' => 0, 'update_at'=>date("Y-m-d H:i:s"));
                //Открываю акт
                $updateArr['act'] = array('completed' => 0, 'update_at'=>date("Y-m-d H:i:s"));
                break;
        }

        \DB::table('scheduler_tasks')
            ->where('user_id', $id)
            ->where('type_task', 2)
            ->update($updateArr['task']);
        //Закрываю акт
        \DB::table('scheduler_act')
            ->where('id', $id)
            ->update($updateArr['act']);

        return true;
    }

    //Проверяет выполненые задачи в Акте. Выставляет статус выполнения и закрывает акт если задачи все выполнены
    public function chkComplite($id = null){
        if ($id == null){ return false; }

        $fullCount = \DB::table('scheduler_tasks')->where('user_id', $id)->where('type_task', 2)->count();
        $compliteCount = \DB::table('scheduler_tasks')->where('user_id', $id)->where('type_task', 2)->where('completed', 1)->count();
        $statusComplite = ($compliteCount * 100)/$fullCount;
        $akt = \DB::table('scheduler_act')->where('id', $id)->first();
        $compliteAkt = $akt->completed;
        if ($statusComplite<20){ $statusComplite = 0; }
        elseif ($statusComplite >= 20 && $statusComplite<40 ){ $statusComplite = 20; }
        elseif ($statusComplite >= 40 && $statusComplite<60 ){ $statusComplite = 40; }
        elseif ($statusComplite >= 60 && $statusComplite<80 ){ $statusComplite = 60; }
        elseif ($statusComplite >= 80 && $statusComplite<100 ){ $statusComplite = 80; }
        elseif ($statusComplite >= 100 ){ $statusComplite = 100;  $compliteAkt = 1; }
        \DB::table('scheduler_act')
            ->where('id', $id)
            ->update(array('status'=>$statusComplite, 'completed'=>$compliteAkt, 'update_at'=>date("Y-m-d H:i:s")));
        $typeHistory = 3;
        $txt = "Статус акта изменен, т.к. была закрыта одна из задач этого акта.";
        if ($akt->completed != $compliteAkt){
            if($compliteAkt == 1) { $typeHistory = 4; $txt = 'Пользователь '.(\DB::table('users')->where('userid', \Auth::id())->first()->username).' закрыл задачу. Т.к. все задачи закрыты - акт закрыт.'; }
            else  { $typeHistory = 1; $txt = "Создан новый акт."; }
        }
        //Пишем в историю
        $modelHistory = new \App\Models\SchedulerHistory();
        $modelHistory->setUser(\Auth::id());
        $modelHistory->setTasks($id);
        $modelHistory->setType($typeHistory);
        $modelHistory->setTypeTask(2);
        $modelHistory->setAction( $txt );
        $modelHistory->addHistory();
    }

    //удаление акта
    public function deleteAct($id = null){
        if (empty($id)){ return false; }
        //Удаляем исплнителей и задачи в акте
        $task = \DB::table('scheduler_tasks')->where('user_id',$id)->where('type_task',2)->get();
        //удаляем исполнителей
        foreach ($task as $data){
            \DB::table('scheduler_worker')->where('scheduler_tasks_id', $data->id)->delete();
        }
        //удаляем задачи
        \DB::table('scheduler_tasks')->where('user_id',$id)->where('type_task',2)->delete();
        //Удаляем акт
        \DB::table('scheduler_act')->where('id',$id)->delete();

        //Пишем в историю
        $modelHistory = new \App\Models\SchedulerHistory();
        $modelHistory->setUser(\Auth::id());
        $modelHistory->setTasks($id);
        $modelHistory->setTypeTask(2);
        $modelHistory->setType(5);
        $userName = current(\DB::table('users')->where('userid', \Auth::id())->get());
        $modelHistory->setAction( 'Акт и входящие в него задачи были удалены пользователем '.($userName->username) );
        $modelHistory->addHistory();

        return true;
    }

    //очищает переменные
    function clearData(){
        $this->name = null;
        $this->task = array();
        return true;
    }







}
