<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use  App\Traits\SendMailTrait;

class SchedulerTasks extends Model
{

    use SendMailTrait;

    public $type_action = [1 => 'Задача', 2 => 'Акт'];
    public $status_arr = [0 => 'Новая', 20 => 'Выполнено на 20%', 40 => 'Выполнено на 40%', 60 => 'Выполнено на 60%', 80 => 'Выполнено на 80%', 100 => 'Выполнено на 100%'];
    public $name = null;
    public $description = "";
    public $worker_users = array();
    public $worker_department = array();
    public $status = 0;
    public $burn = null;
    public $deadline = null;
    public $attach_files = array();
    public $completed = 0;
    public $type_task = 1;


    public function __construct()
    {
        parent::__construct();
    }

    public function getUserOrders($uid)
    {
        $result = \DB::table('users_orders')->where('user_id', $uid)->get();
        return $result;
    }

    public function  getTasksUserData($uid)
    {

        $result = \DB::table('users_orders')
            ->leftJoin('dir_department', 'dir_department.id', '=', 'users_orders.order_id')
            ->leftJoin('users', 'users.userid', '=', 'users_orders.user_id')
            ->select('users_orders.id as id',
                'users.username as username',
                'users.fio as fio',
                'users_orders.order_id as order_id',
                'users_orders.rule as rule')
            ->where('users_orders.user_id', $uid)
            ->get();

        return $result;

    }

    public function setName($name)
    {
        $this->name = $name;
        return true;
    }

    public function setDescription($description)
    {
        $this->description = $description;
        return true;
    }

    public function setWorkerUsers($worker_users)
    {
        $this->worker_users = $worker_users;
        return true;
    }

    public function setWorkerDepartment($worker_department)
    {
        $this->worker_department = $worker_department;
        return true;
    }

    public function setStatus($status)
    {
        $this->status = $status;
        return true;
    }

    public function setBurn($burn)
    {
        $this->burn = $burn;
        return true;
    }

    public function setDeadline($deadline)
    {
        $this->deadline = date('Y-m-d', strtotime($deadline));
        return true;
    }

    public function setAttachFiles($file_name, $url_to_file)
    {
        $this->attach_files[] = array('file_name' => $file_name, 'url_to_file' => $url_to_file);
        return true;
    }

    public function setCompleted($completed)
    {
        $this->completed = $completed;
        return true;
    }

    public function setTypeTask($type_task)
    {
        $this->type_task = $type_task;
        return true;
    }

    /**
     * Создает задачи акта
     *
     */
    public function addActTasks($actID = null)
    {
        if (empty($actID) || empty($this->name) || empty($this->deadline) || (empty($this->worker_users) && empty($this->worker_department))) {
            return false;
        }

        //каждому пользователю своя копия задачи
        //1 - получаю список пользователей которым надо делать задачи
        $arrUserDepartament = array();
        if (!empty($this->worker_department)) {
            foreach ($this->worker_department as $value) {
                //получаю пользователей в департамента
                foreach (\DB::table('users_orders')->where('order_id', $value)->where('type_order', 1)->get() as $data) {
                    $arrUserDepartament[$data->user_id] = 1;
                }
            }
        }
        if (!empty($this->worker_users)) {
            foreach ($this->worker_users as $value) {
                $arrUserDepartament[$value] = 1;
            }
        }

        //2 - каждому пользователю создаем задание
        foreach ($arrUserDepartament as $key => $value) {
            $task_id = \DB::table('scheduler_tasks')->insertGetId(
                array('user_id' => $actID, 'name' => $this->name, 'description' => $this->description, 'type_task' => $this->type_task,
                    'status' => $this->status, 'burn' => $this->burn, 'deadline' => $this->deadline, 'update_at' => date('Y-m-d h:i:s'))
            );
            $user_id = \DB::table('scheduler_worker')->insertGetId(
                array('scheduler_tasks_id' => $task_id, 'worker_id' => $key, 'type' => 1)
            );


            //Пишем в историю
            $modelHistory = new \App\Models\SchedulerHistory();
            $modelHistory->setUser(\Auth::id());
            $modelHistory->setTasks($task_id);
            $modelHistory->setType(1);
            $modelHistory->setAction('Как часть Акта ' . $actID . ' была создана новая задача ' . $task_id . ' для пользователя ' . $user_id);
            $modelHistory->addHistory();
        }


        $this->clearData();


    }

    public function addTask()
    {

        if (empty($this->name) || empty($this->deadline) || (empty($this->worker_users) && empty($this->worker_department))) {
            return false;
        }
        //Создаем задание в таблице scheduler_tasks
        $task_id = \DB::table('scheduler_tasks')->insertGetId(
            array('user_id' => \Auth::id(), 'name' => $this->name, 'description' => $this->description, 'type_task' => $this->type_task,
                'status' => $this->status, 'burn' => $this->burn, 'deadline' => $this->deadline, 'update_at' => date('Y-m-d H:i:s'))
        );


        if (!empty($this->attach_files)) {
            foreach ($this->attach_files as $key => $val) {
                $this->taskFile($task_id, 1, $val['file_name'], $val['url_to_file']);
            }
        }


        //Создаем запись о работниках/отделах участвующих в задании в таблице scheduler_worker
        if (!empty($this->worker_users)) {
            foreach ($this->worker_users as $value) {
                \DB::table('scheduler_worker')->insert(
                    array('scheduler_tasks_id' => $task_id, 'worker_id' => $value, 'type' => 1)
                );
            }
        }
        //Если задача на отдел
        if (!empty($this->worker_department)) {
            foreach ($this->worker_department as $value) {
                \DB::table('scheduler_worker')->insert(
                    array('scheduler_tasks_id' => $task_id, 'worker_id' => $value, 'type' => 2)
                );
            }
        }


        $this->clearData();

        //Пишем в историю
        $modelHistory = new \App\Models\SchedulerHistory();
        $modelHistory->setUser(\Auth::id());
        $modelHistory->setTasks($task_id);
        $modelHistory->setType(1);
        $userName = current(\DB::table('users')->where('userid', \Auth::id())->get());
        $modelHistory->setAction('Была создана новая задача пользователем ' . ($userName->username));
        $modelHistory->addHistory();

        //пишем в рассылку
        $userMail = array();
        foreach (\DB::table('scheduler_worker')->where('scheduler_tasks_id', $task_id)->get() as $data) {
            //Смотрим на type. Если 1 - берем email и данные из таблицы пользователей по id в поле worker_id,
            //если type=2 то из таблицы users_orders получаем id пользователей (type_order=1 AND order_id)
            if ($data->type == 1) {
                //пользователь
                $userMail[] = current(\DB::table('users')->where('userid', $data->worker_id)->get());
            } elseif ($data->type == 2) {
                //отдел
                foreach (\DB::table('users_orders')->where('type_order', 1)->where('order_id', $data->worker_id)->get() as $dataOrders) {
                    $userMail[] = current(\DB::table('users')->where('userid', $dataOrders->user_id)->get());
                }
            }
        }
        $tasks = current(\DB::table('scheduler_tasks')->where('id', $task_id)->get());
        //Добавляем автора заявки в рассылку
        $userMail[] = current(\DB::table('users')->where('userid', $tasks->user_id)->get());
        foreach ($userMail as $key => $value) {
            if (empty($value->email)) {
                continue;
            }
            $bodyArr = [
                'taskid' => $tasks->id,
                'titlerow' => 'Создана новая задача',
                'title' => $tasks->name,
                'description' => $tasks->description,
                'username' => $userName->username,
                'userid' => $userName->userid,
                'datetime' => $tasks->created_at,
                'content' => '',
                'comment' => '',
            ];
            $this->sendMailDelivery($value->email, $bodyArr, 'Уведомление с сайта cloud.secondlab.ru. Новая задача ' . $tasks->name, 'scheduler');

        }

        return true;
    }

    /*
     * $acrions - тип действия с файлом
     * 1 - добавление файла
     * 2 - удаление файла
     * $file_name - имя файла, которое было у файла на компе пользователя
     * $url_to_file - имя файла, которое он получил после загрузки на сервер (меняем имя на свое, т.к. могут быть проблеммы из-за руских имен файлов)
     * $type - файл относится к заданию или акту 1- задание, 2-акт
     */
    public function taskFile($id_task = null, $acrions = null, $file_name = null, $url_to_file = null, $type = 1)
    {
        //добавление файла
        if ($acrions == 1) {
            if (empty($id_task) || empty($file_name) || empty($url_to_file)) {
                return false;
                exit;
            }
            $file_id = \DB::table('scheduler_file')->insertGetId(
                array('task_id' => $id_task, 'file_name' => $file_name, 'url_to_file' => 'uploads/' . $url_to_file, 'type_task' => $type)
            );
            if (isset($file_id)) {
                return true;
            } else {
                return false;
            }
        }
        //Удаление файла
        if ($acrions == 2) {
            if (empty($url_to_file)) {
                return false;
                exit;
            }
            //Проверяем, есть ли запись в БД
            $chkRecords = \DB::table('scheduler_file')->where('url_to_file', 'uploads/' . $url_to_file)->get();
            if (!empty($chkRecords)) {
                \DB::table('scheduler_file')->where('url_to_file', 'uploads/' . $url_to_file)->delete();
            }
            if (unlink(\public_path() . '/uploads/' . $url_to_file)) {
                return true;
            } else {
                return false;
            }
        }
    }


    public function updateTask($id_task = null)
    {

        if (empty($id_task)) {
            return false;
        }
        $updateArr = array();
        if (!empty($this->name)) {
            $updateArr['name'] = $this->name;
        }
        if (!empty($this->description)) {
            $updateArr['description'] = $this->description;
        }
        if (!empty($this->status)) {
            $updateArr['status'] = $this->status;
        }
        if (!empty($this->burn)) {
            $updateArr['burn'] = $this->burn;
        }
        if (!empty($this->deadline)) {
            $updateArr['deadline'] = $this->deadline;
        }
        $updateArr['completed'] = $this->completed;
        $updateArr['update_at'] = date('Y-m-d H:i:s');

        \DB::table('scheduler_tasks')
            ->where('id', $id_task)
            ->update($updateArr);


        //Если добавили исполнителей  - добавляем к заданию
        $task = \DB::table('scheduler_tasks')->where('id', $id_task)->first();
        //Если задача часть акта - создаем новые задачи
        if ($task->type_task == 2) {
            $this->addActTasks($task->id);
        } else {
            if (!empty($this->worker_users)) {
                foreach ($this->worker_users as $value) {
                    \DB::table('scheduler_worker')->insert(
                        array('scheduler_tasks_id' => $id_task, 'worker_id' => $value, 'type' => 1)
                    );
                }
            }
            if (!empty($this->worker_department)) {
                foreach ($this->worker_department as $value) {
                    \DB::table('scheduler_worker')->insert(
                        array('scheduler_tasks_id' => $id_task, 'worker_id' => $value, 'type' => 2)
                    );
                }
            }
        }

        //Если делигировали задачу (тот кто делигирует, не автор), убераю из задачи прежнего исполнителя
        $deligate = null;
        if (\Auth::id() != $task->user_id) {
            //if ($task->type_task == 1){  }
            \DB::table('scheduler_worker')->where('scheduler_tasks_id', $task->id)->where('type', 1)->where('worker_id', \Auth::id())->delete();
            $deligate = 1;
        }


        //Если задача - часть Акта, вычисляем процент звершения акта и если закрыты ВСЕ задачи акта - закрываю акт
        $task = \DB::table('scheduler_tasks')->where('id', $id_task)->where('type_task', 2)->first();

        if (!empty($task) && $task->type_task == 2) {
            $this->modelAct = new \App\Models\SchedulerAct();
            $this->modelAct->chkComplite($task->user_id);
        }

        //Пишем в историю
        $modelHistory = new \App\Models\SchedulerHistory();
        $modelHistory->setUser(\Auth::id());
        $modelHistory->setTasks($id_task);
        $userName = current(\DB::table('users')->where('userid', \Auth::id())->get());
        if ((int)$this->completed == 1) {
            //Задача закрыта
            $text = "Пользователь " . ($userName->username) . " закрыл задачу";
            $modelHistory->setType(4);
        } else {
            $text = "Пользователь " . ($userName->username) . " внес изменения в задачу:</BR>";
            if (!empty($this->name)) {
                $text .= "Изменили название заявки на " . $this->name . "</BR>";
            }
            if (!empty($this->description)) {
                $text .= "Изменили описание на " . $this->description . "</BR>";
            }
            if (!empty($this->status)) {
                $text .= "Изменили статус выполнения заявки на " . $this->status . "</BR>";
            }
            if (!empty($this->burn)) {
                $text .= "Заявке был присвоен статус 'срочно'</BR>";
            }
            if (!empty($this->deadline)) {
                $text .= "Был изменен срок сдачи заявки на дату " . $this->deadline . "</BR>";
            }
            $modelHistory->setType(6);
            if (!empty($deligate)) {
                $text .= "Пользователь " . ($userName->username) . " делегировал задачу пользователю id=" . (current($this->worker_users)) . "</BR>";
                $modelHistory->setType(8);
            }
        }
        $userName = current(\DB::table('users')->where('userid', \Auth::id())->get());
        $modelHistory->setAction($text);
        $modelHistory->addHistory();


        //пишем в рассылку
        $userMail = array();
        foreach (\DB::table('scheduler_worker')->where('scheduler_tasks_id', $id_task)->get() as $data) {
            //Смотрим на type. Если 1 - берем email и данные из таблицы пользователей по id в поле worker_id,
            //если type=2 то из таблицы users_orders получаем id пользователей (type_order=1 AND order_id)
            if ($data->type == 1) {
                //пользователь
                $userMail[] = current(\DB::table('users')->where('userid', $data->worker_id)->get());
            } elseif ($data->type == 2) {
                //отдел
                foreach (\DB::table('users_orders')->where('type_order', 1)->where('order_id', $data->worker_id)->get() as $dataOrders) {
                    $userMail[] = current(\DB::table('users')->where('userid', $dataOrders->user_id)->get());
                }
            }
        }


        $tasks = \DB::table('scheduler_tasks')->where('id', $id_task)->first();
        //Добавляем автора заявки в рассылку
        $userMail[] = \DB::table('users')->where('userid', $tasks->user_id)->first();
        $modelEmail = new \App\Models\EmailDelivery();
        foreach ($userMail as $key => $value) {
            if (empty($value->email)) {
                continue;
            }
            $body = array(
                'titlerow' => 'Уведомление с сайта cloud.secondlab.ru. Изменения в задаче',
                'title' => $tasks->name,
                'taskid' => $tasks->id,
                'comment' => 'В задачу ' . $tasks->name . ' [' . $tasks->id . ']' . ' внесены изменения. ' . $text
            );
            $modelEmail->setRecipient($value->email);
            $modelEmail->setTemplate('scheduleraction');
            $modelEmail->setTitle('Уведомление с сайта cloud.secondlab.ru. Изменения в задаче ' . $tasks->name . ' [' . $tasks->id . ']');
            $modelEmail->setBody($body);
            $modelEmail->addRecord();

        }


        /*
        //отправялем письма получателям задания
        $userMail = array();
        foreach ( \DB::table('scheduler_worker')->where('scheduler_tasks_id', $id_task)->get() as $data ){
            //Смотрим на type. Если 1 - берем email и данные из таблицы пользователей по id в поле worker_id,
            //если type=2 то из таблицы users_orders получаем id пользователей (type_order=1 AND order_id)
            if ($data->type == 1){
                //пользователь
                $userMail[] = current(\DB::table('users')->where('userid', $data->worker_id)->get());
            }
            elseif($data->type == 2) {
                //отдел
                foreach ( \DB::table('users_orders')->where('type_order', 1)->where('order_id', $data->worker_id)->get() as $dataOrders ){
                    $userMail[] = current(\DB::table('users')->where('userid', $dataOrders->user_id)->get());
                }
            }
        }
        $tasks = current(\DB::table('scheduler_tasks')->where('id', $id_task)->get());
        $userMail[] = current(\DB::table('users')->where('userid', $tasks->user_id)->get());
        foreach ($userMail as $key=>$value){
            if (empty($value->email)){  continue; }
            preg_match('|([a-z0-9_\.\-]{1,20})@([a-z0-9\.\-]{1,20})\.([a-z]{2,4})|is', $value->email, $match);
            if (!empty($match)){
                $this->sendMail($value->email, 'В задачу '.$tasks->name.' ['.$tasks->id.']'.' внесены изменения. '.$text, $userName->username, 'Изменения в задаче '.$tasks->name.' ['.$tasks->id.']');
            }
        }
        */


        $this->clearData();
        return true;

    }

    //Список заданий пользователя
    //$sort - порядок сортировки, DESC - по убыванию (первыми выводим новые), ASC- по возрастанию
    public function getTaskListUser($uid, $sort = 'DESC')
    {
        $result = null;
        //Исходящие
        $data[0] = \DB::table('scheduler_tasks')->select('scheduler_tasks.*', \DB::raw('0 as akt'))->where('user_id', $uid)->where('completed', 0)->orderBy('id', $sort)->get();
        $data[1] = \DB::table('scheduler_act')->select('scheduler_act.*', \DB::raw('1 as akt'))->where('user_id', $uid)->where('completed', 0)->orderBy('id', $sort)->get();
        $result['outbox'] = array_merge($data[0], $data[1]);

        //Входящие
        //Определяю к какому отделу относится пользователь
        $orders[] = $uid;
        foreach (\DB::table('users_orders')->where('type_order', 1)->where('user_id', $uid)->get() as $data) {
            $orders[] = $data->order_id;
        }

        $result['inbox'] =
            \DB::table('scheduler_tasks')
                ->leftJoin('scheduler_worker', 'scheduler_tasks.id', '=', 'scheduler_worker.scheduler_tasks_id')
                ->select('scheduler_tasks.id as id',
                    'scheduler_tasks.user_id as user_id',
                    'scheduler_tasks.name as name',
                    'scheduler_tasks.type_task as type_task',
                    'scheduler_tasks.description as description',
                    'scheduler_tasks.status as status',
                    'scheduler_tasks.burn as burn',
                    'scheduler_tasks.deadline as deadline',
                    'scheduler_tasks.update_at as update_at',
                    'scheduler_tasks.created_at as created_at')
                ->whereIn('scheduler_worker.worker_id', $orders)
                ->where('scheduler_tasks.completed', 0)
                //->where('scheduler_tasks.type_task',1)
                ->orderBy('scheduler_tasks.id', $sort)
                ->groupBy('scheduler_tasks.id')
                ->get();

        //Завершеные
        $complite = array();
        $complite[0] =
            \DB::table('scheduler_tasks')
                ->leftJoin('scheduler_worker', 'scheduler_tasks.id', '=', 'scheduler_worker.scheduler_tasks_id')
                ->select('scheduler_tasks.id as id',
                    'scheduler_tasks.user_id as user_id',
                    'scheduler_tasks.name as name',
                    'scheduler_tasks.type_task as type_task',
                    'scheduler_tasks.description as description',
                    'scheduler_tasks.status as status',
                    'scheduler_tasks.burn as burn',
                    'scheduler_tasks.deadline as deadline',
                    'scheduler_tasks.update_at as update_at',
                    'scheduler_tasks.created_at as created_at')
                ->whereIn('scheduler_worker.worker_id', $orders)
                ->where('scheduler_tasks.completed', 1)
                ->orderBy('scheduler_tasks.id', $sort)
                ->groupBy('scheduler_tasks.id')
                ->get();
        $complite[1] = \DB::table('scheduler_tasks')->where('user_id', $uid)->where('completed', 1)->orderBy('id', $sort)->get();
        $result['complite'] = array_merge($complite[0], $complite[1]);

        return $result;
    }

    //получить задачу по id
    public function getTaskByID($task_id)
    {
        if (empty($task_id)) {
            return false;
        }
        $task = array();
        $task['task'] = \DB::table('scheduler_tasks')->where('id', $task_id)->get();
        //файлы задания
        foreach (\DB::table('scheduler_file')->where('task_id', $task_id)->get() as $data) {
            $task['files'][] = $data;
        }

        return $task;
    }

    //получить пользователей и отделы назначеных на выполнентие задачи
    public function getTaskWorkerByID($task_id)
    {
        if (empty($task_id)) {
            return false;
        }
        $result['user'] = \DB::table('scheduler_worker')->where('scheduler_tasks_id', $task_id)->where('type', 1)->get();
        $result['departament'] = \DB::table('scheduler_worker')->where('scheduler_tasks_id', $task_id)->where('type', 2)->get();
        return $result;
    }

    //Удаление задания
    public function deleteTask($task_id = null)
    {
        if (empty($task_id)) {
            return false;
        } else {
            \DB::table('scheduler_worker')->where('scheduler_tasks_id', $task_id)->delete();  //удаляю отделы и пользователей на задании
            \DB::table('scheduler_tasks')->where('id', $task_id)->delete();  //удаляю задание

            //Пишем в историю
            $modelHistory = new \App\Models\SchedulerHistory();
            $modelHistory->setUser(\Auth::id());
            $modelHistory->setTasks($task_id);
            $modelHistory->setType(5);
            $userName = current(\DB::table('users')->where('userid', \Auth::id())->get());
            $modelHistory->setAction('Задача была удалена пользователем ' . ($userName->username));
            $modelHistory->addHistory();


            return true;
        }
    }

    //Удаление исполнителя из задания
    //$task_id - задание, $worker_id - id исполнителя, $type - тип 1/2 (1-пользователь / 2-отдел)
    public function deleteWorkerTask($task_id = null, $worker_id = null, $type = null)
    {
        if (empty($task_id) || empty($worker_id) || empty($type)) {
            return false;
        }
        \DB::table('scheduler_worker')->where('worker_id', $worker_id)->where('scheduler_tasks_id', $task_id)->where('type', $type)->delete();
        //Смотрим задачу, если она часть акта, то удаляем и ее (в актах каждому исполнителю ставится своя задача)
        $task = \DB::table('scheduler_tasks')->where('id', $task_id)->first();
        if ($task->type_task == 2) {
            \DB::table('scheduler_tasks')->where('id', $task_id)->delete();
        }

        //Пишем в историю
        $modelHistory = new \App\Models\SchedulerHistory();
        $modelHistory->setUser(\Auth::id());
        $modelHistory->setTasks($task_id);
        $modelHistory->setType(7);
        $userName = current(\DB::table('users')->where('userid', \Auth::id())->get());
        if ($type == 1) {
            $workerName = \DB::table('users')->where('userid', $worker_id)->first();
            $txt = " исполнитель " . $workerName->username;
        } else {
            $workerName = \DB::table('dir_department')->where('id', $worker_id)->first();
            $txt = " отдел " . $workerName->name;
        }
        $workerName = \DB::table('users')->where('userid', $worker_id)->first();
        $modelHistory->setAction('Из задания был удален ' . $txt);
        $modelHistory->addHistory();

        return true;

    }


    //очищает переменные
    function clearData()
    {
        $this->name = null;
        $this->description = "";
        $this->worker_users = array();
        $this->worker_department = array();
        $this->status = 0;
        $this->burn = null;
        $this->deadline = null;
        return true;
    }


}
