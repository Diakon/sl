<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Telegram extends Model
{
    
	public $table = "telegram";
    public $timestamps = false;  //отключает необходимость в поле updated_at - иначе требует

	public $fillable = [
	    "id",
		"user_id",
		"digital_code",
		"created_at"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"user_id" => "integer",
		"digital_code" => "string"
    ];

    public static $rules = [
        'user_id' => "required",
        'digital_code' => "required",
    ];

    public static $messages = array(
        'required' => "Поле :attribute обязательно для заполнения!",
        'unique' => "Пользователь :attribute уже существует!",
        'same' => "Пароли не совпадают!",
        'min' => "Длина поля :attribute должна быть минимум :min символов!"
    );

    public static function generateDigitalCode($num_simbol = 6){
        do {
            $new_code = '';
            for($i=0; $i<(int)$num_simbol;$i++){
                $new_code .= rand(0,9);
            }
            //Проверяем, что такого номера нет в БД
            if( !\DB::table('telegram')->where('digital_code', $new_code)->first() && strlen($new_code)==6){
                break;
            }
        } while (1<2);
        return $new_code;
    }


}
