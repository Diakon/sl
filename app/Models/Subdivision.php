<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Subdivision extends Model
{
    
	public $table = "subdivision";
    public $timestamps = false;  //отключает необходимость в поле updated_at - иначе требует

    public function objects() {
        return $this->belongsTo('App\Models\Objects', 'object_id', 'id');
    }

    public $fillable = [
	    "id",
		"object_id",
		"name",
		"object",
		"type",
		"description",
		"subdivision_comments"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"object_id" => "integer",
		"name" => "string",
		"object" => "string",
		"type" => "string",
		"description" => "string",
		"subdivision_comments" => "string"
    ];

    private $statusList = [1=>'не в работе', 2=>'в работе'];
    public function getStatus($status=null){
        if (!empty($status)){
            if (is_int($status)){return $this->statusList[(int)$status];}
            else {return array_search($status, $this->statusList); }
        }
        else {return $this->statusList;}
    }

    public static $rules = [
        'object_id' => 'required',
        'name' => 'required',
        'subdivision_comments' => 'required',
    ];


    public static $messages = array(
        'required' => "Поля `Объект`, `Название` и `Статус` обязательны для заполнения!",
        'unique' => "Запись :attribute уже существует!",
        'same' => "Пароли не совпадают!",
        'min' => "Длина поля :attribute должна быть минимум :min символов!"
    );

    /**
     * Список подразделений для списков (select)
     * @param int $withNull
     * @return array
     */
    public static function getActiveSubdivisions($withNull=0){
        $subdivisions = Self::where('subdivision_comments','=','в работе')->orderBy('name', 'asc')->get();
        $activeSubdivisions = [];
        if($withNull) {
            $activeSubdivisions[] = 'Все';
        }

        foreach($subdivisions as $subdivision){
            $activeSubdivisions[$subdivision->id] = $subdivision->nameAndObject;
        }
        return $activeSubdivisions;
    }

    /**
     * Название подразделения вместе с объектом
     * @return string
     */
    public function getNameAndObjectAttribute()
    {
        return sprintf('%s (%s)', $this->name, $this->object);
    }

    public function object()
    {
        return $this->belongsTo('App\Models\Object','object_id');
    }
}
