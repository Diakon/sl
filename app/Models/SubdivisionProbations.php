<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class SubdivisionProbations extends Model
{
    
	public $table = "subdivision_probations";
	public $timestamps = false;

	public $fillable = [
	    "id",
		"subdivision_id",
		"probation_days",
		"repeat_days",
		"object_days",
		"date_from",
		"date_to",
		"created_at"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"subdivision_id" => "integer",
		"probation_days" => "integer",
		"repeat_days" => "integer",
		"object_days" => "integer"
    ];

	public static $rules = [
		'subdivision_id' => "required",
	];


	public static $messages = array(
		'required' => "Поле :attribute обязательно для заполнения!",
		'unique' => "Пользователь :attribute уже существует!",
		'same' => "Пароли не совпадают!",
		'min' => "Длина поля :attribute должна быть минимум :min символов!"
	);

	public function getSubdivisions()
	{
		return $this->hasOne('App\Models\Subdivision', 'id', 'subdivision_id');
	}

}
