<?php namespace App\Models\Tuov;

use App\Models\LuovDocument;

class TuovSiz extends TuovFabric
{
    public $table = "tuov_siz";

    public function buildForm(LuovDocument $document)
    {
        return [
            'coat1' => ['text', null, 'Куртка', ['rules' => 'integer|max:24|min:0', 'total-amount' =>true]],
            'trousers1' => ['text', null, 'Штаны', ['rules' => 'integer|max:24|min:0', 'total-amount' =>true]],
            'jacket' => ['text', null, 'Жилет', ['rules' => 'integer|max:24|min:0', 'total-amount' =>true]],
            'coat2' => ['text', null, 'Куртка', ['rules' => 'integer|max:24|min:0', 'total-amount' =>true]],
            'trousers2' => ['text', null, 'Брюки', ['rules' => 'integer|max:24|min:0', 'total-amount' =>true]],
            'gloves' => ['text', null, 'Перчат', ['rules' => 'integer|max:24|min:0', 'total-amount' =>true]],
            'shirt' => ['text', null, 'Футб', ['rules' => 'integer|max:24|min:0', 'total-amount' =>true]],
            'boots' => ['text', null, 'Ботин', ['rules' => 'integer|max:24|min:0', 'total-amount' =>true]],
            'knife' => ['text', null, 'Нож', ['rules' => 'integer|max:24|min:0', 'total-amount' =>true]],
            'kragi' => ['text', null, 'Краги', ['rules' => 'integer|max:24|min:0', 'total-amount' =>true]],
            'money' => ['text', null, 'Сумма', ['rules' => 'numeric|max:99999|min:0', 'total-amount' =>true]],
            'date' => ['text', null, 'Дата', ['rules' => 'date']],
            'sum' => ['text', null, 'Кол', ['rules' => 'integer|max:50|min:0', 'total-amount' =>true]],
        ];
    }
}
