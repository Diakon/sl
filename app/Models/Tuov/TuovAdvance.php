<?php namespace App\Models\Tuov;

use App\Models\LuovDocument;

class TuovAdvance extends TuovFabric
{

    public function buildForm(LuovDocument $document)
    {
        return [
            'sum' => ['text', null, 'Сумма', [
                'rules' => 'required|min:1',
                'total-amount' =>true
            ]],
        ];
    }
}
