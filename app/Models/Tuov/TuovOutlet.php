<?php namespace App\Models\Tuov;

use App\Models\LuovDocument;

class TuovOutlet extends TuovFabric
{
    public $table = "tuov_hours";

    public function buildForm(LuovDocument $document)
    {
        return [
            'outlet' => ['select', ['0'=>' - '] + $this->getDirOutlet(), 'Выходной', [
                'rules' => 'required:integer'
            ]],
        ];
    }


}
