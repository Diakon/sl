<?php namespace App\Models\Tuov;

use App\Models\LuovDocument;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class TuovDeals extends TuovFabric
{
    public $attributes = ['anketa_id', 'document_id', 'deal_id', 'count', 'start', 'end', 'hours', 'outlet'];

    public function buildForm(LuovDocument $document)
    {
        $forms = [
            'outlet' => ['select', ['0'=>' - '] + $this->getDirOutlet(), 'Выходной', [
                'rules' => 'required:integer'
            ]],

            'start' => ['text', null, 'С', [
                'rules' => 'integer|max:24|min:0'
            ]],

            'end' => ['text', null, 'По', [
                'rules' => 'integer|max:24|min:0'
            ]],

            'hours' => ['text', null, 'Часы', [
                'rules' => 'integer|max:24|min:0',
                'exclude_filter' => true,
                'total-amount' =>true
            ]],
        ];

        $deals = $this->getDealRates($document);
        if (count($deals)) {
            foreach($deals as $deal) {
                $forms['deals[' . $deal->deal_id . ']'] = [
                    'text', null, $deal->name, ['rules' => 'numeric|min:0', 'has-error' => ($deal->type==2), 'total-amount' =>true]
                ];
            }
        }
        return $forms;
    }

    public function search($id) {
        $arr = [];
        foreach (parent::search($id) as $deal) {
            if (!isset($arr[$deal->anketa_id])) {
                $arr[$deal->anketa_id] = $deal;
            }
            $arr[$deal->anketa_id]->attributes['deals[' . $deal->deal_id . ']'] = $deal->count;
        }
        return array_values($arr);
    }

    public function updateForm($datas, $document_id = null) {
        $document = LuovDocument::find($document_id);
        $inserts = [];
        foreach($datas as $data) {
            foreach($this->getDealRates($document) as $deal) {
                if (!isset($data['deals[' . $deal->deal_id . ']'])) {
                    continue;
                }
                $insert = [];
                foreach($this->getAttributes() as $attribute) {
                    if (isset($data[$attribute])) {
                        $insert[$attribute] = $data[$attribute];
                    } else {
                        if (!isset($insert['count'])) {
                            $insert['count'] = ($data['outlet'] ? 0 : $data['deals[' . $deal->deal_id . ']']);
                            $insert[$attribute] = $deal->deal_id;
                        }
                    }
                }

                if ($insert['count'] > 0 || $insert['outlet'] > 0) {
                    $inserts[] = $insert;
                }
            }
        }
        return parent::updateForm($inserts, $document_id);
    }

    protected function getDealRates($document)
    {
        $file_name = __METHOD__ . $document->id;
        $deals = Cache::remember($file_name, 1, function () use ($document) {
            $deal_ids = DB::table($this->getTable())->whereDocumentId($document->id)->lists('deal_id');
            return DB::table('tuov_deal_rates as sd')
                ->select('sd.*', 'd.name', 'd.type')
                ->join('tuov_deal_names as d', 'd.id', '=', 'sd.deal_id')
                ->where('subdivision_id', $document->subdivision_id)
                ->where(function($query) use ($document, $deal_ids) {
                    $query->where(function($query) use ($document) {
                        $query->where('from', '<=', $document->date)->where('to', '>=', $document->date)->whereNull('sd.deleted_at');
                    });
                    $query->orWhere(function($query) use ($deal_ids) {
                        $query->whereIn('sd.deal_id', $deal_ids);
                    });
                })
                ->groupby('deal_id')
                ->get();
        });
        $arr = [];
        foreach ($deals as $deal) {
            $arr[$deal->deal_id] = $deal;
        }
        return $arr;
    }
}
