<?php namespace App\Models\Tuov;

use Event;
use App\Events\documentChanged;
use App\Models\LuovDocument;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class TuovFabric extends Model
{
    public function anketa()
    {
        return $this->belongsTo('App\Models\PersMain', 'anketa_id', 'id_anketa');
    }

    public function document()
    {
        return $this->belongsTo('App\Models\LuovDocument', 'document_id', 'id');
    }

    public function subdivision()
    {
        return $this->belongsTo('App\Models\Subdivision', 'subdivision_id', 'id');
    }

    public function buildForm(LuovDocument $document){
        return [];
    }

    public function search($id) {
        return $this->whereDocumentId($id)->with('anketa')->get();
    }

    public function updateForm($data, $document_id = null) {
        if ($document_id) {
            $old = $this->where('document_id', $document_id)->get();

            $this->where('document_id', $document_id)->delete();

        } elseif (isset($data[0]['document_id'])) {
            $doc_id = $data[0]['document_id'];
            $old = $this->where('document_id', $doc_id)->get();

            $this->where('document_id', $doc_id)->delete();
        }
        $oldData = [];
        foreach($old as $d){
            $oldData[] = $d->toArray();

        }
        return $this->insert($data,$oldData);
    }

    public function insert($data,$oldData){
        $parent = parent::insert($data);
        Event::fire(new documentChanged($data,$oldData));
        return $parent;
    }

    /**
     * Список возможных выходных
     * @return array
     */
    public function getDirOutlet()
    {
        return DB::table('dir_outlet')->whereIn('outlet_id',[1,3])->lists('outlet_name', 'outlet_id');
    }

    public function models()
    {
        return [
            1 =>'TuovHours',
            2 =>'TuovAdvance',
            3 =>'TuovPenalty',
            6 =>'TuovSiz',
            7 =>'TuovOutlet',
            9 =>'TuovDeals',
            10 =>'TuovAdvance',
            11 =>'TuovAdvance',
            12 =>'TuovAdvance',
            13 =>'TuovAdvance',
            14 =>'TuovDealsRange',
        ];
    }

    public function fabric(LuovDocument $luov)
    {
        if (!isset($this->models()[$luov->document_category])) {
            abort(404, "Edit form not found");
        }
        $model = app()->make('App\Models\Tuov\\' . $this->models()[$luov->document_category]);
        if (!$model instanceof Model) {
            throw new RepositoryException("Class must be an instance of Illuminate\\Database\\Eloquent\\Model");
        }
        return $model;
    }
}