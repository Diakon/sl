<?php namespace App\Models\Tuov;

use App\Models\LuovDocument;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Cache;

class TuovHours extends TuovFabric
{

    public function buildForm(LuovDocument $document)
    {
        return [
            'outlet' => ['select', ['0'=>' - '] + $this->getDirOutlet(), 'Выходной', [
                'rules' => 'required:integer'
            ]],

            'position' => ['select', ['0'=>' - '] + $this->getDirVacancy($document), 'Вакансия/Ставка/Разряд', [
                'rules' => 'required|integer'
            ]],

            'start' => ['text', null, 'С', [
                'rules' => 'integer|max:24|min:0'
            ]],

            'end' => ['text', null, 'По', [
                'rules' => 'integer|max:24|min:0'
            ]],

            'hours' => ['text', null, 'Часы', [
                'rules' => 'integer|max:24|min:0',
                'exclude_filter' => true,
                'total-amount' =>true
            ]],
        ];
    }


    /**
     * Возможные вакансии для документа
     * @return array
     */
    public function getDirVacancy($document)
    {
        $file_name = __METHOD__ . $document->id;
        $subdivision_rates = Cache::remember($file_name, 1, function () use ($document) {
            return DB::table('subdivision_rates')
                ->select('id', 'position', 'rate', 'rank')
                ->where('subdivision_id', $document->subdivision_id)
                ->where('date_from', '<=', $document->date)
                ->where('date_to', '>=', $document->date)
                ->groupby('id')
                ->orderby('position')->get();
        });

        $arr = [];
        foreach ($subdivision_rates as $row) {
            $rate = (isset($row->rate))? $row->rate:'-';
            $rank = (isset($row->rank))? $row->rank:'-';
            $arr[$row->id] = "{$row->position} / {$rate} / {$rank}";
        }
        return $arr;
    }
}
