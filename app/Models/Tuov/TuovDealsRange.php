<?php namespace App\Models\Tuov;

use App\Models\LuovDocument;

class TuovDealsRange extends TuovDeals
{
    public $attributes = ['anketa_id', 'document_id', 'deal_id', 'count', 'start', 'end', 'hours', 'outlet', 'date'];
    public $table = 'tuov_deals';

    public function buildForm(LuovDocument $document)
    {
        $forms = [
            'outlet' => ['select', ['0'=>' - '] + $this->getDirOutlet(), 'Выходной', [
                'rules' => 'required:integer'
            ]],

            'start' => ['text', null, 'С', [
                'rules' => 'integer|max:24|min:0'
            ]],

            'end' => ['text', null, 'По', [
                'rules' => 'integer|max:24|min:0'
            ]],

            'hours' => ['text', null, 'Часы', [
                'rules' => 'integer|max:24|min:0',
                'exclude_filter' => true,
                'total-amount' =>true
            ]],
            'date' => ['text', null, 'Дата', [
                'rules' => 'date',
            ]],

        ];

        $deals = $this->getDealRates($document);
        if (count($deals)) {
            foreach($deals as $deal) {
                $forms['deals[' . $deal->deal_id . ']'] = [
                    'text', null, $deal->name, ['rules' => 'numeric|min:0', 'has-error' => ($deal->type==2), 'total-amount' =>true]
                ];
            }
        }
        return $forms;
    }

    public function search($id) {
        $arr = [];
        foreach (TuovFabric::search($id) as $deal) {
            $key = $deal->anketa_id . $deal->date;
            if (!isset($arr[$key])) {
                $arr[$key] = $deal;
            }
            $arr[$key]->attributes['deals[' . $deal->deal_id . ']'] = $deal->count;
        }
        return array_values($arr);
    }


}
