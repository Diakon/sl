<?php namespace App\Models\Tuov;

use App\Models\LuovDocument;

class TuovPenalty extends TuovFabric
{
    public $table = "tuov_penalty";

    public function buildForm(LuovDocument $document)
    {
        return [
            'type' => ['select', [
                '1' => 'Штраф',
                '2' => 'Премия'
            ], 'Тип', [
                'rules' => 'required|in:1,2'
            ]],
            'sum' => ['text', null, 'Сумма', [
                'rules' => 'required|min:1',
                'total-amount' =>true
            ]],
            'context' => ['select', [
                '1'=>'Общежитие',
                '2'=>'Объект',
            ], 'Место', [
                'rules' => 'required|in:1,2'
            ]],
            'act' => ['text', null, '№ акта', [
                'rules' => 'min:1|max:10'
            ]]
        ];
    }
}
