<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class CardStaffing extends Model
{
    
	public $table = "card_staffing";
    

	public $fillable = [
	    "id",
		"balance",
		"official_name",
		"provisional_name",
		"alternative _name",
		"address",
		"region",
		"city",
		"phone",
		"fax",
		"email",
		"web_site",
		"comments",
		"author_id",
		"updated_at",
		"created_at"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"official_name" => "string",
		"provisional_name" => "string",
		"alternative _name" => "string",
		"address" => "string",
		"region" => "integer",
		"city" => "integer",
		"phone" => "string",
		"fax" => "string",
		"email" => "string",
		"web_site" => "string",
		"comments" => "string",
		"author_id" => "integer"
    ];

    public static $rules = [
        'official_name' => 'required',
        'address'=>'required',
        'email'=>'email',
        'balance'=>'numeric',
    ];

    public static $messages = array(
        'required' => "Поле :attribute обязательно для заполнения!",
        'unique' => "Пользователь :attribute уже существует!",
        'same' => "Пароли не совпадают!",
        'min' => "Длина поля :attribute должна быть минимум :min символов!",
        'integer' => "В поле :attribute можно вводить только числа!",
        'numeric' => "В поле :attribute можно вводить только целые или дробные числа!",
    );

}
