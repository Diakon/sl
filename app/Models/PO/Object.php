<?php namespace App\Models\PO;

use App\Models\Dormitory;
use App\Models\JobApplication;
use Illuminate\Database\Eloquent\Model as Model;

class Object extends Model
{
	protected static function boot() {
		parent::boot();

		static::deleting(function($object) { // before delete() method call this
			$object->expense()->delete();
			$object->applications()->delete();
		});
	}

    
	public $table = "po_objects";
    

	public $fillable = [
	    "id",
		"name",
		"address",
		"dormitory_id",
		"is_active",
		"deleted_at",
		"created_at",
		"updated_at"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"name" => "string",
		"address" => "string",
		"dormitory_id" => "integer",
		"is_active" => "integer"
    ];

	public static $rules = [
	    'name' => 'required|string|max:16',
		'address' => 'required|string|max:193',
		"dormitory_id" => "required|integer|exists:domitory,id",
		"is_active" => "required|integer|in:0,1"
	];





	/*
		relationships
	*/
	public function dormitory()
	{
		return $this->belongsTo(Dormitory::class);
	}
	public function expense()
	{
		return $this->hasOne(Expense::class);
	}
	public function applications()
	{
		return $this->hasMany(JobApplication::class);
	}

}
