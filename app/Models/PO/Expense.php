<?php namespace App\Models\PO;

use App\Models\PO\Object;
use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    public $table = "po_job_expenses";

    protected $fillable = ['object_id', 'author_id', 'dormitory_cost', 'transport_cost', 'food_cost', 'mk_cost', 'ud_cost', 'other_cost'];
    protected $hidden = ['id', 'object_id', 'author_id', 'created_at', 'updated_at'];

    public function getAttributeNames () {
        return [
            "object_id" => "ID объекта",
            "author_id" => "ID автора",
            "dormitory_cost" => "Проживание",
            "transport_cost" => "Транспорт",
            "food_cost" => "Питание",
            "mk_cost" => "МК (мед. книжка)",
            "ud_cost" => "УД (уд-ние)",
            "other_cost" => "Другие док-ты",
        ];
    }

    public static function getAttributeNamesStatic () {
        return (new Self)->getAttributeNames();
    }

    public function object()
    {
        return $this->belongsTo(Object::class);
    }
}
