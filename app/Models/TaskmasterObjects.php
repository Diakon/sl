<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class TaskmasterObjects extends Model
{
    
	public $table = "taskmaster_objects";
    public $timestamps = false;


    public $fillable = [
	    "id",
		"taskmaster_id",
		"object_id",
		"default"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"taskmaster_id" => "integer",
		"object_id" => "integer",
		"default" => "boolean"
    ];

	public static $rules = [
	    
	];

	public function object()
	{
		return $this->belongsTo(\App\Models\Objects::class);
	}
}
