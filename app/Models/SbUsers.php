<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class SbUsers extends Model
{
    public function object()
    {
        return $this->belongsTo('App\Models\Objects', 'objects_id');
    }

    const STATE_NEW = 0;
    const STATE_ACTIVE = 1;
    const STATE_BLOCKED = 2;
    
	public $table = "sb_users";
    

	public $fillable = [
        "id",
        "author_id",
        "username",
        "password",
        "active",
        "objects_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
        "author_id" => "integer",
        "username" => "string",
        "password" => "string",
        "active" => "integer",
        "objects_id" => "objects_id"
    ];

    public static $rules = [
        'create' => [
            'username' => "required|unique:sb_users,username",
            'password'       => 'required|min:6|same:password_repeat',
            'password_repeat' => 'required',
            'objects_id' => 'required|exists:objects,id'
        ],
        'update' => [
            'username' => "required",
            'password'       => 'same:password_repeat',
        ]
    ];


    public static $messages = array(
        'required' => "Поле :attribute обязательно для заполнения!",
        'unique' => "Пользователь :attribute уже существует!",
        'same' => "Пароли не совпадают!",
        'min' => "Длина поля :attribute должна быть минимум :min символов!",
        'exists' => "Объекта :attribute не существует!"
    );

    public static function boot()
    {
        parent::boot();
        self::created(function($model){
            $user = new User;
            $user->username = $model->username;
            $user->password = $model->password;
            $user->role = User::ROLE_USER;
            $user->save();
        });
    }

}
