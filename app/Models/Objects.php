<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Objects extends Model
{
    
	public $table = "objects";
	public $timestamps = false;
    
    public $fillable = [
	    "id",
		"name",
		"description",
		"coordinator",
		"comments",
		"juridical_name",
		"address",
		"fio",
		"domitory"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"name" => "string",
		"description" => "string",
		"coordinator" => "string",
		"comments" => "string",
		"juridical_name" => "string",
		"address" => "string",
		"fio" => "string",
		"domitory" => "string"
    ];

	public static $rules = [
	    
	];

    function subdivisions(){
        return $this->hasMany('App\Models\Subdivision','object_id');
    }

    public static function getActiveObjects(){
        $objects = Objects::where('comments','=','в работе')->orderBy('name', 'asc')->get();
        $activeObjects = [];
        foreach($objects as $object){
            $activeObjects[$object->id] = $object;
        }
        return $activeObjects;
    }

}
