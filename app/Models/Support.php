<?php namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use  App\Traits\SendMailTrait;

class Support extends Model   {

    use SendMailTrait;

    protected $table_support = 'support';
    protected $table_solution = 'support_solution';
    protected $table_tiket = 'support_tiket';
    protected $table_type3 = 'support_type3';
    protected $table_users = 'users';
    protected $resultTiket = null;
    public $table = 'support';

    public static $rules = [
        'tiket' => [
            'fromuser' => 'regex:/^((vahta)?[0-9]+)$/',
            'body' => 'required|string|min:10',
            'department' => 'required|integer|in:1,2,3',
            'user_id' => 'integer|exists:users,userid'
        ],
        'adduser' => [
            'User.fio' => 'required|string|min:3',
            'User.phone' => 'string|min:3',
            'User.email' => 'email',
            'User.alevel' => 'integer|min:0|max:15',
        ]
    ];

    public function getRules($type)
    {
        return (isset(static::$rules[$type])?static::$rules[$type]:null);
    }

    public static function getAllVahtovik($id=null){

        $users = ((empty($id))?(\DB::table('users_vahtovik')->get()):(\DB::table('users_vahtovik')->where('id', $id)->get()));
        $results = array();
        foreach ($users as  $data){
            $results[$data->id] =  $data->phone;
        }
        return $results;

    }

    public static function getUserStatus($id=null){

        $users = ((empty($id))?(\DB::table('users_status')->get()):(\DB::table('users_status')->where('id', $id)->get()));
        $results = array();
        foreach ($users as  $data){
            $results[$data->id] =  $data->name;
        }
        return $results;

    }

    public function genPass($number = 10){
        //Генерирует пароль
        $arr = array('a','b','c','d','e','f',
            'g','h','i','j','k','l',
            'm','n','o','p','r','s',
            't','u','v','x','y','z',
            'A','B','C','D','E','F',
            'G','H','I','J','K','L',
            'M','N','O','P','R','S',
            'T','U','V','X','Y','Z',
            '1','2','3','4','5','6',
            '7','8','9','0');
        // Генерируем пароль
        $pass = "";
        for($i = 0; $i < $number; $i++)
        {
            // Вычисляем случайный индекс массива
            $index = rand(0, count($arr) - 1);
            $pass .= $arr[$index];
        }
        return $pass;
    }

    private  function getTranslit($string, $gost=false)
    {
        if($gost)
        {
            $replace = array("А"=>"A","а"=>"a","Б"=>"B","б"=>"b","В"=>"V","в"=>"v","Г"=>"G","г"=>"g","Д"=>"D","д"=>"d",
                "Е"=>"E","е"=>"e","Ё"=>"E","ё"=>"e","Ж"=>"Zh","ж"=>"zh","З"=>"Z","з"=>"z","И"=>"I","и"=>"i",
                "Й"=>"I","й"=>"i","К"=>"K","к"=>"k","Л"=>"L","л"=>"l","М"=>"M","м"=>"m","Н"=>"N","н"=>"n","О"=>"O","о"=>"o",
                "П"=>"P","п"=>"p","Р"=>"R","р"=>"r","С"=>"S","с"=>"s","Т"=>"T","т"=>"t","У"=>"U","у"=>"u","Ф"=>"F","ф"=>"f",
                "Х"=>"Kh","х"=>"kh","Ц"=>"Tc","ц"=>"tc","Ч"=>"Ch","ч"=>"ch","Ш"=>"Sh","ш"=>"sh","Щ"=>"Shch","щ"=>"shch",
                "Ы"=>"Y","ы"=>"y","Э"=>"E","э"=>"e","Ю"=>"Iu","ю"=>"iu","Я"=>"Ia","я"=>"ia","ъ"=>"","ь"=>"");
        }
        else
        {
            $arStrES = array("ае","уе","ое","ые","ие","эе","яе","юе","ёе","ее","ье","ъе","ый","ий");
            $arStrOS = array("аё","уё","оё","ыё","иё","эё","яё","юё","ёё","её","ьё","ъё","ый","ий");
            $arStrRS = array("а$","у$","о$","ы$","и$","э$","я$","ю$","ё$","е$","ь$","ъ$","@","@");

            $replace = array("А"=>"A","а"=>"a","Б"=>"B","б"=>"b","В"=>"V","в"=>"v","Г"=>"G","г"=>"g","Д"=>"D","д"=>"d",
                "Е"=>"Ye","е"=>"e","Ё"=>"Ye","ё"=>"e","Ж"=>"Zh","ж"=>"zh","З"=>"Z","з"=>"z","И"=>"I","и"=>"i",
                "Й"=>"Y","й"=>"y","К"=>"K","к"=>"k","Л"=>"L","л"=>"l","М"=>"M","м"=>"m","Н"=>"N","н"=>"n",
                "О"=>"O","о"=>"o","П"=>"P","п"=>"p","Р"=>"R","р"=>"r","С"=>"S","с"=>"s","Т"=>"T","т"=>"t",
                "У"=>"U","у"=>"u","Ф"=>"F","ф"=>"f","Х"=>"Kh","х"=>"kh","Ц"=>"Ts","ц"=>"ts","Ч"=>"Ch","ч"=>"ch",
                "Ш"=>"Sh","ш"=>"sh","Щ"=>"Shch","щ"=>"shch","Ъ"=>"","ъ"=>"","Ы"=>"Y","ы"=>"y","Ь"=>"","ь"=>"",
                "Э"=>"E","э"=>"e","Ю"=>"Yu","ю"=>"yu","Я"=>"Ya","я"=>"ya","@"=>"y","$"=>"ye");

            $string = str_replace($arStrES, $arStrRS, $string);
            $string = str_replace($arStrOS, $arStrRS, $string);
        }

        return iconv("UTF-8","UTF-8//IGNORE",strtr($string,$replace));
    }

    /*
     * взращает данные о пользователе
     */
    private function saveUserData($uid=null, $source = 1){

        $userArray = array();

        $ip=(isset($_SERVER["REMOTE_ADDR"]))?$_SERVER["REMOTE_ADDR"]:0;
        $referer=(isset($_SERVER["HTTP_REFERER"]))?$_SERVER["HTTP_REFERER"]:0;
        $user_agent=(isset($_SERVER["HTTP_USER_AGENT"]))?$_SERVER["HTTP_USER_AGENT"]:0;

        $sqlLog = 'SELECT username, phone,email, fio, status FROM users WHERE userid='.(int)$uid;
        $resultLog = \DB::select($sqlLog);
        foreach ($resultLog as $data){
            $usernameLog = $data->username;
            $phoneLog = $data->phone;
            $emailLog = $data->email;
            $fioLog = $data->fio;
            $statusLog = $data->status;
        }

        $dataLog = "IP-аддрес: ".$ip;
        $dataLog .= "~~Страница с которой пришли: ".$referer;
        $dataLog .= "~~ФИО: ".((!empty($fioLog))?$fioLog:'не указана. ');
        $dataLog .= "~~Телефон: ".((!empty($phoneLog))?$phoneLog:'не указан. ');
        $dataLog .= "~~E-mail: ".((!empty($emailLog))?$emailLog:'не указан. ');
        $dataLog .= "~~Браузер: ".((!empty($user_agent))?$user_agent:'не удалось получить. ');
        $dataLog .= "~~Источник: ".((!empty($referer))?$referer:'не удалось получить. ');
        $dataLog .= "~~Статус: ".$statusLog;


        return $dataLog;
    }

    /*
     * создать новый тикет
     * $user_id - кто составил заявку
     * $source - пользователь из таблицы 1-users, 2-tuov
     * $u_id - какому пользователю нужна помошь
     * $department - в какой отдел запрос (1-информационный, 2-финансовый, 3-ОП
     */

    public function  getAddTiket($user_id, $source = 1, $u_id, $body, $department = 1, $type = null, $addUserForm = null){
        $userData = $this->saveUserData($user_id, 1);

        //Смотрим в body. Если передают код картинки (скриншот). Убираем его из body, сохраняем как файл на сервере
        preg_match_all('/(img|src)=("|\')[^"\'>]+/i', $body, $matches);
        $imgArr = array();
        foreach ($matches as $key=>$val){
            $img = current($val);
            $img = str_replace('src="data:image/png;base64,', '', $img);
            if (empty($img) || $img=='src' || $img=='"'){ continue; }
            $imgArr[] = $img;
        }
        $bodyImg = "";
        if (!empty($imgArr)){
            $bodyImg .= "<BR>Приложены изображения:<BR>";
            foreach ($imgArr as $data){
                $img = base64_decode($data);
                $file = md5(date('d-m-Y H:i:s')).(rand(0, 1005)).".png";
                $fpng = fopen(\public_path()."/uploads/comments/".$file, "w");
                fwrite($fpng,$img);
                fclose($fpng);
                $bodyImg .= '<BR><a href="/uploads/comments/'.$file.'" target="_blank"><img style="max-width:300px;" src="/uploads/comments/'.$file.'" /></a>';
            }
        }
        $body = preg_replace("/<img[^>]+\>/i", "", $body);
        $body .= $bodyImg;

        $reqSQL = array();
        $reqSQL['user_id'] = $user_id;
        $reqSQL['source'] = $source;
        if (!empty($u_id)) { $reqSQL['u_id'] = $u_id; }
        $reqSQL['body'] = $body;
        $reqSQL['data_user'] = $userData;

        $tiket_id = \DB::table($this->table_tiket)->insertGetId($reqSQL);

        if (empty($tiket_id)){
            return false;
        }

        if ( !empty($addUserForm) && !empty($type) ){
            //форма добавления пользователя

            $username =  $this->getTranslit($addUserForm['fio']);
            $username = str_replace(" ","",$username);

            $sql = "INSERT INTO ".$this->table_type3." (";
            foreach ($addUserForm as $key=>$val){
                switch ($key) {
                    case 'alevel':
                        $sql .= "`lvl`,";
                        break;
                    case 'fio':
                        $sql .= "`fio`,`username`,";
                        break;
                    case 'email':
                        $sql .= "`email`,";
                        break;
                    case 'phone':
                        $sql .= "`phone`,";
                        break;
                }
            }
            $sql = substr($sql, 0, -1);
            $sql .= ") VALUES (";
            foreach ($addUserForm as $key=>$val){
                if ($key=="fio"){ $sql .= "'".$val."',"."'".$username."',"; }
                else { $sql .= "'".$val."',"; }
            }
            $sql = substr($sql, 0, -1);
            $sql .= ");";
            $type_id = \DB::insert($sql);

            \DB::table($this->table_tiket)
                ->where('id', $tiket_id)
                ->update(array('type' => $type, 'type_table_id' => $type_id));
        }

        if (\DB::table($this->table_support)->insert(array('support_tiket_id' => $tiket_id, 'department' => $department))){
            $user_email = \DB::table('users')->where('userid', $user_id)->first();
            if (isset($user_email->email)){
                preg_match('|([a-z0-9_\.\-]{1,20})@([a-z0-9\.\-]{1,20})\.([a-z]{2,4})|is', $user_email->email, $match);
                if (!empty($match)){
                    //пишем в рассылку уведомление на почту автора
                    $bodyArr = [
                        'title'=>'Ваше обращение в службу поддержки успешно зарегистрировано',
                        'description'=>$body,
                        'date'=>date('d-m-Y, H:i:s'),
                    ];
                    $this->sendMail($user_email->email, $bodyArr, 'Ваше обращение в службу поддержки зарегестрировано', 'support');
                    //$this->sendMailDelivery($user_email->email, $bodyArr, 'Ваше обращение в службу поддержки зарегестрировано', 'support');
                }
            }

            return true;
        }
        else {return false;}

    }


    /*
     *    Обрабатывает действия формы (например, добавляет пользователя в БД)
     *      $typeAction - тип действий формы,
     *          1- добавление нового пользователя
     *
     */

    public function getActionForm($typeAction = 1, $param=array()){
        $sql = null;
        switch ($typeAction) {
            case 1:
                $pass = (md5($param['pass']));
                $username = str_replace(" ","",$param['username']);
                $sql = "INSERT INTO `users` (`username`, `password`, `enable`, `status`, `fio`, `phone`, `email`) VALUES ( '".$username."', '".$pass."', ".((int)$param['activ']).", ".$param['lvl'].", '".$param['fio']."', '".$param['phone']."', '".$param['email']."'  );";
                break;
            case 2:

                break;
        }

        if (!empty($sql) && \DB::insert($sql)){ return  true;}
        else { return false; }

    }




    /**
     * ответ на тикет
     * $id - id тикета
     * $user_id - id пользователя работающего с тикетом
     * $type:
     * complite - заявка закрыта
     * inwork - в работе
     * proroguep - отложена
     *
     * $body - коментарий к тикету
     * $sebdEmail - флаг отправки письма автору тикета
     **/
    public function  getAddSolution($id, $type, $user_id, $body = null, $sebdEmail = true){
        $this->resultTiket = null;
        $sqlArr = array();
        switch ($type) {
            case "complite":
                if ( $solution_id = \DB::table($this->table_solution)->insertGetId(array('support_id'=>(int)$id, 'user_id'=>(int)$user_id, 'solution'=>$body, 'status_tiket'=>3)) ){
                    $sqlArr = array('support_solution_id' => $solution_id, 'status_tiket' => 3, 'created_at' => date('Y-m-d h:i:s') );
                    $this->resultTiket = " Заявка закрыта";
                    echo json_encode("ok");
                } else
                { echo json_encode("error"); }

                break;
            case "inwork":
                if ( $solution_id = \DB::table($this->table_solution)->insertGetId(array('support_id'=>(int)$id, 'user_id'=>(int)$user_id, 'solution'=>"Задача в работе", 'status_tiket'=>2)) ){
                    $sqlArr = array('support_solution_id' => $solution_id, 'status_tiket' => 2, 'created_at' => date('Y-m-d h:i:s') );
                    $this->resultTiket = " Заявка взята в работу";
                    echo json_encode("ok");

                } else
                { echo json_encode("error"); }

                break;
            case "proroguep":
                if ( $solution_id = \DB::table($this->table_solution)->insertGetId(array('support_id'=>(int)$id, 'user_id'=>(int)$user_id, 'solution'=>$body, 'status_tiket'=>4)) ){
                    $sqlArr = array('support_solution_id' => $solution_id, 'status_tiket' => 4, 'created_at' => date('Y-m-d h:i:s') );
                    $this->resultTiket = " Выполнение заявки отложено";
                    echo json_encode("ok");
                } else
                { echo json_encode("error"); }

                break;
            case "disabled_true":
                $sqlArr = array('disabled' => 1, 'created_at' => date('Y-m-d h:i:s') );
                $this->resultTiket = null;
                echo json_encode("ok");
                break;
            case "disabled_false":
                $sqlArr = array('disabled' => 0, 'created_at' => date('Y-m-d h:i:s') );
                $this->resultTiket = null;
                echo json_encode("error");
                break;
        }

        \DB::table($this->table_support)
            ->where('id', (int)$id)
            ->update($sqlArr);

        //Если тикет из ТУОВа - отправляем уведомление по SMS
        $sql = "
            SELECT stiket.source as source, stiket.u_id as u_id FROM `support` as s
            LEFT JOIN `support_tiket` as stiket ON stiket.id = s.support_tiket_id
            WHERE s.id = ".(int)$id." LIMIT 1";
        $result = \DB::select($sql);
        if ( count($result[0]) == 2 ){
            $source = $result[0]['source'];
            $u_id = $result[0]['u_id'];
            if ( !empty($this->resultTiket) && isset($source) && isset($u_id) && (int)$source==2 ){
                //Получаем телефон из анкеты
                if ($result = \DB::select("SELECT phone_mobile FROM pers_address WHERE id_anketa = ".(int)$u_id." LIMIT 1")){
                    $phone_mobile = $result[0]['phone_mobile'];
                    if (!empty($phone_mobile)){
                        require_once \public_path()."/sms/smsc_api.php";
                        send_sms($phone_mobile, "По вашему обращению №: ".$id." дан ответ:".$this->resultTiket, $translit = 0, $time = 0, $id = 0, $format = 0, $sender = "79168877777");
                    }
                }
            }
        }


        if ($sebdEmail == true && !empty($this->resultTiket)){

            $sql = "
                SELECT t.user_id as id, u.email as email, t.body as body, t.created_at as data, ss.solution as solution, ss.created_at as datasolution FROM support_tiket as t
                LEFT JOIN support as s ON s.support_tiket_id = t.id
                LEFT JOIN users as u ON u.userid = t.user_id
                LEFT JOIN support_solution as ss ON ss.id = s.support_solution_id
                WHERE s.id = ".((int)$id)."
                LIMIT 1
            ";
            $data = \DB::selectOne($sql);
            if ($data && preg_match('|([a-z0-9_\.\-]{1,20})@([a-z0-9\.\-]{1,20})\.([a-z]{2,4})|is', $data->email, $match)){
                $bodyArr = array(
                    'title' => 'Уведомление от службы поддержки',
                    'description' => $data->body,
                    'resultTiket' => $this->resultTiket,
                    'solution' => $data->solution,
                    'date' => date('d-m-Y H:i:s'),
                );
                $this->sendMailDelivery($data->email, $bodyArr, 'Уведомление с сайта cloud.secondlab.ru', 'support_solution');
            }
        }

    }

    /** Меняет статус на просмотрено при 1ом открытии и отправляет СМС если заявка из ТУОВа
    id - ID заявки с таблицы support
     **/
    public function chkStatus($id){
        /*
        $sql = "
                SELECT st.source as source, s.status_tiket as status_tiket, st.u_id as u_id FROM support as s
                LEFT JOIN support_tiket as st ON st.id = s.support_tiket_id
                WHERE s.id = ".(int)$id." LIMIT 1";
        -*/

        $result = \DB::table($this->table_support)
            ->leftJoin($this->table_tiket, $this->table_tiket.'.id', '=', $this->table_support.'.support_tiket_id')
            ->select($this->table_tiket.'.source as source', $this->table_support.'.status_tiket as status_tiket', $this->table_tiket.'.u_id as u_id')
            ->where($this->table_support.'.id', '=', (int)$id)
            ->first();

        $source = (isset($result->source))?$result->source:null;
        $status_tiket = (isset($result->status_tiket))?$result->status_tiket:null;
        $u_id = (isset($result->u_id))?$result->u_id:null;

        if ((int)$status_tiket==1){
            //Ставлю просмотрена для новых заявок
            \DB::table($this->table_support)
                ->where('id', (int)$id)
                ->update(array('status_tiket' => 5));
            //Если ТУОВ заявка - отправлюя СМС
            if ( !empty($source) && !empty($u_id) && (int)$source==2 ){
                //Получаем телефон из анкеты
                $anketa = \DB::table('pers_address')->where('id_anketa', (int)$u_id)->first();
                $phone_mobile = $anketa->phone_mobile;
                if (!empty($phone_mobile)){
                    require_once \public_path()."/sms/smsc_api.php";
                    send_sms($phone_mobile, "Ваше обращение зерегестрировано под номером №: ".$id." и просмотренно.", $translit = 0, $time = 0, $id = 0, $format = 0, $sender = "79168877777");
                }
            }

        }
        return true;
    }


    /** Устанавливает для задания дедлайн
     * id - ID заявки с таблицы support, $time - время дедлайна
     **/
    public function setDeadline($id, $time)
    {
        $time = date('Y-m-d', strtotime($time));
        \DB::table($this->table_support)
            ->where('id', (int)$id)
            ->update(array('dead_line' => $time));

        //отправляем уведомление автору заявки
        //Получем E-mail автора зачвки
        $result = \DB::table($this->table_support)
            ->leftJoin($this->table_tiket, $this->table_tiket.'.id', '=', $this->table_support.'.support_tiket_id')
            ->leftJoin($this->table_users, $this->table_users.'.userid', '=', $this->table_tiket.'.user_id')
            ->select($this->table_users.'.username as username', $this->table_users.'.email as email')
            ->where($this->table_support.'.id', '=', (int)$id)
            ->first();
        preg_match('|([a-z0-9_\.\-]{1,20})@([a-z0-9\.\-]{1,20})\.([a-z]{2,4})|is', $result->email, $match);
        if (!empty($match)){
            $bodyArr = array(
                'title'=>'Уведомление от службы поддержки',
                'date'=>(date('d.m.Y', strtotime($time))),
            );
            $this->sendMail($result->email, $bodyArr, 'Уведомление с сайта cloud.secondlab.ru', 'support_deadline');
        }

        return true;
    }

    /** Отрабатывает реакцию пользователя (автора заявки) на ее решение
     * id - ID заявки, status - статус заявки выставленный автором её, 1 - заявка Не решена, 2-заявка решена
     */
    public function setConfirm($id, $status)
    {
        $result = \DB::table($this->table_support)
            ->leftJoin($this->table_tiket, $this->table_tiket . '.id', '=', $this->table_support . '.support_tiket_id')
            ->where($this->table_support . '.id', $id)
            ->where($this->table_tiket . '.user_id', \Auth::id())
            ->where($this->table_tiket . '.user_id', \Auth::id())
            ->select($this->table_support . '.id as support_id')
            ->first();
        if (isset($result->support_id) && (int)$result->support_id == $id) {

            \DB::table('support')
                ->where('id', $result->support_id)
                ->update(array('confirm_date' => date('Y-m-d H:i:s'), 'confirm_author' => $status));
            if ($status == 1) {
                //Если задача не решене - ставлю статус задаче в работе  и сообщение на емайл исполнителю
                \DB::table('support')
                    ->where('id', $result->support_id)
                    ->update(array('status_tiket' => 2, 'confirm_author' => $status));

                $email = \DB::table($this->table_support)
                    ->leftJoin($this->table_solution, $this->table_solution . '.id', '=', $this->table_support . '.support_solution_id')
                    ->leftJoin($this->table_users, $this->table_users . '.userid', '=', $this->table_solution . '.user_id')
                    ->where($this->table_support . '.id', $result->support_id)
                    ->select($this->table_users . '.email as email')
                    ->first();
                $email = $email->email;

                preg_match('|([a-z0-9_\.\-]{1,20})@([a-z0-9\.\-]{1,20})\.([a-z]{2,4})|is', $email, $match);
                if (!empty($match)) {
                    $bodyArr = array(
                        'title' => 'Автор заявки, которой вы поставили статус исполнено, вернул заявку на доработку',
                        'task' => 'http://cloud.secondlab.ru/support/viewtiket?id='.$result->support_id,
                    );
                    $this->sendMail($email, $bodyArr, 'Уведомление с сайта cloud.secondlab.ru', 'support_return_task');
                }

            }


        }
    }

    /**
     * @param $id - ID заявки (таблица support)
     * @param $username  - логин в JIRO
     * @param $password  - пароль в JIRO
     * @param $task - тип задания в JIRO
     */
    public function addTaskInJiro($id, $username, $password, $task){

        //Получаю данные заявки
        $task_data = \DB::table('support')
            ->leftJoin('support_tiket', 'support_tiket.id', '=', 'support.support_tiket_id')
            ->leftJoin('users', 'users.userid', '=', 'support_tiket.user_id')
            ->select('support_tiket.body as body', 'support_tiket.created_at as created', 'users.username as username')
            ->where('support.id', '=', $id)
            ->first();
        $summary = 'Заявка  пользователя '.$task_data->username.' от '.(date('d-m-Y H:i:s',strtotime($task_data->created)));
        $description = $task_data->body.' [Ссылка на задачу в секонтлаб: http://cloud.secondlab.ru/support/viewtiket?id='.$id.' ]';

        $data = array(

            'fields' => array(

                'project' => array(

                    'key' => 'SL',

                ),

                'summary' => $summary,

                'description' => $description,

                'issuetype' => array(
                    'name' => $task,
                ),
            ),

        );
        $url = 'https://vlscode.atlassian.net/rest/api/2/issue/';
        $data = json_encode($data);
        $userJIRO = $username.':'.$password;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_USERPWD, $userJIRO);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-type: application/json'));
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);

        $issue_list = (curl_exec($curl));

        $issue_list_arr = json_decode($issue_list);
        if (isset($issue_list_arr->id)){
            //Добавили в JIRO запись успешно
            $id = \DB::table('support_jira')->insertGetId(
                array('support_id' => $id, 'user_id' => \Auth::id(), 'return_jiro' => $issue_list, 'url_jiro' => 'https://vlscode.atlassian.net/projects/SL/issues/'.$issue_list_arr->key)
            );
            return true;
        }
        return false;



    }
}