<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;

class LuovDocument extends Model
{

    const SHIFT_NIGHT = 0;
    const SHIFT_DAY = 1;

    const TYPE_NEW = 'Новый';
    const TYPE_CORRECTION = 'Корректировка';

    const STATUS_SEND = 'Отправлен';
    const STATUS_OPEN = 'Открыт';
    const STATUS_PROCESSED = 'Обработан';
    const STATUS_DELETE = 'Удален';

    public $table = "luov_documents";
    public $timestamps = false;  //отключает необходимость в поле updated_at - иначе требует
    public $validExcelCategory = [2, 3, 6, 10, 11, 12];

    /**
     * История изменения Туов документа
     * @return mixed
     */
    public function history()
    {
        return $this->hasMany(\App\Models\LuovDocumentsHistory::class, 'document_id', 'id');
    }

    /**
     * Подразделение
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function subdivision()
    {
        return $this->hasOne(\App\Models\Subdivision::class, 'id', 'subdivision_id');
    }

    /**
     * Пользователь
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user()
    {
        return $this->hasOne(\App\User::class, 'userid', 'user_id');
    }

    /**
     * Категория
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function category()
    {
        return $this->hasOne(\App\Models\DirDocumentCategory::class, 'id', 'document_category');
    }

    public static function shifts() {
        return [
            self::SHIFT_DAY => 'Дневная',
            self::SHIFT_NIGHT => 'Ночная',
        ];
    }

    public static function documentTypes() {
        return [
            self::TYPE_NEW => 'Новый',
            self::TYPE_CORRECTION => 'Корректировка',
        ];
    }

    public static function documentStatus() {
        return [
            self::STATUS_SEND => 'Отправлен',
            self::STATUS_OPEN => 'Открыт',
            self::STATUS_PROCESSED => 'Обработан',
            self::STATUS_DELETE => 'Удален',
        ];
    }

    public function shiftLabel () {
        return self::shifts()[$this->shift];
    }

    public function getContainer() {
        return Config::get('app.cloud.documents_path');
    }

    public function filename($ext = false) {
        if ($ext) {
            $this->path = sprintf('luov_document.%d.%s', $this->id, $ext);
            $this->save();
        }
        return $this->path;
    }

    public function filepath() {
        $path = $this->path;
        if (strpos($path, 'public')) {
            return 'http://vls.secondlab.ru' . preg_replace('/.*public(.*)/','$1', $path);
        }
        return '/tuov/image/' . $path;
    }

    public static function create(array $attributes = []) {
        $date = Carbon::parse($attributes['date']);
        $attributes['date'] = $date;
        $attributes['document_type'] = 'Новый';
        if (self::existsDuplicate($attributes)) {
            $attributes['document_type'] = 'Корректировка';
        }
        $attributes['document_status'] = self::STATUS_SEND;
        if ($model = parent::create($attributes)) {
            (new LuovDocumentsHistory)->add($model, $model->user_id, $model->document_status);
        }
        return $model;
    }

    public static function existsDuplicate($attributes) {
        $where = [];
        foreach ($attributes as $name=>$value) {
            if (in_array($name, ['date', 'document_category', 'shift', 'user_id', 'subdivision_id'])) {
                $where[$name] = $value;
            }
        }
        return self::where($where)->exists();
    }

    public static $rules = [

        'document' => 'required|array|myCheckExt',
        'subdivision_id' => "required|integer|exists:subdivision,id",
        'date' => "required|date",
        'date_end' => "date",
        'document_category' => "required",
        'shift' => "in:0,1",
        'user_id' => 'integer|exists:users,userid'
    ];
    public static $messages = [
        'document.required' => 'Файл обязателен для заполнения.',
        'date.required' => 'Поле "Дата" обязателен для заполнения.',
        'subdivision_id.exists' => '"Подразделение" не существует.',
        'user_id.exists' => '"Пользователь" не существует.',
    ];

    protected $fillable = ['subdivision_id', 'date', 'date_end', 'document_category', 'user_id', 'document_type', 'document_status', 'shift'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
        "cnt" => "integer"
    ];


    public function getChartsData() {
        $expiresAt = Carbon::now()->addHour();
        return Cache::remember(__METHOD__, $expiresAt, function() {
            $data = self::select(DB::raw("date_format(`created_at`, '%m-%d') as date_created"), DB::raw("count(*) as cnt"), DB::raw('document_category as type'))
                ->where('created_at', '>=', DB::raw('curdate()-interval 2 week'))
                ->groupBy('date_created', 'document_category')
                ->orderBy('created_at');

            $categories = DirDocumentCategory::all();
            $data_charts = [];
            $dates = [];
            foreach ($data->get() as $row) {
                $dates[$row->date_created] = $row->date_created;
                $data_charts[$row->type][$row->date_created] = $row->cnt;
            };
            $charts = [];
            foreach ($data_charts as $type => $row) {
                $charts[$type]['label'] = $categories->find($type)->name;
                foreach ($dates as $date) {
                    $charts[$type]['data'][] = [$date, (isset($row[$date])?$row[$date]:0)];
                }
            }
            return $charts;
        });
    }
}