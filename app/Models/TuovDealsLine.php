<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TuovDealsLine extends Model
{
    use SoftDeletes;

	public $table = "tuov_deal_lines";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "deal_id",
		"subdivision_id",
		"from",
		"to",
		"koef_min",
		"koef_max",
		"rate"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "deal_id" => "integer",
		"subdivision_id" => "integer",
		"koef_min" => "float",
		"koef_max" => "float",
		"rate" => "float"
    ];

	public static $rules = [
	    "deal_id" => "required",
		"subdivision_id" => "required",
		"from" => "required",
		"to" => "required",
		"koef_min" => "required",
		"koef_max" => "required",
		"rate" => "required"
	];

}
