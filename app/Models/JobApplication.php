<?php namespace App\Models;

use App\Models\PO\Object;
use Illuminate\Database\Eloquent\Model as Model;

class JobApplication extends Model
{
    
	public $table = "po_job_application";

	public function object()
	{
		return $this->belongsTo(Object::class);
	}

	public $fillable = [
	    "id",
		"job_vacancy_id",
		"author_id",
		"manage_costs",
		"request",
		"vz_employee",
		"payment_us_no_nds",
		"schedule_day",
		"schedule_month",
		"clock",
		"payed",
		"updated_at",
		"created_at"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"job_vacancy_id" => "integer",
		"author_id" => "integer",
		"manage_costs" => "boolean",
		"request" => "integer",
		"schedule_day" => "integer",
		"schedule_month" => "integer",
		"clock" => "integer"
    ];

	public static $rules = [
		'job_vacancy_id' => "required|integer",
		'request' => "required|integer|min:1",
		'schedule_day' => "integer|min:0",
		'schedule_month' => "integer|min:0",
		'clock' => "integer|min:0",
	];
	public static $messages = array(
		'required' => "Поле :attribute обязательно для заполнения!",
		'unique' => "Пользователь :attribute уже существует!",
		'same' => "Пароли не совпадают!",
		'exists' => "Объекта :attribute не существует!"
	);

	/**
	 * Кол-во рабочих дней
	 * @return int
	 */
	public function getWorkDaysAttribute () {
		return intval(ceil($this->clock / $this->schedule_day));
	}

	/**
	 * Кол-во календарных дней
	 * @return int
	 */
	public function getCalendarDaysAttribute () {
		return intval(ceil($this->clock / $this->schedule_day));
	}

	public static function boot()
	{
		parent::boot();
		self::deleted(function($model){
			\SbUsers::where('id', $model->author_id)->update(['status' => \SbUsers::STATE_BLOCKED]);
		});
	}
}
