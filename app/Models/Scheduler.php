<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Scheduler extends Model   {
	
	protected $table_orders = 'users_orders';
    protected $table_department = 'dir_department';
	protected $primaryKey = 'id';

	public function __construct() {
		parent::__construct();
		
	}

	public static function querySelect(  ){
		
		
		return "  SELECT users_orders.* FROM users_orders  ";
	}
	public static function queryWhere(  ){
		
		return " WHERE users_orders.id IS NOT NULL   ";
	}
	
	public static function queryGroup(){
		return "  ";
	}


    public static function returnUserData($id){
        /*
        $sql = "
          SELECT u.username as username, uo.id as idorder, uo.order_id as order_id, uo.type_order as type_order, uo.rule as rule  FROM users as u
          LEFT JOIN users_orders as uo ON uo.user_id = u.userid
          WHERE u.userid = ".(int)$id."
        ";

        $results = \DB::select($sql);
        */
        $results = \DB::table('users_orders')->where('user_id', (int)$id)->get();
        $user = array();
        $i = 0;

        foreach ($results as $data){

            $type_order = "";
            if ((int)$data->type_order==1){
                //Отдел
                $type_order = "отдел";
                $sql = "SELECT name FROM dir_department WHERE id = ".(int)$data->order_id." LIMIT 1";
            }
            if ((int)$data->type_order==2){
                //Пользователь
                $type_order = "пользователь";
                $sql = "SELECT username as name FROM users WHERE userid = ".(int)$data->order_id." LIMIT 1";
            }
            $resultOrder = \DB::select($sql);

            $user[$i]['id'] = $data->id;

            $user[$i]['name'] = $resultOrder[0]->name;
            $user[$i]['type_order'] = $type_order;
            $user[$i]['rule'] = (($data->rule==1)?"начальник":"сотрудник");
            ++$i;

        }



        return $user;
    }


    public static function getAllUsers($id=null){
        $users = ((empty($id))?(\DB::table('users')->where('enable',1)->get()):(\DB::table('users')->where('userid', $id)->get()));
        $results = array();
        foreach ($users as  $data){
            $results[$data->userid]['username'] =  $data->username;
            $results[$data->userid]['fio'] =  $data->fio;
            $results[$data->userid]['email'] =  $data->email;
        }
        return $results;
    }

    public  static function  getDepartment ($id=null){
        $users = ((empty($id))?(\DB::table('dir_department')->get()):(\DB::table('dir_department')->where('id', $id)->get()));
        $results = array();
        foreach ($users as  $data){
            $results[$data->id] =  $data->name;
        }
        return $results;
    }




}
