<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TuovSubdivisionDeals extends Model
{
    use SoftDeletes;

	public $table = "tuov_subdivisionDeals";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "deal_id",
		"subdivision_id",
		"from",
		"to"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "deal_id" => "integer",
		"subdivision_id" => "integer"
    ];

	public static $rules = [
	    "deal_id" => "required",
		"subdivision_id" => "required",
		"from" => "required",
		"to" => "required"
	];

}
