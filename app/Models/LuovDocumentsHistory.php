<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class LuovDocumentsHistory extends Model
{
    public $table = "luov_documents_history";
    public $timestamps = false;

    protected $fillable = ['document_id', 'document_status', 'user_id'];

    public function add(LuovDocument $document, $user_id, $status) {
        $last_status = self::whereDocumentId($document->id)->orderby('date', SORT_ASC)->first();
        if ($last_status && $last_status->document_status == $status && $last_status->user_id == $user_id) {
            return false;
        }

        $document->document_status = $status;
        $document->save();

        $history = new self;
        $history->document_status = $status;
        $history->user_id = $user_id;
        $document->history()->save($history);
    }

    public function user() {
        return $this->belongsTo('App\User');
    }
}