<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use App\User;
class LoginLog extends Model
{
    
	public $table = "login_log";
    public $timestamps = false;

	public $fillable = [
	    "id",
		"id_user",
		"ip_user",
		"username",
		"data",
		"created_at",
		"action"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"id_user" => "integer",
		"ip_user" => "string",
		"username" => "string",
		"data" => "string",
		"action" => "boolean"
    ];

	public static $rules = [
	    
	];

    /**
     * Пишем вход/выход в программу
     * @param $userid
     * @param int $action   тип записи: 0-вход, 1-выход
     */
    public static function addToLog($userid, $action = 0){
        $user = User::getActiveUsersList($userid);
        $ip = (isset($_SERVER["REMOTE_ADDR"])) ? $_SERVER["REMOTE_ADDR"] : 0;
        $referer = (isset($_SERVER["HTTP_REFERER"])) ? $_SERVER["HTTP_REFERER"] : 0;
        $user_agent = (isset($_SERVER["HTTP_USER_AGENT"])) ? $_SERVER["HTTP_USER_AGENT"] : 0;
        $dataLog = "ФИО: " . ((!empty($userid->fio)) ? $userid->fio : 'не указана. ');
        $dataLog .= ";Телефон: " . ((!empty($userid->phone)) ? $userid->phone : 'не указан. ');
        $dataLog .= ";E-mail: " . ((!empty($userid->email)) ? $userid->email : 'не указан. ');
        $dataLog .= ";Браузер: " . ((!empty($user_agent)) ? $user_agent : 'не удалось получить. ');
        $dataLog .= ";Источник: " . ((!empty($referer)) ? $referer : 'не удалось получить. ');
        $dataLog .= ";Статус: " . $user->status;
        $id = \DB::table("login_log")->insertGetId(
            array('id_user' => $user->userid, 'ip_user' => $ip, 'username' => $user->username, 'data' => $dataLog, 'action' => $action)
        );
        return $id;
    }

}
