<?php namespace App\Models;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $fillable = array('name', 'display_name', 'description');

    public static $rules = [
        'create' => [
            'name' => 'required|alpha_dash|unique:roles',
            'display_name' =>'required',
            'description' =>'string',
            'permissions' => 'array'
        ],
        'update' => [
            'name' => 'required|alpha_dash|exists:roles',
            'display_name' =>'required',
            'description' =>'string',
            'permissions' => 'array'
        ]
    ];

    public function addUserRoles($user, $ids) {
        $list = self::whereIn('id', $ids)->lists('id');
        if (!empty($list)) {
            $ids = [];
            foreach ($list as $l) {
                $ids[] = $l;
            }
            $user->roles()->sync($ids);
        }
    }
}