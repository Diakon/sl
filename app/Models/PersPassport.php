<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PersPassport extends Model
{
	public function main()
	{
		return $this->belongsTo('App\Models\PersMain', 'id_anketa', 'id_anketa');
	}
    
	public $table = "pers_passport";


}
