<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Deal extends Model
{
    use SoftDeletes;

	public $table = "tuov_deal_names";
    
	protected $dates = ['deleted_at'];


	public $fillable = [
	    "name","type"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "name" => "string",
        "type" => "integer"
    ];

	public static $rules = [
        "name" => "required",
        "type" => "required|in:1,2"
	];

    public static function getActiveDeals($withNull=0){
        $deals = Deal::all();
        $activeDeals = [];
        if($withNull) {
            $activeDeals[] = 'Все';
        }
        foreach($deals as $deal){
            $activeDeals[$deal->id] = $deal->nameWithType;
        }
        return $activeDeals;
    }

    /**
     * Название сделки вместе с её типом (ошибка, нет)
     * @return string
     */
    public function getNameWithTypeAttribute()
    {
        if ($this->type < 2) {
            return $this->name;
        }
        return sprintf('%s (%s)', $this->name, 'Ошибка');
    }
}
