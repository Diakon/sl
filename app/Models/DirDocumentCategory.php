<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DirDocumentCategory extends Model
{
	protected $fillable = ['name'];
	public $timestamps = false;

	/**
	 * Список категорий документа для удержаний.
	 * @return array
	 */
	public function scopeDeduction($query)
	{
		return $query->wherein('id', [2,10,11,12,13]);
	}

	public function activeList() {
		return self::where('name', '<>', '')->lists('name','id');
	}
}
