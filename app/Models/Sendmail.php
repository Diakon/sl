<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sendmail extends Model   {
	
	protected $layout = 'standart';
    protected $attachfile = array();
    protected $destinationMail = "";
    protected $fromName = "";
    protected $data = array(); //В data передаем массив содержащий данные темплейта, например $data = ['title' =>  $this->headerMail,'content' => $this->messageMail,];

	public function setData($data){  $this->data = $data; return $this;  }
    public function setDestination($destinationMail) { $this->destinationMail = $destinationMail; return $this; }
    public function getDestination() { return $this->destinationMail; }
    public function setLayout($layout) { $this->layout = $layout; return $this; }
    public function getLayout() { return $this->layout; }
    public function setSubject($subject) { $this->subject = $subject; return $this; }
    public function getSubject() { return $this->subject; }
    public function setFromName($fromName) { $this->fromName = $fromName; return $this; }
    public function getFromName() { return $this->fromName; }
    public function setAttach($patch_to_file) {
        $this->attachfile[] = $patch_to_file;
    }
    public function getAttach() {
        return $this->attachfile;
    }

    public function sendMessage($header='',$message='',$destination=''){
        if($header) $this->setHeader($header);
        if($message) $this->setMessage($message);
        if($destination) $this->setDestination($destination);

        $data = $this->data;
        //Помещаем в очередь отправки
        \Mail::send('emails.'.$this->layout, $data, function($message) use ($data) {
            $message->to($this->destinationMail, $this->fromName)->subject($data['title']);

            if ($this->attachfile){
                foreach ($this->attachfile as $key=>$value){
                    $message->attach($value);
                }
            }
        });
    }


    }
