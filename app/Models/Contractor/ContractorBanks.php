<?php

namespace App\Models\Contractor;

use Illuminate\Database\Eloquent\Model;

class ContractorBanks extends Model
{
    protected $fillable = ['id', 'score', 'bank_id', 'is_active', 'is_main'];
}
