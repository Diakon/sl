<?php namespace App\Models\Contractor;

use Illuminate\Database\Eloquent\Model as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contractor extends Model
{
    use SoftDeletes;

    public function types()
    {
        return $this->belongsTo('App\Models\DirContractorType', 'type', 'id');
    }

    public function banks()
    {
        return $this->hasMany('App\Models\Contractor\ContractorBanks');
    }

    protected $fillable = ['type', 'short_name', 'full_name', 'contract_data'];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
        "type" => "integer",
        "short_name" => "string",
        "full_name" => "string",
        "contract_data" => "string",
    ];

    public static $rules = [
        'type' => 'required|integer|exists:dir_contractor_types,id',
        'short_name' => 'required|min:1|max:255',
        'full_name' => 'string|min:1|max:255',

    ];

    /**
     * Добавление нового контрагента и возврат его
     *
     * @param  array $attributes
     * @return static
     */
    public static function create(array $attributes = [])
    {
        $model = parent::create((new self)->setFormJson($attributes));
        $model->saveBanks($attributes);
        return $model;
    }

    /**
     * Редактирует контрагента используя данные из формы (attributes)
     *
     * @param  array $attributes
     * @return $this
     *
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function myfill(array $attributes)
    {
        $this->saveBanks($attributes);
        return parent::fill($this->setFormJson($attributes));
    }

    public function getContractAttribute()
    {
        if (!$this->contract_data_json) {
            $this->contract_data_json = $this->contract_data;
            $this->contract_data_json = json_decode($this->contract_data_json);
        }
        return $this->contract_data_json;
    }

    private function setFormJson($attributes)
    {
        $new_attributes = [];
        $new_attributes['type'] = $attributes['type'];
        $new_attributes['short_name'] = $attributes['short_name'];
        $new_attributes['full_name'] = $attributes['full_name'];
        foreach ([
                     'ceo', 'chief_accountant', 'action_on_basis', 'legal_address_index', 'legal_address_city', 'legal_address',
                     'actual_address_index', 'actual_address_city', 'actual_address', 'mail_address_index', 'mail_address_city',
                     'mail_address', 'property_form', 'inn', 'kpp', 'ogrn', 'taxation_system'] as $name) {
            $new_attributes['contract_data'][$name] = $attributes[$name];
        }
        $new_attributes['contract_data'] = json_encode($new_attributes['contract_data']);
        return $new_attributes;
    }

    /**
     * Добавление банков, удаление банков, редактирование банков контрагента
     * @param $attributes
     */
    private function saveBanks($attributes)
    {
        $exist_ids = [];
        if (isset($attributes['banks'])) {
            $banks = $attributes['banks'];
            foreach ($banks['score'] as $key => $score) {
                $update_banks = [
                    'score' => $score,
                    'bank_id' => $banks['bank_id'][$key],
                    'is_active' => !empty($banks['is_active'][$key]),
                    'is_main' => !empty($banks['is_main'][$key]),
                ];
                if (isset($banks['id'][$key])) {
                    if ($this->banks()->whereId($banks['id'][$key])->update($update_banks)) {
                        $exist_ids[] = intval($banks['id'][$key]);
                    }
                } else {
                    $insert = $this->banks()->create($update_banks);
                    if ($insert) {
                        $exist_ids[] = $insert->id;
                    }
                }
            }

        }
        $this->banks()->whereNotIn('id', $exist_ids)->delete();
    }
}
