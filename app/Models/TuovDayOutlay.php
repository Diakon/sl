<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class TuovDayOutlay extends Model
{
    
	public $table = "tuov_day_outlay";
    

	public $fillable = [
	    "subdivision_id",
		"rate",
		"date_from",
		"date_to",
		"name"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "subdivision_id" => "integer",
		"rate" => "float",
		"name" => "string"
    ];

	public static $rules = [
	    "subdivision_id" => "required",
		"rate" => "required",
		"date_from" => "required",
		"date_to" => "required",
		"name" => "required"
	];

}
