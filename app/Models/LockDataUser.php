<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class LockDataUser extends Model
{
    
	public $table = "lock_data_user";
    
    public $fillable = [
	    "id",
		"user_id",
		"route_name",
		"created_at",
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
        "user_id" => "integer",
		"route_name" => "string",
    ];

	public static $rules = [
	    
	];

    /**
     * @param $route_name
     * Проверяет, работает ли с записью по адресу $route_name другой  пользователь
     */
    public static function chkLockRowUser($route_name){
        $model = \DB::table('lock_data_user')
            ->leftJoin('users', 'users.userid', '=', 'lock_data_user.user_id')
            ->select('users.userid', 'lock_data_user.created_at', 'users.username', 'users.phone', 'users.email', 'users.fio')
            ->where('lock_data_user.route_name', $route_name)->where('lock_data_user.user_id', '<>', \Auth::id())
            ->first();
        if (!empty($model)){
            return $model;
        }
        return false;
    }

    /**
     * @param $route_name
     * Смотрит роут и если указано, прописывает, что пользователь работает с записью (заблокировал). Иначе - удаляет запись из лока
     */
    public static function addLock($route_name){

        $route_name_arr = explode('/',$route_name);
        if ( count($route_name_arr)>=2 && strcasecmp($route_name_arr[0], 'edit') == 0 && strcasecmp($route_name_arr[1], 'update') == 0  ){

            \DB::table('lock_data_user')->where('route_name', 'LIKE', $route_name)->where('user_id', '=', \Auth::id())->delete();
            if ( !\DB::table('lock_data_user')->where('route_name', 'LIKE', $route_name)->first() ){
                $id = \DB::table('lock_data_user')->insertGetId(
                    array('user_id' => \Auth::id(), 'route_name' => $route_name)
                );
            }

        }

        return true;
    }

}
