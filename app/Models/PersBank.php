<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class PersBank extends Model
{

	public function passport()
	{
		return $this->hasOne('App\Models\PersPassport', 'id_anketa', 'id_anketa');
	}
	public function status()
	{
		return $this->hasOne('App\Models\PersStatus', 'id_anketa', 'id_anketa');
	}

	public function main()
	{
		return $this->belongsTo('App\Models\PersMain', 'id_anketa', 'id_anketa');
	}

	/**
	 * @param $bank_id
	 * Возвращает массив данных о пользователях которые НЕ имеют карту  $bank
	 */
	public static function getUsersNoHaveBank($bank_id, $status_id, $periodArray){
		$data =
		\DB::table('pers_bank')
			->leftJoin('pers_main', 'pers_main.id_anketa', '=', 'pers_bank.id_anketa')
			->leftJoin('pers_passport', 'pers_passport.id_anketa', '=', 'pers_passport.id_anketa')
			->leftJoin('pers_address', 'pers_address.id_anketa', '=', 'pers_bank.id_anketa')
			->leftJoin('pers_status', 'pers_status.id_anketa', '=', 'pers_bank.id_anketa')
			->select('pers_main.firstname', 'pers_main.lastname', 'pers_main.patronymic', 'pers_main.birth_date', 'pers_main.birth_place', 'pers_main.gender',
				'pers_main.id_anketa', 'pers_address.district', 'pers_main.speciality', 'pers_main.nation', 'pers_passport.series', 'pers_passport.number', 'pers_passport.give',
				'pers_passport.give_date', 'pers_address.country', 'pers_address.region', 'pers_address.region_socr', 'pers_address.city', 'pers_address.city_code',
				'pers_address.np_socr', 'pers_address.np', 'pers_address.street_socr', 'pers_address.street', 'pers_address.house', 'pers_address.building',
				'pers_address.index', 'pers_address.phone_home', 'pers_address.phone_mobile', 'pers_address.email', 'pers_address.email', 'pers_passport.inn',
				'pers_passport.inn'
				);
		$data = $data->whereNull('pers_main.old_id');
		foreach ($bank_id as $key=>$val){
			$data = $data->where('pers_bank.bank_name', 'not like', '%'.$key.'%');
			$data = $data->where('pers_bank.bank_name', 'not like', '%'.$val.'%');
		}
		if (!empty($periodArray)){
			$data = $data->whereBetween('pers_status.status_date',$periodArray);
		}


		$data = $data->where('pers_status.status_name', '=', $status_id)
			->groupBy('pers_bank.id_anketa')
			->get();
		return $data;
	}

	public $table = "pers_bank";

	protected $primaryKey = 'id';

	public $timestamps = false;

	public $fillable = [
		"id",
	    "id_anketa",
		"card_number",
		"account",
		"bank_name",
		"bank_city",
		"card_expire",
		"card_status",
		"bank_bik_id"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
		"id"=>"integer",
        "id_anketa" => "integer",
		"card_number" => "string",
		"account" => "string",
		"bank_name" => "string",
		"bank_city" => "string",
		"card_expire" => "string",
		"card_status" => "integer",
		"bank_bik_id" => "integer",
    ];

	public static $rules = [
	    
	];

}
