<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use  App\Traits\SendMailTrait;
use Illuminate\View\View;

class SchedulerComments extends Model   {

    use SendMailTrait;

    public $task_id = null;
    public $description = null;
    public $author = null;

    protected $views;
	public function __construct() {
		parent::__construct();
	}

    public function setTask($task_id){ $this->task_id = $task_id; return true; }
    public function getTask(){ return $this->task_id; }
    public function setDescription($description){ $this->description = $description; return true; }
    public function getDescription(){ return $this->description; }
    public function setAuthor($author){ $this->author = $author;  return true; }
    public function getAuthor(){ return $this->author; }


    //Создать комент
    public function addComment(){

        if ( empty($this->task_id) || empty($this->description) || empty($this->author) ){
            return false;
        }


        //Проверяем коментарий, если есть картинка (скриншот), то пересохраняем ее на сервере, и вставляем ссылку
        preg_match_all('/(img|src)=("|\')[^"\'>]+/i', $this->description, $matches);
        $imgArr = array();
        foreach ($matches as $key=>$val){
            $img = current($val);
            $img = str_replace('src="data:image/png;base64,', '', $img);
            if (empty($img) || $img=='src' || $img=='"'){ continue; }
            $imgArr[] = $img;
        }
        $contentImg = "";
        if (!empty($imgArr)){
            foreach ($imgArr as $data){
                $img = base64_decode($data);
                $file = md5(date('d-m-Y H:i:s')).(rand(0, 1005)).".png";
                $fpng = fopen(\public_path()."/uploads/comments/".$file, "w");
                fwrite($fpng,$img);
                fclose($fpng);
                $contentImg .= '<BR><a href="/uploads/comments/'.$file.'" target="_blank"><img src="/uploads/comments/'.$file.'" /></a>';
            }
        }
        $comment = preg_replace("/<img[^>]+\>/i", "", $_POST['comment']);
        $comment .= $contentImg;

        //Создаем коментарий в таблице scheduler_comments
        $comment_id = \DB::table('scheduler_comments')->insertGetId(
            array('tasks_id' => $this->task_id, 'author_id' => $this->author, 'comment' => $comment)
        );

        if ($comment_id) {

            //Пишем в историю
            $modelHistory = new \App\Models\SchedulerHistory();
            $modelHistory->setUser($this->author);
            $modelHistory->setTasks($this->task_id);
            $modelHistory->setType(2);
            $userName = current(\DB::table('users')->where('userid', $this->author)->get());
            $modelHistory->setAction( 'В задаче был оставлен комментарий пользователем '.($userName->username) );
            $modelHistory->addHistory();



            //пишем в рассылку Email
            //получатели задания
            $userMail = array();
            foreach ( \DB::table('scheduler_worker')->where('scheduler_tasks_id', $this->task_id)->get() as $data ){
                //Смотрим на type. Если 1 - берем email и данные из таблицы пользователей по id в поле worker_id,
                //если type=2 то из таблицы users_orders получаем id пользователей (type_order=1 AND order_id)
                if ($data->type == 1){
                    //пользователь
                    $userMail[] = current(\DB::table('users')->where('userid', $data->worker_id)->get());
                }
                elseif($data->type == 2) {
                    //отдел
                    foreach ( \DB::table('users_orders')->where('type_order', 1)->where('order_id', $data->worker_id)->get() as $dataOrders ){
                        $userMail[] = current(\DB::table('users')->where('userid', $dataOrders->user_id)->get());
                    }
                }
            }

            //пишем в рассылку
            $tasks = current(\DB::table('scheduler_tasks')->where('id', $this->task_id)->get());
            $userMail[] = \DB::table('users')->where('userid', $tasks->user_id)->first();
            //$modelEmail = new \App\Models\EmailDelivery();
            foreach ($userMail as $key=>$value){
                if (empty($value->email)){  continue; }
                $bodyArr = [
                    'taskid'=> $tasks->id,
                    'title'=>$tasks->name,
                    'username'=>$userName->username,
                    'datetime'=>$tasks->created_at,
                    'comment'=>$comment,
                ];
                $this->sendMailDelivery($value->email, $bodyArr, 'Уведомление с сайта cloud.secondlab.ru. Коментарий к задаче '.$tasks->name, 'comment');
            }

            return $comment_id;
        }
        else { return false; }

    }

    //получить комментарии в задаче
    public function getComment($task_id=null){

        if ( empty($this->task_id) && empty($task_id) ){
            return false;
        }
        //Получаем комментарий из таблицы scheduler_comments
        $result = \DB::table('scheduler_comments')
            ->where('tasks_id', ((!empty($task_id))?$task_id:$this->task_id))
            ->get();
        return $result;
    }


}
