<?php namespace App\Models;
/**
Работа с почтой в ларавел
=========================

Создать запись в таблицу рассылки
_________________________________
1. Создать темплейт по адресу resources\views\emails
2. Для формирования письма в очередь рассылки надо подключать модель EmailDelivery
$modelEmail = new \App\Models\EmailDelivery();
$modelEmail->setTemplate('scheduler');   //тут пишем название темплейта, в данном случае для scheduler.blade.php
$modelEmail->setRecipient('email@mail.ru'); //E-mail получателя
$modelEmail->setTitle('Уведомление с сайта cloud.secondlab.ru'); //заголовок письма
$modelEmail->setBody($bodyArr); //массив параметров которые перечислены в темплейте. ВАЖНО! Нужно что бы в массиве были перечислены все параметры, которые указаны в темплейте иначе ларавел вернет ошибку
$modelEmail->addRecord();  //добавить запись в таблицу очереди рассылки

Л И Б О    В О С П О Л Ь З О В А Т Ь С Я    Т Р Е Й Т О М      $this->sendMailDelivery($value->email, $bodyArr, 'Уведомление с сайта cloud.secondlab.ru. Коментарий к задаче '.$tasks->name, 'comment');

Для запуска рассылки не отправленных писем следует использовать команду (желательно в CRON занести)
http://cloud.secondlab.ru/cron/faadstHfewHEew5webdbte4635fdfdsp


Как создать темплейт и формат массива передаваемый в setBody
____________________________________________________________
Для шаблона вида:
<!DOCTYPE html>
<html>
<head>
<title></title>
</head>
<body>
<h1>{{$title}}</h1>
<p>{{$content}}</p></br>

</body>
</html>
В $modelEmail->setBody($bodyArr); массив $bodyArr должен иметь вид:

$bodyArr = ['title'=>'Бла-бла-бла', 'content'=>'Па-па-па'];
ВАЖНО! Если не нужен какой то параметр в шаблоне, например, {{$content}}, то в массиве
$bodyArr всеравно надо передавать этот параметр с пустым значением, например 'content'=>''

*/
use Illuminate\Database\Eloquent\Model;
use  App\Traits\SendMailTrait;

class EmailDelivery extends Model   {

    use SendMailTrait;

	protected $recipient = null;
    protected $title = null;
    protected $body = array();
    protected $sent_flag = 0;
    protected $template = "standart";

	public function __construct() {
		parent::__construct();
		
	}

    public function setRecipient($recipient){ $this->recipient = $recipient; return true; }
    public function setTitle($title){ $this->title = $title; return true; }
    public function setBody($body){
        $this->body = json_encode($body);
        return true;
    }
    public function setSentflag($sent_flag){ $this->sent_flag = $sent_flag; return true; }
    public function setTemplate($template){ $this->template = $template; return true; }

    /** Добавляет письма в таблицу  email_delivery для последующей отправки через public function sendRecord **/
    public function addRecord(){
        if ( empty($this->recipient) || empty($this->title) || empty($this->body) ){
            return false;
        }

        preg_match('|([a-z0-9_\.\-]{1,20})@([a-z0-9\.\-]{1,20})\.([a-z]{2,4})|is', $this->recipient, $match);
        if (empty($match)){ return false; }


        \DB::table('email_delivery')->insertGetId(
            [
                'recipient' => $this->recipient,
                'title' => $this->title,
                'body' => $this->body,
                'sent_flag' => $this->sent_flag,
                'template' => $this->template
            ]
        );
        return true;
    }

    /** Рассылает не отправленные письма */
    public function sendRecord(){
        $cnt = 0;
        foreach (\DB::table('email_delivery')->where('sent_flag', 0)->limit(5)->get() as $data) {
            $body = json_decode($data->body, true);
            if ($body) {
                $this->sendMail($data->recipient, $body, $data->title, $data->template);
            }
            \DB::table('email_delivery')
                ->where('id', $data->id)
                ->update(array('sent_flag' => 1, 'sent_at' => date("Y-m-d H:i:s")));
            $cnt++;
        };
        return $cnt;
    }


}
