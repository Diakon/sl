<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class CardContact extends Model
{
    
	public $table = "card_contact";
    

	public $fillable = [
	    "id",
		"card_staffing_id",
		"fio",
		"user_login",
		"position",
		"phone",
		"phone_mobile",
		"email",
		"author_id",
		"updated_at",
		"created_at"
	];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        "id" => "integer",
		"card_staffing_id" => "integer",
		"fio" => "string",
		"user_login" => "string",
		"position" => "string",
		"phone" => "string",
		"phone_mobile" => "string",
		"email" => "string",
		"author_id" => "integer"
    ];

    public static $rules = [
        'fio' => 'required|min:5',
        'card_staffing_id'=>'required',
        'email'=>'email',
    ];

    public static $messages = array(
        'required' => "Поле :attribute обязательно для заполнения!",
        'unique' => "Пользователь :attribute уже существует!",
        'same' => "Пароли не совпадают!",
        'min' => "Длина поля :attribute должна быть минимум :min символов!",
        'integer' => "В поле :attribute можно вводить только числа!",
    );

}
