<?php

namespace App\Listeners;

use Redis;
use App\Events\documentChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class updateTuovCache
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  documentChanged  $event
     * @return void
     */
    public function handle(documentChanged $event)
    {
        $document = $event->document;
        //
    }
}
