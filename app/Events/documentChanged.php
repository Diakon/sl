<?php

namespace App\Events;

use App\Events\Event;
use App\Models\LuovDocument;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class documentChanged extends Event
{
    public $document;
    public $data;
    use SerializesModels;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($data, $oldData)
    {
        if(empty($data)) {
            $data = [];
        }
        if(empty($oldData)){
            $oldData = [];
        }

        $this->oldData = $oldData;
        $this->data = $data;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }


}
