<?php

use \Illuminate\Support\Facades\Artisan;

class TestCase extends Illuminate\Foundation\Testing\TestCase {

	/**
	 * Creates the application.
	 *
	 * @return \Illuminate\Foundation\Application
	 */
	public function createApplication()
	{
		$this->baseUrl = env('APP_URL');
		$app = require __DIR__.'/../bootstrap/app.php';

		$app->make('Illuminate\Contracts\Console\Kernel')->bootstrap();

		return $app;
	}

	public function setUp()
	{
		parent::setUp();
		Artisan::call('migrate');
	}

	public function tearDown()
	{
		Artisan::call('migrate:reset');
		parent::tearDown();
	}
}
