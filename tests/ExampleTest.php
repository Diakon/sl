<?php

use Mockery as m;

class ExampleTest extends TestCase {

	public function testIndexPage()
	{
		$response = $this->call('GET', '/');
		$this->assertEquals(302, $response->getStatusCode());
	}

	public function testLoginPage()
	{
		$response = $this->call('GET', '/auth/login');
		$this->assertContains('Авторизация', $response->getContent());
		$this->assertEquals(200, $response->getStatusCode());
	}

	public function testLoginPageSubmit()
	{
		Session::start();
		$credentials = [
			'username' => 'wronguser',
			'password' => 'wrongpass',
			//'_token' => csrf_token()
		];


		try {
			$response = $this->makeRequest('POST', '/auth/login', $credentials);
 		} catch(Exception $e) {
			echo ($e->getMessage());
			//$this->assertEquals(200, $response->getStatusCode());
		}

	}

	public function testIsShownUsers()
	{
		$this->withoutMiddleware();
		$this->visit('/admin/users')->dump();
	}
}
