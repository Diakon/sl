#!/usr/bin/env bash

pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null

cd "$SCRIPTPATH/../"
php5 composer.phar self-update

exit 0
