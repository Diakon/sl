#!/usr/bin/env bash

WAY="/home/tst1_secondlab/cloud"
BRANCH="master"

mkdir $WAY
chown apache:apache -R $WAY
cd $WAY
git init
git remote add origin https://vlspro:Costa-2013@bitbucket.org/vlscode/secondlab_laravel.git
git fetch origin $BRANCH

#For remove origin use or delete whole dir
#git remote remove origin
#git remote -v

#for updating use:
git pull origin $BRANCH

#copy config and change access
chmod 777 -R $WAY/js
chmod 777 -R $WAY/images
chmod 777 -R $WAY/css
chmod 777 -R $WAY/uploads


#clear cache
php artisan cache:clear

#for installation all dependencies use:
php5 composer.phar self-update
php5 composer.phar install
#php5 composer.phar update
exit 0