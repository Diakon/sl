#!/usr/bin/env bash

## This script take one parameter: $BRANCH which used as branch name
## Example of usage:
## ./gitpull.sh master

pushd `dirname $0` > /dev/null
SCRIPTPATH=`pwd -P`
popd > /dev/null

if [ "$1" != "" ]; then
    BRANCH=$1
    echo "pull using parameter"
else
    BRANCH="master"
fi

echo "$SCRIPTPATH/../"
cd "$SCRIPTPATH/../"
git pull origin $BRANCH
php artisan migrate --force

exit 0
